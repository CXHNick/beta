SET @sPluginName = 'aqb_friends_broadcaster';

DELETE FROM `sys_permalinks` WHERE `check`=CONCAT('permalinks_module_', @sPluginName);

DELETE FROM `sys_options` WHERE `Name` = CONCAT('permalinks_module_', @sPluginName);

DELETE FROM `sys_menu_top` WHERE `Name` = 'Friends Broadcaster';

DELETE FROM `sys_objects_actions` WHERE `Type` = 'Profile' AND `Eval` LIKE '%aqb_friends_broadcaster%';