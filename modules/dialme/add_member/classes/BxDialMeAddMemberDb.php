<?php
/***************************************************************************
* Date              : Sun January 26, 2014
* Website           : http://www.dialme.com
*
* Product Name          : Add A New Member Module
* Product Version       : 1.0.0
* Product Use           : For Boonex Dolphin 7.0.x & 7.1.x
*
***************************************************************************/

bx_import('BxDolModuleDb');

class BxDialMeAddMemberDb extends BxDolModuleDb {

    public function __construct(&$config) {
        parent::__construct();
        $this->_sPrefix = $config->getDbPrefix();
    }
    
    public function insertProfile($data) {
        $nickname = $this->escape($data['NickName'], false);
        $email = $this->escape($data['Email']);
        $salt = base64_encode(substr(md5(microtime()), 2, 6));
        $password = encryptUserPwd($data['Password'], $salt);
        $fullname = '';
        if(isset($data['FullName']))
            $fullname = $this->escape($data['FullName']);
        /*$lastname = '';
        if(isset($data['LastName']))
            $lastname = $this->escape($data['LastName']);*/
        $zip = '';
        if(isset($data['zip']))
            $zip = $this->escape($data['zip']);
        $city = '';
        if(isset($data['City']))
            $city = $this->escape($data['City']);
        $country = '';
        if(isset($data['Country']))
            $country = $this->escape($data['Country']);
    $Sex = ($data['Sex']=='male')? $data['Sex']:'female';

    $EmailNotify = ($data['EmailNotify']=='1')? $data['EmailNotify']:'0';

    $Role = ($data['Role']=='1')? $data['Role']:'3';
        $query = "
            INSERT INTO `Profiles`
                (`NickName`, `Email`, `Password`, `Salt`, `Status`, `Role`, `Sex`, `Country`, `City`, `DateReg`, `zip`, `EmailNotify`, `FullName`)
            VALUES
                ('{$nickname}', '{$email}', '{$password}', '{$salt}', 'Active', '{$Role}', '{$Sex}', '{$country}', '{$city}', NOW(), '{$zip}', '{$EmailNotify}', '{$fullname}')
        ";
        $this->query($query);
    }
    
    public function isUnique($key, $value){
        $value = addslashes($value);
        $query = "SELECT COUNT(*) FROM `Profiles` WHERE `$key` = '$value'";
        if((int)db_value($query) > 0)
            return false;
        return true;
    }
}

?>
