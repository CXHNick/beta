<?
/**
 * Copyright (c) BoonEx Pty Limited - http://www.boonex.com/
 * CC-BY License - http://creativecommons.org/licenses/by/3.0/
 */

//require_once(BX_DIRECTORY_PATH_CLASSES . 'BxDolFilesDb.php');
//require_once(BX_DIRECTORY_PATH_CLASSES . 'BxDolTwigModuleDb.php');
bx_import('BxDolTwigModuleDb');
//('BxDolFilesDb');

class BxLibraryDb extends BxDolTwigModuleDb
{

    //var $userId;

    //databases to access files
    /*var $photoDb;
    var $videoDb;
    var $soundDb;
    var $fileDb;
    var $groupsDb;

    var $userId;
    var $groupsJoined;*/

    /*
     * Constructor.
     */
    function BxLibraryDb (&$oConfig)
    {
        parent::BxDolTwigModuleDb($oConfig);
    }

    function deleteMediaFile ($iMediaId, $sMediaType)
    {
        return 1;
        //return $this->query ("DELETE FROM `" . $this->_sPrefix . $this->_sTableMediaPrefix . "{$sMediaType}` WHERE `media_id` = '$iMediaId'");
    }

    function getUserGroupsById($iProfileId) {

        $oGroups = BxDolModule::getInstance('BxGroupsModule');
        $count = 100;
        $userGroups = array();

        $i = 0;

        for ($i = 0; $i < $count; $i++) {
            if ($oGroups->_oDb->isFan($i, $iProfileId, true)) {
                $groupInfo = $this->getEntryById($i);
                array_push($userGroups, $i);
            }
        }

        return $userGroups;
    }

    function getNumReportPosts($iGroupId) {

        return $this->getOne("SELECT COUNT(`id`) FROM `bx_groups_blog_main` WHERE `group_id` = '$iGroupId'");
    }

    function getLastAuthor($iGroupId) {

        return $this->getOne("SELECT `author_id` FROM `bx_groups_blog_main` WHERE `group_id` = '$iGroupId' ORDER BY `id` DESC LIMIT 1");
    }
}

?>
