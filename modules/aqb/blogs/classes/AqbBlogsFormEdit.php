<?php
/***************************************************************************
* 
*     copyright            : (C) 2009 AQB Soft
*     website              : http://www.aqbsoft.com
*      
* IMPORTANT: This is a commercial product made by AQB Soft. It cannot be modified for other than personal usage.
* The "personal usage" means the product can be installed and set up for ONE domain name ONLY. 
* To be able to use this product for another domain names you have to order another copy of this product (license).
* 
* This product cannot be redistributed for free or a fee without written permission from AQB Soft.
* 
* This notice may not be removed from the source code.
* 
***************************************************************************/

aqb_blogs_import('FormAdd');

class AqbBlogsFormEdit extends AqbBlogsFormAdd
{
    function AqbBlogsFormEdit ($oMain, $iProfileId, $iEntryId, &$aDataEntry)
    {
        parent::AqbBlogsFormAdd ($oMain, $iProfileId, $iEntryId, $aDataEntry['thumb'], true);

        $aFormInputsId = array (
            'id' => array (
                'type' => 'hidden',
                'name' => 'id',
                'value' => $iEntryId,
            ),
        );

        bx_import('BxDolCategories');
        $oCategories = new BxDolCategories();
        $oCategories->getTagObjectConfig ();
        $this->aInputs['categories'] = $oCategories->getGroupChooser ($this->_oMain->_oConfig->getSystemName('categories'), (int)$iProfileId, true, $aDataEntry['categories']);

        $this->aInputs = array_merge($this->aInputs, $aFormInputsId);
    }

}
