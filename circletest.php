<?php
require_once( 'inc/design.inc.php' );
require_once( 'inc/db.inc.php' );
//require_once( 'inc/classes/BxDolModule.php' );

//$oGroups = BxDolModule::getInstance('BxGroupsModule');
$id = 16;
//$entry = $oGroups->oDb->getEntryById($id);
//$thumb = $entry[$oGroups->oDb->_sFieldThumb];

$groupQuery = "SELECT `thumb` FROM `bx_groups_main` WHERE `id` = '$id'";
$thumb = db_value($groupQuery);

$titleQuery = "SELECT `Title` FROM `bx_groups_main` WHERE `id` = '$id'";
$title = db_value($titleQuery);

$uriQuery = "SELECT `uri` FROM `bx_groups_main` WHERE `id` = '$id'";
$uri = db_value($uriQuery);
$groupLink = BX_DOL_URL_ROOT . 'm/groups/view/' . $uri;

if ($thumb != 0) {
    $thumbQuery = "SELECT `Hash` FROM `bx_photos_main` WHERE `id` = '$thumb'";
    $hash = db_value($thumbQuery);
    $url = BX_DOL_URL_ROOT . 'm/photos/get_image/browse/' . $hash . '.jpg';
} else {
    $url = BX_DOL_URL_ROOT . 'modules/boonex/groups/templates/base/images/no-image-thumb.png';
}

$query = "SELECT * FROM `bx_groups_fans` WHERE `id_entry` = '$id'";

$pos = array("deg0", "deg45", "deg90", "deg135", "deg180", "deg225", "deg270", "deg315");
$groupMembers = db_res_assoc_arr($query);
$pictures = array();
$links = array();

$code = "<head>
<style type=\"text/css\">
.circle-container {
    position: relative;
    width: 12em;
    height: 12em;
    padding: 1em;
    margin: 1.75em auto 0;
}
.circle-container a {
    display: block;
    position: absolute;
    top: 50%; left: 50%;
    width: 4em; height: 4em;
    margin: -2em;
}
.circle-container img { 
    display: block; 
    border-radius: 50%;

}



.center { 
    transform: rotate(225deg) translate(1em) rotate(-225deg); 
}
.deg0 { 
    transform: translate(6em); 
}
.deg45 { 
    transform: rotate(315deg) translate(6em) rotate(-315deg); 
}
.deg90 { transform: rotate(270deg) translate(6em) rotate(-270deg); }
.deg135 { transform: rotate(225deg) translate(6em) rotate(-225deg); }
.deg180 { transform: translate(-6em); }
.deg225 { transform: rotate(135deg) translate(6em) rotate(-135deg); }
.deg270 { transform: rotate(90deg) translate(6em) rotate(-90deg); }
.deg315 { transform: rotate(45deg) translate(6em) rotate(-45deg); }
</style>
</head>
<div class='circle-container'>
    <a href='$groupLink' class='center'><img title='". $title . "' src=" . $url . " alt=\"\" width=\"90\" height=\"90\"></a>"; 

for ($i = 0; $i < sizeof($groupMembers); $i++) {
    $thumb = getUserThumbUrl($groupMembers[$i]['id_profile']);
    $link = getProfileLink($groupMembers[$i]['id_profile']);

    if (!$thumb) {
        $thumb = "http://cdn.ipetitions.com/rev/176/assets/v3/img/default-avatar.png";
    }

    $code .= "<a href='" . $link . "' class='" . $pos[$i] . "'><img src=\"" . $thumb . "\" alt=\"\" width=\"64\" height=\"64\"></a>";
    //array_push($pictures, $thumb);
    //array_push($links, $link);
}

$code .= "</div>";

//$message = $pictures[0];
//echo "<script type='text/javascript'>alert('$message');</script>";


?>
<html>

<body>
<!--<div class='circle-container'>
    <a href='#' class='center'><img src="https://ssl.gstatic.com/s2/oz/images/faviconr2.ico" alt="" width="64" height="64"></a>
    <a href='#' class='deg0'><img src="https://image.freepik.com/free-icon/add-button-with-plus-symbol-in-a-black-circle_318-48599.png" alt="" width="64" height="64"></a>
    <a href='<?php echo $links[0]; ?>' class='deg45'><img src="<?php echo $pictures[0]; ?>" alt="" width="64" height="64"></a>
    <a href='<?php echo $links[1]; ?>' class='deg90'><img src="<?php echo $pictures[1]; ?>" alt="" width="64" height="64"></a>
    <a href='<?php echo $links[2]; ?>' class='deg135'><img src="<?php echo $pictures[2]; ?>" alt="" width="64" height="64"></a>
    <a href='#' class='deg180'><img src="https://ssl.gstatic.com/s2/oz/images/faviconr2.ico" alt="" width="64" height="64"></a>
    <a href='#' class='deg225'><img src="https://ssl.gstatic.com/s2/oz/images/faviconr2.ico" alt="" width="64" height="64"></a>
    <a href='#' class='deg270'><img src="https://ssl.gstatic.com/s2/oz/images/faviconr2.ico" alt="" width="64" height="64"></a>
    <a href='#' class='deg315'><img src="https://ssl.gstatic.com/s2/oz/images/faviconr2.ico" alt="" width="64" height="64"></a>
</div>-->

<?php echo $code; ?>

</body>
</html>