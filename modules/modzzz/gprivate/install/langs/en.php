<?php
/***************************************************************************
*                            Dolphin Smart Community Builder
*                              -------------------
*     begin                : Mon Mar 23 2006
*     copyright            : (C) 2007 BoonEx GPrivate
*     website              : http://www.boonex.com
* This file is part of Dolphin - Smart Community Builder
*
* Dolphin is free software; you can redistribute it and/or modify it under
* the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 2 of the
* License, or  any later version.
*
* Dolphin is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
* without even the implied warranty of  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
* You should have received a copy of the GNU General Public License along with Dolphin,
* see license.txt file; if not, write to marketing@boonex.com
***************************************************************************/

$sLangCategory = 'Modzzz GPrivate';

$aLangContent = array(

    '_modzzz_gprivate' => 'Group Privacy', 
    '_modzzz_gprivate_page_title_administration' => 'Administration', 
    '_modzzz_gprivate_menu_admin_settings' => 'Admin Settings', 
    '_modzzz_gprivate_menu_active_categories' => 'Groups', 
 	'_modzzz_gprivate_menu_admin_add_category' => 'Add Group',  
  
	'_modzzz_gprivate_form_header_info' => 'Add a Group to Privacy',   
  
	'_modzzz_gprivate_admin_delete' => 'Delete',   
  	'_modzzz_gprivate_form_caption_active' => 'Active', 
	
	'_modzzz_gprivate_page_title_active_categories' => 'Group Privacy Groups', 
	'_modzzz_gprivate_page_title_categories' => 'Group Privacy Groups', 
  
	'_modzzz_gprivate_form_caption_group' => 'Group', 
  
);