<?php
/**
 * Copyright (c) BoonEx Pty Limited - http://www.boonex.com/
 * CC-BY License - http://creativecommons.org/licenses/by/3.0/
 */

//ini_set('display_errors', 'On');
//error_reporting(E_ALL);

bx_import('BxDolPageView');
bx_import('BxDolMemberMenu');

class BxBaseLibraryView extends BxDolPageView
{
    var $iMember;
    var $aMemberInfo;
    var $aConfSite;
    var $aConfDir;

    var $aVisible = array();

    function BxBaseLibraryView($iMember, &$aSite, &$aDir)
    {
        $this->iMember = (int)$iMember;
        $this->aMemberInfo = getProfileInfo($this->iMember);

        $this->aConfSite = $aSite;
        $this->aConfDir  = $aDir;

        


        //if ($this->iMemberID)
            //$this->aVisible[] = BX_DOL_PG_MEMBERS;

        parent::BxDolPageView('library');


    }

    function getBlockCode_PhotoAlbums() {

        //echo "<script type='text/javascript'>alert('$this->iMember');</script>";

        //global $oSysTemplate;

        //$message3 = "hey";

        /*$paramName = "album";

        require_once('/var/www/vhosts/combinexlife.com/root/cxlife/modules/boonex/photos/classes/BxPhotosSearch.php');

        $photoSearch = new BxPhotosSearch($paramName);

        $photoSearch->aCurrent['restriction']['allow_view']['value'] = $this->aVisible;
        $photoSearch->aCurrent['restriction']['activeStatus']['value'] = 'approved';
        $photoSearch->aCurrent['restriction']['album_status']['value'] = 'active';

        $aAlbumParams = array(
            'allow_view' => $aVisible,
        );


        return $photoSearch->getAlbumsBlock(array(), $aAlbumParams, array());*/

        //return BxDolService::call('photos', 'get_profile_album_block', array($this->iMember), 'Search');

        $aResult = getBlockCode_Echo(584, 'BxDolService::call('photos', 'get_profile_album_block', array($this->iMember), 'Search');');

        $aPoop = "<?php" . $aResult . "?>";

        return showPhotoBlock($aPoop);

        //return showPhotoBlock();

        //return 0;
    }

    /*function getBlockCode_VideoAlbums() {

        return 0;
    }

    function getBlockCode_SoundAlbums() {

        return 0;
    }

    function getBlockCode_FileAlbums() {

        return 0;
    }*/

    function showPhotoBlock($meh) {

        //bx_import('BxDolService');
        //echo BxDolService::call('photos', 'get_profile_album_block', array($this->iMember), 'Search');
        echo $meh;
    }

}
