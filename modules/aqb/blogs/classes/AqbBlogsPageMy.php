<?php
/***************************************************************************
* 
*     copyright            : (C) 2009 AQB Soft
*     website              : http://www.aqbsoft.com
*      
* IMPORTANT: This is a commercial product made by AQB Soft. It cannot be modified for other than personal usage.
* The "personal usage" means the product can be installed and set up for ONE domain name ONLY. 
* To be able to use this product for another domain names you have to order another copy of this product (license).
* 
* This product cannot be redistributed for free or a fee without written permission from AQB Soft.
* 
* This notice may not be removed from the source code.
* 
***************************************************************************/

bx_import('BxDolPageView');

class AqbBlogsPageMy extends BxDolPageView
{
    var $_oMain;
    var $_oTemplate;
    var $_oDb;
    var $_oConfig;
    var $_aProfile;
	var $_sPrefix;
    var $_sPrefixLangs;
	var $_sPrefixOptions;
	
    function AqbBlogsPageMy(&$oMain, &$aProfile)
    {
        $this->_oMain = &$oMain;
        $this->_oTemplate = $oMain->_oTemplate;
        $this->_oDb = $oMain->_oDb;
        $this->_oConfig = $oMain->_oConfig;
        $this->_aProfile = $aProfile;

        $this->_sPrefix = $oMain->_oConfig->getUri();
		$this->_sPrefixLangs = $oMain->_oConfig->getSystemName('langs');
        $this->_sPrefixOptions = $oMain->_oConfig->getSystemName('options');

        parent::BxDolPageView($this->_sPrefix . '_my');
    }

    function getBlockCode_Owner()
    {
        if (!$this->_oMain->_iProfileId || !$this->_aProfile)
            return '';

        $sContent = '';
        switch (bx_get($this->_oMain->_sFilterName)) {
        case 'add_group':
            $sContent = $this->getBlockCode_Add ();
            break;
        case 'manage_groups':
            $sContent = $this->getBlockCode_My ();
            break;
        case 'pending_groups':
            $sContent = $this->getBlockCode_Pending ();
            break;
        default:
            $sContent = $this->getBlockCode_Main ();
        }

        $sBaseUrl = BX_DOL_URL_ROOT . $this->_oConfig->getBaseUri() . "browse/my";
        $aMenu = array(
            _t($this->_sPrefixLangs . '_block_submenu_main') => array('href' => $sBaseUrl, 'active' => !bx_get($this->_oMain->_sFilterName)),
            _t($this->_sPrefixLangs . '_block_submenu_add_group') => array('href' => $sBaseUrl . '&' . $this->_oMain->_sFilterName . '=add_group', 'active' => 'add_group' == bx_get($this->_oMain->_sFilterName)),
            _t($this->_sPrefixLangs . '_block_submenu_manage_groups') => array('href' => $sBaseUrl . '&' . $this->_oMain->_sFilterName . '=manage_groups', 'active' => 'manage_groups' == bx_get($this->_oMain->_sFilterName)),
            _t($this->_sPrefixLangs . '_block_submenu_pending_groups') => array('href' => $sBaseUrl . '&' . $this->_oMain->_sFilterName . '=pending_groups', 'active' => 'pending_groups' == bx_get($this->_oMain->_sFilterName)),
        );
        return array($sContent, $aMenu, '', '');
    }

    function getBlockCode_Browse()
    {
        aqb_blogs_import ('SearchResult');
        $o = new AqbBlogsSearchResult('user', process_db_input ($this->_aProfile['NickName'], BX_TAGS_NO_ACTION, BX_SLASHES_NO_ACTION));
        $o->aCurrent['rss'] = 0;

        $o->sBrowseUrl = "browse/my";
        $o->aCurrent['title'] = _t($this->_sPrefixLangs . '_page_title_my_groups');

        if ($o->isError) {
            return DesignBoxContent(_t($this->_sPrefixLangs . '_block_users_groups'), MsgBox(_t('_Empty')), 1);
        }

        if ($s = $o->processing()) {
            $this->_oTemplate->addCss (array('unit.css', 'twig.css', 'main.css'));
            return $s;
        } else {
            return DesignBoxContent(_t($this->_sPrefixLangs . '_block_users_groups'), MsgBox(_t('_Empty')), 1);
        }
    }

    function getBlockCode_Main()
    {
        $iActive = $this->_oDb->getCountByAuthorAndStatus($this->_aProfile['ID'], 'approved');
        $iPending = $this->_oDb->getCountByAuthorAndStatus($this->_aProfile['ID'], 'pending');
        $sBaseUrl = BX_DOL_URL_ROOT . $this->_oConfig->getBaseUri() . "browse/my";
        $aVars = array ('msg' => '');
        if ($iPending)
            $aVars['msg'] = sprintf(_t($this->_sPrefixLangs . '_msg_you_have_pending_approval_groups'), $sBaseUrl . '&' . $this->_oMain->_sFilterName . '=pending_groups', $iPending);
        elseif (!$iActive)
            $aVars['msg'] = sprintf(_t($this->_sPrefixLangs . '_msg_you_have_no_groups'), $sBaseUrl . '&' . $this->_oMain->_sFilterName . '=add_group');
        else
            $aVars['msg'] = sprintf(_t($this->_sPrefixLangs . '_msg_you_have_some_groups'), $sBaseUrl . '&' . $this->_oMain->_sFilterName . '=manage_groups', $iActive, $sBaseUrl . '&' . $this->_oMain->_sFilterName . '=add_group');
        return $this->_oTemplate->parseHtmlByName('my_groups_main', $aVars);
    }

    function getBlockCode_Add()
    {
        if (!$this->_oMain->isAllowedAdd()) {
            return MsgBox(_t('_Access denied'));
        }
        ob_start();
        $this->_oMain->_addForm(BX_DOL_URL_ROOT . $this->_oConfig->getBaseUri() . 'browse/my');
        $aVars = array ('form' => ob_get_clean(), 'id' => '');
        $this->_oTemplate->addCss ('forms_extra.css');
        return $this->_oTemplate->parseHtmlByName('my_groups_create_group', $aVars);
    }

    function getBlockCode_Pending()
    {
        $sForm = $this->_oMain->_manageEntries ('my_pending', '', false, $this->_sPrefix . '_pending_user_form', array(
            'action_delete' => $this->_sPrefixLangs . '_admin_delete',
        ), $this->_sPrefix . '_my_pending', false, 7);
        if (!$sForm)
            return MsgBox(_t('_Empty'));
        $aVars = array ('form' => $sForm, 'id' => $this->_sPrefix . '_my_pending');
        return $this->_oTemplate->parseHtmlByName('my_groups_manage', $aVars);
    }

    function getBlockCode_My()
    {
        $sForm = $this->_oMain->_manageEntries ('user', process_db_input ($this->_aProfile['NickName'], BX_TAGS_NO_ACTION, BX_SLASHES_NO_ACTION), false, $this->_sPrefix . '_user_form', array(
            'action_delete' => $this->_sPrefixLangs . '_admin_delete',
        ), $this->_sPrefix . '_my_active', true, 7);
        $aVars = array ('form' => $sForm, 'id' => $this->_sPrefix . '_my_active');
        return $this->_oTemplate->parseHtmlByName('my_groups_manage', $aVars);
    }
}
