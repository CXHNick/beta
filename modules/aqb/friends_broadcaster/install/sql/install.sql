SET @sPluginName = 'aqb_friends_broadcaster';

INSERT INTO `sys_permalinks`(`standard`, `permalink`, `check`) VALUES
(CONCAT('modules/?r=', @sPluginName, '/'), CONCAT('m/', @sPluginName, '/'), CONCAT('permalinks_module_', @sPluginName));

INSERT INTO `sys_options`(`Name`, `VALUE`, `kateg`, `desc`, `Type`, `check`, `err_text`, `order_in_kateg`) VALUES
(CONCAT('permalinks_module_', @sPluginName), 'on', 26, 'Enable user friendly permalinks for Friends Broadcaster module', 'checkbox', '', '', 0);

SET @iMailID := (SELECT `ID` FROM `sys_menu_top` WHERE `Name` = 'Mail');
SET @iTMOrder := (SELECT MAX(`Order`) FROM `sys_menu_top` WHERE `Parent`= @iMailID);
INSERT INTO `sys_menu_top` (`Parent`, `Name`, `Caption`, `Link`, `Order`, `Visible`, `Target`, `Onclick`, `Check`, `Editable`, `Deletable`, `Active`, `Type`, `Picture`, `Icon`, `BQuickLink`, `Statistics`) VALUES
(@iMailID, 'Friends Broadcaster', '_[db_prefix]top_menu_item', 'modules/?r=aqb_friends_broadcaster/', @iTMOrder+1, 'memb', '', '', '', 1, 1, 1, 'custom', '', '', 0, '');


SET @iMOrder := (SELECT MAX(`Order`) FROM `sys_objects_actions` WHERE `Type`= 'Profile');
INSERT INTO `sys_objects_actions` (`Caption`, `Icon`, `Url`, `Script`, `Eval`, `Order`, `Type`)
VALUES ('{evalResult}', 'envelope', 'm/aqb_friends_broadcaster?RecID={ID}', '', 'return BxDolService::call(''aqb_friends_broadcaster'', ''get_profile_action_button'', array({ID}, {member_id}));', 2, 'Profile');