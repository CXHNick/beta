<?php
/***************************************************************************
*
* IMPORTANT: This is a commercial product made by AndrewP. It cannot be modified for other than personal usage.
* The "personal usage" means the product can be installed and set up for ONE domain name ONLY.
* To be able to use this product for another domain names you have to order another copy of this product (license).
* This product cannot be redistributed for free or a fee without written permission from AndrewP.
* This notice may not be removed from the source code.
*
***************************************************************************/
define('AGPALBUMS_APPL', 'modules/andrew/gpalbums/appl/');

class AGpalbumsExtraViews {
    var $sAlbumTitle;
    var $oMainModule;

    /**
     * Constructor
     */
    function AGpalbumsExtraViews($oMainModule) {
        $this->oMainModule = $oMainModule;
    }

    function getXmlData2Flash2($aUnits) {
        if (count($aUnits) > 0) {

            $sAlbumTitle = htmlspecialchars(strip_tags($this->sAlbumTitle));
            $sPhotosUrl = $this->oMainModule->sUploadUrl;
            $sPhotosPath = $this->oMainModule->sUploadDir;
            $sTemplate = '<img src="__fileurl____filename__" width="__width__" height="__height__" />';

            foreach ($aUnits as $iID => $aSPic) {
                $iImgID = (int)$aSPic['id'];
                $sTitle = $aSPic['title'];
                if ( $sTitle=='' || $sTitle==' ' || $sTitle=='&nbsp;' ) {
                    $sTitle = 'no data';
                }

                $sFilename = 'i_' . $aSPic['filename'];
                $sFilePath = $sPhotosPath . $sFilename;
                list( $width, $height, $type, $attr ) = getimagesize($sFilePath);

                $aKeys = array(
                    'filename' => $sFilename,
                    'fileurl' => $sPhotosUrl,
                    'title' => $sTitle,
                    'album_title' => $sAlbumTitle,
                    'file_href' => '',
                    'width' => $width,
                    'height' => $height
                );
                $sVarTemplate = $sTemplate;
                foreach ($aKeys as $sKey => $sValue) {
                    $sVarTemplate = str_replace('__' . $sKey . '__', $sValue, $sVarTemplate);
                }
                $sImagesXML .= $sVarTemplate;
            }

            if ($sImagesXML == '') {
                $sNothingC = _t('_Sorry, nothing found');
                $sPicNotAvail = getTemplateIcon('group_no_pic.gif');

                $aKeys = array(
                    'filename' => $sPicNotAvail,
                    'fileurl' => $sPicNotAvail,
                    'title' => $sNothingC,
                    'album_title' => $sAlbumTitle,
                    'file_href' => ""
                );
                $sVarTemplate = $sTemplate;
                foreach ($aKeys as $sKey => $sValue) {
                    $sVarTemplate = str_replace('__' . $sKey . '__', $sValue, $sVarTemplate);
                }
                $sImagesXML .= $sVarTemplate;
            }

            $sGalleryRet = <<<EOF
<?xml version="1.0" ?>
{$sImagesXML}
EOF;

            header ('Content-Type: application/xml; charset=UTF-8');
            echo $sGalleryRet; exit;

        }
    }

    function serviceGetSwitcherModes($sAlbumUrl, $sMainUrl) {
        $aVariables = array(
            'album_url' => $sAlbumUrl,
            'album_url' => $sAlbumUrl,
            'main_url' => $sMainUrl,
        );
        return $this->oMainModule->_oTemplate->parseHtmlByName('breadcrumb.html', $aVariables);
    }

    function getViewModesActions($sUrl) {
        $aModes = array();
        $aModes['Flash2'] = array('href' => $sUrl, 'title' => 'flash', 'onclick' => '', 'active' => false, 'icon' => 'film');
        return $aModes;
    }

    function getViewableResults($sAlbumTitle, $iAlbumID, $sSubAction, $sMainUrl, $sMain2Url, $sAlbumUrl) {
        switch ($sSubAction) {
            case 'xml_flash':
                require_once( $this->oMainModule->_oConfig->getClassPath() . 'AGpalbumsImgSearch.php');
                $oAlbumsSearch = new AGpalbumsImgSearch();
                $oAlbumsSearch->sAlbumUri = $sAlbumUriSafe;
                $oAlbumsSearch->aCurrent['restriction']['album_id']['value'] = (int)$iAlbumID;
                $oAlbumsSearch->aCurrent['paginate']['perPage'] = 999;
                $aUnits = $oAlbumsSearch->getSearchDataByParams();
                $this->getXmlData2Flash2($aUnits);

                break;
            case 'flash':
                $sBreadcrumb = $this->serviceGetSwitcherModes($sAlbumUrl, $sMain2Url);
                $this->oMainModule->aPageTmpl['name_index'] = 534;
                $aPageVars = array(
                    'page_main_code' => '',
                    'breadcrumb' => $sBreadcrumb,
                    'title' => $sAlbumTitle,
                    'appls_url' => BX_DOL_URL_ROOT . AGPALBUMS_APPL,
                    'xml_src_url' => $sMainUrl,
                );
                $this->oMainModule->_oTemplate->pageCode($this->oMainModule->aPageTmpl, $aPageVars);
                exit;
                break;
        }
    }
}