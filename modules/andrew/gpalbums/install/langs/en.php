<?php
/***************************************************************************
*
* IMPORTANT: This is a commercial product made by AndrewP. It cannot be modified for other than personal usage.
* The "personal usage" means the product can be installed and set up for ONE domain name ONLY.
* To be able to use this product for another domain names you have to order another copy of this product (license).
* This product cannot be redistributed for free or a fee without written permission from AndrewP.
* This notice may not be removed from the source code.
*
***************************************************************************/
$sLangCategory = 'Photo Albums for Groups';

$aLangContent = array(
    '_gpa_main' => 'Group Albums',
    '_gpa_main_e' => 'Group Albums',
    '_gpa_privacy_album_view' => 'Allow viewing to',
    '_gpa_new_album' => 'New album',
    '_gpa_back' => 'Back',
    '_gpa_share' => 'Share',
    '_gpa_rss' => 'RSS',
    '_gpa_upload_photos' => 'Upload content',
    '_gpa_manage_photos' => 'Manage content',
    '_gpa_edit_album' => 'Edit album',
    '_gpa_embed_videos' => 'Embed Videos',
    '_gpa_prefix' => 'Prefix',
    '_gpa_prefix_expl' => 'This prefix will applied to names of content',
    '_gpa_empty_need_upload' => 'You haven`t uploaded any content yet, choose upload action',
    '_gpa_photos_of_album' => 'Album Content',
    '_gpa_images' => 'item(s)',
    '_gpa_upload' => 'Upload',
    '_gpa_fupload' => 'Flash',
    '_gpa_uploadu' => 'Pin',
    '_gpa_photo_added_success' => 'Photo added successfully',
    '_gpa_photo_added_failed' => 'Failed to add content',
    '_gpa_image_files' => 'Files',
    '_gpa_privacy_restr' => 'You can`t view this album due privacy restrictions',
    '_gpa_groups_privacy_fans' => 'Group Members',
    '_gpa_groups_privacy_admins_only' => 'Admins Only',
    '_gpa_select_images' => 'Click here to select files',
    '_gpa_add_description' => 'Add / Edit title and description',
    '_gpa_public_mode' => 'Allow other members of group to post images to this album',
    '_gpa_download' => 'Download original',
    '_gpa_albums' => 'Content Albums',
    '_mma_agpalbums_feature' => 'Mark Feature Photos (Group Albums)',
    '_gpa_featured_list' => 'Feature Group Photos',
    '_gpa_feature_action' => 'Feature / Defeature it',
    '_gpa_featured' => 'Featured',
    '_gpa_defeatured' => 'Defeatured',
    '_gpa_spy_upload' => '<a href="{profile_link}">{profile_nick}</a> upload new photo <a href="{obj_url}">{obj_caption}</a> into group album',
    '_gpa_wall_post' => 'added new photo album',
    '_gpa_wall_object' => 'photo album',
    '_gpa_photo_by' => 'This content was added by',
    '_gpa_file_view' => 'View File',
);
