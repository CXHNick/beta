<?php
/***************************************************************************
* 
*     copyright            : (C) 2009 AQB Soft
*     website              : http://www.aqbsoft.com
*      
* IMPORTANT: This is a commercial product made by AQB Soft. It cannot be modified for other than personal usage.
* The "personal usage" means the product can be installed and set up for ONE domain name ONLY. 
* To be able to use this product for another domain names you have to order another copy of this product (license).
* 
* This product cannot be redistributed for free or a fee without written permission from AQB Soft.
* 
* This notice may not be removed from the source code.
* 
***************************************************************************/

bx_import('BxDolProfileFields');

class AqbBlogsFormSearch extends BxTemplFormView
{
    function AqbBlogsFormSearch ()
    {
    	$oMain = $this->getMain();
    	$sPrefixLang = $oMain->_oConfig->getSystemName('langs');

        bx_import('BxDolCategories');
        $oCategories = new BxDolCategories();
        $oCategories->getTagObjectConfig ();
        $aCategories = $oCategories->getCategoriesList($oMain->_oConfig->getSystemName('categories'), (int)$iProfileId, true);

        $aCustomForm = array(

            'form_attrs' => array(
                'name'     => 'form_search_groups',
                'action'   => '',
                'method'   => 'get',
            ),

            'params' => array (
                'db' => array(
                    'submit_name' => 'submit_form',
                ),
                'csrf' => array(
                    'disable' => true,
                ),
            ),

            'inputs' => array(
                'Keyword' => array(
                    'type' => 'text',
                    'name' => 'Keyword',
                    'caption' => _t($sPrefixLang . '_form_caption_keyword'),
                    'required' => true,
                    'checker' => array (
                        'func' => 'length',
                        'params' => array(3,100),
                        'error' => _t ($sPrefixLang . '_form_err_keyword'),
                    ),
                    'db' => array (
                        'pass' => 'Xss',
                    ),
                ),
                'Category' => array(
                    'type' => 'select_box',
                    'name' => 'Category',
                    'caption' => _t($sPrefixLang . '_form_caption_category'),
                    'values' => $aCategories,
                    'required' => true,
                    'checker' => array (
                        'func' => 'avail',
                        'error' => _t ($sPrefixLang . '_form_err_category'),
                    ),
                    'db' => array (
                        'pass' => 'Xss',
                    ),
                ),
                'Submit' => array (
                    'type' => 'submit',
                    'name' => 'submit_form',
                    'value' => _t('_Submit'),
                    'colspan' => true,
                ),
            ),
        );

        parent::BxTemplFormView ($aCustomForm);
    }
    
	function getMain()
    {
        return BxDolModule::getInstance('AqbBlogsModule');
    }
    
}
