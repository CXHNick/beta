<?php
/**
 * Copyright (c) BoonEx Pty Limited - http://www.boonex.com/
 * CC-BY License - http://creativecommons.org/licenses/by/3.0/
 */

bx_import ('BxTemplFormView');

/**
 * Base class for form which is using a lot of media uploads
 */
class BxDolFormMedia extends BxTemplFormView
{
    var $_aMedia = array();
    var $iEntryId = 0;
    //Nick
    var $videoGroupAlbumName = '';
    var $soundGroupAlbumName = '';
    var $fileGroupAlbumName = '';
    var $photoGroupAlbumName = '';

    function BxDolFormMedia ($aCustomForm, $groupId = 0, $iPostID = 0)
    {
        /*$groupReport = 'None';

        //Nick
        if (isset($aCustomForm['inputs']['group_reports']['values'])) {
            $groupReport = $aCustomForm['inputs']['group_reports']['values'][$_POST['group_reports']];
        }*/

        if (isset($groupId) && $groupId !== 0) {
            $this->iEntryId = $groupId;
        }

        if (isset($aCustomForm['inputs']['allow_post_in_forum_to']['type'])) {
            $oModuleDb = new BxDolModuleDb(); 
            if (!$oModuleDb->getModuleByUri('forum'))
                $aCustomForm['inputs']['allow_post_in_forum_to']['type'] = 'hidden';
        }

        //Nick
        if (!empty($aCustomForm['inputs']['videos_albums']['values'])) {
            if ($aCustomForm['inputs']['videos_albums']['values'][$_POST['videos_albums']] !== 'None') {
                $this->_oMain->videoGroupAlbumName = $aCustomForm['inputs']['videos_albums']['values'][$_POST['videos_albums']];
            }
        }

        if (!empty($aCustomForm['inputs']['sounds_albums']['values'])) {
            if ($aCustomForm['inputs']['sounds_albums']['values'][$_POST['sounds_albums']] !== 'None') {
                $this->soundGroupAlbumName = $aCustomForm['inputs']['sounds_albums']['values'][$_POST['sounds_albums']];
            }
        }

        if (!empty($aCustomForm['inputs']['youtube_albums']['values'])) {
            if ($aCustomForm['inputs']['youtube_albums']['values'][$_POST['youtube_albums']] !== 'None') {
                $this->_oMain->videoGroupAlbumName = $aInfo['inputs']['youtube_albums']['values'][$_POST['youtube_albums']];
            }
        }

        /*if (!empty($aCustomForm['inputs']['files_albums']['values'])) {
            if ($aCustomForm['inputs']['files_albums']['values'][$_POST['files_albums']] !== 'None') {
                $this->fileGroupAlbumName = $aCustomForm['inputs']['files_albums']['values'][$_POST['files_albums']];
            }
        }

        if (!empty($aCustomForm['inputs']['photos_albums']['values'])) {
            if ($aCustomForm['inputs']['photos_albums']['values'][$_POST['photos_albums']] !== 'None') {
                $this->photoGroupAlbumName = $aCustomForm['inputs']['photos_albums']['values'][$_POST['photos_albums']];
            }
        }*/


        parent::BxTemplFormView ($aCustomForm, $iPostID);
    }

    /**
     * upload photos to photos module
     * @param $sTag a tag to accociate with an image
     * @param $sCat a category to accociate with an image
     * @param $sName form field name with a files
     * @param $sTitle form field name with image titles
     * @param $sTitleAlt alternative form field name with image title
     * @return array of uploaded images ids
     */
    function uploadPhotos ($sTag, $sCat, $sName = 'images', $sTitle = 'images_titles', $sTitleAlt = 'title')
    {
        $aRet = array ();
        $aTitles = $this->getCleanValue($sTitle);

        foreach ($_FILES[$sName]['tmp_name'] as $i => $sUploadedFile) {
            $aFileInfo = array (
                'medTitle' => is_array($aTitles) && isset($aTitles[$i]) && $aTitles[$i] ? stripslashes($aTitles[$i]) : stripslashes($this->getCleanValue($sTitleAlt)),
                'medDesc' => is_array($aTitles) && isset($aTitles[$i]) && $aTitles[$i] ? stripslashes($aTitles[$i]) : stripslashes($this->getCleanValue($sTitleAlt)),
                'medTags' => $sTag,
                'Categories' => array($sCat),
                'group_id' => $this->iEntryId,
            );
            //Nick
            $aFileInfo['truth'] = $this->photoGroupAlbumName ? true : false;
            //Nick
            $aPathInfo = pathinfo ($_FILES[$sName]['name'][$i]);
            $sTmpFile = BX_DIRECTORY_PATH_ROOT . 'tmp/i' . time() . $i . getLoggedId() . '.' . $aPathInfo['extension'];
            if (move_uploaded_file($sUploadedFile,  $sTmpFile)) {
                $iRet = BxDolService::call('photos', 'perform_photo_upload', array($sTmpFile, $aFileInfo, false), 'Uploader');
                //Nick
                if ($this->photoGroupAlbumName) {
                    //$msg = "INSIDE UPLOADVIDEOS";
                    //echo "<script type='text/javascript'>alert('$msg');</script>";
                    $aFileInfo['owner'] = getLoggedId();
                    $aFileInfo['ext'] = $aPathInfo['extension'];
                    $aFileInfo['AssocID'] = $iRet;
                    $meow = BxDolService::call('gphotos', 'add_file_to_db', array($this->photoGroupAlbumName, $aFileInfo), 'Uploaders');
                }
                //Nick
                @unlink ($sTmpFile);
                if ($iRet)
                    $aRet[] = $iRet;
            }
        }
        return $aRet;
    }

    /**
     * upload videos to videos module
     * @param $sTag a tag to accociate with an image
     * @param $sCat a category to accociate with an image
     * @param $sName form field name with a files
     * @param $sTitle form field name with image titles
     * @param $sTitleAlt alternative form field name with image title
     * @return array of uploaded images ids
     */
    function uploadVideos ($sTag, $sCat, $sName = 'videos', $sTitle = 'videos_titles', $sTitleAlt = 'title')
    {
        //$msg = "INSIDE UPLOADVIDEOS";
        //echo "<script type='text/javascript'>alert('$msg');</script>";

        /* AQB Extended Video Uploader begin */
        //if ($_POST['aqb_uploader_chooser'] == 'youtube') {            
            //return BxDolService::call('aqb_evu', 'upload_videos', array($this, $sTag, $sCat, $sName, $sTitle, $sTitleAlt));             
            //Nick
            //$aRet = BxDolService::call('aqb_evu', 'upload_videos', array($this, $sTag, $sCat, $sName, $sTitle, $sTitleAlt)); 
            
            //require_once('/var/www/vhosts/combinexlife.com/root/cxlife/modules/boonex/videos/classes/BxVideosDb.php');
            //require_once('/var/www/vhosts/combinexlife.com/root/cxlife/modules/boonex/videos/classes/BxVideosModule.php');
            //require_once('/var/www/vhosts/combinexlife.com/root/cxlife/modules/boonex/videos/classes/BxVideosConfig.php');

            //$vidMod = BxDolModule::getInstance('BxVideosModule');
            //$vidConfig = new BxVideosConfig($vidMod);
            //$vidDb = new BxVideosDb($vidConfig);
            //$vidDb = $vidMod->_oDb;

            /*$fileInfo = $this->_oDb->getYoutubeInfo($aRet[0]);

            if ($this->videoGroupAlbumName) {                
                $info = array (
                'title' => $fileInfo['Title'],
                'desc' => $fileInfo['Description'],
                'tags' => $fileInfo['Tags'],
                'categories' => $fileInfo['Categories'],
                //'group_id' => $this->iEntryId,
                'owner' => getLoggedId(),
                'ext' => '',
                'AssocID' => $aRet[0],
            );
                $meow = BxDolService::call('gphotos', 'add_file_to_db', array($this->videoGroupAlbumName, $info), 'Uploaders');
            }
            //Nick
            return $aRet;*/
        //}
        /* AQB Extended Video Uploader end */

        $aRet = array ();
        $aTitles = $this->getCleanValue($sTitle);

        foreach ($_FILES[$sName]['tmp_name'] as $i => $sUploadedFile) {
            $aFileInfo = array (
                'title' => is_array($aTitles) && isset($aTitles[$i]) && $aTitles[$i] ? stripslashes($aTitles[$i]) : stripslashes($this->getCleanValue($sTitleAlt)),
                'desc' => is_array($aTitles) && isset($aTitles[$i]) && $aTitles[$i] ? stripslashes($aTitles[$i]) : stripslashes($this->getCleanValue($sTitleAlt)),
                'tags' => $sTag,
                'categories' => $sCat,
                'group_id' => $this->iEntryId,
            );
            //Nick
            $aFileInfo['truth'] = $this->videoGroupAlbumName ? true : false;
            //Nick
            $aPathInfo = pathinfo ($_FILES[$sName]['name'][$i]);
            $sTmpFile = BX_DIRECTORY_PATH_ROOT . 'tmp/v' . time() . $i . getLoggedId() . '.' . $aPathInfo['extension'];
            if (move_uploaded_file($sUploadedFile,  $sTmpFile)) {
                $iRet = BxDolService::call('videos', 'perform_video_upload', array($sTmpFile, $aFileInfo, false), 'Uploader');

                //Nick
                if ($this->videoGroupAlbumName) {
                    //$msg = "INSIDE UPLOADVIDEOS";
                    //echo "<script type='text/javascript'>alert('$msg');</script>";
                    $aFileInfo['owner'] = getLoggedId();
                    $aFileInfo['ext'] = $aPathInfo['extension'];
                    $aFileInfo['AssocID'] = $iRet;
                    $meow = BxDolService::call('gphotos', 'add_file_to_db', array($this->videoGroupAlbumName, $aFileInfo), 'Uploaders');
                }
                //Nick

                @unlink ($sTmpFile);
                if ($iRet)
                    $aRet[] = $iRet;
            }
        }
        return $aRet;
    }

    /**
     * upload sounds to sounds module
     * @param $sTag a tag to accociate with an image
     * @param $sCat a category to accociate with an image
     * @param $sName form field name with a files
     * @param $sTitle form field name with image titles
     * @param $sTitleAlt alternative form field name with image title
     * @return array of uploaded images ids
     */
    function uploadSounds ($sTag, $sCat, $sName = 'sounds', $sTitle = 'sounds_titles', $sTitleAlt = 'title')
    {
        $aRet = array ();
        $aTitles = $this->getCleanValue($sTitle);

        foreach ($_FILES[$sName]['tmp_name'] as $i => $sUploadedFile) {
            $aFileInfo = array (
                'title' => is_array($aTitles) && isset($aTitles[$i]) && $aTitles[$i] ? stripslashes($aTitles[$i]) : stripslashes($this->getCleanValue($sTitleAlt)),
                'desc' => is_array($aTitles) && isset($aTitles[$i]) && $aTitles[$i] ? stripslashes($aTitles[$i]) : stripslashes($this->getCleanValue($sTitleAlt)),
                'tags' => $sTag,
                'categories' => $sCat,
                'group_id' => $this->iEntryId,
            );
            //Nick
            $aFileInfo['truth'] = $this->soundGroupAlbumName ? true : false;
            //Nick
            $aPathInfo = pathinfo ($_FILES[$sName]['name'][$i]);
            $sTmpFile = BX_DIRECTORY_PATH_ROOT . 'tmp/s' . time() . $i . getLoggedId() . '.' . $aPathInfo['extension'];
            if (move_uploaded_file($sUploadedFile,  $sTmpFile)) {
                $iRet = BxDolService::call('sounds', 'perform_music_upload', array($sTmpFile, $aFileInfo, false), 'Uploader');
                
                //Nick
                if ($this->soundGroupAlbumName) {
                    //$msg = "INSIDE UPLOADVIDEOS";
                    //echo "<script type='text/javascript'>alert('$msg');</script>";
                    $aFileInfo['owner'] = getLoggedId();
                    $aFileInfo['ext'] = $aPathInfo['extension'];
                    $aFileInfo['AssocID'] = $iRet;
                    $meow = BxDolService::call('gphotos', 'add_file_to_db', array($this->soundGroupAlbumName, $aFileInfo), 'Uploaders');
                }
                //Nick

                @unlink ($sTmpFile);
                if ($iRet)
                    $aRet[] = $iRet;
            }
        }
        return $aRet;
    }

    /**
     * upload files to files module
     * @param $sTag a tag to accociate with an image
     * @param $sCat a category to accociate with an image
     * @param $sName form field name with a files
     * @param $sTitle form field name with image titles
     * @param $sTitleAlt alternative form field name with image title
     * @return array of uploaded images ids
     */
    function uploadFiles ($sTag, $sCat, $sName = 'files', $sTitle = 'files_titles', $sTitleAlt = 'title')
    {
        $aRet = array ();
        $aTitles = $this->getCleanValue($sTitle);

        foreach ($_FILES[$sName]['tmp_name'] as $i => $sUploadedFile) {
            $aFileInfo = array (
                'medTitle' => is_array($aTitles) && isset($aTitles[$i]) && $aTitles[$i] ? stripslashes($aTitles[$i]) : stripslashes($this->getCleanValue($sTitleAlt)),
                'medDesc' => is_array($aTitles) && isset($aTitles[$i]) && $aTitles[$i] ? stripslashes($aTitles[$i]) : stripslashes($this->getCleanValue($sTitleAlt)),
                'medTags' => $sTag,
                'Categories' => array($sCat),
                'Type' => $_FILES[$sName]['type'][$i],
                'group_id' => $this->iEntryId,
            );
            //Nick
            $aFileInfo['truth'] = $this->fileGroupAlbumName ? true : false;
            //Nick
            $aPathInfo = pathinfo ($_FILES[$sName]['name'][$i]);
            $sTmpFile = BX_DIRECTORY_PATH_ROOT . 'tmp/v' . time() . getLoggedId() . $i . '.' . $aPathInfo['extension'];
            if (move_uploaded_file($sUploadedFile,  $sTmpFile)) {
                $iRet = BxDolService::call('files', 'perform_file_upload', array($sTmpFile, $aFileInfo), 'Uploader');
                //Nick
                if ($this->fileGroupAlbumName) {
                    //$msg = "INSIDE UPLOADVIDEOS";
                    //echo "<script type='text/javascript'>alert('$msg');</script>";
                    $aFileInfo['owner'] = getLoggedId();
                    $aFileInfo['ext'] = $aPathInfo['extension'];
                    $aFileInfo['AssocID'] = $iRet;
                    $meow = BxDolService::call('gphotos', 'add_file_to_db', array($this->fileGroupAlbumName, $aFileInfo), 'Uploaders');
                }
                //Nick
                @unlink ($sTmpFile);
                if ($iRet)
                    $aRet[] = $iRet;
            }
        }
        return $aRet;
    }

    /**
     * Insert media to database
     * @param $iEntryId associated entry id
     * @param $aMedia media id's array
     * @param $sMediaType media type, like images, videos, etc
     */
    function insertMedia ($iEntryId, $aMedia, $sMediaType)
    {
        $aMediaValidated = $this->_validateMediaIds ($aMedia);
        $this->_oDb->insertMedia ($iEntryId, $aMediaValidated, $sMediaType);
    }

    /**
     * Update media in database
     * First it delete media ids from database, then adds new
     * Be carefull if you store more information than just a pair of ids
     * @param $iEntryId associated entry id
     * @param $aMedia media id's array
     * @param $sMediaType media type, like images, videos, etc
     */
    function updateMedia ($iEntryId, $aMediaAdd, $aMediaDelete, $sMediaType)
    {
        $aMediaValidated = $this->_validateMediaIds ($aMediaAdd);
        $this->_oDb->updateMedia ($iEntryId, $aMediaValidated, $aMediaDelete, $sMediaType);
    }

    /**
     * Delete media from database
     * @param $iEntryId associated entry id
     * @param $aMedia media id's array
     * @param $sMediaType media type, like images, videos, etc
     */
    function deleteMedia ($iEntryId, $aMedia, $sMediaType)
    {
        $aMediaValidated = $this->_validateMediaIds ($aMedia);
        $this->_oDb->deleteMedia ($iEntryId, $aMediaValidated, $sMediaType);
    }

    /**
     * @access private
     */
    function _validateMediaIds ($aMedia)
    {
        if (is_array ($aMedia)) {
            $aMediaValidated = array ();
            foreach ($aMedia as $iId) {
                $iId = (int)$iId;
                $aMediaValidated[$iId] = $iId;
            }
            return $aMediaValidated;
        } else {
            return (int)$aMedia;
        }
    }

    /**
     * @access private
     */
    function _getFilesInEntry ($sModuleName, $sServiceMethod, $sName, $sMediaType, $iIdProfile, $iEntryId)
    {

        $aReadyMedia = array ();
        if ($iEntryId)
            $aReadyMedia = $this->_oDb->getMediaIds($iEntryId, $sMediaType);

        if (!$aReadyMedia)
            return array();

        $aDataEntry = $this->_oDb->getEntryById($iEntryId);

        $aFiles = array ();
        foreach ($aReadyMedia as $iMediaId) {
            switch ($sModuleName) {
            case 'photos':
                $aRow = BxDolService::call($sModuleName, $sServiceMethod, array($iMediaId, 'icon'), 'Search');
                break;
            case 'sounds':
                $aRow = BxDolService::call($sModuleName, $sServiceMethod, array($iMediaId, 'browse'), 'Search');
                break;
            default:
                $aRow = BxDolService::call($sModuleName, $sServiceMethod, array($iMediaId), 'Search');
            }

            if (!$this->_oMain->isEntryAdmin($aDataEntry, $iIdProfile) && $aRow['owner'] != $iIdProfile)
                continue;

            $aFiles[] = array (
                'name' => $sName,
                'id' => $iMediaId,
                'title' => $aRow['title'],
                'icon' => $aRow['file'],
                'owner' => $aRow['owner'],
                'checked' => 'checked',
            );
        }
        return $aFiles;
    }

    /**
     * process media upload updates
     * call it after successful call $form->insert/update functions
     * @param $iEntryId associated entry id
     * @return nothing
     */
    function processMedia ($iEntryId, $iProfileId)
    {
        $aDataEntry = $this->_oDb->getEntryById($iEntryId);

        foreach ($this->_aMedia as $sName => $a) {

            $aFiles2Delete = array ();
            $aFiles = $this->_getFilesInEntry ($a['module'], $a['service_method'], $a['post'], $sName, (int)$iProfileId, $iEntryId);
            foreach ($aFiles as $aRow)
                $aFiles2Delete[$aRow['id']] = $aRow['id'];

            if (is_array($_REQUEST[$a['post']]) && $_REQUEST[$a['post']] && $_REQUEST[$a['post']][0]) {
                $this->updateMedia ($iEntryId, $_REQUEST[$a['post']], $aFiles2Delete, $sName);
            } else {
                $this->deleteMedia ($iEntryId, $aFiles2Delete, $sName);
            }

            $sUploadFunc = $a['upload_func'];
            if ($aMedia = $this->$sUploadFunc($a['tag'], $a['cat'])) {
                $this->_oDb->insertMedia ($iEntryId, $aMedia, $sName);
                if ($a['thumb'] && !$aDataEntry[$a['thumb']] && !$_REQUEST[$a['thumb']])
                    $this->_oDb->setThumbnail ($iEntryId, 0);
            }

            $aMediaIds = $this->_oDb->getMediaIds($iEntryId, $sName);

            if ($a['thumb']) { // set thumbnail to another one if current thumbnail is deleted
                $sThumbFieldName = $a['thumb'];
                if ($aDataEntry[$sThumbFieldName] && !isset($aMediaIds[$aDataEntry[$sThumbFieldName]])) {
                    $this->_oDb->setThumbnail ($iEntryId, 0);
                }
            }

            // process all deleted media - delete actual file
            $aDeletedMedia = array_diff ($aFiles2Delete, $aMediaIds);
            if ($aDeletedMedia) {
                foreach ($aDeletedMedia as $iMediaId) {
                    if (!$this->_oDb->isMediaInUse($iMediaId, $sName))
                        BxDolService::call($a['module'], 'remove_object', array($iMediaId));
                }
            }
        }

    }

    /**
     * Generate templates for custom media elements
     * @param $iProfileId current profile id
     * @param $iEntryId associated entry id
     * @return array with templates grouped by media typed
     */
    function generateCustomMediaTemplates ($iProfileId, $iEntryId, $iThumb = 0)
    {
        
        //$msg = "hi mom";
        //echo "<script type='text/javascript'>alert('$iEntryId');</script>";

        $aTemplates = array ();
        foreach ($this->_aMedia as $sName => $a) {

            $aFiles = $this->_getFilesInEntry ($a['module'], $a['service_method'], $a['post'], $sName, (int)$iProfileId, $iEntryId);

            // files choice / check boxes
            $aVarsChoice = array (
                'bx_if:empty' => array(
                    'condition' => empty($aFiles),
                    'content' => array ()
                ),
                'bx_repeat:files' => $aFiles,
            );
            $aTemplates[$sName]['choice'] = $aFiles ? $this->_oMain->_oTemplate->parseHtmlByName('form_field_files_choice', $aVarsChoice) : '';

            // thumb choice / radio buttons
            if ($a['thumb']) {
                foreach ($aFiles as $k => $r) {
                    $aFiles[$k]['checked'] = ($iThumb == $r['id'] ? 'checked' : '');
                    $aFiles[$k]['name'] = $a['thumb'];
                }
                $aVarsThumbsChoice = array (
                    'bx_if:empty' => array (
                        'condition' => empty($aFiles),
                        'content' => array ()
                    ),
                    'bx_repeat:files' => $aFiles,
                );
                $aTemplates[$sName]['thumb_choice'] = $aFiles ? $this->_oMain->_oTemplate->parseHtmlByName('form_field_thumb_choice', $aVarsThumbsChoice) : '';
            }

            // upload form
            $aVarsUpload = array (
                'file' => $sName,
                'title' => $a['title_upload_post'],
                'file_upload_title' => $a['title_upload'],
                'bx_if:price' => array (
                    'condition' => false, 'content' => array('price' => '', 'file_price_title' => '')
                ),
                'bx_if:privacy' => array (
                    'condition' => false, 'content' => array('select' => '', 'file_permission_title' => '')
                ),
            );
            //$aTemplates[$sName]['upload'] = $this->_oMain->_oTemplate->parseHtmlByName('form_field_files_upload', $aVarsUpload);
            /* AQB Extended Video Uploader begin */         
            
            //if ($sName != 'videos') {
                $aTemplates[$sName]['upload'] = $this->_oMain->_oTemplate->parseHtmlByName('form_field_files_upload', $aVarsUpload);
            //} else {
            //    $aTemplates[$sName]['upload'] = BxDolService::call('aqb_evu', 'get_extended_template', array($aVarsUpload));
            //}
            
            /* AQB Extended Video Uploader end */
            
        }
        return $aTemplates;
    }

    function processMembershipChecksForMediaUploads (&$aInputs)
    {
        $isAdmin = $GLOBALS['logged']['admin'] && isProfileActive($this->_iProfileId);

        defineMembershipActions (array('photos add', 'sounds add', 'videos add', 'files add'));

        if (defined('BX_PHOTOS_ADD'))
            $aCheck = checkAction(getLoggedId(), BX_PHOTOS_ADD);
        if (!defined('BX_PHOTOS_ADD') || ($aCheck[CHECK_ACTION_RESULT] != CHECK_ACTION_RESULT_ALLOWED && !$isAdmin)) {
            unset($aInputs['thumb']);
        }

        $a = array ('images' => 'PHOTOS', 'videos' => 'VIDEOS', 'sounds' => 'SOUNDS', 'files' => 'FILES');
        foreach ($a as $k => $v) {
            if (defined("BX_{$v}_ADD"))
                $aCheck = checkAction(getLoggedId(), constant("BX_{$v}_ADD"));
            if ((!defined("BX_{$v}_ADD") || $aCheck[CHECK_ACTION_RESULT] != CHECK_ACTION_RESULT_ALLOWED) && !$isAdmin) {
                unset($this->_aMedia[$k]);
                unset($aInputs['header_'.$k]);
                unset($aInputs[$k.'_choice']);
                unset($aInputs[$k.'_upload']);
            }
        }
    }
}
