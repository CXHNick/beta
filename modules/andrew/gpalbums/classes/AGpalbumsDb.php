<?php
/***************************************************************************
*
* IMPORTANT: This is a commercial product made by AndrewP. It cannot be modified for other than personal usage.
* The "personal usage" means the product can be installed and set up for ONE domain name ONLY.
* To be able to use this product for another domain names you have to order another copy of this product (license).
* This product cannot be redistributed for free or a fee without written permission from AndrewP.
* This notice may not be removed from the source code.
*
***************************************************************************/
bx_import('BxDolModuleDb');

class AGpalbumsDb extends BxDolModuleDb {
    var $_oConfig;

    /**
     * Constructor
     */
    function AGpalbumsDb(&$oConfig) {
        parent::BxDolModuleDb($oConfig);
        $this->_oConfig = $oConfig;
    }
    function getGroupInfo($sID) {
        $aInfos = $this->getAll("SELECT * FROM `bx_groups_main` WHERE `id`='{$sID}'");
        return $aInfos[0];
    }
    function getGroupInfoByUri($sGroupUri) {
        $aInfos = $this->getAll("SELECT * FROM `bx_groups_main` WHERE `uri`='{$sGroupUri}'");
        return $aInfos[0];
    }
    function getAlbumPhotosCnt($iID) {
        return (int)$this->getOne("SELECT COUNT(*) FROM `agrp_palbums_units` WHERE `AlbumID`='{$iID}'");
    }
    function getAlbumIDByUri($sUri) {
        return (int)$this->getOne("SELECT `ID` FROM `agrp_palbums_albums` WHERE `Uri`='{$sUri}'");
    }
    function getAlbumOwnerByID($iID) {
        return (int)$this->getOne("SELECT `Owner` FROM `agrp_palbums_albums` WHERE `ID`='{$iID}'");
    }
    //Nick
    function getAlbumTitleByID($iID) {
        return $this->getOne("SELECT `Title` FROM `agrp_palbums_albums` WHERE `ID`='{$iID}'");
    }
    //Nick
    function getAlbumUriByID($iID) {
        return $this->getOne("SELECT `Uri` FROM `agrp_palbums_albums` WHERE `ID`='{$iID}'");
    }
    //Nick
    function getUnitOwnerByID($iID) {
        return getNickName((int)$this->getOne("SELECT `Owner` FROM `agrp_palbums_units` WHERE `ID`='{$iID}'"));
    }
    function getAlbumInfo($iID) {
        $aInfos = $this->getAll("SELECT * FROM `agrp_palbums_albums` WHERE `ID`='{$iID}'");
        return $aInfos[0];
    }
    //Nick
    function getAlbumIdByTitle($sTitle) {
        $aInfos = $this->getOne("SELECT `ID` FROM `agrp_palbums_albums` WHERE `Title`='{$sTitle}'");
        return $aInfos;
    }
    //Nick
    function getAlbumTypeByID($iID) {
        return $this->getOne("SELECT `Type` FROM `agrp_palbums_albums` WHERE `ID`='{$iID}'");
    }
    function getLastImagesOfAlbum($iID) {
        $aAlbumsInfo = $this->getAll("SELECT * FROM `agrp_palbums_units` WHERE `AlbumID` = '{$iID}' ORDER BY `When` DESC");
        return $aAlbumsInfo;
    }
    function insertImage2Album($aParams) {
        
        $title = $this->escape($aParams['Title']);
        $fileName = $this->escape($aParams['Filename']);

        $sSQL = "
            INSERT INTO `agrp_palbums_units`
            SET
                `AlbumID` = '{$aParams['AlbumID']}',
                `Title` = '{$title}',
                `Uri` = '{$aParams['Uri']}',
                `When` = '{$aParams['When']}',
                `Size` = '{$aParams['Size']}',
                `Filename` = '{$fileName}',
                `owner` = '{$aParams['owner']}',
                `AssocID` = '{$aParams['AssocID']}'
        ";
        $msg = $this->query($sSQL);
        /*$meow = "hi";
        $poop = "bye";

        if (msg) {
            echo "<script type='text/javascript'>alert('$meow');</script>";
        } else {
            echo "<script type='text/javascript'>alert('$poop');</script>";
        }*/

        return $this->lastId();
    }
    function getImageInfo($iID) {
        $sImageInfos = $this->getAll("SELECT * FROM `agrp_palbums_units` WHERE `ID`='{$iID}'");
        return $sImageInfos[0];
    }
    function getImageInfoByUri($sUri) {
        $sImageInfos = $this->getAll("SELECT * FROM `agrp_palbums_units` WHERE `Uri`='{$sUri}'");
        return $sImageInfos[0];
    }
    function deleteImage($iID) {
        return $this->query("DELETE FROM `agrp_palbums_units` WHERE `ID`='{$iID}'");
    }
    function deleteAlbumImages($iID) {
        return $this->query("DELETE FROM `agrp_palbums_units` WHERE `AlbumID`='{$iID}'");
    }
    function deleteAlbum($iID) {
        return $this->query("DELETE FROM `agrp_palbums_albums` WHERE `ID`='{$iID}'");
    }
    function getImagesOfAlbum($iID, $bGetMaxHeight = false) {
        $aImagesInAlbum = $this->getAll("SELECT * FROM `agrp_palbums_units` WHERE `AlbumID`='{$iID}' ORDER BY `When` DESC");

        $iMaxHeight = 0;
        $aPhotos = array();
        foreach ($aImagesInAlbum as $iID => $aIDs) {
            if ($bGetMaxHeight) {
                list($iImgWidth, $iImgHeight) = explode("x", $aIDs['Size']);
                $iMaxHeight = ($iImgHeight > $iMaxHeight) ? $iImgHeight : $iMaxHeight;
            }
            $aPhotos[] = $aIDs['ID'];
        }
        return ($bGetMaxHeight) ? array($aPhotos, $iMaxHeight) : $aPhotos;
    }
    function updateImage2Thumb($iAlbumID, $iUnitID) {
        return $this->query("UPDATE `agrp_palbums_albums` SET `Thumb`='{$iUnitID}' WHERE `ID`='{$iAlbumID}'");
    }
    function isFan($iEntryId, $iProfileId, $isConfirmed) {
        $isConfirmed = $isConfirmed ? 1 : 0;
        return $this->getOne ("SELECT `when` FROM `bx_groups_fans` WHERE `id_entry` = '$iEntryId' AND `id_profile` = '$iProfileId' AND `confirmed` = '$isConfirmed' LIMIT 1");
    }
    function isGroupAdmin($iEntryId, $iProfileId) {
        return $this->getOne ("SELECT `when` FROM `bx_groups_admins` WHERE `id_entry` = '$iEntryId' AND `id_profile` = '$iProfileId' LIMIT 1");
    }
    function updateImageDescription($iID, $sTitle, $sDesc) {
        return $this->query("UPDATE `agrp_palbums_units` SET `Title`='{$sTitle}', `Description`='{$sDesc}' WHERE `ID`='{$iID}'");
    }
    function updateFeature($iPostID, $iStatus = 2) {
        return $this->query("UPDATE `agrp_palbums_units` SET `featured` = '{$iStatus}' WHERE `ID`='{$iPostID}'");
    }
    //Nick
    function insertVideo($aParams) {
        $lastId = $this->getFirstRow("Select MAX(`ID`) AS 'Macks' FROM `RayVideoFiles`");

        $last = $lastId['Macks'] + 1;
        //echo "<script type='text/javascript'>alert('$lastId');</script>";  

        $sSQL = "
            INSERT INTO `RayVideoFiles`
            SET
                `ID` = '{$last}',
                `Title` = '{$aParams['Title']}',
                `Uri` = '{$aParams['Uri']}',
                `Time` = '{$aParams['When']}',
                `owner` = '{$aParams['owner']}',
                `Status` = 'approved'
        ";
        $msg = $this->query($sSQL);
    }
    //Nick
    function getVideoUriById($iID) {
        return $this->getOne("SELECT `Uri` FROM `RayVideoFiles` WHERE `ID` = '$iID'");
    }

    //Nick
    function getSoundUriById($iID) {
        return $this->getOne("SELECT `Uri` FROM `RayMp3Files` WHERE `ID` = '$iID'");    
    }
    //Nick
    function getGroupIdByAlbumId($iID) {
        return $this->getOne("SELECT `GroupID` FROM `agrp_palbums_albums` WHERE `ID` = '$iID'");
    }
    //Nick
    function getUploadLink($groupId, $aAlbumType) {
        
        $groupUri = $this->getOne("SELECT `uri` FROM `bx_groups_main` WHERE `id` = '$groupId'");
        $uploadLink = BX_DOL_URL_ROOT . 'm/groups/';
        
        if ($aAlbumType == 'Photo') {
            $uploadLink .= 'upload_photos/';
        } else if ($aAlbumType == 'Video') {
            $uploadLink .= 'upload_videos/';
        } else if ($aAlbumType == 'File') {
            $uploadLink .= 'upload_files/';
        } else if ($aAlbumType == 'Sound') {
            $uploadLink .= 'upload_sounds/';
        }

        $uploadLink .= $groupUri;

        return $uploadLink;
    }

    //Nick
    function getEmbedLink($groupId) {

        $groupUri = $this->getOne("SELECT `uri` FROM `bx_groups_main` WHERE `id` = '$groupId'");
        $embedLink = BX_DOL_URL_ROOT . 'm/groups/embed/' . $groupUri;

        return $embedLink;
    }

    //Nick
    function isYoutube($AssocID) {
        $sqlQuery = "SELECT `Source` FROM `RayVideoFiles` WHERE `ID` = '$AssocID'";
        $source = $this->getOne($sqlQuery);

        return ($source == 'youtube') ? true : false;

    }

    //Nick
    function updateVideoInfo($albumId) {
        
        $sqlQuery = "SELECT * FROM `agrp_palbums_units` WHERE `AlbumID` = '$albumId'";
        $videoList = $this->getAll($sqlQuery);
        $i = 0;

        for ($i = 0; i < sizeof($videoList); $i++) {
            
            $newTitle = $videoList[i]['Title'];
            $newDesc = $videoList[i]['AssocID'];
            $assoc = $videoList[i]['Description'];
            $query2 = "SELECT * FROM `RayVideoFiles` WHERE `ID` = '$assoc'";
            $comp = $this->getOne($query2);

            if ($videoList[i]['Title'] != $comp['Title']) {
                $newTitle = $comp['Title'];
                $this->updateImageDescription($videoList[i]['ID'], $newTitle, $newDesc);
            }
        }

    }

    //Nick
    function getThumbUrl($albumId) {
        $sqlQuery = "SELECT `AssocID` FROM `agrp_palbums_units` WHERE `AlbumID` = '$albumId' ORDER BY `ID` DESC";
        $latestVideo = $this->getOne($sqlQuery);

        if (!$latestVideo) {
            return;
        }

        //$picPath = '';

        $picPath = BX_DOL_URL_ROOT . 'flash/modules/video/files/' . $latestVideo . '_small.jpg';
        if ($this->isYoutube($latestVideo)) {
                //$picPath = BxDolService::call('videos', 'get_thumb', array($aData['assoc_id']), 'Uploader');
                $picPath = BX_DOL_URL_ROOT . 'modules/andrew/gpalbums/templates/base/images/icons/YouTubeSmall.png';
            }

            //echo "<script type='text/javascript'>alert('$picPath');</script>";

        return $picPath;
    }
    

}