<?php
/***************************************************************************
* Date				: Sun January 26, 2014
* Website			: http://www.dialme.com
*
* Product Name			: Add A New Member Module
* Product Version		: 1.0.0
* Product Use			: For Boonex Dolphin 7.0.x & 7.1.x
*
***************************************************************************/

require_once(BX_DIRECTORY_PATH_INC . 'profiles.inc.php');

check_logged();

bx_import('BxDolRequest');
BxDolRequest::processAsAction($GLOBALS['aModule'], $GLOBALS['aRequest']);

?>