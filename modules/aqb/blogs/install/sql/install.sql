SET @sPluginName = 'aqb_blogs';

-- create tables
CREATE TABLE IF NOT EXISTS `[db_prefix]main` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `title` varchar(100) NOT NULL default '',
  `uri` varchar(255) NOT NULL,
  `desc` text NOT NULL,
  `status` enum('approved','pending') NOT NULL default 'approved',
  `thumb` int(11) NOT NULL,
  `created` int(11) NOT NULL,
  `author_id` int(10) unsigned NOT NULL default '0',
  `tags` varchar(255) NOT NULL default '',
  `categories` text NOT NULL,
  `views` int(11) NOT NULL,
  `rate` float NOT NULL,
  `rate_count` int(11) NOT NULL,
  `comments_count` int(11) NOT NULL,
  `likes_count` int(11) NOT NULL,
  `featured` tinyint(4) NOT NULL,
  `allow_view_to` int(11) NOT NULL,
  `allow_view_fans_to` varchar(16) NOT NULL,
  `allow_comment_to` varchar(16) NOT NULL,
  `allow_rate_to` varchar(16) NOT NULL,  
  `allow_post_in_forum_to` varchar(16) NOT NULL,
  `allow_join_to` int(11) NOT NULL,
  `join_confirmation` tinyint(4) NOT NULL default '0',
  `allow_upload_photos_to` varchar(16) NOT NULL,
  `allow_upload_videos_to` varchar(16) NOT NULL,
  `allow_upload_sounds_to` varchar(16) NOT NULL,
  `allow_upload_files_to` varchar(16) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uri` (`uri`),
  KEY `author_id` (`author_id`),
  KEY `created` (`created`),
  FULLTEXT KEY `search` (`title`,`desc`,`tags`,`categories`),
  FULLTEXT KEY `tags` (`tags`),
  FULLTEXT KEY `categories` (`categories`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `[db_prefix]likes` (
  `id_entry` int(10) unsigned NOT NULL,
  `id_profile` int(10) unsigned NOT NULL,
  `when` int(10) unsigned NOT NULL,
  `confirmed` tinyint(4) UNSIGNED NOT NULL default '0',
  PRIMARY KEY (`id_entry`, `id_profile`)
) ENGINE=MYISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `[db_prefix]admins` (
  `id_entry` int(10) unsigned NOT NULL,
  `id_profile` int(10) unsigned NOT NULL,
  `when` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_entry`, `id_profile`)
) ENGINE=MYISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `[db_prefix]images` (
  `entry_id` int(10) unsigned NOT NULL,
  `media_id` int(10) unsigned NOT NULL,
  UNIQUE KEY `entry_id` (`entry_id`,`media_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `[db_prefix]videos` (
  `entry_id` int(10) unsigned NOT NULL,
  `media_id` int(11) NOT NULL,
  UNIQUE KEY `entry_id` (`entry_id`,`media_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `[db_prefix]sounds` (
  `entry_id` int(10) unsigned NOT NULL,
  `media_id` int(11) NOT NULL,
  UNIQUE KEY `entry_id` (`entry_id`,`media_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `[db_prefix]files` (
  `entry_id` int(10) unsigned NOT NULL,
  `media_id` int(11) NOT NULL,
  UNIQUE KEY `entry_id` (`entry_id`,`media_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `[db_prefix]rating` (
  `gal_id` smallint( 6 ) NOT NULL default '0',
  `gal_rating_count` int( 11 ) NOT NULL default '0',
  `gal_rating_sum` int( 11 ) NOT NULL default '0',
  UNIQUE KEY `gal_id` (`gal_id`)
) ENGINE=MYISAM DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `[db_prefix]rating_track` (
  `gal_id` smallint( 6 ) NOT NULL default '0',
  `gal_ip` varchar( 20 ) default NULL,
  `gal_date` datetime default NULL,
  KEY `gal_ip` (`gal_ip`, `gal_id`)
) ENGINE=MYISAM DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `[db_prefix]cmts` (
  `cmt_id` int( 11 ) NOT NULL AUTO_INCREMENT ,
  `cmt_parent_id` int( 11 ) NOT NULL default '0',
  `cmt_object_id` int( 12 ) NOT NULL default '0',
  `cmt_author_id` int( 10 ) unsigned NOT NULL default '0',
  `cmt_text` text NOT NULL ,
  `cmt_mood` tinyint( 4 ) NOT NULL default '0',
  `cmt_rate` int( 11 ) NOT NULL default '0',
  `cmt_rate_count` int( 11 ) NOT NULL default '0',
  `cmt_time` datetime NOT NULL default '0000-00-00 00:00:00',
  `cmt_replies` int( 11 ) NOT NULL default '0',
  PRIMARY KEY ( `cmt_id` ),
  KEY `cmt_object_id` (`cmt_object_id` , `cmt_parent_id`)
) ENGINE=MYISAM DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `[db_prefix]cmts_track` (
  `cmt_system_id` int( 11 ) NOT NULL default '0',
  `cmt_id` int( 11 ) NOT NULL default '0',
  `cmt_rate` tinyint( 4 ) NOT NULL default '0',
  `cmt_rate_author_id` int( 10 ) unsigned NOT NULL default '0',
  `cmt_rate_author_nip` int( 11 ) unsigned NOT NULL default '0',
  `cmt_rate_ts` int( 11 ) NOT NULL default '0',
  PRIMARY KEY (`cmt_system_id` , `cmt_id` , `cmt_rate_author_nip`)
) ENGINE=MYISAM DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `[db_prefix]views_track` (
  `id` int(10) unsigned NOT NULL,
  `viewer` int(10) unsigned NOT NULL,
  `ip` int(10) unsigned NOT NULL,
  `ts` int(10) unsigned NOT NULL,
  KEY `id` (`id`,`viewer`,`ip`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- page compose pages
SET @iMaxOrder = (SELECT `Order` FROM `sys_page_compose_pages` ORDER BY `Order` DESC LIMIT 1);
INSERT INTO `sys_page_compose_pages` (`Name`, `Title`, `Order`) VALUES 
(CONCAT(@sPluginName, '_view'), 'Blog Post View', @iMaxOrder+1),
(CONCAT(@sPluginName, '_celendar'), 'Blogs Calendar', @iMaxOrder+2),
(CONCAT(@sPluginName, '_main'), 'Blogs Home', @iMaxOrder+3),
(CONCAT(@sPluginName, '_my'), 'My Blog Posts', @iMaxOrder+4);

-- page compose blocks
INSERT INTO `sys_page_compose` (`Page`, `PageWidth`, `Desc`, `Caption`, `Column`, `Order`, `Func`, `Content`, `DesignBox`, `ColWidth`, `Visible`, `MinWidth`) VALUES
(CONCAT(@sPluginName, '_view'), '1140px', 'Blog Post''s actions block', '_aqb_blogs_block_actions', 2, 0, 'Actions', '', '1', 28.1, 'non,memb', '0'),
(CONCAT(@sPluginName, '_view'), '1140px', 'Blog Post''s info block', '_aqb_blogs_block_info', 2, 1, 'Info', '', '1', 28.1, 'non,memb', '0'),    
(CONCAT(@sPluginName, '_view'), '1140px', 'Blog Post''s rate block', '_aqb_blogs_block_rate', 2, 2, 'Rate', '', '1', 28.1, 'non,memb', '0'),
(CONCAT(@sPluginName, '_view'), '1140px', 'Blog Post''s likes block', '_aqb_blogs_block_fans', 2, 3, 'Fans', '', '1', 28.1, 'non,memb', '0'),
(CONCAT(@sPluginName, '_view'), '1140px', 'Blog Post''s social sharing block', '_sys_block_title_social_sharing', 2, 4, 'SocialSharing', '', 1, 28.1, 'non,memb', 0),
(CONCAT(@sPluginName, '_view'), '1140px', 'Blog Post''s files block', '_aqb_blogs_block_files', 2, 5, 'Files', '', '1', 28.1, 'non,memb', '0'),
(CONCAT(@sPluginName, '_view'), '1140px', 'Blog Post''s description block', '_aqb_blogs_block_desc', 1, 0, 'Desc', '', '1', 71.9, 'non,memb', '0'),
(CONCAT(@sPluginName, '_view'), '1140px', 'Blog Post''s photo block', '_aqb_blogs_block_photo', 1, 2, 'Photo', '', '1', 71.9, 'non,memb', '0'),
(CONCAT(@sPluginName, '_view'), '1140px', 'Blog Post''s videos block', '_aqb_blogs_block_video', 1, 3, 'Video', '', '1', 71.9, 'non,memb', '0'),    
(CONCAT(@sPluginName, '_view'), '1140px', 'Blog Post''s sounds block', '_aqb_blogs_block_sound', 1, 4, 'Sound', '', '1', 71.9, 'non,memb', '0'),        
(CONCAT(@sPluginName, '_view'), '1140px', 'Blog Post''s comments block', '_aqb_blogs_block_comments', 1, 5, 'Comments', '', '1', 71.9, 'non,memb', '0'),

(CONCAT(@sPluginName, '_main'), '1140px', 'Latest Featured Blog Posts', '_aqb_blogs_block_latest_featured_group', '1', '0', 'LatestFeaturedGroup', '', '1', '71.9', 'non,memb', '0'),
(CONCAT(@sPluginName, '_main'), '1140px', 'Recent Blog Posts', '_aqb_blogs_block_recent', '1', '1', 'Recent', '', '1', '71.9', 'non,memb', '0'),
(CONCAT(@sPluginName, '_main'), '1140px', 'Categories', '_aqb_blogs_block_categories', '2', '0', 'Categories', '', '1', '28.1', 'non,memb', '0'),

(CONCAT(@sPluginName, '_my'), '1140px', 'Administration Owner', '_aqb_blogs_block_administration_owner', '1', '0', 'Owner', '', '1', '100', 'non,memb', '0'),
(CONCAT(@sPluginName, '_my'), '1140px', 'User''s Blog', '_aqb_blogs_block_users_groups', '1', '1', 'Browse', '', '0', '100', 'non,memb', '0'),

('index', '1140px', 'Blogs', '_aqb_blogs_block_homepage', 0, 0, 'PHP', 'bx_import(''BxDolService''); return BxDolService::call(''aqb_blogs'', ''homepage_block'');', 1, 71.9, 'non,memb', 0),
('profile', '1140px', 'Liked Blog Posts', '_aqb_blogs_block_my_blogs_joined', 0, 0, 'PHP', 'bx_import(''BxDolService''); return BxDolService::call(''aqb_blogs'', ''profile_block_joined'', array($this->oProfileGen->_iProfileID));', 1, 71.9, 'non,memb', 0),
('profile', '1140px', 'User Blog Posts', '_aqb_blogs_block_my_blogs', 0, 0, 'PHP', 'bx_import(''BxDolService''); return BxDolService::call(''aqb_blogs'', ''profile_block'', array($this->oProfileGen->_iProfileID));', 1, 71.9, 'non,memb', 0),
('member', '1140px', 'Liked Blog Posts', '_aqb_blogs_block_my_blogs_joined', 0, 0, 'PHP', 'bx_import(''BxDolService''); return BxDolService::call(''aqb_blogs'', ''profile_block_joined'', array($this->oProfileGen->_iProfileID));', 1, 71.9, 'non,memb', 0);

-- permalinkU
INSERT INTO `sys_permalinks` VALUES (NULL, CONCAT('modules/?r=', @sPluginName, '/'), CONCAT('m/', @sPluginName, '/'), 'aqb_blogs_permalinks');

-- settings
SET @iMaxOrder = (SELECT `menu_order` + 1 FROM `sys_options_cats` ORDER BY `menu_order` DESC LIMIT 1);
INSERT INTO `sys_options_cats` (`name`, `menu_order`) VALUES (@sPluginName, @iMaxOrder);
SET @iCategId = (SELECT LAST_INSERT_ID());

INSERT INTO `sys_options` (`Name`, `VALUE`, `kateg`, `desc`, `Type`, `check`, `err_text`, `order_in_kateg`, `AvailableValues`) VALUES
(CONCAT(@sPluginName, '_permalinks'), 'on', 26, 'Enable friendly permalinks for Blogs Premium module', 'checkbox', '', '', '0', ''),
(CONCAT(@sPluginName, '_autoapproval'), 'on', @iCategId, 'Activate all blog posts after creation automatically', 'checkbox', '', '', '0', ''),
(CONCAT(@sPluginName, '_author_comments_admin'), 'on', @iCategId, 'Allow blog post owner to edit and delete any comment', 'checkbox', '', '', '0', ''),
('category_auto_app_aqb_blogs', 'on', @iCategId, 'Activate all categories after creation automatically', 'checkbox', '', '', '0', ''),
(CONCAT(@sPluginName, '_perpage_view_fans'), '6', @iCategId, 'Number of liked members to show on blog post view page', 'digit', '', '', '0', ''),
(CONCAT(@sPluginName, '_perpage_browse_fans'), '30', @iCategId, 'Number of liked members to show on browse liked page', 'digit', '', '', '0', ''),
(CONCAT(@sPluginName, '_perpage_main_recent'), '10', @iCategId, 'Number of recently added blog posts to show on Blogs Premium home page', 'digit', '', '', '0', ''),
(CONCAT(@sPluginName, '_perpage_browse'), '14', @iCategId, 'Number of blog posts to show on browse pages', 'digit', '', '', '0', ''),
(CONCAT(@sPluginName, '_perpage_profile'), '4', @iCategId, 'Number of blog posts to show on profile page', 'digit', '', '', '0', ''),
(CONCAT(@sPluginName, '_perpage_homepage'), '5', @iCategId, 'Number of blog posts to show on site''s homepage', 'digit', '', '', '0', ''),
(CONCAT(@sPluginName, '_homepage_default_tab'), 'featured', @iCategId, 'Default Blogs Premium block tab on homepage', 'select', '', '', '0', 'featured,recent,top,popular'),
(CONCAT(@sPluginName, '_max_rss_num'), '10', @iCategId, 'Max number of rss items to provide', 'digit', '', '', '0', '');

-- search objects
INSERT INTO `sys_objects_search` VALUES(NULL, @sPluginName, '_aqb_blogs', 'AqbBlogsSearchResult', 'modules/aqb/blogs/classes/AqbBlogsSearchResult.php');

-- vote objects
INSERT INTO `sys_objects_vote` VALUES (NULL, @sPluginName, '[db_prefix]rating', '[db_prefix]rating_track', 'gal_', '5', 'vote_send_result', 'BX_PERIOD_PER_VOTE', '1', '', '', '[db_prefix]main', 'rate', 'rate_count', 'id', 'AqbBlogsVoting', 'modules/aqb/blogs/classes/AqbBlogsVoting.php');

-- comments objects
INSERT INTO `sys_objects_cmts` VALUES (NULL, @sPluginName, '[db_prefix]cmts', '[db_prefix]cmts_track', '0', '1', '90', '5', '1', '-3', 'none', '0', '1', '0', 'cmt', '[db_prefix]main', 'id', 'comments_count', 'AqbBlogsCmts', 'modules/aqb/blogs/classes/AqbBlogsCmts.php');

-- views objects
INSERT INTO `sys_objects_views` VALUES(NULL, @sPluginName, '[db_prefix]views_track', 86400, '[db_prefix]main', 'id', 'views', 1);

-- tag objects
INSERT INTO `sys_objects_tag` VALUES (NULL, @sPluginName, 'SELECT `tags` FROM `[db_prefix]main` WHERE `id` = {iID} AND `status` = ''approved''', CONCAT(@sPluginName, '_permalinks'), CONCAT('m/', @sPluginName, '/browse/tag/{tag}'), CONCAT('modules/?r=', @sPluginName, '/browse/tag/{tag}'), '_aqb_blogs');

-- category objects
INSERT INTO `sys_objects_categories` VALUES (NULL, @sPluginName, 'SELECT `Categories` FROM `[db_prefix]main` WHERE `id` = {iID} AND `status` = ''approved''', CONCAT(@sPluginName, '_permalinks'), CONCAT('m/', @sPluginName, '/browse/category/{tag}'), CONCAT('modules/?r=', @sPluginName, '/browse/category/{tag}'), '_aqb_blogs');

INSERT INTO `sys_categories` (`Category`, `ID`, `Type`, `Owner`, `Status`) VALUES 
('Blogs Premium', '0', 'bx_photos', '0', 'active'),
('Arts & Literature', '0', @sPluginName, '0', 'active'),
('Animals & Pets', '0', @sPluginName, '0', 'active'),
('Activities', '0', @sPluginName, '0', 'active'),
('Automotive', '0', @sPluginName, '0', 'active'),
('Business & Money', '0', @sPluginName, '0', 'active'),
('Companies & Co-workers', '0', @sPluginName, '0', 'active'),
('Cultures & Nations', '0', @sPluginName, '0', 'active'),
('Dolphin Community', '0', @sPluginName, '0', 'active'),
('Family & Friends', '0', @sPluginName, '0', 'active'),
('Fan Clubs', '0', @sPluginName, '0', 'active'),
('Fashion & Style', '0', @sPluginName, '0', 'active'),
('Fitness & Body Building', '0', @sPluginName, '0', 'active'),
('Food & Drink', '0', @sPluginName, '0', 'active'),
('Gay, Lesbian & Bi', '0', @sPluginName, '0', 'active'),
('Health & Wellness', '0', @sPluginName, '0', 'active'),
('Hobbies & Entertainment', '0', @sPluginName, '0', 'active'),
('Internet & Computers', '0', @sPluginName, '0', 'active'),
('Love & Relationships', '0', @sPluginName, '0', 'active'),
('Mass Media', '0', @sPluginName, '0', 'active'),
('Music & Cinema', '0', @sPluginName, '0', 'active'),
('Places & Travel', '0', @sPluginName, '0', 'active'),
('Politics', '0', @sPluginName, '0', 'active'),
('Recreation & Sports', '0', @sPluginName, '0', 'active'),
('Religion', '0', @sPluginName, '0', 'active'),
('Science & Innovations', '0', @sPluginName, '0', 'active'),
('Sex', '0', @sPluginName, '0', 'active'),
('Teens & Schools', '0', @sPluginName, '0', 'active'),
('Other', '0', @sPluginName, '0', 'active');

-- users actions
INSERT INTO `sys_objects_actions` (`Caption`, `Icon`, `Url`, `Script`, `Eval`, `Order`, `Type`) VALUES 
('{TitleEdit}', 'edit', '{evalResult}', '', '$oConfig = $GLOBALS[''oAqbBlogsModule'']->_oConfig; return  BX_DOL_URL_ROOT . $oConfig->getBaseUri() . ''edit/{ID}'';', '0', @sPluginName),
('{TitleDelete}', 'remove', '', 'getHtmlData( ''ajaxy_popup_result_div_{ID}'', ''{evalResult}'', false, ''post'', true); return false;', '$oConfig = $GLOBALS[''oAqbBlogsModule'']->_oConfig; return  BX_DOL_URL_ROOT . $oConfig->getBaseUri() . ''delete/{ID}'';', 1, @sPluginName),
('{TitleShare}', 'share', '', 'showPopupAnyHtml (''{BaseUri}share_popup/{ID}'');', '', '2', @sPluginName),
('{TitleJoin}', '{IconJoin}', '', 'getHtmlData( ''ajaxy_popup_result_div_{ID}'', ''{evalResult}'', false, ''post'');return false;', '$oConfig = $GLOBALS[''oAqbBlogsModule'']->_oConfig; return BX_DOL_URL_ROOT . $oConfig->getBaseUri() . ''join/{ID}/{iViewer}'';', '3', @sPluginName),
('{TitleInvite}', 'plus-sign', '{evalResult}', '', '$oConfig = $GLOBALS[''oAqbBlogsModule'']->_oConfig; return BX_DOL_URL_ROOT . $oConfig->getBaseUri() . ''invite/{ID}'';', '4', @sPluginName),
('{AddToFeatured}', 'star-empty', '', 'getHtmlData( ''ajaxy_popup_result_div_{ID}'', ''{evalResult}'', false, ''post'');return false;', '$oConfig = $GLOBALS[''oAqbBlogsModule'']->_oConfig; return BX_DOL_URL_ROOT . $oConfig->getBaseUri() . ''mark_featured/{ID}'';', '5', @sPluginName),
('{TitleUploadPhotos}', 'picture', '{BaseUri}upload_photos/{URI}', '', '', '6', @sPluginName),
('{TitleUploadVideos}', 'film', '{BaseUri}upload_videos/{URI}', '', '', '7', @sPluginName),
('{TitleUploadSounds}', 'music', '{BaseUri}upload_sounds/{URI}', '', '', '8', @sPluginName),
('{TitleUploadFiles}', 'save', '{BaseUri}upload_files/{URI}', '', '', '9', @sPluginName),
('{TitleSubscribe}', 'paper-clip', '', '{ScriptSubscribe}', '', '10', @sPluginName),
('{evalResult}', 'plus', '{BaseUri}browse/my&aqb_blogs_filter=add_group', '', 'return ($GLOBALS[''logged''][''member''] && BxDolModule::getInstance(''AqbBlogsModule'')->isAllowedAdd()) || $GLOBALS[''logged''][''admin''] ? _t(''_aqb_blogs_action_add_group'') : '''';', 1, CONCAT(@sPluginName, '_title')),
('{evalResult}', 'book', '{BaseUri}browse/my', '', 'return $GLOBALS[''logged''][''member''] || $GLOBALS[''logged''][''admin''] ? _t(''_aqb_blogs_action_my_groups'') : '''';', '2', CONCAT(@sPluginName, '_title'));
    
-- top menu 
INSERT INTO `sys_menu_top`(`ID`, `Parent`, `Name`, `Caption`, `Link`, `Order`, `Visible`, `Target`, `Onclick`, `Check`, `Editable`, `Deletable`, `Active`, `Type`, `Picture`, `Icon`, `BQuickLink`, `Statistics`) VALUES
(NULL, 0, 'Blogs Premium', '_aqb_blogs_menu_root', CONCAT('modules/?r=', @sPluginName, '/view/|modules/?r=', @sPluginName, '/edit/'), '', 'non,memb', '', '', '', 1, 1, 1, 'system', 'modules/aqb/blogs/|top_menu_picture.png', '', '0', '');
SET @iCatRoot := LAST_INSERT_ID();

INSERT INTO `sys_menu_top`(`ID`, `Parent`, `Name`, `Caption`, `Link`, `Order`, `Visible`, `Target`, `Onclick`, `Check`, `Editable`, `Deletable`, `Active`, `Type`, `Picture`, `Icon`, `BQuickLink`, `Statistics`) VALUES
(NULL, @iCatRoot, 'Blog Post View', '_aqb_blogs_menu_view_group', CONCAT('modules/?r=', @sPluginName, '/view/{aqb_blogs_view_uri}'), 0, 'non,memb', '', '', '', 1, 1, 1, 'custom', '', '', 0, ''),
(NULL, @iCatRoot, 'Blog Post View Liked', '_aqb_blogs_menu_view_fans', CONCAT('modules/?r=', @sPluginName, '/browse_fans/{aqb_blogs_view_uri}'), 1, 'non,memb', '', '', '', 1, 1, 1, 'custom', '', '', 0, ''),
(NULL, @iCatRoot, 'Blog Post View Comments', '_aqb_blogs_menu_view_comments', CONCAT('modules/?r=', @sPluginName, '/comments/{aqb_blogs_view_uri}'), 2, 'non,memb', '', '', '', 1, 1, 1, 'custom', '', '', 0, '');


SET @iMaxMenuOrder := (SELECT `Order` + 1 FROM `sys_menu_top` WHERE `Parent` = 0 ORDER BY `Order` DESC LIMIT 1);
INSERT INTO `sys_menu_top`(`ID`, `Parent`, `Name`, `Caption`, `Link`, `Order`, `Visible`, `Target`, `Onclick`, `Check`, `Editable`, `Deletable`, `Active`, `Type`, `Picture`, `Icon`, `BQuickLink`, `Statistics`) VALUES
(NULL, 0, 'Blogs Premium', '_aqb_blogs_menu_root', CONCAT('modules/?r=', @sPluginName, '/home/|modules/?r=', @sPluginName, '/'), @iMaxMenuOrder, 'non,memb', '', '', '', 1, 1, 1, 'top', 'modules/aqb/blogs/|top_menu_picture.png', 'modules/aqb/blogs/|top_menu_icon.png', 1, '');
SET @iCatRoot := LAST_INSERT_ID();

INSERT INTO `sys_menu_top`(`ID`, `Parent`, `Name`, `Caption`, `Link`, `Order`, `Visible`, `Target`, `Onclick`, `Check`, `Editable`, `Deletable`, `Active`, `Type`, `Picture`, `Icon`, `BQuickLink`, `Statistics`) VALUES
(NULL, @iCatRoot, 'Blogs Main Page', '_aqb_blogs_menu_main', CONCAT('modules/?r=', @sPluginName, '/home/'), 0, 'non,memb', '', '', '', 1, 1, 1, 'custom', '', '', 0, ''),
(NULL, @iCatRoot, 'Recent Posts', '_aqb_blogs_menu_recent', CONCAT('modules/?r=', @sPluginName, '/browse/recent'), 2, 'non,memb', '', '', '', 1, 1, 1, 'custom', '', '', 0, ''),
(NULL, @iCatRoot, 'Top Rated Posts', '_aqb_blogs_menu_top_rated', CONCAT('modules/?r=', @sPluginName, '/browse/top'), 3, 'non,memb', '', '', '', 1, 1, 1, 'custom', '', '', 0, ''),
(NULL, @iCatRoot, 'Popular Posts', '_aqb_blogs_menu_popular', CONCAT('modules/?r=', @sPluginName, '/browse/popular'), 4, 'non,memb', '', '', '', 1, 1, 1, 'custom', '', '', 0, ''),
(NULL, @iCatRoot, 'Featured Posts', '_aqb_blogs_menu_featured', CONCAT('modules/?r=', @sPluginName, '/browse/featured'), 5, 'non,memb', '', '', '', 1, 1, 1, 'custom', '', '', 0, ''),
(NULL, @iCatRoot, 'Blogs Tags', '_aqb_blogs_menu_tags', CONCAT('modules/?r=', @sPluginName, '/tags'), 8, 'non,memb', '', '', '', 1, 1, 1, 'custom', '', '', 0, @sPluginName),
(NULL, @iCatRoot, 'Blogs Categories', '_aqb_blogs_menu_categories', CONCAT('modules/?r=', @sPluginName, '/categories'), 9, 'non,memb', '', '', '', 1, 1, 1, 'custom', '', '', 0, @sPluginName),
(NULL, @iCatRoot, 'Calendar', '_aqb_blogs_menu_calendar', CONCAT('modules/?r=', @sPluginName, '/calendar'), 10, 'non,memb', '', '', '', 1, 1, 1, 'custom', '', '', 0, ''),
(NULL, @iCatRoot, 'Search', '_aqb_blogs_menu_search', CONCAT('modules/?r=', @sPluginName, '/search'), 11, 'non,memb', '', '', '', 1, 1, 1, 'custom', '', '', 0, '');

SET @iCatProfileOrder := (SELECT MAX(`Order`)+1 FROM `sys_menu_top` WHERE `Parent` = 9 ORDER BY `Order` DESC LIMIT 1);
INSERT INTO `sys_menu_top`(`ID`, `Parent`, `Name`, `Caption`, `Link`, `Order`, `Visible`, `Target`, `Onclick`, `Check`, `Editable`, `Deletable`, `Active`, `Type`, `Picture`, `Icon`, `BQuickLink`, `Statistics`) VALUES
(NULL, 9, 'Blogs Premium', '_aqb_blogs_menu_my_blogs_profile', CONCAT('modules/?r=', @sPluginName, '/browse/user/{profileUsername}|modules/?r=', @sPluginName, '/browse/joined/{profileUsername}'), @iCatProfileOrder, 'non,memb', '', '', '', 1, 1, 1, 'custom', '', '', 0, '');

SET @iCatProfileOrder := (SELECT MAX(`Order`)+1 FROM `sys_menu_top` WHERE `Parent` = 4 ORDER BY `Order` DESC LIMIT 1);
INSERT INTO `sys_menu_top`(`ID`, `Parent`, `Name`, `Caption`, `Link`, `Order`, `Visible`, `Target`, `Onclick`, `Check`, `Editable`, `Deletable`, `Active`, `Type`, `Picture`, `Icon`, `BQuickLink`, `Statistics`) VALUES
(NULL, 4, 'Blogs Premium', '_aqb_blogs_menu_my_blogs_profile', CONCAT('modules/?r=', @sPluginName, '/browse/my'), @iCatProfileOrder, 'memb', '', '', '', 1, 1, 1, 'custom', '', '', 0, '');

-- member menu
SET @iMemberMenuParent = (SELECT `ID` FROM `sys_menu_member` WHERE `Name` = 'AddContent');
SET @iMemberMenuOrder = (SELECT MAX(`Order`) + 1 FROM `sys_menu_member` WHERE `Parent` = IFNULL(@iMemberMenuParent, -1));
INSERT INTO `sys_menu_member` SET `Name` = @sPluginName, `Eval` = CONCAT('return BxDolService::call(''', @sPluginName, ''', ''get_member_menu_item_add_content'');'), `Type` = 'linked_item', `Parent` = IFNULL(@iMemberMenuParent, 0), `Order` = IFNULL(@iMemberMenuOrder, 1);

-- admin menu
SET @iMax = (SELECT MAX(`order`) FROM `sys_menu_admin` WHERE `parent_id` = '2');
INSERT IGNORE INTO `sys_menu_admin` (`parent_id`, `name`, `title`, `url`, `description`, `icon`, `order`) VALUES
(2, @sPluginName, '_aqb_blogs', CONCAT('{siteUrl}modules/?r=', @sPluginName, '/administration/'), 'Blogs Premium module by AQB Soft', 'modules/aqb/blogs/|admin_menu_icon.png', @iMax+1);

-- site stats
SET @iStatSiteOrder := (SELECT `StatOrder` + 1 FROM `sys_stat_site` WHERE 1 ORDER BY `StatOrder` DESC LIMIT 1);
INSERT INTO `sys_stat_site` VALUES(NULL, @sPluginName, 'aqb_blogs', CONCAT('modules/?r=', @sPluginName, '/browse/recent'), 'SELECT COUNT(`id`) FROM `[db_prefix]main` WHERE `status`=''approved''', CONCAT('modules/?r=', @sPluginName, '/administration'), 'SELECT COUNT(`id`) FROM `[db_prefix]main` WHERE `status`=''pending''', 'book', @iStatSiteOrder);

-- PQ statistics
INSERT INTO `sys_stat_member` VALUES (@sPluginName, 'SELECT COUNT(*) FROM `[db_prefix]main` WHERE `author_id` = ''__member_id__'' AND `status`=''approved''');
INSERT INTO `sys_stat_member` VALUES (CONCAT(@sPluginName, 'p'), 'SELECT COUNT(*) FROM `[db_prefix]main` WHERE `author_id` = ''__member_id__'' AND `Status`!=''approved''');

-- email templates
INSERT INTO `sys_email_templates` (`Name`, `Subject`, `Body`, `Desc`, `LangID`) VALUES 
(CONCAT(@sPluginName, '_fan_become_admin'), 'You Are An Blog Post Admin Now', '<bx_include_auto:_email_header.html />\r\n\r\n<p>Hello <NickName>,</p> \r\n\r\n<p>You are an admin of a <a href="<EntryUrl>"><EntryTitle></a> ablog postnow.</p>\r\n\r\n<bx_include_auto:_email_footer.html />', 'Blog post admin status granted', 0),
(CONCAT(@sPluginName, '_admin_become_fan'), 'Your Blog Post Admin Status Was Revoked', '<bx_include_auto:_email_header.html />\r\n\r\n<p>Hello <NickName>,</p> \r\n\r\n<p>Your admin status was revoked from a <a href="<EntryUrl>"><EntryTitle></a> blog post by its creator.</p> \r\n\r\n<bx_include_auto:_email_footer.html />', 'Blog post admin status revoked', 0),
(CONCAT(@sPluginName, '_invitation'), 'Invitation to read <EntryName> blog post', '<bx_include_auto:_email_header.html />\r\n\r\n<p>Hello <NickName>,</p> \r\n\r\n<p><a href="<InviterUrl>"><InviterNickName></a> invited you to read the <a href="<EntryUrl>"><EntryName> blog post</a>.</p> \r\n\r\n<p>\r\n<hr><InvitationText><hr> \r\n</p>\r\n\r\n<bx_include_auto:_email_footer.html />', 'Invitation to read a blog post', 0),
(CONCAT(@sPluginName, '_sbs'), 'Subscription: Blog Post Details Changed', '<bx_include_auto:_email_header.html />\r\n\r\n<p>Hello <NickName>,</p> \r\n\r\n<p><a href="<ViewLink>"><EntryTitle></a> blog post details changed: <br /> <ActionName> </p> \r\n<hr>\r\n<p>Cancel this subscription: <a href="<UnsubscribeLink>"><UnsubscribeLink></a></p>\r\n\r\n<bx_include_auto:_email_footer.html />', 'Subscription: blog post changes', 0);


-- membership actions
SET @iLevelNonMember := 1;
SET @iLevelStandard := 2;
SET @iLevelPromotion := 3;

INSERT INTO `sys_acl_actions` VALUES (NULL, 'blogs premium view blogs', NULL);
SET @iAction := LAST_INSERT_ID();
INSERT INTO `sys_acl_matrix` (`IDLevel`, `IDAction`) VALUES 
    (@iLevelNonMember, @iAction), (@iLevelStandard, @iAction), (@iLevelPromotion, @iAction);

INSERT INTO `sys_acl_actions` VALUES (NULL, 'blogs premium browse', NULL);
SET @iAction := LAST_INSERT_ID();
INSERT INTO `sys_acl_matrix` (`IDLevel`, `IDAction`) VALUES 
    (@iLevelNonMember, @iAction), (@iLevelStandard, @iAction), (@iLevelPromotion, @iAction);

INSERT INTO `sys_acl_actions` VALUES (NULL, 'blogs premium search', NULL);
SET @iAction := LAST_INSERT_ID();
INSERT INTO `sys_acl_matrix` (`IDLevel`, `IDAction`) VALUES 
    (@iLevelNonMember, @iAction), (@iLevelStandard, @iAction), (@iLevelPromotion, @iAction);

INSERT INTO `sys_acl_actions` VALUES (NULL, 'blogs premium add blogs', NULL);
SET @iAction := LAST_INSERT_ID();
INSERT INTO `sys_acl_matrix` (`IDLevel`, `IDAction`) VALUES 
    (@iLevelStandard, @iAction), (@iLevelPromotion, @iAction);

INSERT INTO `sys_acl_actions` VALUES (NULL, 'blogs premium comments delete and edit', NULL);
INSERT INTO `sys_acl_actions` VALUES (NULL, 'blogs premium edit any blogs', NULL);
INSERT INTO `sys_acl_actions` VALUES (NULL, 'blogs premium delete any blogs', NULL);
INSERT INTO `sys_acl_actions` VALUES (NULL, 'blogs premium mark as featured', NULL);
INSERT INTO `sys_acl_actions` VALUES (NULL, 'blogs premium approve blogs', NULL);

-- alert handlers
INSERT INTO `sys_alerts_handlers` VALUES (NULL, CONCAT(@sPluginName, '_profile_delete'), '', '', CONCAT('BxDolService::call(''', @sPluginName, ''', ''response_profile_delete'', array($this));'));
SET @iHandler := LAST_INSERT_ID();
INSERT INTO `sys_alerts` VALUES (NULL , 'profile', 'delete', @iHandler);

INSERT INTO `sys_alerts_handlers` VALUES (NULL, CONCAT(@sPluginName, '_media_delete'), '', '', CONCAT('BxDolService::call(''', @sPluginName, ''', ''response_media_delete'', array($this));'));
SET @iHandler := LAST_INSERT_ID();
INSERT INTO `sys_alerts` VALUES (NULL , 'bx_photos', 'delete', @iHandler);
INSERT INTO `sys_alerts` VALUES (NULL , 'bx_videos', 'delete', @iHandler);
INSERT INTO `sys_alerts` VALUES (NULL , 'bx_sounds', 'delete', @iHandler);
INSERT INTO `sys_alerts` VALUES (NULL , 'bx_files', 'delete', @iHandler);

-- privacy
INSERT INTO `sys_privacy_actions` (`module_uri`, `name`, `title`, `default_group`) VALUES
(@sPluginName, 'view', '_aqb_blogs_privacy_view_blogs', '3'),
(@sPluginName, 'view_liked', '_aqb_blogs_privacy_view_liked', '3'),
(@sPluginName, 'comment', '_aqb_blogs_privacy_comment', 'f'),
(@sPluginName, 'rate', '_aqb_blogs_privacy_rate', 'f'),
(@sPluginName, 'join', '_aqb_blogs_privacy_join', '3'),
(@sPluginName, 'upload_photos', '_aqb_blogs_privacy_upload_photos', 'a'),
(@sPluginName, 'upload_videos', '_aqb_blogs_privacy_upload_videos', 'a'),
(@sPluginName, 'upload_sounds', '_aqb_blogs_privacy_upload_sounds', 'a'),
(@sPluginName, 'upload_files', '_aqb_blogs_privacy_upload_files', 'a');

-- subscriptions
INSERT INTO `sys_sbs_types` (`unit`, `action`, `template`, `params`) VALUES
(@sPluginName, '', '', CONCAT('return BxDolService::call(''', @sPluginName, ''', ''get_subscription_params'', array($arg2, $arg3));')),
(@sPluginName, 'change', CONCAT(@sPluginName, '_sbs'), CONCAT('return BxDolService::call(''', @sPluginName, ''', ''get_subscription_params'', array($arg2, $arg3));')),
(@sPluginName, 'commentPost', CONCAT(@sPluginName, '_sbs'), CONCAT('return BxDolService::call(''', @sPluginName, ''', ''get_subscription_params'', array($arg2, $arg3));')),
(@sPluginName, 'join', CONCAT(@sPluginName, '_sbs'), CONCAT('return BxDolService::call(''', @sPluginName, ''', ''get_subscription_params'', array($arg2, $arg3));'));

-- sitemap
SET @iMaxOrderSiteMaps = (SELECT MAX(`order`)+1 FROM `sys_objects_site_maps`);
INSERT INTO `sys_objects_site_maps` (`object`, `title`, `priority`, `changefreq`, `class_name`, `class_file`, `order`, `active`) VALUES
(@sPluginName, '_aqb_blogs', '0.8', 'auto', 'AqbBlogsSiteMaps', 'modules/aqb/blogs/classes/AqbBlogsSiteMaps.php', @iMaxOrderSiteMaps, 1);

-- chart
SET @iMaxOrderCharts = (SELECT MAX(`order`)+1 FROM `sys_objects_charts`);
INSERT INTO `sys_objects_charts` (`object`, `title`, `table`, `field_date_ts`, `field_date_dt`, `query`, `active`, `order`) VALUES
(@sPluginName, '_aqb_blogs', '[db_prefix]main', 'created', '', '', 1, @iMaxOrderCharts);

