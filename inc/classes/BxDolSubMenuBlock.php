<?php

require_once( BX_DIRECTORY_PATH_CLASSES . 'BxDolDb.php' );

class BxDolSubMenuBlock
{
	var $parentId;
	var $oDb;

	function BxDolSubMenuBlock($parentId) {
		$this->parentId = $parentId;
		$this->oDb = new BxDolDb();
	}

	function genSubMenuBlock($uri, $type) {

		$items = $this->getItems();

		if (!$items) {
			return false;
		}

		$size = sizeof($items);
		$buttons = array();
		$sCode = '';
		$keepers = array("Recent Groups", "Popular Groups", "Local Groups", "Groups Tags", "Groups Categories", "Search");

		for ($i = 0; $i < $size; $i++) {
			$name = _t($items[$i]['Caption']);
			$link = $items[$i]['Link'];
			$groupsName = $items[$i]['Name'];
			//$order = $items[$i]['Order'];
			$doThis = true;

				if ($type == "groups") {
					if (!in_array($groupsName, $keepers)) {
						//echo "<script type='text/javascript'>alert('$groupsName');</script>";
						$doThis = false;
					} else if ($groupsName == "Search") { 
						$name = "Advanced Search";
					}
				}

				if ($type == "groupView") {
					$goodLink = str_replace("{bx_groups_view_uri}", $uri, $link);
					if ($groupsName == "Group View Forum") {
						$goodLink = substr($goodLink, 0, -14);
					}
				} else if ($type == "eventView") {
					$goodLink = str_replace("{bx_events_view_uri}", $uri, $link);
				} else {
					$goodLink = $link;
				}

				$aKeys = array(
					'item_link' => $goodLink,
					'item_name' => $name,
					//'bx_events_view_uri' => $uri,
					);

				if ($doThis) {
					$sCode .= $GLOBALS['oSysTemplate']->parseHtmlByName("submenubuttons.html", $aKeys);
				}

		}

		if ($sCode == '') {
			//$message = "hello";
        	//echo "<script type='text/javascript'>alert('$message');</script>";
			return false;
		} else {
			//$message = "yo";
        	//echo "<script type='text/javascript'>alert('$message');</script>";
		}

		return $GLOBALS['oSysTemplate']->parseHtmlByName('default_padding.html', array('content' => $sCode));

	}

	function getItems() {

		$query = "SELECT * FROM `sys_menu_top` WHERE `Parent` = '$this->parentId'";
		$items = $this->oDb->getAll($query);

		return $items;
	}


}