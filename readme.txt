This is Nick's implementation of Boonex dolphin (Sandbox). JP requested we have a repository for documentation purposes.

-Many aspects of the program design are driven by the database. The Sandbox is connected to the "cxops" database on our 1and1 server.

-Most alterations are prefaced by a "//Nick" comment.

///////////////////////////////////////////////////////////////////

Thank you for choosing Dolphin Smart Community Builder from BoonEx.

This is a Dolphin 7.2.0
Released on 01/09/2015

Check http://www.boonex.com for version updates.

To install Dolphin, please refer to Install.txt. 

Latest installation manual and docs can be found 
online at http://www.boonex.com/trac/dolphin/wiki