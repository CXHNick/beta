SET @sPluginName = 'aqb_blogs';

-- tables
DROP TABLE IF EXISTS `[db_prefix]main`;
DROP TABLE IF EXISTS `[db_prefix]likes`;
DROP TABLE IF EXISTS `[db_prefix]admins`;
DROP TABLE IF EXISTS `[db_prefix]images`;
DROP TABLE IF EXISTS `[db_prefix]videos`;
DROP TABLE IF EXISTS `[db_prefix]sounds`;
DROP TABLE IF EXISTS `[db_prefix]files`;
DROP TABLE IF EXISTS `[db_prefix]rating`;
DROP TABLE IF EXISTS `[db_prefix]rating_track`;
DROP TABLE IF EXISTS `[db_prefix]cmts`;
DROP TABLE IF EXISTS `[db_prefix]cmts_track`;
DROP TABLE IF EXISTS `[db_prefix]views_track`;

-- compose pages
DELETE FROM `sys_page_compose_pages` WHERE `Name` IN(CONCAT(@sPluginName, '_view'), CONCAT(@sPluginName, '_celendar'), CONCAT(@sPluginName, '_main'), CONCAT(@sPluginName, '_my'));
DELETE FROM `sys_page_compose` WHERE `Page` IN(CONCAT(@sPluginName, '_view'), CONCAT(@sPluginName, '_celendar'), CONCAT(@sPluginName, '_main'), CONCAT(@sPluginName, '_my'));
DELETE FROM `sys_page_compose` WHERE `Page` = 'index' AND `Caption` = '_aqb_blogs_block_homepage';
DELETE FROM `sys_page_compose` WHERE `Page` = 'member' AND `Caption` = '_aqb_blogs_block_my_blogs_joined';
DELETE FROM `sys_page_compose` WHERE `Page` = 'profile' AND `Caption` = '_aqb_blogs_block_my_blogs';
DELETE FROM `sys_page_compose` WHERE `Page` = 'profile' AND `Caption` = '_aqb_blogs_block_my_blogs_joined';

-- system objects
DELETE FROM `sys_permalinks` WHERE `check` = 'aqb_blogs_permalinks';
DELETE FROM `sys_objects_vote` WHERE `ObjectName` = @sPluginName;
DELETE FROM `sys_objects_cmts` WHERE `ObjectName` = @sPluginName;
DELETE FROM `sys_objects_views` WHERE `name` = @sPluginName;
DELETE FROM `sys_objects_categories` WHERE `ObjectName` = @sPluginName;
DELETE FROM `sys_categories` WHERE `Type` = @sPluginName;
DELETE FROM `sys_categories` WHERE `Type` = 'bx_photos' AND `Category` = 'Blogs Premium';
DELETE FROM `sys_objects_tag` WHERE `ObjectName` = @sPluginName;
DELETE FROM `sys_tags` WHERE `Type` = @sPluginName;
DELETE FROM `sys_objects_search` WHERE `ObjectName` = @sPluginName;
DELETE FROM `sys_objects_actions` WHERE `Type` IN (@sPluginName, CONCAT(@sPluginName, '_title'));
DELETE FROM `sys_stat_site` WHERE `Name` = @sPluginName;
DELETE FROM `sys_stat_member` WHERE `Type` IN (@sPluginName, CONCAT(@sPluginName, 'p'));

-- email templates
DELETE FROM `sys_email_templates` WHERE `Name` IN (CONCAT(@sPluginName, '_fan_become_admin'), CONCAT(@sPluginName, '_admin_become_fan'), CONCAT(@sPluginName, '_invitation'), CONCAT(@sPluginName, '_sbs'));

-- top menu
SET @iCatRoot := (SELECT `ID` FROM `sys_menu_top` WHERE `Caption` = '_aqb_blogs_menu_root' AND `Parent` = 0 LIMIT 1);
DELETE FROM `sys_menu_top` WHERE `Parent` = @iCatRoot;
DELETE FROM `sys_menu_top` WHERE `ID` = @iCatRoot;

SET @iCatRoot := (SELECT `ID` FROM `sys_menu_top` WHERE `Caption` = '_aqb_blogs_menu_root' AND `Parent` = 0 LIMIT 1);
DELETE FROM `sys_menu_top` WHERE `Parent` = @iCatRoot;
DELETE FROM `sys_menu_top` WHERE `ID` = @iCatRoot;

DELETE FROM `sys_menu_top` WHERE `Parent` = 9 AND `Caption` = '_aqb_blogs_menu_my_blogs_profile';
DELETE FROM `sys_menu_top` WHERE `Parent` = 4 AND `Caption` = '_aqb_blogs_menu_my_blogs_profile';

-- member menu
DELETE FROM `sys_menu_member` WHERE `Name` = @sPluginName;

-- admin menu
DELETE FROM `sys_menu_admin` WHERE `name` = @sPluginName;

-- settings
SET @iCategId = (SELECT `ID` FROM `sys_options_cats` WHERE `name` = @sPluginName LIMIT 1);
DELETE FROM `sys_options` WHERE `kateg` = @iCategId;
DELETE FROM `sys_options_cats` WHERE `ID` = @iCategId;
DELETE FROM `sys_options` WHERE `Name` = CONCAT(@sPluginName, '_permalinks');

-- membership levels
DELETE `sys_acl_actions`, `sys_acl_matrix` FROM `sys_acl_actions`, `sys_acl_matrix` WHERE `sys_acl_matrix`.`IDAction` = `sys_acl_actions`.`ID` AND `sys_acl_actions`.`Name` IN('blogs premium view blogs', 'blogs premium browse', 'blogs premium search', 'blogs premium add blogs', 'blogs premium comments delete and edit', 'blogs premium edit any blogs', 'blogs premium delete any blogs', 'blogs premium mark as featured', 'blogs premium approve blogs');
DELETE FROM `sys_acl_actions` WHERE `Name` IN('blogs premium view blogs', 'blogs premium browse', 'blogs premium search', 'blogs premium add blogs', 'blogs premium comments delete and edit', 'blogs premium edit any blogs', 'blogs premium delete any blogs', 'blogs premium mark as featured', 'blogs premium approve blogs');

-- alerts
SET @iHandler := (SELECT `id` FROM `sys_alerts_handlers` WHERE `name` = CONCAT(@sPluginName, '_profile_delete') LIMIT 1);
DELETE FROM `sys_alerts` WHERE `handler_id` = @iHandler;
DELETE FROM `sys_alerts_handlers` WHERE `id` = @iHandler;

SET @iHandler := (SELECT `id` FROM `sys_alerts_handlers` WHERE `name` = CONCAT(@sPluginName, '_media_delete') LIMIT 1);
DELETE FROM `sys_alerts` WHERE `handler_id` = @iHandler;
DELETE FROM `sys_alerts_handlers` WHERE `id` = @iHandler;

-- privacy
DELETE FROM `sys_privacy_actions` WHERE `module_uri` = @sPluginName;

-- subscriptions
DELETE FROM `sys_sbs_entries` USING `sys_sbs_types`, `sys_sbs_entries` WHERE `sys_sbs_types`.`id`=`sys_sbs_entries`.`subscription_id` AND `sys_sbs_types`.`unit`=@sPluginName;
DELETE FROM `sys_sbs_types` WHERE `unit`=@sPluginName;

-- sitemap
DELETE FROM `sys_objects_site_maps` WHERE `object` = @sPluginName;

-- chart
DELETE FROM `sys_objects_charts` WHERE `object` = @sPluginName;
