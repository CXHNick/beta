<?php
/***************************************************************************
* Date              : Sun January 26, 2014
* Website           : http://www.dialme.com
*
* Product Name          : Add A New Member Module
* Product Version       : 1.0.0
* Product Use           : For Boonex Dolphin 7.0.x & 7.1.x
*
***************************************************************************/

bx_import('BxDolProfileFields');

class BxDialMeAddMemberModule extends BxDolModule {

    public function __construct(&$aModule) {        
        parent::__construct($aModule);
    }

public function actionAdministration() {
        if (!$GLOBALS['logged']['admin']) {
            $this->_oTemplate->displayAccessDenied();
            return;  
        }

        $this->_oTemplate->pageStart();

        if(isset($_POST['save'])) {
            
            $error = false;
            
            if(!isset($_POST['NickName'])){
                echo MsgBox(_t('_FieldError_NickName_Mandatory'));
                $error = true;
            }
            if(!$this->_oDb->isUnique('NickName', $_POST['NickName'])){
                echo MsgBox(_t('_FieldError_NickName_Unique'));
                $error = true;
            }
            if(!isset($_POST['Email'])){
                echo MsgBox(_t('_FieldError_Email_Mandatory'));
                $error = true;
            }
            if(!$this->_oDb->isUnique('Email', $_POST['Email'])){
                echo MsgBox(_t('_FieldError_Email_Unique'));
                $error = true;
            }
            if(!isset($_POST['Password'])){
                echo MsgBox(_t('_FieldError_Password_Mandatory'));
                $error = true;
            }
            
            if(!$error){
                $this->_oDb->insertProfile($_POST);
                echo MsgBox(_t('_dialme_add_member_profile_created'));
                $_POST = array();
            }
        }

        $oProfileFields = new BxDolProfileFields(0);
        $countries = $oProfileFields->convertValues4Input('#!Country');
        asort($countries);

        $aForm = array(
            'form_attrs' => array(
                'id' => 'adm-settings-form',
                'name' => 'adm-settings-form',
                'action' => '',
                'method' => 'post',
                'enctype' => 'multipart/form-data'
            ),
            'params' => array(
                'db' => array(
                    'table' => 'Profiles',
                    'key' => 'Name',
                    'uri' => '',
                    'uri_title' => '',
                    'submit_name' => 'save'
                ),
            ),
            'inputs' => array(
                'nickname' => array(
                    'type' => 'text',
                    'name' => 'NickName',
                    'value' => isset($_POST['nickname']) ? $_POST['nickname'] : '',
                    'caption' => _t('_NickName'),
                    'required' => true,
                    'checker' => array (
                        'func' => 'length',
                        'params' => array(3, 100),
                        'error' => _t('_categ_form_field_name_err'),
                    ),
                    'db' => array(
                        'pass' => 'Xss'
                    ),
                    'display' => true,
                ),
                'password' => array(
                    'type' => 'text',
                    'name' => 'Password',
                    'value' => isset($_POST['password']) ? $_POST['password'] : '',
                    'caption' => _t('_FieldCaption_Password_Join'),
                    'required' => true,
                    'checker' => array (
                        'func' => 'length',
                        'params' => array(3, 100),
                        'error' => _t('_FieldError_Password_Min', 3),
                    ),
                    'db' => array(
                        'pass' => 'Xss'
                    ),
                    'display' => true,
                ),
                'fullname' => array(
                    'type' => 'text',
                    'name' => 'FullName',
                    'value' => isset($_POST['fullname']) ? $_POST['fullname'] : '',
                    'caption' => "Full Name",
                    'required' => true,
                    'checker' => array (),
                    'db' => array(
                        'pass' => 'Xss'
                    ),
                    'display' => true,
                ),
                /*'lastname' => array(
                    'type' => 'text',
                    'name' => 'LastName',
                    'value' => isset($_POST['lastname']) ? $_POST['lastname'] : '',
                    'caption' => _t('_FieldCaption_LastName_Edit'),
                    'required' => false,
                    'checker' => array (),
                    'db' => array(
                        'pass' => 'Xss'
                    ),
                    'display' => true,
                ),*/
                'email' => array(
                    'type' => 'text',
                    'name' => 'Email',
                    'value' => isset($_POST['email']) ? $_POST['email'] : '',
                    'caption' => _t('_E-mail'),
                    'required' => true,
                    'checker' => array (
                        'func' => 'length',
                        'params' => array(3, 100),
                        'error' => _t('_categ_form_field_name_err'),
                    ),
                    'db' => array(
                        'pass' => 'Xss'
                    ),
                    'display' => true,
                ),
                'zip' => array(
                    'type' => 'text',
                    'name' => 'zip',
                    'value' => isset($_POST['zip']) ? $_POST['zip'] : '',
                    'caption' => _t('_FieldCaption_zip_Edit'),
                    'required' => false,
                    'checker' => array (),
                    'db' => array(
                        'pass' => 'Xss'
                    ),
                    'display' => true,
                ),
                'city' => array(
                    'type' => 'text',
                    'name' => 'City',
                    'value' => isset($_POST['city']) ? $_POST['city'] : '',
                    'caption' => _t('_City'),
                    'required' => false,
                    'checker' => array (),
                    'db' => array(
                        'pass' => 'Xss'
                    ),
                    'display' => true,
                ),
                'country' => array(
                    'type' => 'select',
                    'name' => 'Country',
                    'values' => $countries,
                    'caption' => _t('_Country'),
                    'required' => false,
                    'checker' => array (),
                    'db' => array(
                        'pass' => 'Xss'
                    ),
                    'display' => true,
                ),
                'Sex' => array(
                    'type' => 'custom',
                    'name' => 'Sex',
                    'caption' => 'Sex',
            //'content' => '&nbsp;&nbsp;<div style="margin:0 auto;"><input style="border:0px; width:10%; height:11px;margin-bottom:6px;" type="radio" class="big" name="Sex" value="male" checked /> <span style="float:center;height:11px;">Man</span>&nbsp;&nbsp;&nbsp;&nbsp;<input style="border:0px; width:10%; height:11px;margin-bottom:6px;" type="radio" name="Sex" value="female" /> Woman</div>',
            //'content' => '&nbsp;&nbsp;<span style="margin:0 auto; text-align:center;"><input style="border:0px; width:10%; margin-bottom:6px;" type="radio" name="Sex" value="male" checked /> Man&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input style="border:0px; width:10%; margin-bottom:6px;" type="radio" name="Sex" value="female" /> Woman</span>',
            'content' => '<div><div style="display: inline-block; width:80px; margin-left:5px;"><input style="margin-bottom:6px;" type="radio" name="Sex" value="male" checked /> Man</div><div style="display: inline-block; width:80px;"><input style="margin-bottom:6px;" type="radio" name="Sex" value="female" /> Woman</div></div>',
                    'value' => 'Male',
                    'required' => false,
                    'db' => array (
                        'pass' => 'Xss'
                    ),
                    'display' => true,
                ),
                'EmailNotify' => array(
                    'type' => 'custom',
                    'name' => 'EmailNotify',
                    'caption' => 'Receive site notifications',
            //'content' => '&nbsp;&nbsp;&nbsp;&nbsp;<div style="margin:0; padding:0; text-align:center;"><b>Email Notify</b>&nbsp;&nbsp;<input style="border:0px; width:10%; height:11px;margin-bottom:6px;" type="checkbox" checked="checked" name="EmailNotify" value="1" /></div>',
            //'content' => '<div STYLE="font-weight: bold;"> &nbsp;&nbsp;<input style="border:0px; width:10%; margin-top:2px;" type="checkbox" checked="checked" name="EmailNotify" value="1" />&nbsp;&nbsp;<span style="margin-top:-6px width:100px; text-align:center;"><b>Email Notify</b>&nbsp;&nbsp;&nbsp;</span></div>',
            'content' => '<div><div style="display: inline-block; width:24px; margin-left:5px;"><input style="margin-top:2px;" type="checkbox" checked="checked" name="EmailNotify" value="1" /></div><div style="display: inline-block; width:80px;"><b>Email Notify</b></div></div>',
                    'value' => '1',
                    'required' => 0,
                    'db' => array (
                        'pass' => 'Xss'
                    ),
                    'display' => true,
                ),
                'Role' => array(
                    'type' => 'custom',
                    'name' => 'Role',
                    'caption' => '<b>Member Level</b>',
            //'content' => '&nbsp;&nbsp;<input style="border:0px; width:10%; height:11px;margin-bottom:6px;" type="radio" name="Role" value="1" checked /> Standard<br />&nbsp;&nbsp;<input style="border:0px; width:10%; height:11px;margin-bottom:6px;" type="radio" name="Role" value="3" /> Administrator<br />',
            //'content' => '&nbsp;<span style="margin:0 auto; text-align:center;"><input style="border:0px; width:10%; margin-bottom:6px;" type="radio" name="Role" value="1" checked /></span> <span style="margin:0 auto; text-align:center;">Standard&nbsp;&nbsp;&nbsp;<input style="border:0px; width:10%; margin-bottom:6px;" type="radio" name="Role" value="3" /> Administrator</span>',
            'content' => '<div><div style="display: inline-block; width:80px; margin-left:5px;"><input style="margin-bottom:6px;" type="radio" name="Role" value="1" checked /> Standard</div><div style="display: inline-block; width:80px;"><input style="margin-bottom:6px;" type="radio" name="Role" value="3" /> Administrator</div></div>',
                    'value' => '1',
                    'required' => 0,
                    'db' => array (
                        'pass' => 'Xss'
                    ),
                    'display' => true,
                ),
                'save' => array(
                    'type' => 'submit',
                    'name' => 'save',
                    'value' => _t('_adm_btn_settings_save'),
                ),
        'DialMe' => array(
        'type' => 'custom',
        'content' => '<center><b>Add A New Member Module</b><br /> by: <a href="http://www.dialme.com" target="_blank"><b>DialMe.com</b></a><br /><br /><a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=L4ASN7J9H55P6" target="_blank"><img src="../modules/dialme/add_member/templates/base/images/donate.gif" border="0"></a></center><br />',
        ),
            )
        );
        
        $oForm = new BxTemplFormView($aForm);
        $oForm->initChecker();

        $sResult = $oForm->getCode();

        echo DesignBoxAdmin(_t('_dialme_add_member_pinfo'), $sResult);
        
        $this->_oTemplate->pageCodeAdmin(_t('_dialme_add_member'));
    }
}

?>