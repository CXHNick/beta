<?
/**
 * Copyright (c) BoonEx Pty Limited - http://www.boonex.com/
 * CC-BY License - http://creativecommons.org/licenses/by/3.0/
 */

bx_import('BxDolTwigTemplate');

class BxLibraryTemplate extends BxDolTwigTemplate
{
    var $oDb;

    function BxLibraryTemplate (&$oConfig, &$oDb)
    {
        $this->oDb = $oDb;

        parent::BxDolTwigTemplate($oConfig, $oDb);
    }

    function report_unit($iGroupId, $iProfileId) {

    	$oGroups = BxDolModule::getInstance('BxGroupsModule');
    	$groupUri = $this->oDb->getOne("SELECT `uri` FROM `bx_groups_main` WHERE `id` = '$iGroupId'");
    	$groupUrl = BX_DOL_URL_ROOT . 'm/groups/blog/browse/' . $groupUri;
    	//$oGroups->getGroupUrl($iGroupId);
    	$groupTitle = $this->oDb->getOne("SELECT `title` FROM `bx_groups_main` WHERE `id` = '$iGroupId'");
    	$reportTitle = $groupTitle . ' Report';
    	$numPosts = $this->oDb->getNumReportPosts($iGroupId);
    	$authorId = $this->oDb->getLastAuthor($iGroupId);
    	$posts = '';

    	if ($numPosts > 0) {
    		$author = getNickName($authorId);
    		$authorUrl = getProfileLink($authorId);
    		$posts = $numPosts . ' posts. Last post by: ';
    	} else {
    		$author = '';
    		$authorUrl = '';
    		$posts = "No posts yet.";
    	}

    	$aVars = array(
    		'url' => $groupUrl,
    		'title' => $reportTitle,
    		'num_posts' => $posts,
    		'author_url' => $authorUrl,
    		'author' => $author,
    		);

    	/*$aVars = array(
    		'url' => "hiiii",
    		'title' => "hiiii",
    		'num_posts' => "hiiii",
    		'author_url' => "hiiii",
    		'author' => "hiiii",
    		);*/

    	return $this->parseHtmlByName("report_unit.html", $aVars);
    }
}

?>
