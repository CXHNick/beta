<?php
/***************************************************************************
*                            Dolphin Smart Community Builder
*                              -----------------
*     begin                : Mon Mar 23 2006
*     copyright            : (C) 2006 BoonEx Group
*     website              : http://www.boonex.com/
* This file is part of Dolphin - Smart Community Builder
*
* Dolphin is free software. This work is licensed under a Creative Commons Attribution 3.0 License. 
* http://creativecommons.org/licenses/by/3.0/
*
* Dolphin is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
* without even the implied warranty of  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the Creative Commons Attribution 3.0 License for more details. 
* You should have received a copy of the Creative Commons Attribution 3.0 License along with Dolphin, 
* see license.txt file; if not, write to marketing@boonex.com
***************************************************************************/

require_once( BX_DIRECTORY_PATH_INC . "design.inc.php");
bx_import('BxDolAlerts');
bx_import('BxDolDb');
bx_import('BxDolModule');

class BxGPrivateResponse extends BxDolAlertsResponse {

	function BxGPrivateResponse() {
	    parent::BxDolAlertsResponse();
	}

    function response ($oAlert) {
 
		if ($oAlert->sUnit != 'bx_groups') 
			return;

		$oGPrivate = BxDolModule::getInstance('BxGPrivateModule');  
		switch ($oAlert->sAction) {
 
			case 'change': 
					$oGPrivate->_oDb->editGroupPrivacy($oAlert->iObject, $oAlert->aExtras['Status']); 
				break; 
			case 'delete': 
					$oGPrivate->_oDb->deleteGroupFromPrivacy($oAlert->iObject);  
 				break; 
 
		}//end switch  
	
	}



}
