<?php
/***************************************************************************
*                            Dolphin Smart Community Builder
*                              -------------------
*     begin                : Mon Mar 23 2006
*     copyright            : (C) 2007 BoonEx Confession
*     website              : http://www.boonex.com
* This file is part of Dolphin - Smart Community Builder
*
* Dolphin is free software; you can redistribute it and/or modify it under
* the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 2 of the
* License, or  any later version.
*
* Dolphin is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
* without even the implied warranty of  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
* You should have received a copy of the GNU General Public License along with Dolphin,
* see license.txt file; if not, write to marketing@boonex.com
***************************************************************************/

bx_import('BxDolTwigSearchResult');

class BxGPrivateSearchResult extends BxDolTwigSearchResult {
 
	 var $aCurrent = array(
		'name' => 'modzzz_gprivate',
		'title' => '_modzzz_gprivate_page_title_browse',
		'table' => 'modzzz_gprivate_main', 
		'ownFields' => array('id'), 
		'searchFields' => array(),
		'join' => array(
            'grp' => array(
                    'type' => 'inner',
                    'table' => 'bx_groups_main',
                    'mainField' => 'group_id',
                    'onField' => 'id',
                    'joinFields' => array('title'),
            ),		 
		),  
		'restriction' => array( 
			'activeStatus' => array('value' => 'approved', 'field'=>'status', 'operator'=>'='), 
		), 
		'paginate' => array('perPage' => 14, 'page' => 1, 'totalNum' => 0, 'totalPages' => 1),
		'sorting' => 'last',
		'rss' => array(),
		'ident' => 'id'
	 );

     function BxGPrivateSearchResult($sMode = '', $sValue = '', $sValue2 = '', $sValue3 = '') {   

		unset($this->aCurrent['rss']);

        switch ($sMode) {

            case 'categories':
 
                $this->aCurrent['restriction']['activeStatus']['value'] = 'approved';
                $this->sBrowseUrl = "browse/categories";
                $this->aCurrent['title'] = _t('_modzzz_gprivate_page_title_categories');
				break;

            case 'active':
                if (isset($_REQUEST['filter']))
                    $this->aCurrent['restriction']['keyword'] = array('value' => process_db_input($_REQUEST['filter'], BX_TAGS_STRIP), 'field' => 'title', 'operator' => 'against');
                $this->aCurrent['restriction']['activeStatus']['value'] = 'approved';
                $this->sBrowseUrl = "administration/active";
                $this->aCurrent['title'] = _t('_modzzz_gprivate_page_title_active_categories');
				break;
				 
	         default:
                $this->isError = true;
        }

        $oMain = $this->getMain();

        $this->aCurrent['paginate']['perPage'] = 20;
 
        $this->sFilterName = 'filter';

        parent::BxDolTwigSearchResult();
    }

    function getAlterOrder() {
		if ($this->aCurrent['sorting'] == 'last') {
			$aSql = array();
			$aSql['order'] = " ORDER BY `modzzz_gprivate_main`.`id` ASC";
			return $aSql; 
		}
	    return array();
    }

    function displaySearchUnit ($aData) {
        $oMain = $this->getMain(); 
		return $oMain->_oTemplate->unit($aData, $this->sUnitTemplate);
    }
 
    function displayResultBlock () {
        global $oFunctions;
 
        $s = parent::displayResultBlock ();
        if ($s) {
            $oMain = $this->getMain();
            $GLOBALS['oSysTemplate']->addDynamicLocation($oMain->_oConfig->getHomePath(), $oMain->_oConfig->getHomeUrl());
            $GLOBALS['oSysTemplate']->addCss(array('unit.css', 'twig.css'));
            return $GLOBALS['oSysTemplate']->parseHtmlByName('default_padding.html', array('content' => $s));
        }
        return '';
    }

    function getMain() {
        return BxDolModule::getInstance('BxGPrivateModule');
    }
 
    
    function _getPseud () {
        return array();
    }
  
}