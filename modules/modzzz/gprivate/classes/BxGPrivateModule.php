<?php
/***************************************************************************
*                            Dolphin Smart Community Builder
*                              -------------------
*     begin                : Mon Mar 23 2006
*     copyright            : (C) 2007 BoonEx 
*     website              : http://www.boonex.com
* This file is part of Dolphin - Smart Community Builder
*
* Dolphin is free software; you can redistribute it and/or modify it under
* the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 2 of the
* License, or  any later version.
*
* Dolphin is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
* without even the implied warranty of  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
* You should have received a copy of the GNU General Public License along with Dolphin,
* see license.txt file; if not, write to marketing@boonex.com
***************************************************************************/

function modzzz_gprivate_import ($sClassPostfix, $aModuleOverwright = array()) {
    global $aModule;
    $a = $aModuleOverwright ? $aModuleOverwright : $aModule;
    if (!$a || $a['uri'] != 'gprivate') {
        $oMain = BxDolModule::getInstance('BxGPrivateModule');
        $a = $oMain->_aModule;
    }
    bx_import ($sClassPostfix, $a) ;
}

require_once( BX_DIRECTORY_PATH_INC . 'admin_design.inc.php' ); 
require_once( BX_DIRECTORY_PATH_INC . 'utils.inc.php' ); 
 
bx_import('BxDolPaginate');
bx_import('BxDolAlerts');
bx_import('BxDolTwigModule');
bx_import('BxDolAdminSettings'); 
bx_import('BxTemplSearchResult');  

/*
 * GPrivate module
 *
 * This module allow users to create user's gprivate, 
 * users can rate, comment and discuss gprivate.
 * GPrivate can have photos, videos, sounds and files, uploaded
 * by gprivate's admins.
 * 
 *
 */
class BxGPrivateModule extends BxDolTwigModule {

    var $_oPrivacy;
    var $_aQuickCache = array ();

    function BxGPrivateModule(&$aModule) {

        parent::BxDolTwigModule($aModule);        
        $this->_sFilterName = 'modzzz_gprivate_filter';
        $this->_sPrefix = 'modzzz_gprivate';

        $GLOBALS['oBxGPrivateModule'] = &$this; 
    }
 
    function actionHome () {
		header ('Location:' . BX_DOL_URL_ROOT . 'member.php'); 
    }
	  
	function hasPrivelege($iProfileId, $iId){

		return $this->_oDb->hasPrivelege($iProfileId, $iId);
	}
 

    // ================================== admin actions

    function actionAdministration ($sUrl = '', $sParam1 = '', $sParam2 = '') {

        if (!$this->isAdmin()) {
            $this->_oTemplate->displayAccessDenied ();
            return;
        }        

        $this->_oTemplate->pageStart();

        $aMenu = array( 
            'active' => array(
                'title' => _t('_modzzz_gprivate_menu_active_categories'), 
                'href' => BX_DOL_URL_ROOT . $this->_oConfig->getBaseUri() . 'administration/active',
                '_func' => array ('name' => 'actionAdministrationManage', 'params' => array(true)),
            ),  
            'create' => array(
                'title' => _t('_modzzz_gprivate_menu_admin_add_category'), 
                'href' => BX_DOL_URL_ROOT . $this->_oConfig->getBaseUri() . 'administration/create',
                '_func' => array ('name' => 'actionAdministrationCreate', 'params' => array($sUrl, $sParam1)),
            ), 
            'settings' => array(
                'title' => _t('_modzzz_gprivate_menu_admin_settings'), 
                'href' => BX_DOL_URL_ROOT . $this->_oConfig->getBaseUri() . 'administration/settings',
                '_func' => array ('name' => 'actionAdministrationSettings', 'params' => array()),
            ),
        );

        if (empty($aMenu[$sUrl]))
            $sUrl = 'settings';

        $aMenu[$sUrl]['active'] = 1;
        $sContent = call_user_func_array (array($this, $aMenu[$sUrl]['_func']['name']), $aMenu[$sUrl]['_func']['params']);

        echo $this->_oTemplate->adminBlock ($sContent, _t('_modzzz_gprivate_page_title_administration'), $aMenu);
 
        $this->_oTemplate->addCssAdmin ('twig.css');
        $this->_oTemplate->addCssAdmin ('admin.css');
        $this->_oTemplate->addCssAdmin ('unit.css');
        $this->_oTemplate->addCssAdmin ('main.css');
        $this->_oTemplate->addCssAdmin ('forms_extra.css'); 
        $this->_oTemplate->addCssAdmin ('forms_adv.css');    
        $this->_oTemplate->addCssAdmin ('settings.css');    
        //$this->_oTemplate->addJsAdmin ('gprivate_templates.js');    
  
        $this->_oTemplate->pageCodeAdmin (_t('_modzzz_gprivate_page_title_administration'));
    }
 
    function actionAdministrationManage ($isActiveEntries = false) {
        return $this->_actionAdministrationManage ($isActiveEntries, '_modzzz_gprivate_admin_delete');
    }

    function _actionAdministrationManage ($isActiveEntries, $sKeyBtnDelete, $sUrl = false)
    {
        if ($_POST['action_delete'] && is_array($_POST['entry'])) {

            foreach ($_POST['entry'] as $iId) {

                if (!$this->isAdmin())continue;
                $aDataEntry = $this->_oDb->getEntryById($iId);
                   
               $this->_oDb->deleteEntryById($aDataEntry);
            }
        }

		$sContent = $this->_manageEntries ('active', '', true, 'bx_twig_admin_form', array(
			'action_delete' => $sKeyBtnDelete,
		), '', true, 0, $sUrl);

        return $sContent;
    }
  
    function _addForm ($sRedirectUrl) {

        bx_import ('FormAdd', $this->_aModule);
        $sClass = $this->_aModule['class_prefix'] . 'FormAdd';
        $oForm = new $sClass ($this);
        $oForm->initChecker();

        if ($oForm->isSubmittedAndValid ()) {
	 
			$oGroup = BxDolModule::getInstance('BxGroupsModule'); 
			$aGroupData = $oGroup->_oDb->getEntryById($oForm->getCleanValue('group_id'));
 
			$sTitle = $aGroupData[$oGroup->_oDb->_sFieldTitle];

            $aValsAdd = array (
				$this->_oDb->_sFieldTitle => $sTitle
 			);                        
  
            $iEntryId = $oForm->insert ($aValsAdd);

            if ($iEntryId) {
  
				$this->_oDb->addPrivacy($iEntryId, $sTitle);

				$sRedirectUrl = BX_DOL_URL_ROOT . $this->_oConfig->getBaseUri() . 'administration/active';
                header ('Location:' . $sRedirectUrl);
                exit;

            } else { 
                MsgBox(_t('_Error Occured'));
            }
                         
        } else { 
            echo $oForm->getCode (); 
        }
    }
   
    function actionAdministrationCreate($sUrl='', $iEntryId=0) {

        $iEntryId = (int)$iEntryId;
  
		if($iEntryId) {
				
			 if (!($aDataEntry = $this->_oDb->getEntryById($iEntryId))) {
				$this->_oTemplate->displayPageNotFound ();
				exit;
			 }
 		 
			ob_start();        
			$this->_editForm($aDataEntry, $iEntryId);
			$aVars = array (
				'content' => ob_get_clean(),
			); 
		}else{
  
			ob_start();        
			$this->_addForm(BX_DOL_URL_ROOT . $this->_oConfig->getBaseUri() . 'administration/active');
			$aVars = array (
				'content' => ob_get_clean(),
			);  
		}
        return $this->_oTemplate->parseHtmlByName('default_padding', $aVars);
    }

    function _editForm ($aDataEntry, $iEntryId) { 

        $iEntryId = (int)$iEntryId;
   
        bx_import ('FormEdit', $this->_aModule);
        $sClass = $this->_aModule['class_prefix'] . 'FormEdit';
        $oForm = new $sClass ($this, $iEntryId, $aDataEntry);
  
        $oForm->initChecker($aDataEntry);

        if ($oForm->isSubmittedAndValid ()) {
 
            $aValsAdd = array (); 
  
            if ($oForm->update ($iEntryId, $aValsAdd)) {
	 
                header ('Location:' . BX_DOL_URL_ROOT . $this->_oConfig->getBaseUri() . 'administration/active' );
                exit;

            } else { 
                echo MsgBox(_t('_Error Occured')); 
            }            

        } else { 
            echo $oForm->getCode (); 
        }
 
    }
 
    function actionAdministrationSettings () {
        return parent::_actionAdministrationSettings ('GPrivate');
    }

    function isAllowedAdd () {
        return $this->isAdmin();
    } 

	function isPermalinkEnabled() {
		$bEnabled = isset($this->_isPermalinkEnabled) ? $this->_isPermalinkEnabled : ($this->_isPermalinkEnabled = (getParam('permalinks_gprivate') == 'on'));
		 
        return $bEnabled;
    }

    function serviceCleanup()
    {
		$this->_oDb->cleanup(); 
	}	

  
 }
