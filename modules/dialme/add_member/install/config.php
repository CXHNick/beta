<?php
/***************************************************************************
* Date				: Sun January 26, 2014
* Website			: http://www.dialme.com
*
* Product Name			: Add A New Member Module
* Product Version		: 1.0.0
* Product Use			: For Boonex Dolphin 7.0.x & 7.1.x
*
***************************************************************************/

$aConfig = array(
	
	'title' => 'Add A New Member',
        'version' => '1.0.0',
	'vendor' => 'DialMe.com',
	'update_url' => '',
	
	'compatible_with' => array(
        '7.x.x'
    ),

	'home_dir' => 'dialme/add_member/',
	'home_uri' => 'add_member',
	
	'db_prefix' => 'bx_dialme_',
        'class_prefix' => 'BxDialMeAddMember',
    
	'install' => array(
		'show_introduction' => 1,
		'change_permissions' => 0,
		'execute_sql' => 1,
		'update_languages' => 1,
		'recompile_global_paramaters' => 1,
		'recompile_main_menu' => 0,
		'recompile_member_menu' => 0,
		'recompile_site_stats' => 0,
		'recompile_page_builder' => 0,
		'recompile_profile_fields' => 0,
		'recompile_comments' => 0,
		'recompile_member_actions' => 0,
		'recompile_tags' => 0,
		'recompile_votes' => 0,
		'recompile_categories' => 0,
		'recompile_search' => 0,
		'recompile_injections' => 1,
		'recompile_permalinks' => 0,
		'recompile_alerts' => 1,
		'clear_db_cache' => 0,
		'show_conclusion' => 1
	),
	   
	'uninstall' => array (
		'show_introduction' => 1,
		'change_permissions' => 0,
		'execute_sql' => 1,
		'update_languages' => 1,
		'recompile_global_paramaters' => 1,
		'recompile_main_menu' => 0,
		'recompile_member_menu' => 0,
		'recompile_site_stats' => 0,
		'recompile_page_builder' => 0,
		'recompile_profile_fields' => 0,
		'recompile_comments' => 0,
		'recompile_member_actions' => 0,
		'recompile_tags' => 0,
		'recompile_votes' => 0,
		'recompile_categories' => 0,
		'recompile_search' => 0,
		'recompile_injections' => 1,
		'recompile_permalinks' => 0,
		'recompile_alerts' => 1,
		'clear_db_cache' => 0,
		'show_conclusion' => 1
    ),

	'language_category' => 'DialMe Add Member',

	'install_permissions' => array(),
        'uninstall_permissions' => array(),

	'install_info' => array(
		'introduction' => 'inst_intro.html',
		'conclusion' => 'inst_concl.html'
	),
	'uninstall_info' => array(
		'introduction' => 'uninst_intro.html',
		'conclusion' => 'uninst_concl.html'
	)
);

?>