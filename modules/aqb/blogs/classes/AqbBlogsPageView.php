<?php
/***************************************************************************
* 
*     copyright            : (C) 2009 AQB Soft
*     website              : http://www.aqbsoft.com
*      
* IMPORTANT: This is a commercial product made by AQB Soft. It cannot be modified for other than personal usage.
* The "personal usage" means the product can be installed and set up for ONE domain name ONLY. 
* To be able to use this product for another domain names you have to order another copy of this product (license).
* 
* This product cannot be redistributed for free or a fee without written permission from AQB Soft.
* 
* This notice may not be removed from the source code.
* 
***************************************************************************/

bx_import('BxDolTwigPageView');
bx_import('BxDolSubMenuBlock');

class AqbBlogsPageView extends BxDolTwigPageView
{
	var $_sPrefix;
	var $_sPrefixLangs;
	var $_sPrefixOptions;

    function AqbBlogsPageView(&$oMain, &$aDataEntry)
    {
        $this->_sPrefix = $oMain->_oConfig->getUri();
        $this->_sPrefixLangs = $oMain->_oConfig->getSystemName('langs');
        $this->_sPrefixOptions = $oMain->_oConfig->getSystemName('options');
        
        parent::BxDolTwigPageView($this->_sPrefix . '_view', $oMain, $aDataEntry);
        
        //Nick
        /*$blogPostId = $aDataEntry['id'];
        $blogPostUri = $aDataEntry['uri'];
        $post = $this->_oDb->getOne("SELECT * FROM `aqb_blogs_main` WHERE `id` = '$blogPostId'");

        
        if (!$post) {
            $groupRedirId = $this->_oDb->getOne("SELECT `group_id` FROM `bx_groups_blog_main` WHERE `uri` = '$blogPostUri'");
            $groupUri = $this->_oDb->getGroupUriById($groupRedirId);

            header("Location: " . BX_DOL_URL_ROOT . "m/groups/view/" . $groupUri);
            exit;
        }*/
    }

    function getBlockCode_Info()
    {
        return array($this->_blockInfo ($this->aDataEntry, $this->_oTemplate->blockFields($this->aDataEntry), $this->_oMain->_formatLocation($this->aDataEntry, false, true)));
    }

    function getBlockCode_Desc()
    {
        return array($this->_oTemplate->blockDesc ($this->aDataEntry));
    }

    function getBlockCode_Photo()
    {
        return $this->_blockPhoto ($this->_oDb->getMediaIds($this->aDataEntry['id'], 'images'), $this->aDataEntry['author_id']);
    }

    function getBlockCode_Video()
    {
        return $this->_blockVideo ($this->_oDb->getMediaIds($this->aDataEntry['id'], 'videos'), $this->aDataEntry['author_id']);
    }

    function getBlockCode_Sound()
    {
        return $this->_blockSound ($this->_oDb->getMediaIds($this->aDataEntry['id'], 'sounds'), $this->aDataEntry['author_id']);
    }

    function getBlockCode_Files()
    {
        return $this->_blockFiles ($this->_oDb->getMediaIds($this->aDataEntry['id'], 'files'), $this->aDataEntry['author_id']);
    }

    function getBlockCode_Rate()
    {
        aqb_blogs_import('Voting');
        $o = new AqbBlogsVoting ($this->_oConfig->getSystemName('votes'), (int)$this->aDataEntry['id']);
        if (!$o->isEnabled()) return '';
        return array($o->getBigVoting ($this->_oMain->isAllowedRate($this->aDataEntry)));
    }

    function getBlockCode_Comments()
    {
        aqb_blogs_import('Cmts');
        $o = new AqbBlogsCmts ($this->_oConfig->getSystemName('comments'), (int)$this->aDataEntry['id']);
        if (!$o->isEnabled()) return '';
        return $o->getCommentsFirst ();
    }

    function getBlockCode_Actions()
    {
        global $oFunctions;

        if ($this->_oMain->_iProfileId || $this->_oMain->isAdmin()) {

            $oSubscription = new BxDolSubscription();
            $aSubscribeButton = $oSubscription->getButton($this->_oMain->_iProfileId, $this->_sPrefix, '', (int)$this->aDataEntry['id']);

            $isFan = $this->_oDb->isFan((int)$this->aDataEntry['id'], $this->_oMain->_iProfileId, 0) || $this->_oDb->isFan((int)$this->aDataEntry['id'], $this->_oMain->_iProfileId, 1);

            $aInfo = array (
                'BaseUri' => $this->_oMain->_oConfig->getBaseUri(),
                'iViewer' => $this->_oMain->_iProfileId,
                'ownerID' => (int)$this->aDataEntry['author_id'],
                'ID' => (int)$this->aDataEntry['id'],
                'URI' => $this->aDataEntry['uri'],
                'ScriptSubscribe' => $aSubscribeButton['script'],
                'TitleSubscribe' => $aSubscribeButton['title'],
                'TitleEdit' => $this->_oMain->isAllowedEdit($this->aDataEntry) ? _t($this->_sPrefixLangs . '_action_title_edit') : '',
                'TitleDelete' => $this->_oMain->isAllowedDelete($this->aDataEntry) ? _t($this->_sPrefixLangs . '_action_title_delete') : '',
                'TitleJoin' => $this->_oMain->isAllowedJoin($this->aDataEntry) ? ($isFan ? _t($this->_sPrefixLangs . '_action_title_leave') : _t($this->_sPrefixLangs . '_action_title_join')) : '',
                'IconJoin' => $isFan ? 'thumbs-down' : 'thumbs-up',
                'TitleInvite' => $this->_oMain->isAllowedSendInvitation($this->aDataEntry) ? _t($this->_sPrefixLangs . '_action_title_invite') : '',
                'TitleShare' => $this->_oMain->isAllowedShare($this->aDataEntry) ? _t($this->_sPrefixLangs . '_action_title_share') : '',
                'TitleBroadcast' => $this->_oMain->isAllowedBroadcast($this->aDataEntry) ? _t($this->_sPrefixLangs . '_action_title_broadcast') : '',
                'AddToFeatured' => $this->_oMain->isAllowedMarkAsFeatured($this->aDataEntry) ? ($this->aDataEntry['featured'] ? _t($this->_sPrefixLangs . '_action_remove_from_featured') : _t($this->_sPrefixLangs . '_action_add_to_featured')) : '',
                'TitleManageFans' => $this->_oMain->isAllowedManageFans($this->aDataEntry) ? _t($this->_sPrefixLangs . '_action_manage_fans') : '',
                'TitleUploadPhotos' => $this->_oMain->isAllowedUploadPhotos($this->aDataEntry) ? _t($this->_sPrefixLangs . '_action_upload_photos') : '',
                'TitleUploadVideos' => $this->_oMain->isAllowedUploadVideos($this->aDataEntry) ? _t($this->_sPrefixLangs . '_action_upload_videos') : '',
                'TitleUploadSounds' => $this->_oMain->isAllowedUploadSounds($this->aDataEntry) ? _t($this->_sPrefixLangs . '_action_upload_sounds') : '',
                'TitleUploadFiles' => $this->_oMain->isAllowedUploadFiles($this->aDataEntry) ? _t($this->_sPrefixLangs . '_action_upload_files') : '',
            );

            if (!$aInfo['TitleEdit'] && !$aInfo['TitleDelete'] && !$aInfo['TitleJoin'] && !$aInfo['TitleInvite'] && !$aInfo['TitleShare'] && !$aInfo['TitleBroadcast'] && !$aInfo['AddToFeatured'] && !$aInfo['TitleManageFans'] && !$aInfo['TitleUploadPhotos'] && !$aInfo['TitleUploadVideos'] && !$aInfo['TitleUploadSounds'] && !$aInfo['TitleUploadFiles'])
                return '';

            return $oSubscription->getData() . $oFunctions->genObjectsActions($aInfo, $this->_oMain->_oConfig->getSystemName('actions'));
        }

        return '';
    }

    function getBlockCode_Fans()
    {
        return parent::_blockFans ($this->_oDb->getParam($this->_sPrefixOptions . '_perpage_view_fans'), 'isAllowedViewFans', 'getFans');
    }

    function getBlockCode_FansUnconfirmed()
    {
        return parent::_blockFansUnconfirmed (AQB_BLOGS_MAX_FANS);
    }

    /*function getBlockCode_SubMenu() {
        $page = "Blogs Premium";
        $subMenu = new BxDolSubMenuBlock($page);
        return $subMenu->genSubMenuBlock();
    }*/

    function getCode()
    {
        $this->_oMain->_processFansActions ($this->aDataEntry, AQB_BLOGS_MAX_FANS);

        return parent::getCode();
    }

}
