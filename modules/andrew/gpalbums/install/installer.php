<?php
/***************************************************************************
*
* IMPORTANT: This is a commercial product made by AndrewP. It cannot be modified for other than personal usage.
* The "personal usage" means the product can be installed and set up for ONE domain name ONLY.
* To be able to use this product for another domain names you have to order another copy of this product (license).
* This product cannot be redistributed for free or a fee without written permission from AndrewP.
* This notice may not be removed from the source code.
*
***************************************************************************/
bx_import('BxDolInstaller');

class AGpalbumsInstaller extends BxDolInstaller {
    function AGpalbumsInstaller($aConfig) {
        parent::BxDolInstaller($aConfig);
    }
    function install($aParams) {
        $aResult = parent::install($aParams);

        if($aResult['result'] && BxDolRequest::serviceExists('spy', 'update_handlers'))
            BxDolService::call('spy', 'update_handlers', array($this->_aConfig['home_uri'], true));

        return $aResult;
    }
    function uninstall($aParams) {
        if(BxDolRequest::serviceExists('spy', 'update_handlers')) 
            BxDolService::call('spy', 'update_handlers', array($this->_aConfig['home_uri'], false));

        return parent::uninstall($aParams);
    }
}
