<?php
bx_import('BxDolPageView');
bx_import('Module', $aModule);

global $_page;
global $_page_cont;

$iIndex = 1;
$_page['name_index'] = $iIndex;

if (!isLogged()) login_form();


$oModule = new AqbFriendsBroadcasterModule($aModule);


$_page_cont[$iIndex]['page_main_code'] = $oModule->getComposeForm();

$oModule->_oTemplate->setPageTitle(_t('_aqb_friends_broadcaster_top_menu_item'));
PageCode($oModule->_oTemplate);
