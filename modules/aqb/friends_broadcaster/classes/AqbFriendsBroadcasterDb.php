<?php
/***************************************************************************
*
*     copyright            : (C) 2014 AQB Soft
*     website              : http://www.aqbsoft.com
*
* IMPORTANT: This is a commercial product made by AQB Soft. It cannot be modified for other than personal usage.
* The "personal usage" means the product can be installed and set up for ONE domain name ONLY.
* To be able to use this product for another domain names you have to order another copy of this product (license).
*
* This product cannot be redistributed for free or a fee without written permission from AQB Soft.
*
* This notice may not be removed from the source code.
*
***************************************************************************/

bx_import('BxDolModuleDb');
bx_import('BxDolEmailTemplates');

class AqbFriendsBroadcasterDb extends BxDolModuleDb {
	/*
	 * Constructor.
	 */
	function __construct(&$oConfig) {
		parent::__construct($oConfig);
	}

	function getProfileFriends($iProfile) {
		return  $this->getColumn("
			SELECT `ID` AS `ID` FROM `sys_friend_list` WHERE `Profile` = {$iProfile} AND `Check` = 1
            UNION
            SELECT `Profile` AS `ID` FROM `sys_friend_list` WHERE `ID` = {$iProfile} AND `Check` = 1
        ");
	}

	function sendMessage($iSender, $iRecipient, $sSubject, $sMessage, $bSendToEmail, $bNotifyByEmail) {
		$sMessage = process_db_input($sMessage, BX_TAGS_VALIDATE);
		$sMessageCopy = $GLOBALS['MySQL']->unescape($sMessage);
		$sSubject = process_db_input($sSubject, BX_TAGS_STRIP);
        $sSubjectCopy = $GLOBALS['MySQL']->unescape($sSubject);

		$this->query("INSERT INTO `sys_messages` (`Date`, `Sender`, `Recipient`, `Subject`, `Text`, `New`, `Type`) VALUES (NOW(), {$iSender}, {$iRecipient}, '{$sSubject}', '{$sMessage}', '1', 'letter')");

		bx_import('BxDolAlerts');
        $aAlertData = array(
            'msg_id' => $this->lastId(),
            'subject' => $sSubject,
            'body' => $sMessage,
            'send_copy' => (bool)$bSendToEmail,
            'notification' => (bool)$bNotifyByEmail,
            'send_copy_to_me' => false,
        );
        $oZ = new BxDolAlerts('profile', 'send_mail_internal', $iSender, $iRecipient, $aAlertData);


        $aRecipientInfo = getProfileInfo($iRecipientID);
        $aRecipientEmail = $this->getOne("SELECT `Email` FROM `Profiles` WHERE `ID` = '{$iRecipient}'");
        $oEmailTemplate = new BxDolEmailTemplates();

        if ($bSendToEmail) {
			$aTemplate = $oEmailTemplate->getTemplate('t_Message', $iRecipient);
			$aPlus = array();
			$aPlus['MessageText']	= replace_full_uris($sMessageCopy);
			$aPlus['ProfileReference'] = getNickName($iSender);
			$aPlus['ProfileUrl'] = getProfileLink($iSender);
			sendMail($aRecipientEmail, $sSubjectCopy, $aTemplate['Body'], $iRecipient, $aPlus);
			//sendMail($aRecipientInfo['Email'], $sSubjectCopy, $aTemplate['Body'], $iRecipient, $aPlus);
		}

		if ($bNotifyByEmail) {
			$aTemplate = $oEmailTemplate->getTemplate('t_Compose', $iRecipient);
			$aPlus['ProfileReference'] = getNickName($iSender);
			$aPlus['ProfileUrl'] = getProfileLink($iSender);
			//sendMail($aRecipientInfo['Email'], $aTemplate['Subject'], $aTemplate['Body'], $iRecipient, $aPlus);
			sendMail($aRecipientEmail, $sSubjectCopy, $aTemplate['Body'], $iRecipient, $aPlus);

		}
	}

	function areFriends($iProfile, $iViewer) {
		return $this->getOne("SELECT COUNT(*) FROM `sys_friend_list` WHERE (`Profile` = {$iProfile} AND `ID` = {$iViewer} OR `Profile` = {$iViewer} AND `ID` = {$iProfile}) AND `Check` = 1");
	}

	//Nick
	function getSubject($messageID) {
		$subj = $this->getOne("SELECT `Subject` FROM `sys_messages` WHERE `ID` = '$messageID'");
		return 'Re: ' . $subj;
	}

	function getBody($messageID) {
		$sender = getNickName($this->getRecip($messageID));
		$body = $this->getOne("SELECT `Text` FROM `sys_messages` WHERE `ID` = '$messageID'");
		$date = $this->getOne("SELECT `Date` FROM `sys_messages` WHERE `ID` = '$messageID'");

		return $this->addReToBody($body, $sender, $date);
	}

	//Nick
        function addReToBody($sBody, $sSender, $sWhen) {

            $str1 = "<blockquote>";

            $newBody = '<br><br>' . $str1 . $sSender . ' wrote (' . $sWhen . '):<br><br>' . $sBody . '</blockquote>';

            return $newBody;
        }

	function getRecip($messageID) {
		return array($this->getOne("SELECT `Sender` FROM `sys_messages` WHERE `ID` = '$messageID'"));
	}

	function getRecipients($messageID) {
		$subject = mysql_escape_string($this->getOne("SELECT `Subject` FROM `sys_messages` WHERE `ID` = '$messageID'"));
		$date = $this->getOne("SELECT `Date` FROM `sys_messages` WHERE `ID` = '$messageID'");

		$recips = $this->getAll("SELECT `Recipient` FROM `sys_messages` WHERE `Subject` = '$subject' AND `Date` = '$date'");
		$recipIds = array();

		for ($i = 0; $i < sizeof($recips); $i++) {
			$recipIds[$i] = $recips[$i]['Recipient'];
		}

		return array_merge($this->getRecip($messageID), $recipIds);		
	}
}