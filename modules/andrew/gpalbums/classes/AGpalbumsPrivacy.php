<?php
/***************************************************************************
*
* IMPORTANT: This is a commercial product made by AndrewP. It cannot be modified for other than personal usage.
* The "personal usage" means the product can be installed and set up for ONE domain name ONLY.
* To be able to use this product for another domain names you have to order another copy of this product (license).
* This product cannot be redistributed for free or a fee without written permission from AndrewP.
* This notice may not be removed from the source code.
*
***************************************************************************/
bx_import('BxDolPrivacy');

class AGpalbumsPrivacy extends BxDolPrivacy {

    var $oModule;

    /**
    * Constructor
    */
    function AGpalbumsPrivacy(&$oModule) {
        $this->oModule = $oModule;
        parent::BxDolPrivacy('agrp_palbums_albums', 'ID', 'Owner');
    }

    /**
    * Get database field name for action.
    *
    * @param string $sAction action name.
    * @return string with field name.
    */
    function getFieldAction($sAction) {
        return 'Allow' . str_replace(' ', '', ucwords(str_replace('_', ' ', $sAction)));
    }

    function isDynamicGroupMember($mixedGroupId, $iObjectOwnerId, $iViewerId, $iObjectId) {
        /*if ($this->oModule->_iVisitorID == $this->oModule->_aGrpInfo['author_id'] || isAdmin()) return true;

        $iObjectId = $this->oModule->_iGrpID; // as Group ID
        $aDataEntry = array ('id' => $iObjectId, 'author_id' => $iObjectOwnerId);
        if ('f' == $mixedGroupId) // fans only
            return $this->oModule->isFan ($aDataEntry, $iViewerId, true); 
        elseif ('a' == $mixedGroupId) // admins only
            return $this->oModule->isEntryAdmin ($aDataEntry, $iViewerId); 
        return false;*/
        return true;
    }
}
