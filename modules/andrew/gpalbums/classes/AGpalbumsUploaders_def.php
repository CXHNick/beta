<?php
/***************************************************************************
*
* IMPORTANT: This is a commercial product made by AndrewP. It cannot be modified for other than personal usage.
* The "personal usage" means the product can be installed and set up for ONE domain name ONLY.
* To be able to use this product for another domain names you have to order another copy of this product (license).
* This product cannot be redistributed for free or a fee without written permission from AndrewP.
* This notice may not be removed from the source code.
*
***************************************************************************/
bx_import('BxDolPageView');

class AGpalbumsUploaders {

    // variables
    var $_iVisitorID;
    var $sUploadDir;
    var $iThumbSize;
    var $iImgSize;
    var $iMaxFilesize;
    var $sModuleUrl;
    var $_oModule;
    var $oDb;

    /**
    * Constructor
    */
    function AGpalbumsUploaders($oGpalbumsModule) {
        $this->_iVisitorID = $oGpalbumsModule->_iVisitorID;
        $this->sUploadDir = $oGpalbumsModule->sUploadDir;
        $this->iThumbSize = $oGpalbumsModule->iThumbSize;
        $this->iImgSize = $oGpalbumsModule->iImgSize;
        $this->iMaxFilesize = 5242880; //5*1024*1024;
        $this->sModuleUrl = $oGpalbumsModule->sModuleUrl;
        $this->oDb = $oGpalbumsModule->_oDb;
        $this->_oModule = $oGpalbumsModule;
    }

    function getAlbumUploadForm($iAlbumID = 0, $sAlbumUriParam = '') {
        $sSubmitC = _t('_Submit');
        $sAction = 'add';
        $sTitle = $sDesc = '';

        //adding form
        $aForm = array(
            'form_attrs' => array(
                'name' => 'upload_files_form',
                'action' => $this->sModuleUrl . 'upload/' . $sAlbumUriParam . '/',
                'method' => 'post',
                'enctype' => 'multipart/form-data',
            ),
            'params' => array (
                'db' => array(
                    'table' => 'agrp_palbums_units',
                    'key' => 'ID',
                    'submit_name' => 'add_button',
                ),
            ),
            'inputs' => array(
                'AlbumID' => array(
                    'type' => 'hidden',
                    'name' => 'AlbumID',
                    'value' => $iAlbumID,
                ),
                'File' => array(
                    'type' => 'file',
                    'name' => 'image[]',
                    'caption' => _t('_gpa_images'),
                    'attrs' => array(
                        'multiplyable' => 'true',
                    )
                ),
                'Prefix' => array(
                    'type' => 'text',
                    'name' => 'Prefix',
                    'caption' => _t('_gpa_prefix'),
                    'required' => false,
                    'value' => $sTitle,
                    'info' => _t('_gpa_prefix_expl'),
                ),
                'add_button' => array(
                    'type' => 'submit',
                    'name' => 'add_button',
                    'value' => $sSubmitC,
                ),
            ),
        );

        $sCode = '';
        $oForm = new BxTemplFormView($aForm);
        $oForm->initChecker();
        if ($oForm->isSubmittedAndValid()) {
            $iLastId = -1;

            if ( $_FILES) {
                for ($i=0; $i<count($_FILES['image']['tmp_name']); $i++) {
                    if( $_FILES['image']['error'][$i] )
                        continue;

                    $sWhen = time();
                    $sPrefix = process_db_input($_POST['Prefix'], BX_TAGS_STRIP);
                    $sRealFilename = $_FILES['image']['name'][$i];
                    $iPointPos = strrpos($sRealFilename, '.');
                    $sFileName = substr($sRealFilename, 0, $iPointPos);
                    $sUnitUri = uriGenerate($sPrefix . $sFileName, 'agrp_palbums_units', 'Uri');

                    list($sFilename, $sDimension) = $this->performPhotoUpload($_FILES['image']['tmp_name'][$i], $_FILES['image']['size'][$i]);

                    if ($sFilename != '') {
                        $aValsAdd = array (
                            'When' => $sWhen,
                            'Uri' => $sUnitUri,
                            'Size' => $sDimension,
                            'Filename' => $sFilename,
                            'AlbumID' => $iAlbumID,
                            'Title' => $sPrefix . $sFileName,
                            'owner' => $this->_iVisitorID,
                        ); 

                        $iLastId = $oForm->insert($aValsAdd);
                        $this->oDb->updateImage2Thumb($iAlbumID, $iLastId);
                    }
                }
                bx_import('BxDolAlerts');
                $oZ = new BxDolAlerts('gphotos', 'upload', $iLastId, $this->_iVisitorID);
                $oZ->alert();
            }

            if ($iLastId > 0) {
                $sCode = MsgBox(_t('_gpa_photo_added_success'), 1);
            } else {
                $sCode = MsgBox(_t('_gpa_photo_added_failed'), 1);
            }
        }
        return $sCode . $oForm->getCode();
    }

    function getFuploadFormFile($sAlbumUriParam, $iAlbumID) {
        if ($_POST['action'] == 'accept_multi_files') {
            $_iOwnerId = (int)$_POST['oid'];
            $this->_iVisitorID = $_iOwnerId;
            if ($_FILES) {
                if ($_FILES['Filedata']['error'] || $_FILES['Filedata']['size'] > $this->iMaxFilesize)
                    return;

                list($sFilename, $sDimension) = $this->performPhotoUpload($_FILES['Filedata']['tmp_name'], $_FILES['Filedata']['size']);

                if ($sFilename != '') {
                    $sWhen = time();
                    $sPrefix = process_db_input($_POST['Prefix'], BX_TAGS_STRIP);
                    $sRealFilename = $_FILES['Filedata']['name'];
                    $iPointPos = strrpos($sRealFilename, '.');
                    //$sExt = substr($sRealFilename, $iPointPos + 1);
                    $sFileName = substr($sRealFilename, 0, $iPointPos);
                    $sUnitUri = uriGenerate($sPrefix . $sFileName, 'agrp_palbums_units', 'Uri');

                    $iPossibleAlbumID = (int)$_POST['possible_album'];

                    $aValsAdd = array (
                        'When' => $sWhen,
                        'Uri' => $sUnitUri,
                        'Size' => $sDimension,
                        'Filename' => $sFilename,
                        'AlbumID' => $iPossibleAlbumID,
                        'Title' => $sPrefix . $sFileName,
                        'owner' => $this->_iVisitorID,
                    ); 
                    $iLastId = $this->oDb->insertImage2Album($aValsAdd);
                    $this->oDb->updateImage2Thumb($iAlbumID, $iLastId);

                    $sResult = $sRealFilename . ' was successfully uploaded';
                    echo <<<EOF
<script type="text/javascript">
    $('#divFileProgressContainer').html('{$sResult}');
</script>
EOF;

                    bx_import('BxDolAlerts');
                    $oZ = new BxDolAlerts('gphotos', 'upload', $iLastId, $this->_iVisitorID);
                    $oZ->alert();
                }
            }
            echo 1;
            exit;
        }

        $aMultiUploaderParams = array(
            'accept_file' => $this->sModuleUrl . 'fupload/' . $sAlbumUriParam . '/',
            'multi' => 'true',
            'auto' => 'true',
            'post_params' => '{action: "accept_multi_files", oid: "'.$this->_iVisitorID.'"}',
            'accept_folder' => 'data/files',
            'accept_format' => '*.jpg;*.jpeg;*.gif;*.png;*.docx;*.wav;*.mov;',
            'accept_format_desc' => _t('_gpa_image_files'),
            'sim_upload_limit' => '2',
            'file_size_limit' => $this->convert($this->iMaxFilesize), //Examples: 2147483648 B, 2097152, 2097152KB, 2048 MB, 2 GB
            'file_upload_limit' => '20',
            'file_queue_limit' => '10',
            'button_image_url' => BX_DOL_URL_PLUGINS . 'swfupload/images/XPButtonUploadText_61x22.png',
        );

        $bAjxMod = ($_POST['amode']=='ajax') ? 'true' : 'false';

        $sAlbumParam = ($iAlbumID) ? ', "possible_album": "'.$iAlbumID.'"'  : '';
        $aTmplKeys = array(
            'plugins_url' => BX_DOL_URL_PLUGINS,
            'accept_file' => $aMultiUploaderParams['accept_file'],
            'possible_album' => $sAlbumParam,
            'owner_id' => $this->_iVisitorID,
            'accept_format' => $aMultiUploaderParams['accept_format'],
            'accept_format_desc' => $aMultiUploaderParams['accept_format_desc'],
            'file_size_limit' => $aMultiUploaderParams['file_size_limit'],
            'file_upload_limit' => 0,
            'file_queue_limit' => 50,
            'button_image_url' => $this->_oModule->_oTemplate->getIconUrl('SmallSpyGlassWithTransperancy_17x18.png'),
            'Upload_lbl' => _t('_Select file'),
            'ajx_mode' => $bAjxMod,
        );
        $sCustomElement = $this->_oModule->_oTemplate->parseHtmlByName('swf_upload_integration.html', $aTmplKeys);

        $aForm = array(
            'form_attrs' => array(
                'action' => '',
                'method' => 'post',
            ),
            'params' => array(
                'remove_form' => true,
            ),
            'inputs' => array(
                'Browse' => array(
                    'type' => 'custom',
                    'name' => 'Browse',
                    'content' => $sCustomElement,
                    'colspan' => true
                ),
                'hidden_action' => array(
                    'type' => 'hidden',
                    'name' => 'action',
                    'value' => 'accept_multi_upload'
                ),
             ),
        );

        $oForm = new BxTemplFormView($aForm);
        return $this->getWrap($oForm->getCode());
    }

    function getUrlUploadForm($iAlbumID = 0, $sAlbumUriParam = '') {
        $sSubmitC = _t('_Submit');
        $sAction = 'add';

        //adding form
        $aForm = array(
            'form_attrs' => array(
                'name' => 'upload_files_form',
                'action' => $this->sModuleUrl . 'uploadu/' . $sAlbumUriParam . '/',
                'method' => 'post',
            ),
            'params' => array (
                'db' => array(
                    'table' => 'agrp_palbums_units',
                    'key' => 'ID',
                    'submit_name' => 'add_button',
                ),
            ),
            'inputs' => array(
                'AlbumID' => array(
                    'type' => 'hidden',
                    'name' => 'AlbumID',
                    'value' => $iAlbumID,
                ),
                'url' => array(
                    'type' => 'text',
                    'name' => 'url',
                    'caption' => _t('_URL'),
                    'required' => true,
                    'value' => '',
                ),
                'add_button' => array(
                    'type' => 'submit',
                    'name' => 'add_button',
                    'value' => $sSubmitC,
                ),
            ),
        );

        $sCode = '';
        $oForm = new BxTemplFormView($aForm);
        $oForm->initChecker();
        if ($oForm->isSubmittedAndValid()) {
            $iLastId = -1;

            if ($_POST && $this->_iVisitorID && $this->sUploadDir){
                $url = $_POST['url'];
                $name = basename($url);
                list($txt, $ext) = explode(".", $name);
                $name = $txt.time();
                $name = $name.".".$ext;

                if ($ext == "jpg" || $ext == "png" || $ext == "gif") {
                    $iSize = $this->curl_get_file_size($url);
                    if ($iSize && $iSize < $this->iMaxFilesize) {

                        $sTempFileName = $this->sUploadDir . $this->_iVisitorID . '_temp';
                        @unlink($sTempFileName);

                        $iUplRes = file_put_contents($sTempFileName, file_get_contents($url));
                        if ($iUplRes) {

                            @chmod($sTempFileName, 0644);
                            if (file_exists($sTempFileName) && filesize($sTempFileName)>0) {
                                $aSize = getimagesize($sTempFileName);
                                if (! $aSize) {
                                    @unlink($sTempFileName);
                                } else {
                                    switch($aSize[2]) {
                                        case IMAGETYPE_JPEG: $sExtension = '.jpg'; break;
                                        case IMAGETYPE_GIF:  $sExtension = '.gif'; break;
                                        case IMAGETYPE_PNG:  $sExtension = '.png'; break;
                                    }

                                    $sStatus = 'processing';
                                    $iImgWidth = (int)$aSize[0];
                                    $iImgHeight = (int)$aSize[1];
                                    $sDimension = $iImgWidth.'x'.$iImgHeight;

                                    $sFileName = md5(uniqid(rand(), true));
                                    @rename($sTempFileName, $this->sUploadDir . $sFileName . $sExtension);

                                    $sFile = $this->sUploadDir . $sFileName . $sExtension;
                                    $sThumbFile = $this->sUploadDir . 't_' . $sFileName . $sExtension;
                                    $sImgFile = $this->sUploadDir . 'i_' . $sFileName . $sExtension;

                                    // watermark postprocessing (optional)
                                    /*if ($sFile != '') {
                                        $iTransparent = getParam('transparent1');
                                        $sWaterMark = $GLOBALS['dir']['profileImage'] . getParam('Water_Mark');
                                        if (strlen(getParam('Water_Mark')) && file_exists($sWaterMark))
                                            applyWatermark($sFile, $sFile, $sWaterMark, $iTransparent);
                                    }*/

                                    // force into JPG
                                    //$sExtension = '.jpg';
                                    $iRes = imageResize($sFile, $sThumbFile, $this->iThumbSize,  $this->iThumbSize);
                                    imageResize($sFile, $sImgFile, $this->iImgSize, $this->iImgSize);

                                    @chmod($sThumbFile, 0644);
                                    @chmod($sImgFile, 0644);
                                    @chmod($sFile, 0644);

                                    //@unlink($sFile); // unlink original

                                    $aSizeImg = getimagesize($sImgFile);
                                    $iImgWidth = (int)$aSizeImg[0];
                                    $iImgHeight = (int)$aSizeImg[1];
                                    $sDimension = $iImgWidth.'x'.$iImgHeight;

                                    // extra stuff
                                    $sWhen = time();
                                    $sUnitUri = uriGenerate($txt, 'agrp_palbums_units', 'Uri');

                                    $aValsAdd = array (
                                        'When' => $sWhen,
                                        'Uri' => $sUnitUri,
                                        'Size' => $sDimension,
                                        'Filename' => $sFileName . $sExtension,
                                        'AlbumID' => $iAlbumID,
                                        'Title' => $txt,
                                        'owner' => $this->_iVisitorID,
                                    ); 

                                    $iLastId = $oForm->insert($aValsAdd);
                                    $this->oDb->updateImage2Thumb($iAlbumID, $iLastId);

                                    bx_import('BxDolAlerts');
                                    $oZ = new BxDolAlerts('gphotos', 'upload', $iLastId, $this->_iVisitorID);
                                    $oZ->alert();
                                }
                            }
                        }
                    }
                }
            }

            if ($iLastId > 0) {
                $sCode = MsgBox(_t('_gpa_photo_added_success'), 1);
            } else {
                $sCode = MsgBox(_t('_gpa_photo_added_failed'), 1);
            }
        }
        return $sCode . $oForm->getCode();
    }
    function curl_get_file_size( $url ) {
      // Assume failure.
      $result = -1;

      $curl = curl_init( $url );

      // Issue a HEAD request and follow any redirects.
      curl_setopt( $curl, CURLOPT_NOBODY, true );
      curl_setopt( $curl, CURLOPT_HEADER, true );
      curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
      curl_setopt( $curl, CURLOPT_FOLLOWLOCATION, true );
      curl_setopt( $curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT'] );

      $data = curl_exec( $curl );
      curl_close( $curl );

      if( $data ) {
        $content_length = "unknown";
        $status = "unknown";

        if( preg_match( "/^HTTP\/1\.[01] (\d\d\d)/", $data, $matches ) ) {
          $status = (int)$matches[1];
        }

        if( preg_match( "/Content-Length: (\d+)/", $data, $matches ) ) {
          $content_length = (int)$matches[1];
        }

        // http://en.wikipedia.org/wiki/List_of_HTTP_status_codes
        if( $status == 200 || ($status > 300 && $status <= 308) ) {
          $result = $content_length;
        }
      }

      return $result;
    }

    function convert($size) {
       $unit = array('B', 'KB', 'MB', 'GB');
       return round($size/pow(1024, ($i = floor(log($size, 1024)))), 2) . ' ' . $unit[$i];
    }

    // simple upload
    function performPhotoUpload($sTmpFile, $iFileSize, $isMoveUploadedFile = true) {
        $iLastID = -1;

        if (! $this->_iVisitorID || file_exists($sTmpFile) == false)
            return false;

        if (! $this->sUploadDir) {
            @unlink($sTmpFile);
            return false;
        }

        if ($iFileSize > $this->iMaxFilesize) return false;

        $sTempFileName = $this->sUploadDir . $this->_iVisitorID . '_temp';
        @unlink($sTempFileName);

        if (($isMoveUploadedFile && is_uploaded_file($sTmpFile)) || !$isMoveUploadedFile) {
            if ($isMoveUploadedFile) {
                move_uploaded_file($sTmpFile, $sTempFileName);
                @unlink($sTmpFile);
            } else {
                $sTempFileName = $sTmpFile;
            }

            @chmod($sTempFileName, 0644);
            if(file_exists($sTempFileName) && filesize($sTempFileName)>0) {
                $aSize = getimagesize($sTempFileName);
                if (!$aSize) {
                    @unlink($sTempFileName);
                    return false;
                }

                switch($aSize[2]) {
                    case IMAGETYPE_JPEG: $sExtension = '.jpg'; break;
                    case IMAGETYPE_GIF:  $sExtension = '.gif'; break;
                    case IMAGETYPE_PNG:  $sExtension = '.png'; break;
                    default:
                        @unlink($sTempFileName);
                        return false;
                }

                $sStatus = 'processing';
                $iImgWidth = (int)$aSize[0];
                $iImgHeight = (int)$aSize[1];
                $sDimension = $iImgWidth.'x'.$iImgHeight;

                $sFileName = md5(uniqid(rand(), true));
                $sFunc = ($isMoveUploadedFile) ? 'rename' : 'copy';
                if (! $sFunc($sTempFileName, $this->sUploadDir . $sFileName . $sExtension)) {
                    @unlink($sTempFileName);
                    return false;
                }

                $sFile = $this->sUploadDir . $sFileName . $sExtension;
                $sThumbFile = $this->sUploadDir . 't_' . $sFileName . $sExtension;
                $sImgFile = $this->sUploadDir . 'i_' . $sFileName . $sExtension;

                // watermark postprocessing (optional)
                /*if ($sFile != '') {
                    $iTransparent = getParam('transparent1');
                    $sWaterMark = $GLOBALS['dir']['profileImage'] . getParam('Water_Mark');
                    if (strlen(getParam('Water_Mark')) && file_exists($sWaterMark))
                        applyWatermark($sFile, $sFile, $sWaterMark, $iTransparent);
                }*/

                // force into JPG
                //$sExtension = '.jpg';
                $iRes = imageResize($sFile, $sThumbFile, $this->iThumbSize,  $this->iThumbSize);
                if ($iRes != 0) return false; //resizing was failed
                imageResize($sFile, $sImgFile, $this->iImgSize, $this->iImgSize);

                @chmod($sThumbFile, 0644);
                @chmod($sImgFile, 0644);
                @chmod($sFile, 0644);

                //@unlink($sFile); // unlink original

                $aSizeImg = getimagesize($sImgFile);
                $iImgWidth = (int)$aSizeImg[0];
                $iImgHeight = (int)$aSizeImg[1];
                $sDimension = $iImgWidth.'x'.$iImgHeight;
            }
        }

        return array($sFileName.$sExtension, $sDimension);
    }

    function serviceGetUploaderBlock($sAlbumUriParam, $iAlbumID, $sSubAction) {
        $sAlbumUriSafe = process_db_input($sAlbumUriParam, BX_TAGS_STRIP);

        $aUploadButtons = array(
            'u0' => array('href' => $this->sModuleUrl . 'album/' . $sAlbumUriSafe . '/', 'title' => _t('_gpa_back'), 'onclick' => '', 'active' => false, 'icon' => 'chevron-left'),
            'u1' => array('href' => $this->sModuleUrl . 'fupload/' . $sAlbumUriSafe . '/', 'title' => _t('_gpa_fupload'), 'onclick' => '', 'active' => false, 'icon' => 'upload-alt'),
            'u2' => array('href' => $this->sModuleUrl . 'upload/' . $sAlbumUriSafe . '/', 'title' => _t('_gpa_upload'), 'onclick' => '', 'active' => false, 'icon' => 'upload'),
            'u3' => array('href' => $this->sModuleUrl . 'uploadu/' . $sAlbumUriSafe . '/', 'title' => _t('_gpa_uploadu'), 'onclick' => '', 'active' => false, 'icon' => 'tag'),
        );

        $sUploadTitle = '';
        switch ($sSubAction) {
            case 'upload':
                $aUploadButtons['u2']['active'] = true;
                $sCodeForm = $this->getAlbumUploadForm($iAlbumID, $sAlbumUriParam);
                $sUploadTitle = _t('_gpa_upload');
                break;
            case 'fupload':
                $aUploadButtons['u1']['active'] = true;
                $sCodeForm = $this->getFuploadFormFile($sAlbumUriParam, $iAlbumID);
                $sUploadTitle = _t('_gpa_fupload');
                break;
            case 'uploadu':
                $aUploadButtons['u3']['active'] = true;
                $sCodeForm = $this->getUrlUploadForm($iAlbumID, $sAlbumUriParam);
                $sUploadTitle = _t('_gpa_uploadu');
                break;
        }
        $sDBActions = BxDolPageView::getBlockCaptionMenu(mktime(), $aUploadButtons);
        return DesignBoxContent($sUploadTitle, $sCodeForm, 1, $sDBActions);
    }

    function getWrap($sCode) {
        return '<div class="dbContent">' . $sCode . '<div class="clear_both"></div></div>';
    }
}
