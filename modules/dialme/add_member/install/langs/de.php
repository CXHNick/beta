﻿<?php
/***************************************************************************
* Date				: Sun January 26, 2014
* Website			: http://www.dialme.com
*
* Product Name			: Add A New Member Module
* Product Version		: 1.0.0
* Product Use			: For Boonex Dolphin 7.0.x & 7.1.x
*
***************************************************************************/

$sLangCategory = 'DialMe Add Member';

$aLangContent = array(
    '_dialme_add_member'                 => 'Ein Neues Mitglied Anlegen',
    '_dialme_add_member_pinfo'           => 'Profilinformationen',
    '_dialme_add_member_profile_created' => 'Mitglied Wurde Angelegt'
);

?>