<?php
/***************************************************************************
* 
*     copyright            : (C) 2009 AQB Soft
*     website              : http://www.aqbsoft.com
*      
* IMPORTANT: This is a commercial product made by AQB Soft. It cannot be modified for other than personal usage.
* The "personal usage" means the product can be installed and set up for ONE domain name ONLY. 
* To be able to use this product for another domain names you have to order another copy of this product (license).
* 
* This product cannot be redistributed for free or a fee without written permission from AQB Soft.
* 
* This notice may not be removed from the source code.
* 
***************************************************************************/

bx_import('BxDolConfig');

class AqbBlogsConfig extends BxDolConfig
{
	private $_sIcon;
	private $_sFilterName;
	private $_aSystemNames;
	
    function AqbBlogsConfig($aModule)
    {
        parent::BxDolConfig($aModule);
        $sUri = $this->getUri();

        $this->_sIcon = 'book';
        $this->_sFilterName = $sUri . '_filter';

        $this->_aSystemNames = array(
        	'langs' => '_' . $sUri,
        	'actions' => $sUri,
        	'comments' => $sUri,
        	'categories' => $sUri,
        	'options' => $sUri,
        	'votes' => $sUri,
        	'search' => $sUri,
        	'privacy' => $sUri
        );
    }

    function getHomeDirectory()
    {
    	return $this->_sDirectory;
    }
    function getIcon()
    {
    	return $this->_sIcon;
    }
    function getSystemName($sType)
    {
    	return $this->_aSystemNames[$sType];
    }
    function getFilterName()
    {
    	return $this->_sFilterName;
    }
}
