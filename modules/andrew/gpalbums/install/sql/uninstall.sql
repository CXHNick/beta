-- tables
DROP TABLE IF EXISTS `[db_prefix]_albums`;
DROP TABLE IF EXISTS `[db_prefix]_units`;
DROP TABLE IF EXISTS `[db_prefix]_cmts`;
DROP TABLE IF EXISTS `[db_prefix]_img_cmts`;
DROP TABLE IF EXISTS `[db_prefix]_img_rating`;
DROP TABLE IF EXISTS `[db_prefix]_img_voting_track`;

-- page compose pages
DELETE FROM `sys_page_compose` WHERE `Caption`='_gpa_main' AND `Func`='PHP';
DELETE FROM `sys_page_compose` WHERE `Caption`='_gpa_featured_list' AND `Func`='PHP';

-- actions
DELETE FROM `sys_objects_actions` WHERE `Type`='[db_prefix]';

DELETE FROM `sys_objects_actions` WHERE `Type`='bx_groups' AND `Caption`='_gpa_albums';

-- settings
DELETE FROM `sys_options` WHERE `Name` = 'permalinks_agpalbums';

-- permalinks
DELETE FROM `sys_permalinks` WHERE `permalink`='m/gphotos/';

-- top menu
SET @iCatRoot := (SELECT `Parent` FROM `sys_menu_top` WHERE `Name` = 'Group View Comments' LIMIT 1);
SET @sCatRootLinks := (SELECT `Link` FROM `sys_menu_top` WHERE `ID` = @iCatRoot LIMIT 1);
UPDATE `sys_menu_top` SET `Link` = TRIM(TRAILING '|modules/?r=gphotos/group/' FROM @sCatRootLinks) WHERE `sys_menu_top`.`ID` = @iCatRoot LIMIT 1 ;

DELETE FROM `sys_menu_top` WHERE `Name` = 'Group Photo Album View';

-- comments objects
DELETE FROM `sys_objects_cmts` WHERE `TableCmts`='[db_prefix]_cmts';
DELETE FROM `sys_objects_cmts` WHERE `TableCmts`='[db_prefix]_img_cmts';

-- vote objects
DELETE FROM `sys_objects_vote` WHERE `ObjectName` = '[db_prefix]_img' AND `TableRating`='[db_prefix]_img_rating';

-- privacy
DELETE FROM `sys_privacy_actions` WHERE `module_uri`='gphotos';

-- chart
DELETE FROM `sys_objects_charts` WHERE `object` = '[db_prefix]';

-- Membership
DELETE `sys_acl_actions`, `sys_acl_matrix` FROM `sys_acl_actions`, `sys_acl_matrix` WHERE `sys_acl_matrix`.`IDAction` = `sys_acl_actions`.`ID` AND `sys_acl_actions`.`Name` IN('agpalbums feature');
DELETE FROM `sys_acl_actions` WHERE `Name` IN('agpalbums feature');

-- group wall handler
-- DELETE FROM `a_gwall_handlers` WHERE `alert_unit` = 'agrp_palbums';
-- DELETE FROM `sys_alerts` WHERE `unit` = 'agrp_palbums';