<?php

$sLangCategory = 'CXIPMods Library';

$aLangContent = array(
    '_cxip_Library' => 'Library',
    '_cxip_library_text' => 'Hello World',
    '_cxipmods_library_menu_main' => 'Library',
    '_cxipmods_library' => 'Library',
    '_cxipmods_library_view' => 'Library',
    '_cxipmods_library_home' => 'Library',
    '_cxipmods_library_block_photos' => 'My Photos',
    '_cxipmods_library_block_files' => 'My Files',
    '_cxipmods_library_block_videos' => 'My Videos',
    '_cxipmods_library_block_sounds' => 'My Sounds',
    '_cxipmods_library_block_reports' => 'My Reports',
    '_cxipmods_library_block_group_photos' => 'Photo Albums of My Groups',
    '_cxipmods_library_block_group_files' => 'File Albums of My Groups',
    '_cxipmods_library_block_group_videos' => 'Video Albums of My Groups',
    '_cxipmods_library_block_group_sounds' => 'Sound Albums of My Groups',
    '_cxipmods_library_block_group_reports' => 'Reports of My Groups',
    '_cxipmods_library_block_info' => 'Info',
    '_cxipmods_library_last_post_by' => 'Last Post By: ',
);