<?php
/***************************************************************************
*                            Dolphin Smart Community Builder
*                              -------------------
*     begin                : Mon Mar 23 2006
*     copyright            : (C) 2007 BoonEx Group
*     website              : http://www.boonex.com
* This file is part of Dolphin - Smart Community Builder
*
* Dolphin is free software; you can redistribute it and/or modify it under
* the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 2 of the
* License, or  any later version.
*
* Dolphin is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
* without even the implied warranty of  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
* You should have received a copy of the GNU General Public License along with Dolphin,
* see license.txt file; if not, write to marketing@boonex.com
***************************************************************************/

bx_import('BxDolTwigModuleDb');
bx_import('BxDolAlbums');
bx_import('BxDolFilesUploader');

/*
 * Groups module Data
 */
class BxGroupsDb extends BxDolTwigModuleDb {	

	/*
	 * Constructor.
	 */
	function BxGroupsDb(&$oConfig) {
        parent::BxDolTwigModuleDb($oConfig);
 
		$this->_oConfig = $oConfig;
 
		/********Sponsor ************/
        $this->_sTableSponsor = 'sponsor_main';
        $this->_sSponsorPrefix = 'bx_groups_sponsor';
        $this->_sTableSponsorMediaPrefix = 'sponsor_';

		/********Blog ************/
        $this->_sTableBlog = 'blog_main';
        $this->_sBlogPrefix = 'bx_groups_blog';
        $this->_sTableBlogMediaPrefix = 'blog_';

		/********Event ************/
        $this->_sTableEvent = 'event_main';
        $this->_sEventPrefix = 'bx_groups_event';
        $this->_sTableEventMediaPrefix = 'event_';
 
		/********Venue ************/
        $this->_sTableVenue = 'venue_main';
        $this->_sVenuePrefix = 'bx_groups_venue';
        $this->_sTableVenueMediaPrefix = 'venue_';

		/********News ************/
        $this->_sTableNews = 'news_main';
        $this->_sNewsPrefix = 'bx_groups_news';
        $this->_sTableNewsMediaPrefix = 'news_';
 
        $this->_sTableMain = 'main';
        $this->_sTableMediaPrefix = '';
        $this->_sFieldId = 'id';
        $this->_sFieldAuthorId = 'author_id';
        $this->_sFieldUri = 'uri';
        $this->_sFieldTitle = 'title';
        $this->_sFieldDescription = 'desc';
        $this->_sFieldTags = 'tags';
        $this->_sFieldThumb = 'thumb';
        $this->_sFieldStatus = 'status';
        $this->_sFieldFeatured = 'featured';
        $this->_sFieldCreated = 'created';
        $this->_sFieldJoinConfirmation = 'join_confirmation';
        $this->_sFieldFansCount = 'fans_count';
        $this->_sTableFans = 'fans';
        $this->_sTableAdmins = 'admins';
        $this->_sFieldAllowViewTo = 'allow_view_group_to';
	}

    function deleteEntryByIdAndOwner ($iId, $iOwner, $isAdmin) {
        if ($iRet = parent::deleteEntryByIdAndOwner ($iId, $iOwner, $isAdmin)) {
            $this->query ("DELETE FROM `" . $this->_sPrefix . "fans` WHERE `id_entry` = $iId");
            $this->query ("DELETE FROM `" . $this->_sPrefix . "admins` WHERE `id_entry` = $iId");
            $this->deleteEntryMediaAll ($iId, 'images');
            $this->deleteEntryMediaAll ($iId, 'videos');
            $this->deleteEntryMediaAll ($iId, 'sounds');
            $this->deleteEntryMediaAll ($iId, 'files');

			$this->removeEntryYoutube($iId); 
 
            $this->query ("DELETE FROM `" . $this->_sPrefix . "featured_orders` WHERE `buyer_id` = $iOwner"); 

        }
        return $iRet;
    }
  
	function isValidInvite($iEntryId, $sCode=''){
  
		return (int)$this->getOne ("SELECT COUNT(`id`) FROM `" . $this->_sPrefix . "invite` WHERE `id_entry` = '$iEntryId' AND `code` = '$sCode'");
	}

	function isExistsInvite($iEntryId, $iProfileId=0){
 
		return (int)$this->getOne ("SELECT COUNT(`id`) FROM `" . $this->_sPrefix . "invite` WHERE `id_entry` = '$iEntryId' AND `id_profile` = '$iProfileId'");
	}

	function removeInvite($iEntryId, $iProfileId){
		$this->query ("DELETE FROM `" . $this->_sPrefix . "invite` WHERE `id_entry` = '$iEntryId' AND `id_profile` = '$iProfileId'");
	}

	function addInvite($iEntryId, $iProfileId=0){ 
		if(!$this->isExistsInvite($iEntryId, $iProfileId)){
			$sInviteCode = $this->_getCode();

			$this->query ("INSERT INTO `" . $this->_sPrefix . "invite` SET `code` = '$sInviteCode' ,`id_entry` = '$iEntryId', `id_profile` = '$iProfileId'");

			return $sInviteCode;
		}
	}

	function _getCode() {
		list($fMilliSec, $iSec) = explode(' ', microtime());
		$fSeed = (float)$iSec + ((float)$fMilliSec * 100000);
		srand($fSeed);

		$sResult = '';
		for($i=0; $i < 16; ++$i) {
			switch(rand(1,2)) {
				case 1: 
					$c = chr(rand(ord('A'),ord('Z')));
					break;
				case 2: 
					$c = chr(rand(ord('0'),ord('9')));
					break;
			}
			$sResult .= $c;
		}
		return $sResult;
	}
 
 	function getStateCount($sState){
		
		if (!$GLOBALS['logged']['admin']){ 
			if ($GLOBALS['logged']['member']){ 
				$aProfile = getProfileInfo($_COOKIE['memberID']); 
				require_once(BX_DIRECTORY_PATH_INC . 'membership_levels.inc.php');
				$aMembershipInfo = getMemberMembershipInfo($_COOKIE['memberID']); 
				$iMembershipId = $aMembershipInfo['ID']; 

			    $sExtraCheck = " AND `group_membership_view_filter` IN ('', '$iMembershipId')"; 
			}else{
				$sExtraCheck = "AND `group_membership_view_filter`=''";
			}
		}
 
		return  $this->getOne("SELECT COUNT(`id`) FROM `" . $this->_sPrefix . "main` WHERE  `state` = '$sState'  AND `status`='approved' {$sExtraCheck}"); 
	}

 	function getStateName($sCountry, $sState=''){
		$sState = $this->getOne("SELECT `State` FROM `States` WHERE `CountryCode`='{$sCountry}' AND `StateCode`='{$sState}' LIMIT 1");
		
		return $sState;
	}

	function getStateOptions($sCountry='') {
		$aStates = $this->getStateArray ($sCountry);

		foreach($aStates as $aEachCode=>$aEachState){ 
			$sOptions .= "<option value='{$aEachCode}'>{$aEachState}</option>";
		}

		return $sOptions;
	}

 	function getStateArray($sCountry=''){
 
		$aStates = array();
		$aDbStates = $this->getAll("SELECT * FROM `States` WHERE `CountryCode`='{$sCountry}'  ORDER BY `State` ");
		
		$aStates[] = '';
		foreach ($aDbStates as $aEachState){
			$sState = $aEachState['State'];
			$sStateCode = $aEachState['StateCode'];
			
			$aStates[$sStateCode] = $sState;
  		} 
		return $aStates;
	}
 
	function getCategories($sType)
	{ 
 		$aAllEntries = $this->getAll("SELECT `Category` FROM `sys_categories` WHERE `Type` = '{$sType}' AND `Status`='active' AND Owner=0 ORDER BY `Category`"); 
		
		return $aAllEntries; 
	}

	function getLocalCategoryCount($sType, $sCategory, $sCountry='', $sState=''){
  
		if($sCountry)
			$sExtraCheck .= " AND `country`='$sCountry'";

		if($sState)
			$sExtraCheck .= " AND `state`='$sState'";
 
		$sCategory = process_db_input($sCategory); 
 
		$iNumCategory = $this->getOne("SELECT count(`" . $this->_sPrefix . "main`.`id`) FROM `" . $this->_sPrefix . "main`  inner JOIN `sys_categories` ON `sys_categories`.`ID`=`" . $this->_sPrefix . "main`.`id` WHERE `sys_categories`.`Category` IN('{$sCategory}') AND `sys_categories`.`Type` = '{$sType}' AND `" . $this->_sPrefix . "main`.`status`='approved' {$sExtraCheck}");
		
		return $iNumCategory;
	}
  
	function getCategoryCount($sType,$sCategory)
	{ 
		$sCategory = process_db_input($sCategory); 
		$iNumCategory = $this->getOne("SELECT count(`" . $this->_sPrefix . "main`.`id`) FROM `" . $this->_sPrefix . "main`  inner JOIN `sys_categories` ON `sys_categories`.`ID`=`" . $this->_sPrefix . "main`.`id` WHERE 1 AND  `sys_categories`.`Category` IN('{$sCategory}') AND `sys_categories`.`Type` = '{$sType}' AND `" . $this->_sPrefix . "main`.`status`='approved'"); 
		
		return $iNumCategory;
	}

	function flagActivity($sType, $iEntryId, $iProfileId, $aParams=array()){
 
		if(!$iEntryId)
			return;

		$aDataEntry = $this->getEntryById($iEntryId);
		
		if( !($aDataEntry[$this->_sFieldAllowViewTo]==BX_DOL_PG_ALL || $aDataEntry[$this->_sFieldAllowViewTo]==BX_DOL_PG_MEMBERS) )
			return;

		switch($sType){ 
			case 'mark_as_featured':
				foreach($aParams as $sKey=>$iValue){ 
					if($sKey=='Featured'){
						if($iValue)
							$sType = "unfeatured";
						else
							$sType = "featured"; 
					}
				}
			break; 
		}

		$aTypes = array(
			'add' => '_bx_groups_feed_post',
			'delete' => '_bx_groups_feed_delete',
			'change' => '_bx_groups_feed_post_change',
			'join' => '_bx_groups_feed_join',
			'unjoin' => '_bx_groups_feed_unjoin',
			'remove' => '_bx_groups_feed_remove',
			'rate' => '_bx_groups_feed_rate',
			'commentPost' => '_bx_groups_feed_comment',
			'featured' => '_bx_groups_feed_featured',
			'unfeatured' => '_bx_groups_feed_unfeatured',
			'makeAdmin' => '_bx_groups_feed_make_admin',
			'removeAdmin' => '_bx_groups_feed_remove_admin'  
		);
   
		$aDataEntry = $this->getEntryById($iEntryId);
		
		$sProfileNick = getNickName($iProfileId);
		$sProfileLink = getProfileLink($iProfileId);
		$sGroupUri = $aDataEntry['uri'];
		$sGroupTitle = process_db_input($aDataEntry['title']);
		$sGroupUrl = BX_DOL_URL_ROOT . $this->_oConfig->getBaseUri() . 'view/' . $sGroupUri;
	
		$sParams = "profile_id|{$iProfileId};profile_link|{$sProfileLink};profile_nick|{$sProfileNick};entry_url|{$sGroupUrl};entry_title|{$sGroupTitle}";

		$sLangKey = $aTypes[$sType];
 
		$this->query("INSERT INTO bx_groups_activity(`group_id`,`lang_key`,`params`,`type`) VALUES ($iEntryId,'$sLangKey','$sParams','$sType')");

	}

	function getActivityFeed($iLimit=5){
		$iLimit = ($iLimit) ? $iLimit : 5;
/*
		if(getLoggedId()){ 
			$sSQLExtra = " m.`".$this->_sFieldAllowViewTo."` = '". BX_DOL_PG_MEMBERS ."'";
		}
*/
		return $this->getAll("SELECT a.`group_id`,a.`lang_key`,a.`params`,a.`type`, UNIX_TIMESTAMP(a.`date`) AS `date` FROM `" . $this->_sPrefix . "activity` a INNER JOIN `" . $this->_sPrefix . "main` m ON a.`group_id`=m.`id` WHERE m.`".$this->_sFieldAllowViewTo."` = '". BX_DOL_PG_ALL ."' {$sSQLExtra} ORDER BY a.`date` DESC LIMIT $iLimit"); 
	}
  
	function getLatestComments($iLimit=5){

		$iLimit = ($iLimit) ? $iLimit : 5;

		return $this->getAll("SELECT `cmt_object_id`,`cmt_author_id`,`cmt_text`, UNIX_TIMESTAMP(`cmt_time`) AS `date` FROM `" . $this->_sPrefix . "cmts` cmt INNER JOIN `" . $this->_sPrefix . "main` m  ON cmt.`cmt_object_id`= m.`id` WHERE m.`allow_view_group_to` = '". BX_DOL_PG_ALL ."' AND `cmt_text` NOT LIKE '<object%' ORDER BY `cmt_time` DESC LIMIT $iLimit"); 
	}  

	function getLatestForumPosts($iLimit=5){

		$iLimit = ($iLimit) ? $iLimit : 5;
 
		return $this->getAll("SELECT e.`title`, f.`forum_uri`, p.`user`, p.`post_text`, t.`topic_uri`, t.`topic_title`,p.`when` FROM `" . $this->_sPrefix . "forum` f, `" . $this->_sPrefix . "forum_topic` t, `" . $this->_sPrefix . "forum_post` p, `" . $this->_sPrefix . "main` e WHERE p.`topic_id`=t.`topic_id` AND t.`forum_id`=f.`forum_id` AND e.`id`=f.`entry_id` AND e.`allow_view_group_to`='". BX_DOL_PG_ALL ."'  ORDER BY  p.`when` LIMIT $iLimit");
	}  

	function getItemForumPosts($iLimit=5, $iEntryId=0){

		$iEntryId = (int)$iEntryId;

		if($iEntryId)
			$sQueryId = "t.`forum_id`=$iEntryId AND ";

		return $this->getAll("SELECT f.`forum_uri`, p.`user`, p.`post_text`, t.`topic_uri`, t.`topic_title`, p.`when` FROM `" . $this->_sPrefix . "forum` f, `" . $this->_sPrefix . "forum_topic` t, `" . $this->_sPrefix . "forum_post` p, `" . $this->_sPrefix . "main` e WHERE {$sQueryId}  p.`topic_id`=t.`topic_id` AND t.`forum_id`=f.`forum_id` AND e.`id`=f.`entry_id` ORDER BY  p.`when` LIMIT $iLimit"); 
	}  
 

	/***** SPONSOR **************************************/
    function getSponsorEntryById($iId) {
         return $this->getRow ("SELECT * FROM `" . $this->_sPrefix . $this->_sTableSponsor . "` WHERE `id` = $iId LIMIT 1");
    }
 
    function getSponsorEntryByUri($sUri) {
         return $this->getRow ("SELECT * FROM `" . $this->_sPrefix . $this->_sTableSponsor . "` WHERE `uri` = '$sUri' LIMIT 1");
    }
 
    function deleteSponsorByIdAndOwner ($iId, $iGroupId,  $iOwner, $isAdmin) {
        $sWhere = '';
        if (!$isAdmin) 
            $sWhere = " AND `author_id` = '$iOwner' ";
        if (!($iRet = $this->query ("DELETE FROM `" . $this->_sPrefix . $this->_sTableSponsor . "` WHERE `id` = $iId AND `group_id`=$iGroupId $sWhere LIMIT 1")))
            return false;
        $this->query ("DELETE FROM `" . $this->_sPrefix . $this->_sTableSponsorMediaPrefix . "images` WHERE `entry_id` = $iId");
  
        return true;
    } 

    function deleteSponsors ($iGroupId,  $iOwner, $isAdmin) {
        $sWhere = '';
        if (!$isAdmin) 
            $sWhere = " AND `author_id` = '$iOwner' ";
       
		$aSponsor = $this->getAllSubItems('sponsor', $iGroupId);

		if (!($iRet = $this->query ("DELETE FROM `" . $this->_sPrefix . $this->_sTableSponsor . "` WHERE `group_id`=$iGroupId $sWhere")))
            return false;

		foreach($aSponsor as $aEachSponsor){
			
			$iId = (int)$aEachSponsor['id'];

			$this->query ("DELETE FROM `" . $this->_sPrefix . $this->_sTableSponsorMediaPrefix . "images` WHERE `entry_id` = $iId");
 		}

        return true;
    }

	/***** BLOG **************************************/
    function getBlogEntryById($iId) {
         return $this->getRow ("SELECT * FROM `" . $this->_sPrefix . $this->_sTableBlog . "` WHERE `id` = $iId LIMIT 1");
    }
 
    function getBlogEntryByUri($sUri) {
         return $this->getRow ("SELECT * FROM `" . $this->_sPrefix . $this->_sTableBlog . "` WHERE `uri` = '$sUri' LIMIT 1");
    }
 
    function deleteBlogByIdAndOwner ($iId, $iGroupId,  $iOwner, $isAdmin) {
        $sWhere = '';
        if (!$isAdmin) 
            $sWhere = " AND `author_id` = '$iOwner' ";
        if (!($iRet = $this->query ("DELETE FROM `" . $this->_sPrefix . $this->_sTableBlog . "` WHERE `id` = $iId AND `group_id`=$iGroupId $sWhere LIMIT 1")))
            return false;
        $this->query ("DELETE FROM `" . $this->_sPrefix . $this->_sTableBlogMediaPrefix . "images` WHERE `entry_id` = $iId");
  
        return true;
    } 

    function deleteBlogs ($iGroupId,  $iOwner, $isAdmin) {
        $sWhere = '';
        if (!$isAdmin) 
            $sWhere = " AND `author_id` = '$iOwner' ";
       
		$aBlog = $this->getAllSubItems('blog', $iGroupId);

		if (!($iRet = $this->query ("DELETE FROM `" . $this->_sPrefix . $this->_sTableBlog . "` WHERE `group_id`=$iGroupId $sWhere")))
            return false;

		foreach($aBlog as $aEachBlog){
			
			$iId = (int)$aEachBlog['id'];

			$this->query ("DELETE FROM `" . $this->_sPrefix . $this->_sTableBlogMediaPrefix . "images` WHERE `entry_id` = $iId");
 		}

        return true;
    }
	
	/***** VENUE **************************************/
    function getVenueEntryById($iId) {
         return $this->getRow ("SELECT * FROM `" . $this->_sPrefix . $this->_sTableVenue . "` WHERE `id` = $iId LIMIT 1");
    }
 
    function getVenueEntryByUri($sUri) {
         return $this->getRow ("SELECT * FROM `" . $this->_sPrefix . $this->_sTableVenue . "` WHERE `uri` = '$sUri' LIMIT 1");
    }
 
    function deleteVenueByIdAndOwner ($iId, $iGroupId,  $iOwner, $isAdmin) {
        $sWhere = '';
        if (!$isAdmin) 
            $sWhere = " AND `author_id` = '$iOwner' ";
        if (!($iRet = $this->query ("DELETE FROM `" . $this->_sPrefix . $this->_sTableVenue . "` WHERE `id` = $iId AND `group_id`=$iGroupId $sWhere LIMIT 1")))
            return false;
        $this->query ("DELETE FROM `" . $this->_sPrefix . $this->_sTableVenueMediaPrefix . "images` WHERE `entry_id` = $iId");
  
        return true;
    } 
 
    function deleteVenues ($iGroupId,  $iOwner, $isAdmin) {
        $sWhere = '';
        if (!$isAdmin) 
            $sWhere = " AND `author_id` = '$iOwner' ";
       
		$aVenue = $this->getAllSubItems('venue', $iGroupId);

		if (!($iRet = $this->query ("DELETE FROM `" . $this->_sPrefix . $this->_sTableVenue . "` WHERE `group_id`=$iGroupId $sWhere")))
            return false;

		foreach($aVenue as $aEachVenue){
			
			$iId = (int)$aEachVenue['id'];

			$this->query ("DELETE FROM `" . $this->_sPrefix . $this->_sTableVenueMediaPrefix . "images` WHERE `entry_id` = $iId");
 		}

        return true;
    }  


	/***** NEWS **************************************/
    function getNewsEntryById($iId) {
         return $this->getRow ("SELECT * FROM `" . $this->_sPrefix . $this->_sTableNews . "` WHERE `id` = $iId LIMIT 1");
    }
 
    function getNewsEntryByUri($sUri) {
         return $this->getRow ("SELECT * FROM `" . $this->_sPrefix . $this->_sTableNews . "` WHERE `uri` = '$sUri' LIMIT 1");
    }
 
    function deleteNewsByIdAndOwner ($iId, $iGroupId,  $iOwner, $isAdmin) {
        $sWhere = '';
        if (!$isAdmin) 
            $sWhere = " AND `author_id` = '$iOwner' ";
        if (!($iRet = $this->query ("DELETE FROM `" . $this->_sPrefix . $this->_sTableNews . "` WHERE `id` = $iId AND `group_id`=$iGroupId $sWhere LIMIT 1")))
            return false;
        $this->query ("DELETE FROM `" . $this->_sPrefix . $this->_sTableNewsMediaPrefix . "images` WHERE `entry_id` = $iId");
  
        return true;
    }
	
	function deleteNews ($iGroupId,  $iOwner, $isAdmin) {
        $sWhere = '';
        if (!$isAdmin) 
            $sWhere = " AND `author_id` = '$iOwner' ";
       
		$aNews = $this->getAllSubItems('news', $iGroupId);

		if (!($iRet = $this->query ("DELETE FROM `" . $this->_sPrefix . $this->_sTableNews . "` WHERE `group_id`=$iGroupId $sWhere")))
            return false;

		foreach($aNews as $aEachNews){
			
			$iId = (int)$aEachNews['id'];

			$this->query ("DELETE FROM `" . $this->_sPrefix . $this->_sTableNewsMediaPrefix . "images` WHERE `entry_id` = $iId");
 		}

        return true;
    }  

	/***** EVENT **************************************/
    function getEventEntryById($iId) {
		$iId = (int)$iId;

         return $this->getRow ("SELECT * FROM `" . $this->_sPrefix . $this->_sTableEvent . "` WHERE `id` = $iId LIMIT 1");
    }
 
    function getEventEntryByUri($sUri) {
         return $this->getRow ("SELECT * FROM `" . $this->_sPrefix . $this->_sTableEvent . "` WHERE `uri` = '$sUri' LIMIT 1");
    }
 
    function deleteEventByIdAndOwner ($iId, $iGroupId,  $iOwner, $isAdmin) {
        $sWhere = '';
        if (!$isAdmin) 
            $sWhere = " AND `author_id` = '$iOwner' ";
        if (!($iRet = $this->query ("DELETE FROM `" . $this->_sPrefix . $this->_sTableEvent . "` WHERE `id` = $iId AND `group_id`=$iGroupId $sWhere LIMIT 1")))
            return false;
        $this->query ("DELETE FROM `" . $this->_sPrefix . $this->_sTableEventMediaPrefix . "images` WHERE `entry_id` = $iId");
  
        return true;
    } 

    function deleteEvents ($iGroupId,  $iOwner, $isAdmin) {
        $sWhere = '';
        if (!$isAdmin) 
            $sWhere = " AND `author_id` = '$iOwner' ";
       
		$aEvents = $this->getAllSubItems('event', $iGroupId);

		if (!($iRet = $this->query ("DELETE FROM `" . $this->_sPrefix . $this->_sTableEvent . "` WHERE `group_id`=$iGroupId $sWhere")))
            return false;

		foreach($aEvents as $aEachEvent){
			
			$iId = (int)$aEachEvent['id'];

			$this->query ("DELETE FROM `" . $this->_sPrefix . $this->_sTableEventMediaPrefix . "files` WHERE `entry_id` = $iId");
			$this->query ("DELETE FROM `" . $this->_sPrefix . $this->_sTableEventMediaPrefix . "images` WHERE `entry_id` = $iId");
			$this->query ("DELETE FROM `" . $this->_sPrefix . $this->_sTableEventMediaPrefix . "videos` WHERE `entry_id` = $iId");
			$this->query ("DELETE FROM `" . $this->_sPrefix . $this->_sTableEventMediaPrefix . "sounds` WHERE `entry_id` = $iId"); 
		}

        return true;
    } 


	function getAllSubItems($sSubItem, $iGroupId){
		$aSubItems = array();
		$iGroupId = (int)$iGroupId;

		switch($sSubItem){
			case 'event':
				$aSubItems = $this->getAll ("SELECT `id` FROM `" . $this->_sPrefix . $this->_sTableEvent . "` WHERE `group_id`=$iGroupId");
			break;
			case 'news':
				$aSubItems = $this->getAll ("SELECT `id` FROM `" . $this->_sPrefix . $this->_sTableNews . "` WHERE `group_id`=$iGroupId");
			break;
 			case 'venue':
				$aSubItems = $this->getAll ("SELECT `id` FROM `" . $this->_sPrefix . $this->_sTableVenue . "` WHERE `group_id`=$iGroupId");
			break; 
			case 'blog':
				$aSubItems = $this->getAll ("SELECT `id` FROM `" . $this->_sPrefix . $this->_sTableBlog . "` WHERE `group_id`=$iGroupId");
			break; 
			case 'sponsor':
				$aSubItems = $this->getAll ("SELECT `id` FROM `" . $this->_sPrefix . $this->_sTableSponsor . "` WHERE `group_id`=$iGroupId");
			break; 
		}
 
		return $aSubItems;	
	}
   

	//BEGIN RSS 
	function getRss($iEntryId, $iLimit=0){

		if($iLimit)
			$sQuery = "LIMIT 0, {$iLimit}";

		return $this->getAll("SELECT `id`, `id_entry`, `url`, `name` FROM `" . $this->_sPrefix . "rss` WHERE `id_entry`={$iEntryId} {$sQuery}");
	}

 	function addRss($iEntryId){
		if(is_array($_POST['rsslink'])){ 
			foreach($_POST['rsslink'] as $iKey=>$sValue){
			
				$sRssLink = process_db_input($sValue);
				$sRssName = process_db_input($_POST['rsscaption'][$iKey]);
	 
				if(trim($sRssLink)){  
					$this->query("INSERT INTO `" . $this->_sPrefix . "rss` SET `id_entry`=$iEntryId, `url`='$sRssLink', `name`='$sRssName'");
				}
			} 
		}
	}

	function removeEntryRss($iEntryId){ 
	   
		$this->query("DELETE FROM `" . $this->_sPrefix . "rss` WHERE `id_entry`='$iEntryId'");   
	}

	function removeRss($iEntryId, $iFeedId){ 
	   
		$this->query("DELETE FROM `" . $this->_sPrefix . "rss` WHERE `id_entry`='$iEntryId' AND `id`='$iFeedId'");   
	}

    function getRssIds ($iEntryId) {
		return $this->getPairs ("SELECT `id` FROM `" . $this->_sPrefix . "rss`  WHERE `id_entry` = '$iEntryId'", 'id', 'id');
    }
	//END RSS
  
	/*featured functions*/ 
    function getCurrencySign($sKey){ 
		
		$sKey = ($sKey) ? $sKey : $this->_oConfig->_sCurrencySign;
		
		return $sKey;  
    }

	function setItemStatus($iItemId, $sStatus) {
		
 		 $this->query("UPDATE `" . $this->_sPrefix . "main` SET `status`='$sStatus' WHERE `id`='$iItemId'"); 
	}
  
   function generateFeaturedPaymentUrl($iGroupId, $iQuantity, $fCost) {
        $fCost   = number_format($fCost, 2);
 
        return  $this ->_oConfig->getPurchaseBaseUrl() . '?cmd=_xclick'
			    . '&rm=2'
                . '&no_shipping=0'      
				. '&currency_code='.$this->_oConfig->getPurchaseCurrency()
                . '&return=' .  $this->_oConfig->getFeaturedCallbackUrl() . $iGroupId
				. '&business='  . getParam('bx_groups_paypal_email')
                . '&item_name=' . getParam('bx_groups_featured_purchase_desc')
                . '&item_number=' . $iGroupId  
                . '&amount=' . $fCost;
    }
 
    function saveFeaturedTransactionRecord($iBuyerId, $iGroupId, $iQuantity, $fPrice, $sTransId, $sTransType) {
        $iBuyerId    = (int)$iBuyerId;
        $iGroupId  = (int)$iGroupId; 
        $iQuantity  = (int)$iQuantity; 
		$iTime = time();

        $bProcessed = $this->query("INSERT INTO `" . $this->_sPrefix . "featured_orders` 
							SET `buyer_id` = {$iBuyerId}, 
							`price` = '{$fPrice}',
							`days` = {$iQuantity},
							`item_id` =  {$iGroupId},
 							`trans_id` = '{$sTransId}',  
							`trans_type` = '{$sTransType}', 
  							`created` = $iTime  
        "); 

 		$iOrderId = $this->lastId();

		if($iOrderId){
			$this->alertOnAction('bx_groups_featured_admin_notify', $iGroupId, $iBuyerId, $iQuantity, true);

			$this->alertOnAction('bx_groups_featured_buyer_notify', $iGroupId, $iBuyerId, $iQuantity);
		}

		return $iOrderId; 
    }

    function updateFeaturedEntryExpiration($iEntryId, $iDays) { 

		 $SECONDS_IN_DAY = 86400; 
		 $iCreated = time();
 
		 $aDataEntry = $this->getEntryById($iEntryId);
		 $iExistExpireDate = $aDataEntry['featured_expiry_date'];
		
		 if($iExistExpireDate < $iCreated){ 
		     $iExpireDate = $iCreated + ($SECONDS_IN_DAY * $iDays);

			 $bProcessed = $this->query("UPDATE `" . $this->_sPrefix . "main` SET `featured`=1, `featured_expiry_date` = $iExpireDate, `featured_date`=$iCreated WHERE `id` = '{$iEntryId}'"); 
		 }else{
		     $iExpireDate = ($SECONDS_IN_DAY * $iDays);

			 $bProcessed = $this->query("UPDATE `" . $this->_sPrefix . "main` SET `featured`=1, `featured_expiry_date` = `featured_expiry_date` + $iExpireDate, `featured_date`=$iCreated  WHERE `id` = '{$iEntryId}'"); 
		 }
	
		 return $bProcessed;
	}


    function isExistFeaturedTransaction($iBuyerId, $sTransID) {
        $iBuyerId  = (int)$iBuyerId;
 
        return $this->getOne("SELECT COUNT(`trans_id`) FROM `" . $this->_sPrefix . "featured_orders` 
            WHERE `buyer_id` = {$iBuyerId} AND `trans_id` =  '{$sTransID}'  
        "); 
    }

	function processGroups(){ 
  
		$this->processFeaturedGroups();
	}
 
	function processFeaturedGroups(){
		
		if(getParam('bx_groups_buy_featured') != 'on')
			return;

		$iTime = time();
   
        $aGroups = $this->getAll("SELECT `id`, `author_id`, `featured`, `featured_expiry_date` FROM `" . $this->_sPrefix . "main` WHERE `featured`=1 AND `featured_expiry_date` <= $iTime"); 

		foreach($aGroups as $aEachList){

		    $iExistExpireDate = (int)$aEachList['featured_expiry_date']; 
		    if($iExistExpireDate==0)
				continue;

			$iGroupId = (int)$aEachList['id'];
			$iRecipientId = (int)$aEachList['author_id'];

			$this->alertOnAction('bx_groups_featured_expire_notify', $iGroupId, $iRecipientId  );
		
	        $this->query("UPDATE `" . $this->_sPrefix . "main` SET `featured`=0, `featured_expiry_date`=0, `featured_date`=0  WHERE `id`=$iGroupId"); 
		}

	}

	function alertOnAction($sTemplate, $iGroupId, $iRecipientId=0, $iDays=0, $bAdmin=false) {
	   
		$aPlus = array();

		if($iGroupId){
			$aDataEntry = $this->getEntryById($iGroupId);
			$aPlus['ListTitle'] = $aDataEntry['title']; 
			$aPlus['ListLink'] = BX_DOL_URL_ROOT . $this->_oConfig->getBaseUri() . 'view/' . $aDataEntry['uri']; 
		}

		if($iRecipientId){
			$aRecipient = getProfileInfo($iRecipientId); 
			$aPlus['NickName'] = $aRecipient['NickName']; 
			$aPlus['NickLink'] = getProfileLink($iRecipientId);
			$sNotifyEmail = $aRecipient['Email']; 
		}

		$aPlus['Days'] = $iDays; 
		$aPlus['SiteName'] = isset($GLOBALS['site']['title']) ? $GLOBALS['site']['title'] : getParam('site_title');
		$aPlus['SiteUrl'] = isset($GLOBALS['site']['url']) ;
 
		$oEmailTemplate = new BxDolEmailTemplates(); 

		$aTemplate = $oEmailTemplate->getTemplate($sTemplate, $iRecipientId);
		$sMessage = $aTemplate['Body'];
		$sSubject = $aTemplate['Subject'];   
		$sSubject = str_replace("<SiteName>", $aPlus['SiteName'], $sSubject);

		if($bAdmin){
			$sNotifyEmail = $GLOBALS['site']['email_notify'];
			$iRecipientId = 0;
		}
 
		sendMail($sNotifyEmail, $sSubject, $sMessage, $iRecipientId, $aPlus, 'html' );   
	}

	//BEGIN Youtube 
	function getYoutubeVideos($iEntryId, $iLimit=0){

		if($iLimit)
			$sQuery = "LIMIT 0, {$iLimit}";

		return $this->getAll("SELECT `id`, `id_entry`, `title`, `url` FROM `" . $this->_sPrefix . "youtube` WHERE `id_entry`={$iEntryId} {$sQuery}");
	}

 	function addYoutube($iEntryId, $videoGroupAlbumName = ''){
		if(is_array($_POST['video_link'])){ 
			foreach($_POST['video_link'] as $iKey=>$sValue){
			
				$sVideoLink = process_db_input($sValue);
 				$sVideoTitle = process_db_input($_POST['video_title'][$iKey]);
	 
				if(trim($sVideoLink)){  
					$this->query("INSERT INTO `" . $this->_sPrefix . "youtube` SET `id_entry`=$iEntryId, `url`='$sVideoLink', `title`='$sVideoTitle'");
				}

				//Nick
				$sVideoId = substr(trim($sVideoLink), -11);

				$aSiteInfo = getSiteInfo($sVideoLink, array(
            		'duration' => array(),
            		'thumbnailUrl' => array('tag' => 'link', 'content_attr' => 'href'),
        		));
				
				$owner = getLoggedId();
				$vidUri = uriGenerate($sVideoTitle, 'RayVideoFiles', 'Uri');
				$sTags = $aSiteInfo['keywords'];
				$sCat = "Groups";
				$sDesc = $aSiteInfo['description'];
				$sWhen = time();
				$sStatus = "approved";
				$sSource = "youtube";
				
				$this->query("INSERT INTO `RayVideoFiles` SET `Categories`='$sCat', `Title`='$sVideoTitle', `Uri`='$vidUri', `Tags`='$sTags', `Description`='$sDesc', `Date`='$sWhen', `Status`='$sStatus', `Source`='$sSource', `Video`='$sVideoId', `group_id`='$iEntryId'");
				
				$iId = $this->getOne("SELECT `ID` FROM `RayVideoFiles` ORDER BY `ID` DESC LIMIT 1");

				$sAlbum = new BxDolAlbums('bx_videos', 1); 
				$uploader = new BxDolFilesUploader();
            	$uploader->addObjectToAlbum($sAlbum, 'Group-Videos', $iId, false, 1, array('privacy' => 4));

				if ($videoGroupAlbumName) {
					$aFileInfo = array(
						'owner' => $owner,
						'title' => $sVideoTitle,
						'AssocID' => $iId);
					$meow = BxDolService::call('gphotos', 'add_file_to_db', array($videoGroupAlbumName, $aFileInfo), 'Uploaders');
				}
				//BxDolService::call('videos', 'accept_embed_file', array((int)$sVideoId), 'Uploader');
				//Nick

			} 
		}
	}

	function removeEntryYoutube($iEntryId){ 
	   
		$this->query("DELETE FROM `" . $this->_sPrefix . "youtube` WHERE `id_entry`='$iEntryId'");   
	}

	function removeYoutube($iEntryId, $iYoutubeId){ 
	   
		$this->query("DELETE FROM `" . $this->_sPrefix . "youtube` WHERE `id_entry`='$iEntryId' AND `id`='$iYoutubeId'");   
	}

    function getYoutubeIds ($iEntryId) {
		return $this->getPairs ("SELECT `id` FROM `" . $this->_sPrefix . "youtube`  WHERE `id_entry` = '$iEntryId'", 'id', 'id');
    }
	//END Youtube

    function removeAdmins ($iEntryId, $aProfileIds)
    {
        if (!$aProfileIds)
            return false;
        $s = implode (' OR `id_profile` = ', $aProfileIds);
 
        if ($this->_sTableAdmins)
            $this->query ("DELETE FROM `" . $this->_sPrefix . $this->_sTableAdmins . "` WHERE `id_entry` = '$iEntryId' AND `id_profile` = $s");
        return $iRet;
    }

    function isSubProfileFan($sTable, $iSubEntryId, $iProfileId, $isConfirmed) {
        $isConfirmed = $isConfirmed ? 1 : 0;

        $iEntryId = (int)$this->getOne ("SELECT `group_id` FROM `" . $this->_sPrefix . $sTable . "` WHERE `id` = '$iSubEntryId' LIMIT 1");
 
        return $this->getOne ("SELECT `when` FROM `" . $this->_sPrefix . $this->_sTableFans . "` WHERE `id_entry` = '$iEntryId' AND `id_profile` = '$iProfileId' AND `confirmed` = '$isConfirmed' LIMIT 1");
    }
 
    function isFan($iEntryId, $iProfileId, $isConfirmed)
    {
        $isConfirmed = $isConfirmed ? 1 : 0;
 
        return $this->getOne ("SELECT `when` FROM `" . $this->_sPrefix . $this->_sTableFans . "` WHERE `id_entry` = '$iEntryId' AND `id_profile` = '$iProfileId' AND `confirmed` = '$isConfirmed' LIMIT 1"); 
    }
 
	function getSubItemEntry($iObjectId, $sTable){
		$iObjectId = (int)$iObjectId;

       return $this->getRow("SELECT * FROM `" . $sTable . "` WHERE `id` = $iObjectId LIMIT 1"); 
	}

	function queueMessage($sEmail, $sSubject, $sMessage){
		$this->query("INSERT INTO `sys_sbs_queue`(`email`, `subject`, `body`) VALUES('" . $sEmail . "', '" . process_db_input($sSubject) . "', '" . process_db_input($sMessage) . "')"); 
	}

	function getModzzzNews($iId){
		return $this->getAll("SELECT * FROM `modzzz_news_main` WHERE `group_id`='$iId'"); 
	}
	//Nick
	function getAlbumTitlesById($iId, $aType = '') {
		if ($aType) {
			return $this->getAll("SELECT `Title` FROM `agrp_palbums_albums` WHERE `GroupID`='$iId' AND `Type`='$aType'"); 
		} else {
			return $this->getAll("SELECT `Title` FROM `agrp_palbums_albums` WHERE `GroupID`='$iId'"); 
		}
	}
	//Nick
	function getYoutubeInfo($fileID) {
		return $this->getRow("SELECT * FROM `RayVideoFiles` WHERE `ID` = '$fileId'");
	}

	//Nick
	function getGroupCount() {
		$count = (int)$this->getOne("SELECT `id` FROM `bx_groups_main` ORDER BY `id` DESC LIMIT 1");
		return $count;
	}

	//Nick
	function getUserGroupsById($iProfileId) {
		
		$oGroups = BxDolModule::getInstance('BxGroupsModule');
		$count = 100;
		$userGroups = array();

		$i = 0;

		array_push($userGroups, "None");
        for ($i = 0; $i < $count; $i++) {
            if ($oGroups->isFan($this->getEntryById($i), $iProfileId, true)) {
                $groupInfo = $this->getEntryById($i);
                array_push($userGroups, $groupInfo['title']);
            } 
        }


        return $userGroups;
	}
	//Nick
	function getUserGroupsInfoById($iProfileId) {

		$oGroups = BxDolModule::getInstance('BxGroupsModule');
		$count = 100;
		$userGroups = array();

		$i = 0;

		array_push($userGroups, "None");
        for ($i = 0; $i < $count; $i++) {
            if ($oGroups->isFan($this->getEntryById($i), $iProfileId, true)) {
                $groupInfo = $this->getEntryById($i);
                array_push($userGroups, $groupInfo);
            } 
        }


        return $userGroups;
	}

	//Nick
	function getReportThumbUrl($iId) {
		return $this->getOne("SELECT `thumbUrl` FROM `bx_groups_blog_main` WHERE `id` = '$iId'");
	}

	//Nick
	function addGroupMember($iGroupId, $iProfileId) {
		$iTime = time();
		$this->query("INSERT INTO `bx_groups_fans` VALUES ('$iGroupId', '$iProfileId', '$iTime', 1)");
	}

	//Nick
    function getGroupIdByTitle($title) {
        return $this->getOne("SELECT `id` FROM `bx_groups_main` WHERE `title` = '{$title}'");
    }

    function getGroupTitleByID($id) {
    	return $this->getOne("SELECT `title` FROM `bx_groups_main` WHERE `id` = '{$id}'");
    }

    function getOwnerById($id) {
    	return $this->getOne("SELECT `author_id` FROM `bx_groups_main` WHERE `id` = '{$id}'");
    }

    function addParentGroup($childGroupId, $parentGroupName) {

    	$parentGroupId = $this->getGroupIdByTitle($parentGroupName);
    	return $this->query("UPDATE `bx_groups_main` SET `parent_group` = '$parentGroupId' WHERE `id` = '$childGroupId'");
    }

    //Nick
    function movePostToGroup($iBlogPostID, $iGroupID) {

    	$fields = array();

    	$fields = $this->getAll("SELECT * FROM `bx_groups_blog_main` WHERE `id` = '$iBlogPostID'");

    	//$fields['group_id'] = $iGroupID;
    	$authorId = $fields[0]['author_id'];
    	$title = $this->escape($fields[0]['title'] . ' 1');
    	$uri = $fields[0]['uri'] . '-1';
    	$desc = $this->escape($fields[0]['desc']);
    	$status = "approved";
    	$thumb = $fields[0]['thumb'];
    	$created = $fields[0]['created'];
 		$thumbUrl = $fields[0]['thumbUrl'];

    	$query = "INSERT INTO `bx_groups_blog_main` VALUES (NULL, '$iGroupID', '$authorId', '$title', '$uri', '$desc', '$status', '$thumb', '$created', 0, 0, 0, 0, 0, 4, 4, 4, 'a', 'a', 'a', 'a', '$thumbUrl')";

    	return $this->query($query);
    }

}
