<?php
/***************************************************************************
*                            Dolphin Smart Community Builder
*                              -------------------
*     begin                : Mon Mar 23 2006
*     copyright            : (C) 2007 BoonEx Confession
*     website              : http://www.boonex.com
* This file is part of Dolphin - Smart Community Builder
*
* Dolphin is free software; you can redistribute it and/or modify it under
* the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 2 of the
* License, or  any later version.
*
* Dolphin is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
* without even the implied warranty of  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
* You should have received a copy of the GNU General Public License along with Dolphin,
* see license.txt file; if not, write to marketing@boonex.com
***************************************************************************/
 
bx_import ('BxDolFormMedia');
bx_import ('BxDolProfileFields');

class BxGPrivateFormAdd extends BxDolFormMedia {

    var $_oMain, $_oDb;
 
 
    function BxGPrivateFormAdd ($oMain, $iEntryId = 0) {

        $this->_oMain = $oMain;
        $this->_oDb = $oMain->_oDb;
 
		$aGroups = $this->_oDb->getFormGroups(); 
		$aGroups = array('' => _t('_Select')) + $aGroups; 

 
		$aCustomForm = array(

            'form_attrs' => array(
                'name'     => 'form_gprivate',
                'action'   => '',
                'method'   => 'post',
                'enctype' => 'multipart/form-data',
            ),      

            'params' => array (
                'db' => array(
                    'table' => 'modzzz_gprivate_main',
                    'key' => 'id', 
                    'submit_name' => 'submit_form',
                ),
            ),
                  
            'inputs' => array(

                'header_info' => array(
                    'type' => 'block_header',
                    'caption' => _t('_modzzz_gprivate_form_header_info')
                ),  
				'group_id' => array (
					'type' => 'select',
					'name' => 'group_id',
					'caption' => _t('_modzzz_gprivate_form_caption_group'),
					'values' => $aGroups,  
					'db' => array (
						'pass' => 'Xss', 
					),
				), 
                'Submit' => array (
                    'type' => 'submit',
                    'name' => 'submit_form',
                    'value' => _t('_Submit'),
                    'colspan' => false,
                ),                            
            ),            
        );
 
        parent::BxDolFormMedia ($aCustomForm);
    }

}
