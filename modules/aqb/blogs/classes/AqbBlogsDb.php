<?php
/***************************************************************************
* 
*     copyright            : (C) 2009 AQB Soft
*     website              : http://www.aqbsoft.com
*      
* IMPORTANT: This is a commercial product made by AQB Soft. It cannot be modified for other than personal usage.
* The "personal usage" means the product can be installed and set up for ONE domain name ONLY. 
* To be able to use this product for another domain names you have to order another copy of this product (license).
* 
* This product cannot be redistributed for free or a fee without written permission from AQB Soft.
* 
* This notice may not be removed from the source code.
* 
***************************************************************************/

bx_import('BxDolTwigModuleDb');

/*
 * Groups module Data
 */
class AqbBlogsDb extends BxDolTwigModuleDb
{
    /*
     * Constructor.
     */
    function AqbBlogsDb(&$oConfig)
    {
        parent::BxDolTwigModuleDb($oConfig);

        $this->_sTableMain = 'main';
        $this->_sTableMediaPrefix = '';
        $this->_sFieldId = 'id';
        $this->_sFieldAuthorId = 'author_id';
        $this->_sFieldUri = 'uri';
        $this->_sFieldTitle = 'title';
        $this->_sFieldDescription = 'desc';
        $this->_sFieldTags = 'tags';
        $this->_sFieldThumb = 'thumb';
        $this->_sFieldStatus = 'status';
        $this->_sFieldFeatured = 'featured';
        $this->_sFieldCreated = 'created';
        $this->_sFieldJoinConfirmation = 'join_confirmation';
        $this->_sFieldFansCount = 'likes_count';
        $this->_sTableFans = 'likes';
        $this->_sTableAdmins = 'admins';
        $this->_sFieldAllowViewTo = 'allow_view_to';
    }

    function createForum ($aDataEntry, $sUsername)
    {
    	return true;
    }
    function deleteForum ($iEntryId)
    {
    	return true;
    }    
    function getForumById ($iForumId)
    {
    	return array();
    }

    function getFullName($sName, $bAddPrefix = true)
    {
    	$sName = '_s' . str_replace(' ', '', ucwords(str_replace('_', ' ', $sName)));
    	return  ($bAddPrefix ? $this->_sPrefix : '') . $this->$sName;
    }

    function deleteEntryByIdAndOwner ($iId, $iOwner, $isAdmin)
    {
        if ($iRet = parent::deleteEntryByIdAndOwner ($iId, $iOwner, $isAdmin)) {
            $this->query ("DELETE FROM `" . $this->_sPrefix . $this->_sTableFans . "` WHERE `id_entry` = $iId");
            $this->query ("DELETE FROM `" . $this->_sPrefix . $this->_sTableAdmins . "` WHERE `id_entry` = $iId");
            $this->deleteEntryMediaAll ($iId, 'images');
            $this->deleteEntryMediaAll ($iId, 'videos');
            $this->deleteEntryMediaAll ($iId, 'sounds');
            $this->deleteEntryMediaAll ($iId, 'files');
        }
        return $iRet;
    }

    //Nick
    function getPostInfo($iPostID)
    {
        $sAllBlogPostInfoSQL = "SELECT `aqb_blogs_main`. * FROM `aqb_blogs_main` WHERE `aqb_blogs_main`.`id` = '{$iPostID}' LIMIT 1";

        $aAllBlogPostInfo = $this->getRow($sAllBlogPostInfoSQL);
        return $aAllBlogPostInfo;
    }
    //Nick
    function deletePost($iPostID)
    {
        $sDelSQL = "DELETE FROM `aqb_blogs_main` WHERE `aqb_blogs_main`.`id` = '{$iPostID}' LIMIT 1";
        $vSqlRes = db_res($sDelSQL);
        return $vSqlRes;
    }

    //Nick
    function getGroupIdByTitle($title) {
        return (int)$this->getOne("SELECT `id` FROM `bx_groups_main` WHERE `title` = '{$title}'");
    }

    //Nick
    function getGroupUriById($iId) {
        return $this->getOne("SELECT `uri` FROM `bx_groups_main` WHERE `id` = '{$iId}'");
    }


    //Nick
    function deleteComment($cmtId) {
        $sDelSQL = "DELETE FROM `aqb_blogs_cmts` WHERE `aqb_blogs_cmts`.`cmt_id` = '{$cmtId}' LIMIT 1";
        $vSqlRes = db_res($sDelSQL);
        return $vSqlRes;
    }

    //Nick
    function movePostToGroup($iBlogPostID, $iGroupID, $delete) {
        $postInfo = $this->getPostInfo($iBlogPostID);

        $caption = $postInfo['title'];
        $uri = $postInfo['uri'];
        $text = $this->escape($postInfo['desc']);
        $created = $postInfo['created'];
        $postStatus = "approved";
        $ownerId = $postInfo['author_id'];
        $comCount = $postInfo['comments_count'];
        $views = $postInfo['views'];
        $rate = $postInfo['rate'];
        $rateCount = $postInfo['rate_count'];
        $featured = $postInfo['featured'];
        $thumb = $postInfo['thumb'];
  
        $dupQuery = "SELECT * FROM `bx_groups_blog_main` WHERE `uri` = '$uri'";
        $isDuplicate = $this->getOne($dupQuery);

        $doop = 1;

        $oldUri = $uri;
        $oldCaption = $caption;
        while ($isDuplicate) {
            $uri = $oldUri . '-' . $doop;
            $caption = $oldCaption . ' ' . $doop;
            $dupQuery = "SELECT * FROM `bx_groups_blog_main` WHERE `uri` = '$uri'";
            $isDuplicate = $this->getOne($dupQuery);
            $doop++;
        }

        //$nextId = (int)$this->getOne("SELECT `id` FROM `bx_groups_blog_main` ORDER BY `id` DESC LIMIT 1");
        //$nextId++;

        /*if ($postInfo['thumb'] !== 0) {
            //$thumbUrl = BX_DOL_URL_ROOT . 'media/images/blog/big_' . $postInfo['PostPhoto'] . '.jpg';
            $thumb = $postInfo['thumb'];
            $sqlQuery = "INSERT INTO `bx_groups_blog_images` SET `entry_id` = '$nextId', `media_id` = '$thumb'";
            $boo = $this->query($sqlQuery);
        }*/

        $sqlQuery = "INSERT INTO `bx_groups_blog_main` SET `id` = NULL, `group_id` = '$iGroupID', `author_id` = '$ownerId', `title` = '$caption', `uri` = '$uri', `desc` = '$text', `status` = '$postStatus', `thumb` = '$thumb', `created` = '$created', `views` = '$views', `rate` = '$rate', `rate_count` = '$rateCount', `comments_count` = '$comCount', `featured` = '$featured', `allow_view_to` = 'f', `allow_comment_to` = 'f', `allow_rate_to` = 'f', `allow_upload_photos_to` = 'a', `allow_upload_videos_to` = 'a', `allow_upload_sounds_to` = 'a', `allow_upload_files_to` = 'a'";
        $success = $this->query($sqlQuery);

        if ($success) {
            $getCommentInfoQuery = "SELECT * FROM `aqb_blogs_cmts` WHERE `cmt_object_id` = '$iBlogPostID'";
            $comInfo = $this->getAll($getCommentInfoQuery);

            $i = 0;
            $IdPairs = array();
            for ($i = 0; $i < sizeof($comInfo); $i++) {
                $oldId = $comInfo[$i]['cmt_id'];

                $author = $comInfo[$i]['cmt_author_id'];
                $comParent = $comInfo[$i]['cmt_parent_id'];
                $comText = $this->escape($comInfo[$i]['cmt_text']);
                $comRate = $comInfo[$i]['cmt_rate'];
                $comRateCount = $comInfo[$i]['cmt_rate_count'];
                $comTime = $comInfo[$i]['cmt_time'];
                $comReplies = $comInfo[$i]['cmt_replies'];
                
                $nextComId = (int)$this->getOne("SELECT `cmt_id` FROM `bx_groups_blog_cmts` ORDER BY `cmt_id` DESC LIMIT 1");
                $nextComId++;

                $IdPairs[$i]['old_id'] = $oldId;
                $IdPairs[$i]['new_id'] = $nextComId;

                $newComParent = 0;

                if ($comParent > 0) {
                    $x = 0;
                    for ($x = 0; $x < sizeof($IdPairs); $x++) {
                        if ($comParent == $IdPairs[$x]['old_id']) {
                            $newComParent = $IdPairs[$x]['new_id'];
                        }
                    }//end for loop
                }

                $sqlCommentQuery = "INSERT INTO `bx_groups_blog_cmts` SET `cmt_id` = '$nextComId', `cmt_author_id` = '$author', `cmt_parent_id` = '$newComParent', `cmt_object_id` = '$nextId', `cmt_text` = '$comText', `cmt_rate` = '$comRate', `cmt_rate_count` = '$comRateCount', `cmt_time` = '$comTime', `cmt_replies` = '$comReplies'";
                $comSuccess = $this->query($sqlCommentQuery);

                if (!$comSuccess) {
                    return false;
                }
                if ($delete == true) {
                    $yup = $this->deleteComment($oldId);
                }
            }//end for loop
        } else {
            return false;
        }

        //transfer images, files, sounds, videos
        $this->transferMedia($iBlogPostID, $nextId);

        if ($delete == true) {
            $buh = $this->deletePost($iBlogPostID);
        }

        $groupUri = $this->getGroupUriById($iGroupID);

        $redir = BX_DOL_URL_ROOT . 'm/groups/view/' . $groupUri;

        //header('Location: ' . $redir);

        return true;
    }

    function transferMedia($iPostID, $newPostID) {

        $soundQuery = "SELECT `media_id` FROM `aqb_blogs_sounds` WHERE `entry_id` = '$iPostID'";
        $sounds = $this->getAll($soundQuery);

        for ($i = 0; $i < sizeof($sounds); $i++) {
            $mediaId = (int)$sounds[$i]['media_id'];
            if ($mediaId !== 0) {
                $success = $this->query("INSERT INTO `bx_groups_blog_sounds` SET `entry_id` = '$newPostID', `media_id` = '$mediaId'");
            }
        }

        $fileQuery = "SELECT `media_id` FROM `aqb_blogs_files` WHERE `entry_id` = '$iPostID'";
        $files = $this->getAll($fileQuery);

        for ($j = 0; $i < sizeof($files); $j++) {
            $mediaId = (int)$files[$j]['media_id'];
            if ($mediaId !== 0) {
                $success = $this->query("INSERT INTO `bx_groups_blog_files` SET `entry_id` = '$newPostID', `media_id` = '$mediaId'");
            }
        }

        $videoQuery = "SELECT `media_id` FROM `aqb_blogs_videos` WHERE `entry_id` = '$iPostID'";
        $videos = $this->getAll($videoQuery);

        for ($k = 0; $k < sizeof($videos); $k++) {
            $mediaId = (int)$videos[$k]['media_id'];
            if ($mediaId !== 0) {
                $success = $this->query("INSERT INTO `bx_groups_blog_videos` SET `entry_id` = '$newPostID', `media_id` = '$mediaId'");
            }
        }

        $imageQuery = "SELECT `media_id` FROM `aqb_blogs_images` WHERE `entry_id` = '$iPostID'";
        $images = $this->getAll($imageQuery);

        for ($l = 0; $l < sizeof($images); $l++) {
            $mediaId = (int)$images[$l]['media_id'];
            if ($mediaId !== 0) {
                $success = $this->query("INSERT INTO `bx_groups_blog_images` SET `entry_id` = '$newPostID', `media_id` = '$mediaId'");
            }
        }
    }
}
