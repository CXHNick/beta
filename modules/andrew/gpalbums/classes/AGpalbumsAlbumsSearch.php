<?php
/***************************************************************************
*
* IMPORTANT: This is a commercial product made by AndrewP. It cannot be modified for other than personal usage.
* The "personal usage" means the product can be installed and set up for ONE domain name ONLY.
* To be able to use this product for another domain names you have to order another copy of this product (license).
* This product cannot be redistributed for free or a fee without written permission from AndrewP.
* This notice may not be removed from the source code.
*
***************************************************************************/
bx_import('BxDolModule');
bx_import('BxTemplSearchResultText');

class AGpalbumsAlbumsSearch extends BxTemplSearchResultText {
    var $oModule;
    var $oTemplate;

    function AGpalbumsAlbumsSearch () {
        parent::BxTemplSearchResultText();

        $this->oModule = BxDolModule::getInstance('AGpalbumsModule');
        $this->oTemplate = $this->oModule->_oTemplate;

        // main settings
        $this->aCurrent = array(
            'name' => 'ayphotoalbum',
            'title' => '_gpa_main',
            'table' => 'agrp_palbums_albums',
            'ownFields' => array('ID', 'Owner', 'Title', 'Uri', 'When', 'Desc', 'Thumb', 'GroupID'),
            'searchFields' => array('Title', 'Desc'),
            'restriction' => array(
                'owner' => array('value'=>'', 'field'=>'Owner', 'operator'=>'='),
                'type' => array('value'=>'', 'field'=>'Type', 'operator'=>'='),
                'group_id'=> array('value'=>'', 'field'=>'GroupID', 'operator'=>'='),
                'id'=> array('value'=>'', 'field'=>'ID', 'operator'=>'='),
                'allow_view' => array('value'=>'', 'field'=>'AllowAlbumView', 'operator'=>'in', 'table'=> 'agrp_palbums_albums'),
            ),
            'paginate' => array('perPage' => 10, 'page' => 1, 'totalNum' => 10, 'totalPages' => 1),
            'sorting' => 'last'
        );
    }

    function _getPseud () {
        return array(
            'id' => 'ID',
            'owner_id' => 'Owner',
            'title' => 'Title',
            'uri' => 'Uri',
            'when' => 'When',
            'desc' => 'Desc',
        );
    }

    function displaySearchUnit($aData) {
        $iAlbumID = (int)$aData['id'];
        $bPossibleToView = $this->oModule->oPrivacy->check('album_view', $iAlbumID, $this->oModule->_iVisitorID);
        if (! $bPossibleToView) return;

        $iOwnerID = (int)$aData['owner_id'];
        $sOwnerLink = getProfileLink($iOwnerID);
        $sOwnerNick = getNickName($iOwnerID);
        $sWhen = defineTimeInterval($aData['when']);

        $sThumbAddon = 'style="background: transparent url(' . $this->oModule->_oTemplate->getIconUrl('album.png') . ') no-repeat center center;"';
        if ((int)$aData['Thumb']) {
            $aThumbInfo = $this->oModule->_oDb->getImageInfo((int)$aData['Thumb']);
            $sThumbAddon = 'style="background: transparent url(' . $this->oModule->sUploadUrl . 't_' . $aThumbInfo['Filename'] . ') no-repeat center center;"';
        } else if ($this->oModule->_oDb->getAlbumTypeByID($iAlbumID) == 'Video') {
            //Nick - set album thumb for video files
            if ($thumb = $this->oModule->_oDb->getThumbUrl($iAlbumID)) {
                $sThumbAddon = 'style="background: transparent url(' . $thumb . ') no-repeat center center;"';
            }
        }

        $iPhotosCnt = $this->oModule->_oDb->getAlbumPhotosCnt($iAlbumID);

        $aGrpInfo = $this->oModule->_oDb->getGroupInfo($aData['GroupID']);

        if (! $this->oModule->sModuleUrl) {
            //$aGrpInfo = $this->oModule->_oDb->getGroupInfo($aData['GroupID']);
            $this->oModule->sModuleUrl = BX_DOL_URL_ROOT . $this->oModule->_oConfig->getBaseUri() . 'group/' . $aGrpInfo['uri'] . '/';
        }

        $oGroups = BxDolModule::getInstance('BxGroupsModule');

        $viewUrl = BX_DOL_URL_ROOT . 'm/gphotos/group/' . $aGrpInfo['uri'] . '/album/' . $aData['uri'];

        $aVariables = array(
            'unit_id' => $iAlbumID,
            //'unit_view_url' => $this->oModule->sModuleUrl . 'album/' . $aData['uri'] . '/',
            'unit_view_url' => $viewUrl,
            'unit_title' => process_text_output($aData['title']),
            'author_url' => $sOwnerLink,
            'author_name' => $sOwnerNick,
            'group_name' => $aGrpInfo['title'],
            'group_link' => $oGroups->getGroupUrl($aGrpInfo['id']),
            'unit_when' => $sWhen,
            'unit_thumb' =>  $sThumbAddon,
            'unit_img_cnt' =>  $iPhotosCnt,

        );
        return $this->oModule->_oTemplate->parseHtmlByName('album_unit.html', $aVariables);
    }

    function getAlterOrder() {
        if ($this->aCurrent['sorting'] == 'last') {
            $aSql = array();
            $aSql['order'] = " ORDER BY `when` DESC";
            return $aSql;
        } /*elseif ($this->aCurrent['sorting'] == 'top') {
            $aSql = array();
            $aSql['order'] = " ORDER BY `rate` DESC";
            return $aSql;
        } elseif ($this->aCurrent['sorting'] == 'popular') {
            $aSql = array();
            $aSql['order'] = " ORDER BY `views` DESC";
            return $aSql;
        }*/
        return array();
    }

    function showSimplePagination($bAdmin = false) {
        bx_import('BxDolPaginate');
        $aLinkAddon = $this->getLinkAddByPrams();

        $oPaginate = new BxDolPaginate(array(
            'page_url' => $this->aCurrent['paginate']['page_url'],
            'count' => $this->aCurrent['paginate']['totalNum'],
            'per_page' => $this->aCurrent['paginate']['perPage'],
            'page' => $this->aCurrent['paginate']['page'],
            'per_page_changer' => true,
            'page_reloader' => false
        ));

        $sPaginate = '<div class="clear_both"></div>'.$oPaginate->getPaginate();
        return $sPaginate;
    }

    function showPagination($bAdmin = false) {
        $aLinkAddon = $this->getLinkAddByPrams();
        $oPaginate = new BxDolPaginate(array(
            'page_url' => $this->aCurrent['paginate']['page_url'],
            'count' => $this->aCurrent['paginate']['totalNum'],
            'per_page' => $this->aCurrent['paginate']['perPage'],
            'page' => $this->aCurrent['paginate']['page'],
            'per_page_changer' => true,
            'page_reloader' => true,
            'on_change_page' => 'return !loadDynamicBlock('.$this->id.', \'searchKeywordContent.php?searchMode=ajax&gpa_mode='.$this->aCurrent['sorting'].'&section[]=gpa&keyword='.bx_get('keyword').$aLinkAddon['params'].'&page={page}&per_page={per_page}\');',
            'on_change_per_page' => 'return !loadDynamicBlock('.$this->id.', \'searchKeywordContent.php?searchMode=ajax&gpa_mode='.$this->aCurrent['sorting'].'&section[]=gpa&keyword='.bx_get('keyword').$aLinkAddon['params'].'&page=1&per_page=\' + this.value);'
        ));
        $sPaginate = '<div class="clear_both"></div>'.$oPaginate->getPaginate();

        return $sPaginate;
    }

    function showPagination2($bAdmin = false, $sAllUrl = '') {
        bx_import('BxDolPaginate');
        $aLinkAddon = $this->getLinkAddByPrams();

        $sAllUrl = ($sAllUrl != '') ? $sAllUrl : $this->oModule->_oConfig->getBaseUri() . 'home/';

        $oPaginate = new BxDolPaginate(array(
            'page_url' => $this->aCurrent['paginate']['page_url'],
            'count' => $this->aCurrent['paginate']['totalNum'],
            'per_page' => $this->aCurrent['paginate']['perPage'],
            'page' => $this->aCurrent['paginate']['page'],
            'per_page_changer' => true,
            'page_reloader' => false,
            'on_change_page' => 'return !loadDynamicBlock({id}, \''.$_SERVER['PHP_SELF'].'?gpa_mode='.$this->aCurrent['sorting'].$aLinkAddon['params'].'&page={page}&per_page={per_page}\');',
        ));

        $sPaginate = '<div class="clear_both"></div>'.$oPaginate->getSimplePaginate($sAllUrl);

        return $sPaginate;
    }
}
