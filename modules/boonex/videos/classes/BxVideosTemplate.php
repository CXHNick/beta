<?php
/**
 * Copyright (c) BoonEx Pty Limited - http://www.boonex.com/
 * CC-BY License - http://creativecommons.org/licenses/by/3.0/
 */

bx_import('BxDolFilesTemplate');

class BxVideosTemplate extends BxDolFilesTemplate
{
    function BxVideosTemplate (&$oConfig, &$oDb)
    {
        parent::BxDolFilesTemplate($oConfig, $oDb);
    }

    function getFileConcept ($iFileId, $aExtra = array())
    {

        $sOverride = false;
        $oAlert = new BxDolAlerts($this->_oConfig->getMainPrefix(), 'display_player', $iFileId, getLoggedId(), array('extra' => $aExtra, 'override' => &$sOverride));
        $oAlert->alert();
        if ($sOverride) {
            //$msg = $this->_oConfig->getMainPrefix();
            //echo "<script type='text/javascript'>alert('$msg');</script>"; 
            return $sOverride;
        }

        $iFileId = (int)$iFileId;
        if(empty($aExtra['ext'])) {
            //$msg = "it me";
            //echo "<script type='text/javascript'>alert('$msg');</script>"; 
            $sPlayer = getApplicationContent('video','player',array('id' => $iFileId, 'user' => $this->iViewer, 'password' => clear_xss($_COOKIE['memberPassword'])),true);
        } else {
            //$msg1 = "no it me";
            //echo "<script type='text/javascript'>alert('$msg1');</script>"; 
            $sPlayer = str_replace("#video#", $aExtra['ext'], YOUTUBE_VIDEO_PLAYER);
            $sPlayer = str_replace("#wmode#", getWMode(), $sPlayer);
            $sPlayer = str_replace("#autoplay#", (getSettingValue("video", "autoPlay") == TRUE_VAL ? "&autoplay=1" : ""), $sPlayer);
        }

        //$mssg = "iGGAAHHH";
        //echo "<script type='text/javascript'>alert('$mssg');</script>"; 

        return '<div class="viewFile" style="width:100%;">' . $sPlayer . '</div>';
    }

    function getViewFile (&$aInfo)
    {
        $oVotingView = new BxTemplVotingView('bx_' . $this->_oConfig->getUri(), $aInfo['medID']);
        $iWidth = (int)$this->_oConfig->getGlParam('file_width');
        if ($aInfo['prevItem'] > 0)
            $aPrev = $this->_oDb->getFileInfo(array('fileId'=>$aInfo['prevItem']), true, array('medUri', 'medTitle'));
        if ($aInfo['nextItem'] > 0)
            $aNext = $this->_oDb->getFileInfo(array('fileId'=>$aInfo['nextItem']), true, array('medUri', 'medTitle'));


        //Nick
        $viewUrl = '';
        $aAlbCaption = '';

        if ($groupFile = $this->_oDb->isInGrpAlbum($aInfo['medID'])) {
                $aAlbInfo = $this->_oDb->getGrpAlbumInfo($groupFile);
                $aGrpInfo = $this->_oDb->getGrpInfo($aAlbInfo['GroupID']);
                $viewUrl = BX_DOL_URL_ROOT . 'm/gphotos/group/' . $aGrpInfo['uri'] . '/album/' . $aAlbInfo['Uri'];
                $aAlbCaption = $aAlbInfo['Title'];

                $aNext = $this->_oDb->getNextItemGrpAlbum($groupFile, $aInfo['medID']);
                $aPrev = $this->_oDb->getPrevItemGrpAlbum($groupFile, $aInfo['medID']);

                $aInfo['prevItem'] = 0;//$aPrev['ID'];
                $aInfo['nextItem'] = 0;//$aNext['ID'];

                $nextInfo = $this->_oDb->getFileInfo(array('fileId' => $aNext['AssocID']));
                $prevInfo = $this->_oDb->getFileInfo(array('fileId' => $aPrev['AssocID']));

                $linkNext = BX_DOL_URL_ROOT . 'm/videos/view/' . $nextInfo['Uri'];
                $linkPrev = BX_DOL_URL_ROOT . 'm/videos/view/' . $prevInfo['Uri'];

                $titleNext = $nextInfo['Title'];
                $titlePrev = $prevInfo['Title'];
        } else {
                $viewUrl = BX_DOL_URL_ROOT . $this->_oConfig->getBaseUri() . 'browse/album/' . $aInfo['albumUri'] . '/owner/' . $aInfo['NickName'];
                $aAlbCaption = $aInfo['albumCaption'];
                $linkNext = BX_DOL_URL_ROOT . $this->_oConfig->getBaseUri() . 'view/' . $aNext['medUri'];
                $linkPrev = BX_DOL_URL_ROOT . $this->_oConfig->getBaseUri() . 'view/' . $aPrev['medUri'];
                $titleNext = $aNext['medTitle'];
                $titlePrev = $aPrev['medTitle'];
        }


        //$meow = 'anus' . $aInfo['medExt'];
        //echo "<script type='text/javascript'>alert('$meow');</script>";
        $aUnit = array(
            'file' => $this->getFileConcept($aInfo['medID'], array('ext'=>$aInfo['medExt'], 'source'=>$aInfo['medSource'])),
            'width_ext' => $iWidth + 2,
            'width' => $iWidth,
            'fileUrl' => BX_DOL_URL_ROOT . $this->_oConfig->getBaseUri() . 'view/' . $aInfo['medUri'],
            'fileTitle' => $aInfo['medTitle'],
            'fileDescription' => $aInfo['medDesc'],
            'rate' => $oVotingView->isEnabled() ? $oVotingView->getBigVoting(1, $aInfo['Rate']): '',
            'favInfo' => isset($aInfo['favCount']) ? $aInfo['favCount'] : '',
            'viewInfo' => $aInfo['medViews'],
            'albumUri' => $viewUrl,
            'albumCaption' => $aAlbCaption,
            'bx_if:prev' => array(
                'condition' => $aInfo['prevItem'] > 0,
                'content' => array(
                    'linkPrev'  => $linkPrev,
                    'titlePrev' => $titlePrev,
                    'percent' => $aInfo['nextItem'] > 0 ? 50 : 100,
                )
            ),
            'bx_if:next' => array(
                'condition' => $aInfo['nextItem'] > 0,
                'content' => array(
                    'linkNext'  => $linkNext,
                    'titleNext' => $titleNext,
                    'percent' => $aInfo['prevItem'] > 0 ? 50 : 100,
                )
            ),
        );
        return $this->parseHtmlByName('view_unit.html', $aUnit);
    }

    function getEmbedCode ($iFileId, $aExtra = array())
    {
        $sOverride = false;
        $oAlert = new BxDolAlerts($this->_oConfig->getMainPrefix(), 'embed_code', $iFileId, getLoggedId(), array('override' => &$sOverride));
        $oAlert->alert();
        if ($sOverride)
            return $sOverride;

        $iFileId = (int)$iFileId;
        switch ($aExtra["source"]) {
            case "":
                $sEmbedCode = getEmbedCode('video', 'player', array('id'=>$iFileId));
                break;
            case "youtube":
                $sEmbedCode = str_replace("#video#", $aExtra["video"], YOUTUBE_VIDEO_EMBED);
                $sEmbedCode = str_replace("#wmode#", getWMode(), $sEmbedCode);
                $sEmbedCode = str_replace("#autoplay#", (getSettingValue("video", "autoPlay") == TRUE_VAL ? "&autoplay=1" : ""), $sEmbedCode);
                break;
            default:
                $sEmbedCode = video_getCustomEmbedCode($aExtra["source"], $aExtra["video"]);
                break;
        }
        return $sEmbedCode;
    }

    function getCompleteFileInfoForm (&$aInfo, $sUrlPref = '')
    {
        $aMain = $this->getBasicFileInfoForm($aInfo, $sUrlPref);
        if ($aInfo['AllowAlbumView'] == BX_DOL_PG_ALL)
        {
            $aAdd = array('embed' => array(
                    'type' => 'text',
                    'value' => $this->getEmbedCode($aInfo['medID'], array('video'=>$aInfo['medExt'], 'source'=>$aInfo['medSource'])),
                    'attrs' => array(
                      'onclick' => 'this.focus(); this.select();',
                      'readonly' => 'readonly',
                    ),
                    'caption'=> _t('_Embed')
                ),
            );
            $aMain = array_merge($aMain, $aAdd);
        }
        return $aMain;
    }

    function getItemType ()
    {
        return 'type="video/x-flv"';
    }
}
