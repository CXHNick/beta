<?php
/**
 * Copyright (c) BoonEx Pty Limited - http://www.boonex.com/
 * CC-BY License - http://creativecommons.org/licenses/by/3.0/
 */

require_once( BX_DIRECTORY_PATH_INC . 'admin_design.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'utils.inc.php' );
require_once( BX_DIRECTORY_PATH_PLUGINS . 'Services_JSON.php' );

bx_import('BxDolPaginate');
bx_import('BxDolAlerts');
bx_import('BxDolTwigModule');
bx_import('BxDolAdminSettings');
bx_import('BxTemplSearchResult');
bx_import('BxDolPageView');

class BxLibraryPageHome extends BxDolPageView
{
    var $_oMain;
    var $_oTemplate;
    var $_oConfig;
    var $_oDb;

    function BxLibraryPageHome(&$oModule)
    {
        $this->_oMain = &$oModule;
        $this->_oTemplate = $oModule->_oTemplate;
        $this->_oConfig = $oModule->_oConfig;
        $this->_oDb = $oModule->_oDb;
        parent::BxDolPageView('cxipmods_library_home');
    }

    function getBlockCode_FileAlbums() {

        //require_once(BX_DIRECTORY_PATH_MODULES . 'boonex/sounds/classes/BxFilesSearch.php');


        $aVisible[] = BX_DOL_PG_ALL;
        if (getLoggedId()) {
            $aVisible[] = BX_DOL_PG_MEMBERS;
        }

        $aBlock = BxDolService::call('files', 'get_files_block', array(array('allow_view'=>$aVisible/*, 'owner'=>getLoggedId()*/), array('menu_top'=>false, 'sorting'=>getParam('bx_files_mode_index'), 'per_page'=>getParam('bx_files_number_index'), /*'lib'=>true*/)), 'Search');

        return $aBlock;
    }

    function getBlockCode_VideoAlbums() {

        require_once(BX_DIRECTORY_PATH_MODULES . 'boonex/videos/classes/BxVideosSearch.php');
        $oMedia = new BxVideosSearch();
        $aVisible[] = BX_DOL_PG_ALL;
        if (getLoggedId()) {
            $aVisible[] = BX_DOL_PG_MEMBERS;
        }

        //$message = "hello";
        //echo "<script type='text/javascript'>alert('$message');</script>";

        //Exclude group album from results. Shitty workaround, I know. On a tight time schedule ok
        $oMedia->aCurrent['workaround'] = true;


        $aCode = $oMedia->getBrowseBlock(array('allow_view'=>$aVisible, 'owner'=>getLoggedId()), array('menu_top'=>true, 'sorting' => getParam('bx_videos_mode_index'), 'per_page'=>(int)getParam('bx_videos_number_index'), 'lib'=>true));

        $aBlock = array($aCode['code'], $aCode['menu_top'], $aCode['menu_bottom'], $aCode['wrapper']);


        return $aBlock;
    }

    function getBlockCode_SoundAlbums() {

        require_once(BX_DIRECTORY_PATH_MODULES . 'boonex/sounds/classes/BxSoundsSearch.php');
        $oMedia = new BxSoundsSearch();
                $aVisible[] = BX_DOL_PG_ALL;
                if (getLoggedId())  {
                    $aVisible[] = BX_DOL_PG_MEMBERS;
                }

        //$aCode = $oMedia->getBrowseBlock(array('allow_view'=>$aVisible), array('menu_top'=>true, 'sorting' => getParam('bx_sounds_mode_index'), 'per_page'=>(int)getParam('bx_sounds_number_index')));

        //$aBlock = array($aCode['code'], $aCode['menu_top'], $aCode['menu_bottom'], $aCode['wrapper']);
        $aBlock = BxDolService::call('sounds', 'profile_sound_block', array($this->oMain->_iProfileId), 'Search');



        return $aBlock;
    }

    /*function getBlockCode_GroupReports() {

        //$oGroups = BxDolModule::getInstance('BxGroupsModule');
        require_once(BX_DIRECTORY_PATH_MODULES . 'boonex/groups/classes/BxGroupsSearchResult.php');
        $oSearch = new BxGroupsSearchResult('blogs');
        $oSearch->aCurrent['isGroupReports'] = true;

        return $oSearch->displaySubProfileResultBlock('blog');
        //return BxDolService::call('groups', 'get_reports_block', array(getLoggedId()), 'BlogPageBrowse');
    }*/

    function getBlockCode_GroupReports() {

        $iProfileId = getLoggedId();

        $aGroups = $this->_oDb->getUserGroupsById($iProfileId);

        $sCode = '';

        foreach ($aGroups as $aValue) {
            $sCode .= $this->_oTemplate->report_unit($aValue, $iProfileId);
        }

        //$sCode .= $this->_oTemplate->report_unit($aGroups[0], $iProfileId);


        $GLOBALS['oSysTemplate']->addCss(array('unit.css', 'twig.css'));

        if (!$sCode) {
            return MsgBox(_t('_Empty'));
        }

        return $GLOBALS['oSysTemplate']->parseHtmlByName('default_padding.html', array('content' => $sCode));
    }

    /*function getBlockCode_GroupsPhotoAlbums() {

    }

    function getBlockCode_GroupsFileAlbums() {

    }

    function getBlockCode_GroupsVideoAlbums() {

    }

    function getBlockCode_GroupsSoundAlbums() {

    }*/

}
