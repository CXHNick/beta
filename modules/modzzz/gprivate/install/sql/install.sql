-- create tables   
CREATE TABLE IF NOT EXISTS `[db_prefix]main`  (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(255) collate utf8_general_ci NOT NULL default '',
  `uri` varchar(255) collate utf8_general_ci NOT NULL default '',  
  `status` enum('approved','pending') NOT NULL default 'approved',
  `group_id` int(11) NOT NULL default '0',   
  `privacy_id` int(11) NOT NULL default '0',   
  PRIMARY KEY (`id`) 
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;  
 
-- permalinkU
INSERT INTO `sys_permalinks` VALUES (NULL, 'modules/?r=gprivate/', 'm/gprivate/', 'modzzz_gprivate_permalinks');

-- settings
SET @iMaxOrder = (SELECT `menu_order` + 1 FROM `sys_options_cats` ORDER BY `menu_order` DESC LIMIT 1);
INSERT INTO `sys_options_cats` (`name`, `menu_order`) VALUES ('GPrivate', @iMaxOrder);
SET @iCategId = (SELECT LAST_INSERT_ID());
INSERT INTO `sys_options` (`Name`, `VALUE`, `kateg`, `desc`, `Type`, `check`, `err_text`, `order_in_kateg`, `AvailableValues`) VALUES
('modzzz_gprivate_permalinks', 'on', 26, 'Enable friendly permalinks in Group Privacy', 'checkbox', '', '', '0', ''),
('modzzz_gprivate_only_admin', 'on', @iCategId, 'Only show Groups created by Admin', 'checkbox', '', '', '0', '');
   
-- admin menu
SET @iMax = (SELECT MAX(`order`) FROM `sys_menu_admin` WHERE `parent_id` = '2');
INSERT IGNORE INTO `sys_menu_admin` (`parent_id`, `name`, `title`, `url`, `description`, `icon`, `order`) VALUES
(2, 'modzzz_gprivate', '_modzzz_gprivate', '{siteUrl}modules/?r=gprivate/administration/', 'Group Privacy module by Modzzz','eye', @iMax+1);
 

 -- alert handlers
INSERT INTO `sys_alerts_handlers` VALUES (NULL, 'modzzz_gprivate', 'BxGPrivateResponse', 'modules/modzzz/gprivate/classes/BxGPrivateResponse.php', '');
SET @iHandler := LAST_INSERT_ID();

INSERT INTO `sys_alerts` VALUES  
(NULL, 'bx_groups', 'change', @iHandler), 
(NULL, 'bx_groups', 'delete', @iHandler);

