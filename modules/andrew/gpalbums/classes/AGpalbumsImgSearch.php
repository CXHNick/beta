<?php
/***************************************************************************
*
* IMPORTANT: This is a commercial product made by AndrewP. It cannot be modified for other than personal usage.
* The "personal usage" means the product can be installed and set up for ONE domain name ONLY.
* To be able to use this product for another domain names you have to order another copy of this product (license).
* This product cannot be redistributed for free or a fee without written permission from AndrewP.
* This notice may not be removed from the source code.
*
***************************************************************************/
bx_import('BxDolModule');
bx_import('BxTemplSearchResultText');
bx_import('BxDolPrivacy');

class AGpalbumsImgSearch extends BxTemplSearchResultText {
    var $oModule;
    var $oTemplate;
    var $bShowCheckboxes;
    var $sAlbumUri;
    var $bGlobMode;
    var $sBaseUri;

    function AGpalbumsImgSearch () {
        parent::BxTemplSearchResultText();

        $this->oModule = BxDolModule::getInstance('AGpalbumsModule');
        $this->oTemplate = $this->oModule->_oTemplate;

        $this->bShowCheckboxes = false;
        $this->sAlbumUri = '';
        $this->bGlobMode = false;
        $this->sBaseUri = BX_DOL_URL_ROOT . $this->oModule->_oConfig->getBaseUri();

        // main settings
        $this->aCurrent = array(
            'name' => 'ayphotoimg',
            'title' => '_gpa_main',
            'table' => 'agrp_palbums_units',
            'ownFields' => array('ID', 'AlbumID', 'Title', 'Uri', 'When', 'Ext', 'Size', 'Filename', 'AssocID'),
            'searchFields' => array('Title', 'Size'),
            'restriction' => array(
                'album_id' => array('value'=>'', 'field'=>'AlbumID', 'operator'=>'='),
                'id'=> array('value'=>'', 'field'=>'ID', 'operator'=>'='),
                'feature_status' => array('value'=>'', 'field'=>'featured', 'operator'=>'=')
            ),
            'paginate' => array('perPage' => 10, 'page' => 1, 'totalNum' => 10, 'totalPages' => 1),
            'sorting' => 'last'
        );
    }

    function _getPseud () {
        return array(
            'id' => 'ID',
            'album_id' => 'AlbumID',
            'title' => 'Title',
            'uri' => 'Uri',
            'when' => 'When',
            'size' => 'Size',
            'filename' => 'Filename',
            'assoc_id' => 'AssocID',
        );
    }

    function displaySearchUnit($aData) {
        $iAlbumID = (int)$aData['album_id'];
        //Nick
        $aAlbumType = $this->oModule->_oDb->getAlbumTypeByID($iAlbumID);


        //Nick

        // album privacy check
        $bPossibleToView = $this->oModule->oPrivacy->check('album_view', $iAlbumID, $this->oModule->_iVisitorID);
        if (! $bPossibleToView) return;

        // group privacy check
        /*$aAlbInfo = $this->oModule->_oDb->getAlbumInfo($iAlbumID);
        $oPrivacy = new BxDolPrivacy('bx_groups_main', 'id', 'author_id');
        $bPossibleToView2 = $oPrivacy->check('view_group', $aAlbInfo['GroupID'], $this->oModule->_iVisitorID);
        if (! $bPossibleToView2) return;
        */

        $sWhen = defineTimeInterval($aData['when']);

        $iImgID = (int)$aData['id'];
        $sCheckbox = ($this->bShowCheckboxes) ? '<input id="ch'.$iImgID.'" type="checkbox" value="'.$iImgID.'" name="gphotos[]" class="bx_sys_unit_checkbox" />' : '';
        $sFilename = $this->oModule->sUploadUrl . 't_' . $aData['filename'];
        $sFullFilename = $this->oModule->sUploadUrl . 'i_' . $aData['filename'];
        $sViewUrl = '';



        if ($this->bGlobMode) {
            $aAlbInfo = $this->oModule->_oDb->getAlbumInfo($aData['album_id']);
            $aEventInfo = $this->oModule->_oDb->getGroupInfo($aAlbInfo['GroupID']);
            $aEventId = $aAlbInfo['GroupID'];
            $sFullFilename = $sViewUrl = $this->sBaseUri . 'group/' . $aEventInfo['uri'] . '/' . 'album/' . $aAlbInfo['Uri'] . '/' . $aData['uri'] . '/';
        } else {
            $sViewUrl = $this->oModule->sModuleUrl . 'album/' . $this->sAlbumUri . '/' . $aData['uri'] . '/';
        }

        //Nick
        if ($aAlbumType == 'Video') {
            //$sViewUrl = $this->sBaseUri . 'view/' . $aData['uri'];
            //$this->oModule->_oDb->updateVideoInfo($aData['album_id']);
            $videoUri = $this->oModule->_oDb->getVideoUriById((int)$aData['assoc_id']);
            $sViewUrl = BX_DOL_URL_ROOT . 'm/videos/view/' . $videoUri;
            $sFilename = $sViewUrl;
            $picPath = BX_DOL_URL_ROOT . 'flash/modules/video/files/' . $aData['assoc_id'] . '_small.jpg';
            if ($this->oModule->_oDb->isYoutube($aData['assoc_id'])) {
                //$picPath = BxDolService::call('videos', 'get_thumb', array($aData['assoc_id']), 'Uploader');
                $picPath = BX_DOL_URL_ROOT . 'modules/andrew/gpalbums/templates/base/images/icons/YouTube.png';
            }
        } else {
            $picPath = $this->oModule->_oTemplate->getIconUrl('default.png');
        }

        if ($aAlbumType == 'Sound') {
            $soundUri = $this->oModule->_oDb->getSoundUriById((int)$aData['assoc_id']);
            $sViewUrl = BX_DOL_URL_ROOT . 'm/sounds/view/' . $soundUri;
        }

        $aVariables = array(
            'pic' => $picPath,
            'unit_id' => $iImgID,
            'unit_owner' => $this->oModule->_oDb->getUnitOwnerByID($iImgID),
            'unit_owner_link' => getProfileLink($this->oModule->_oDb->getUnitOwnerByID($iImgID)),
            'unit_view_url' => $sViewUrl,
            'unit_title' => process_text_output($aData['title']),
            'unit_when' => $sWhen,
            'unit_size' => '',
            'unit_img_url' => $picPath,
            'unit_full_img_url' => $sFullFilename,
            //'unit_img_url' => $sViewUrl,
            //'unit_full_img_url' => '',
            'checkbox' => $sCheckbox,
        );

        //Nick
        $htmlFileName = 'photo_unit.html';

        if ($aAlbumType == 'File') {
            $htmlFileName = 'file_unit.html';
        } else if ($aAlbumType == 'Photo') {
            $htmlFileName = 'photo_unit.html';
        } else if ($aAlbumType == 'Video') {
            //call AGalbumsVideoPageView
            //$htmlFileName = 'video_unit.html';
        } else if ($aAlbumType == 'Sound') {
            $htmlFileName = 'file_unit.html';
        } else {
            $msg = "Oops";
            echo "<script type='text/javascript'>alert('$msg');</script>";
        }

        //return $this->oModule->_oTemplate->parseHtmlByName('photo_unit.html', $aVariables);
        return $this->oModule->_oTemplate->parseHtmlByName($htmlFileName, $aVariables);
        //Nick
    }

    function getAlterOrder() {
        if ($this->aCurrent['sorting'] == 'last') {
            $aSql = array();
            $aSql['order'] = " ORDER BY `when` DESC";
            return $aSql;
        } elseif ($this->aCurrent['sorting'] == 'name') {
            $aSql = array();
            $aSql['order'] = " ORDER BY `title` ASC";
            return $aSql;
        }
        return array();
    }

    function showSimplePagination($bAdmin = false) {
        bx_import('BxDolPaginate');
        $aLinkAddon = $this->getLinkAddByPrams();

        $oPaginate = new BxDolPaginate(array(
            'page_url' => $this->aCurrent['paginate']['page_url'],
            'count' => $this->aCurrent['paginate']['totalNum'],
            'per_page' => $this->aCurrent['paginate']['perPage'],
            'page' => $this->aCurrent['paginate']['page'],
            'per_page_changer' => true,
            'page_reloader' => false
        ));

        $sPaginate = '<div class="clear_both"></div>'.$oPaginate->getPaginate();
        return $sPaginate;
    }

    /*function showPagination($bAdmin = false) {
        $aLinkAddon = $this->getLinkAddByPrams();
        $oPaginate = new BxDolPaginate(array(
            'page_url' => $this->aCurrent['paginate']['page_url'],
            'count' => $this->aCurrent['paginate']['totalNum'],
            'per_page' => $this->aCurrent['paginate']['perPage'],
            'page' => $this->aCurrent['paginate']['page'],
            'per_page_changer' => true,
            'page_reloader' => true,
            'on_change_page' => 'return !loadDynamicBlock('.$this->id.', \'searchKeywordContent.php?searchMode=ajax&gpa_mode='.$this->aCurrent['sorting'].'&section[]=gpa_photos&keyword='.bx_get('keyword').$aLinkAddon['params'].'&page={page}&per_page={per_page}\');',
            'on_change_per_page' => 'return !loadDynamicBlock('.$this->id.', \'searchKeywordContent.php?searchMode=ajax&gpa_mode='.$this->aCurrent['sorting'].'&section[]=gpa_photos&keyword='.bx_get('keyword').$aLinkAddon['params'].'&page=1&per_page=\' + this.value);'
        ));
        $sPaginate = '<div class="clear_both"></div>'.$oPaginate->getPaginate();

        return $sPaginate;
    }*/

    function showPagination2($bAdmin = false, $sAllUrl = '') {
        bx_import('BxDolPaginate');
        $aLinkAddon = $this->getLinkAddByPrams();

        $sAllUrl = ($sAllUrl != '') ? $sAllUrl : $this->oModule->_oConfig->getBaseUri() . 'home/';

        $oPaginate = new BxDolPaginate(array(
            'page_url' => $this->aCurrent['paginate']['page_url'],
            'count' => $this->aCurrent['paginate']['totalNum'],
            'per_page' => $this->aCurrent['paginate']['perPage'],
            'page' => $this->aCurrent['paginate']['page'],
            'per_page_changer' => true,
            'page_reloader' => false,
            'on_change_page' => 'return !loadDynamicBlock({id}, \''.$_SERVER['PHP_SELF'].'?gpa_mode='.$this->aCurrent['sorting'].$aLinkAddon['params'].'&page={page}&per_page={per_page}\');',
        ));

        $sPaginate = '<div class="clear_both"></div>'.$oPaginate->getSimplePaginate($sAllUrl);

        return $sPaginate;
    }
}
