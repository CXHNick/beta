<?php
/***************************************************************************
* 
*     copyright            : (C) 2009 AQB Soft
*     website              : http://www.aqbsoft.com
*      
* IMPORTANT: This is a commercial product made by AQB Soft. It cannot be modified for other than personal usage.
* The "personal usage" means the product can be installed and set up for ONE domain name ONLY. 
* To be able to use this product for another domain names you have to order another copy of this product (license).
* 
* This product cannot be redistributed for free or a fee without written permission from AQB Soft.
* 
* This notice may not be removed from the source code.
* 
***************************************************************************/

bx_import('BxDolSiteMaps');
bx_import('BxDolPrivacy');

/**
 * Sitemaps generator for Groups
 */
class AqbBlogsSiteMaps extends BxDolSiteMaps
{
    protected $_oModule;

    protected function __construct($aSystem)
    {
        parent::__construct($aSystem);

        $this->_oModule = BxDolModule::getInstance('AqbBlogsModule');

        $this->_aQueryParts = array (
            'fields' => "`id`, `uri`, `created`", // fields list
            'field_date' => "created", // date field name
            'field_date_type' => "timestamp", // date field type
            'table' => "`" . $this->_oModule->_oDb->getFullName('table_main') . "`", // table name
            'join' => "", // join SQL part
            'where' => "AND `status` = 'approved' AND `" . $this->_oModule->_oDb->getFullName('field_allow_view_to', false) . "` = '" . BX_DOL_PG_ALL . "'", // SQL condition, without WHERE
            'order' => " `created` ASC ", // SQL order, without ORDER BY
        );
    }

    protected function _genUrl ($a)
    {
        return BX_DOL_URL_ROOT . $this->_oModule->_oConfig->getBaseUri() . 'view/' . $a['uri'];
    }
}
