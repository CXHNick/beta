<?php
/***************************************************************************
*                            Dolphin Smart Community Builder
*                              -------------------
*     begin                : Mon Mar 23 2006
*     copyright            : (C) 2007 BoonEx Confession
*     website              : http://www.boonex.com
* This file is part of Dolphin - Smart Community Builder
*
* Dolphin is free software; you can redistribute it and/or modify it under
* the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 2 of the
* License, or  any later version.
*
* Dolphin is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
* without even the implied warranty of  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
* You should have received a copy of the GNU General Public License along with Dolphin,
* see license.txt file; if not, write to marketing@boonex.com
***************************************************************************/

bx_import('BxDolTwigModuleDb');
bx_import('BxDolCategories');
  
/*
 * GPrivate module Data
 */
class BxGPrivateDb extends BxDolTwigModuleDb {	
	
	var $oConfig;

	/*
	 * Constructor.
	 */
	function BxGPrivateDb(&$oConfig) {
        parent::BxDolTwigModuleDb($oConfig);
		$this->_oConfig = $oConfig;

        $this->_sFieldMinAge = 'min_age'; 
        $this->_sFieldMaxAge = 'max_age'; 

		$this->_sTableFans = '';   
	}
 
    function deleteEntryById ($aDataEntry) {
      
		$iId = (int)$aDataEntry['id'];
		$iPrivacyId = (int)$aDataEntry['privacy_id'];

        if (!($iRet = $this->query ("DELETE FROM `" . $this->_sPrefix . $this->_sTableMain . "` WHERE `{$this->_sFieldId}` = $iId LIMIT 1")))
            return false;

		$this->deletePrivacyEntry ($iPrivacyId); 
	}
 
    function deletePrivacyEntry ($iPrivacyId) {
 		$iPrivacyId = (int)$iPrivacyId;

		$sLangKey = "_ps_group_{$iPrivacyId}_title"; 
		deleteStringFromLanguage($sLangKey); 
  
		$this->query ("DELETE FROM `sys_options` WHERE `Name` = 'sys_ps_enabled_group_{$iPrivacyId}'");

  		$this->query ("DELETE FROM `sys_privacy_groups` WHERE  `id` = $iPrivacyId");

        $GLOBALS['MySQL']->cleanCache('sys_options');  

        return true;
    }
   
	function getLanguageName($iId) { 
		$iId = (int)$iId;

		return $this->getOne ("SELECT `Title` FROM `sys_localization_languages` WHERE `ID`=$iId"); 
	}
 
	function hasPrivelege($iProfileId, $iObjectId){
		$iObjectId = (int)$iObjectId; 
		$iProfileId = (int)$iProfileId;

		$iProfileId = getLoggedId();
	 
		$iGroupId = $this->getOne("SELECT `group_id` FROM `" . $this->_sPrefix . $this->_sTableMain . "` WHERE `id`=$iObjectId LIMIT 1"); 
   		//$iGroupId = $this->getOne("SELECT `group_id` FROM `modzzz_gprivate_main` WHERE `id`=$iObjectId LIMIT 1"); 


		if(!$iGroupId) return false;

		$oGroup = BxDolModule::getInstance('BxGroupsModule');
 
		$aGroupData = $oGroup->_oDb->getEntryById($iGroupId);

		return ($oGroup->isEntryAdmin($aGroupData) || $oGroup->_oDb->isFan($iGroupId, $iProfileId, 1));  
	}

	function addPrivacy($iObjectId, $sTitle){
		$iObjectId = (int)$iObjectId;
		$sTitle = process_db_input($sTitle);

		$this->query ("INSERT INTO `sys_privacy_groups` SET  `title` =  '{$sTitle}', `get_content` =  '\$oModuleDb = new BxDolModuleDb(); if(!\$oModuleDb->getModuleByUri(''gprivate'')){return;}else{ \$oMod = BxDolModule::getInstance(''BxGPrivateModule'');return \$oMod->hasPrivelege(\$iId, $iObjectId);}'");

		$iPrivacyId = $this->lastId();

		$this->query ("UPDATE `modzzz_gprivate_main` SET `privacy_id`=$iPrivacyId WHERE `id`=$iObjectId");

		$sLangKey = "_ps_group_{$iPrivacyId}_title"; 
		addStringToLanguage($sLangKey, $sTitle); 

		$iCategOrd = (int)$this->getOne ("SELECT MAX(`order_in_kateg`)+1 FROM `sys_options` WHERE `kateg`=9");

		$this->query ("INSERT INTO `sys_options` VALUES
		('sys_ps_enabled_group_{$iPrivacyId}', 'on', 9, 'Enable \'{$sTitle}\' group', 'checkbox', '', '', $iCategOrd, '')");

        $GLOBALS['MySQL']->cleanCache('sys_options');  
	}
 
	function getFormGroups(){

 		$bAdminOnly = (getParam('modzzz_gprivate_only_admin')=='on') ? true : false;
   
	    if($bAdminOnly){
			 $aAdminProfiles = array();
			 $aAdmin = $this->getAll("SELECT `ID` FROM `Profiles` WHERE  `Role`=3 AND `Status`='Active'");  	
			 foreach($aAdmin as $aEachAdmin){
				 $aAdminProfiles[] = $aEachAdmin['ID'];
			 }
			 $sAdminProfiles = implode(',', $aAdminProfiles);

			 $sExtra = "AND `author_id` IN ($sAdminProfiles)";
	    }	

		return $this->getPairs ("SELECT `id`, `title` FROM `bx_groups_main` WHERE `status`='approved' $sExtra", 'id', 'title');
	}

    function deleteGroupFromPrivacy ($iGroupId) {
 		$iGroupId = (int)$iGroupId;

		$iPrivacyId = $this->getOne ("SELECT `privacy_id` FROM `" . $this->_sPrefix . $this->_sTableMain . "` WHERE `group_id`=$iGroupId LIMIT 1"); 
		
		if(!$iPrivacyId) return;

		$this->deletePrivacyEntry ($iPrivacyId); 
	}

	function editGroupPrivacy($iGroupId, $sStatus){
		$iGroupId = (int)$iGroupId;

		if($sStatus == 'approved'){

 			$iPrivacyId = $this->getOne ("SELECT `privacy_id` FROM `" . $this->_sPrefix . $this->_sTableMain . "` WHERE `group_id`=$iGroupId LIMIT 1"); 
 
			if(!$iPrivacyId) return;

			$sTitle = $this->getOne ("SELECT `title` FROM `bx_groups_main` WHERE `id`=$iGroupId");
			$sTitle = process_db_input($sTitle);

			$this->query ("UPDATE `sys_privacy_groups` SET `title` = '{$sTitle}' WHERE `id`=$iPrivacyId");
  
			$this->query ("UPDATE `sys_options` SET `desc`='Enable \'{$sTitle}\' group' WHERE `Name`='sys_ps_enabled_group_{$iPrivacyId}'");

			$GLOBALS['MySQL']->cleanCache('sys_options'); 
	 
		}else{
			$this->deleteGroupFromPrivacy($iGroupId); 
		} 
	}

 	function cleanup(){
  
		$aGrp = $this->getAll ("SELECT `id` FROM `sys_privacy_groups` WHERE `get_content` LIKE '%BxGPrivateModule%'");
		foreach($aGrp as $aEachGrp){
			$iId = $aEachGrp['id'];
 			$this->query ("DELETE FROM `sys_options` WHERE `Name` = 'sys_ps_enabled_group_{$iId}'");
		} 
 
		$this->query ("DELETE FROM `sys_privacy_groups` WHERE `get_content` LIKE '%BxGPrivateModule%'");

		$GLOBALS['MySQL']->cleanCache('sys_options'); 
	}


}