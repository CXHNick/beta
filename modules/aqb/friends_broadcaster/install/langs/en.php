<?php
/***************************************************************************
*
*     copyright            : (C) 2014 AQB Soft
*     website              : http://www.aqbsoft.com
*
* IMPORTANT: This is a commercial product made by AQB Soft. It cannot be modified for other than personal usage.
* The "personal usage" means the product can be installed and set up for ONE domain name ONLY.
* To be able to use this product for another domain names you have to order another copy of this product (license).
*
* This product cannot be redistributed for free or a fee without written permission from AQB Soft.
*
* This notice may not be removed from the source code.
*
***************************************************************************/

$aLangContent = array(
	'_aqb_friends_broadcaster_top_menu_item' => 'Compose to Friends',
	'_aqb_friends_broadcaster_action_button' => 'Contact',
	'_aqb_friends_broadcaster_no_friends' => 'No friends found',
	'_aqb_friends_broadcaster_recipients' => 'Recipients',
	'_aqb_friends_broadcaster_subject' => 'Subject',
	'_aqb_friends_broadcaster_send' => 'Send message',
	'_aqb_friends_broadcaster_subject_required' => 'Subject can not be empty',
	'_aqb_friends_broadcaster_recipients_required' => 'You have to select at least one recipient',
	'_aqb_friends_broadcaster_message_required' => 'Message can not be empty',
	'_aqb_friends_broadcaster_sent' => 'Message has been successfully sent',
);