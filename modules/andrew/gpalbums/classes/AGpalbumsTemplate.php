<?php
/***************************************************************************
*
* IMPORTANT: This is a commercial product made by AndrewP. It cannot be modified for other than personal usage.
* The "personal usage" means the product can be installed and set up for ONE domain name ONLY.
* To be able to use this product for another domain names you have to order another copy of this product (license).
* This product cannot be redistributed for free or a fee without written permission from AndrewP.
* This notice may not be removed from the source code.
*
***************************************************************************/
bx_import('BxDolModuleTemplate');

class AGpalbumsTemplate extends BxDolModuleTemplate {

    /**
     * Constructor
     */
    function AGpalbumsTemplate(&$oConfig, &$oDb) {
        parent::BxDolModuleTemplate($oConfig, $oDb);
    }

    /*function getFileConcept ($iFileId, $aExtra = array())
    {

        bx_import('BxDolAlerts');

        //$msg = $this->_oConfig->getMainPrefix();
        //echo "<script type='text/javascript'>alert('$msg');</script>";

        $sOverride = false;
        $oAlert = new BxDolAlerts('bx_videos', 'display_player', $iFileId, getLoggedId(), array('extra' => $aExtra, 'override' => &$sOverride));
        $oAlert->alert();
        if ($sOverride) {

            $msg = "aye";
            echo "<script type='text/javascript'>alert('$msg');</script>";

            return $sOverride;
        }

        //$msg = "getatme";
        //echo "<script type='text/javascript'>alert('$msg');</script>";
        $iFileId = (int)$iFileId;
        if(empty($aExtra['ext']))
            $sPlayer = getApplicationContent('video','player',array('id' => $iFileId, 'user' => $this->iViewer, 'password' => clear_xss($_COOKIE['memberPassword'])),true);
        else {
            $sPlayer = str_replace("#video#", $aExtra['ext'], YOUTUBE_VIDEO_PLAYER);
            $sPlayer = str_replace("#wmode#", getWMode(), $sPlayer);
            $sPlayer = str_replace("#autoplay#", (getSettingValue("video", "autoPlay") == TRUE_VAL ? "&autoplay=1" : ""), $sPlayer);
        }
        return '<div class="viewFile" style="width:100%;">' . $sPlayer . '</div>';
    }*/

    function getViewFile (&$aInfo)
    {
        /*//$msg = $aInfo['ID'];
        //echo "<script type='text/javascript'>alert('$msg');</script>";

        //$oVotingView = new BxTemplVotingView('bx_' . $this->_oConfig->getUri(), $aInfo['medID']);
        //$iWidth = (int)$this->_oConfig->getGlParam('file_width');
        $iWidth = 600;
        //if ($aInfo['prevItem'] > 0)
          //  $aPrev = $this->_oDb->getFileInfo(array('fileId'=>$aInfo['prevItem']), true, array('medUri', 'medTitle'));
        //if ($aInfo['nextItem'] > 0)
          //  $aNext = $this->_oDb->getFileInfo(array('fileId'=>$aInfo['nextItem']), true, array('medUri', 'medTitle'));


        $aUnit = array(
            'file' => $this->getFileConcept($aInfo['ID'], array('ext'=>$aInfo['poopy'])),
            'width_ext' => $iWidth + 2,
            'width' => $iWidth,
            'fileUrl' => BX_DOL_URL_ROOT . $this->_oConfig->getBaseUri() . 'view/' . $aInfo['Uri'],
            'fileTitle' => $aInfo['Title'],
            'fileDescription' => $aInfo['Description'],
            //'rate' => $oVotingView->isEnabled() ? $oVotingView->getBigVoting(1, $aInfo['Rate']): '',
            //'favInfo' => isset($aInfo['favCount']) ? $aInfo['favCount'] : '',
            //'viewInfo' => $aInfo['medViews'],
            'albumUri' => BX_DOL_URL_ROOT . $this->_oConfig->getBaseUri() . 'group/' . $aInfo['groupUri'] . '/album/' . $this->_oDb->getAlbumUriById($aInfo['AlbumID']),
            'albumCaption' => $this->_oDb->getAlbumTitleByID($aInfo['AlbumID']),
            'bx_if:prev' => array(
                'condition' => $aInfo['prevItem'] > 0,
                'content' => array(
                    'linkPrev'  => BX_DOL_URL_ROOT . $this->_oConfig->getBaseUri() . 'view/' . $aPrev['medUri'],
                    'titlePrev' => $aPrev['medTitle'],
                    'percent' => $aInfo['nextItem'] > 0 ? 50 : 100,
                )
            ),
            'bx_if:next' => array(
                'condition' => $aInfo['nextItem'] > 0,
                'content' => array(
                    'linkNext'  => BX_DOL_URL_ROOT . $this->_oConfig->getBaseUri() . 'view/' . $aNext['medUri'],
                    'titleNext' => $aNext['medTitle'],
                    'percent' => $aInfo['prevItem'] > 0 ? 50 : 100,
                )
            ),
        );

        //$msg1 = $aInfo['ID'];
        //echo "<script type='text/javascript'>alert('$msg1');</script>";

        return $this->parseHtmlByName('video_unit.html', $aUnit);
        */
    }

    // Output
    function pageCode ($aPage = array(), $aPageCont = array(), $aCss = array(), $aJs = array(), $bAdminMode = false, $isSubActions = true) {
        if (!empty($aPage)) {
            foreach ($aPage as $sKey => $sValue)
                $GLOBALS['_page'][$sKey] = $sValue;
        }
        if (!empty($aPageCont)) {
            foreach ($aPageCont as $sKey => $sValue)
                $GLOBALS['_page_cont'][$aPage['name_index']][$sKey] = $sValue;
        }
        if (!empty($aCss))
            $this->addCss($aCss);
        if (!empty($aJs))
            $this->addJs($aJs);

        if (!$bAdminMode)
            PageCode($this);
        else
            PageCodeAdmin();
    }

    function displayAccessDenied() {
        //$msg = "sup";
        //echo "<script type='text/javascript'>alert('$msg');</script>";
        return MsgBox(_t('_Access denied'));
    }
}
