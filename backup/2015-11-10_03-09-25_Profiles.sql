--
-- Database Dump For Dolphin: 
--


--
-- Table structure for table `Profiles`
--

DROP TABLE IF EXISTS `Profiles`;
CREATE TABLE `Profiles` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `NickName` varchar(255) NOT NULL DEFAULT '',
  `Email` varchar(255) NOT NULL DEFAULT '',
  `Password` varchar(40) NOT NULL DEFAULT '',
  `Salt` varchar(10) NOT NULL DEFAULT '',
  `Status` enum('Unconfirmed','Approval','Active','Rejected','Suspended') NOT NULL DEFAULT 'Unconfirmed',
  `Role` tinyint(4) unsigned NOT NULL DEFAULT '1',
  `Couple` int(10) unsigned NOT NULL DEFAULT '0',
  `Sex` varchar(255) NOT NULL DEFAULT '',
  `LookingFor` set('male','female') NOT NULL DEFAULT '',
  `DescriptionMe` text NOT NULL,
  `Country` varchar(255) NOT NULL DEFAULT '',
  `City` varchar(255) NOT NULL,
  `DateOfBirth` date NOT NULL,
  `Featured` tinyint(1) NOT NULL DEFAULT '0',
  `DateReg` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `DateLastEdit` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `DateLastLogin` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `DateLastNav` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `aff_num` int(10) unsigned NOT NULL DEFAULT '0',
  `Tags` varchar(255) NOT NULL DEFAULT '',
  `zip` varchar(255) NOT NULL,
  `EmailNotify` tinyint(1) NOT NULL DEFAULT '1',
  `LangID` int(11) NOT NULL,
  `UpdateMatch` tinyint(1) NOT NULL DEFAULT '1',
  `Views` int(11) NOT NULL,
  `Rate` float NOT NULL,
  `RateCount` int(11) NOT NULL,
  `CommentsCount` int(11) NOT NULL,
  `PrivacyDefaultGroup` int(11) NOT NULL DEFAULT '3',
  `allow_view_to` int(11) NOT NULL DEFAULT '3',
  `UserStatus` varchar(64) NOT NULL DEFAULT 'online',
  `UserStatusMessage` varchar(255) NOT NULL DEFAULT '',
  `UserStatusMessageWhen` int(10) NOT NULL,
  `Avatar` int(10) unsigned NOT NULL,
  `Height` varchar(255) NOT NULL,
  `Weight` varchar(255) NOT NULL,
  `Income` varchar(255) NOT NULL,
  `Occupation` varchar(255) NOT NULL,
  `Religion` varchar(255) NOT NULL,
  `Education` varchar(255) NOT NULL,
  `RelationshipStatus` enum('Single','In a Relationship','Engaged','Married','It''s Complicated','In an Open Relationship') DEFAULT NULL,
  `Hobbies` text NOT NULL,
  `Interests` text NOT NULL,
  `Ethnicity` varchar(255) NOT NULL,
  `FavoriteSites` text NOT NULL,
  `FavoriteMusic` text NOT NULL,
  `FavoriteFilms` text NOT NULL,
  `FavoriteBooks` text NOT NULL,
  `FullName` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `NickName` (`NickName`),
  KEY `Country` (`Country`),
  KEY `DateOfBirth` (`DateOfBirth`),
  KEY `DateReg` (`DateReg`),
  KEY `DateLastNav` (`DateLastNav`),
  FULLTEXT KEY `NickName_2` (`NickName`,`City`,`DescriptionMe`,`Tags`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Profiles`
--

INSERT INTO `Profiles` VALUES ('1', 'Nick', 'nickc@combinexholdings.com', '2f41e7351b6e6186ddb681cf2da9c3665ef4b885', 'ZDg2Nzhj', 'Active', '3', '0', 'male', '', '.........................................................', 'US', 'Voorhees', '1993-09-03', '0', '2015-09-02 14:27:50', '2015-10-05 01:12:25', '2015-11-10 03:07:59', '2015-11-10 03:07:59', '0', '', '08043', '1', '0', '1', '5', '0', '0', '0', '3', '4', 'offline', '', '0', '0', '', '', '', '', '', '', 'Single', '', '', '', '', '', '', '', 'Nick C');
INSERT INTO `Profiles` VALUES ('2', 'rfg31', 'ray@combinexholdings.com', 'ed708e45f763943c6857a1b6bce1e579e09becb6', 'sKfB3PqJ', 'Active', '1', '0', 'male', '', 'CJP MA Grad working with C-IKON', 'HK', '95 Che Keng Tuk Rd Sai Kung', '1963-10-27', '0', '2015-09-03 20:19:40', '2015-11-02 23:34:19', '2015-11-09 16:15:57', '2015-11-09 18:12:38', '0', '', '00000', '1', '1', '1', '18', '0', '0', '0', '3', '4', 'online', 'I am looking for individuals interested in working on a civil court justice project', '1441598430', '0', '', '', '', '', '', '', 'Single', '', '', '', '', '', '', '', 'Ray Garman');
INSERT INTO `Profiles` VALUES ('3', 'Member201', 'fivfvfhvkvd@aol.com', '6eb1270be4cc887b06075473713614a4ca4b1682', '82hXd=&d', 'Active', '1', '0', 'male', '', '', '', '', '1993-09-03', '0', '2015-09-03 22:06:40', '0000-00-00 00:00:00', '2015-10-30 11:52:22', '2015-10-30 13:33:32', '0', '', '', '1', '1', '0', '0', '0', '0', '0', '3', '3', 'online', '', '0', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Member 201');
INSERT INTO `Profiles` VALUES ('4', 'Member202', 'dsnvafjnvf@aol.com', '35183721cf2b057a76378e5b80ccb3a374f05faa', 'wDd58QUU', 'Active', '1', '0', 'female', '', 'lkgeakgrlkgrgreogketaogjeotkjhotejhtjhojthojtohjtohjth', 'US', 'Voorhees', '1993-09-01', '0', '2015-09-03 22:07:33', '2015-09-14 17:11:13', '2015-09-14 17:11:13', '2015-09-15 01:38:40', '0', '', '08043', '1', '1', '1', '1', '0', '0', '0', '3', '4', 'online', '', '0', '0', '', '', '', '', '', '', 'Single', '', '', '', '', '', '', '', 'Member 202');
INSERT INTO `Profiles` VALUES ('5', 'Member203', 'wfrofvifvbf@aol.com', '553d967c51ba1c0d69b170d8961edd84f4b27418', 'G?Qyz6Un', 'Active', '1', '0', 'male', '', '', '', '', '1993-09-18', '0', '2015-09-03 22:08:31', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0', '', '', '1', '1', '0', '0', '0', '0', '0', '3', '3', 'online', '', '0', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Member 203');
INSERT INTO `Profiles` VALUES ('6', 'Member301', 'ekfjwijrg@aol.com', 'd6616457dc0e561bb051f271f948e7012c59d12c', '?2hz&9?W', 'Active', '1', '0', 'intersex', '', '', '', '', '1993-09-14', '0', '2015-09-03 22:09:10', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0', '', '', '1', '1', '0', '0', '0', '0', '0', '3', '3', 'online', '', '0', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Member 301');
INSERT INTO `Profiles` VALUES ('7', 'Member302', 'ewifjfjvvfdsk@aol.com', 'd06af1b9acf0ab4011ad0cd38dc0ca8cb0278939', 'FJuaN4kN', 'Active', '1', '0', 'male', '', '', '', '', '1993-09-09', '0', '2015-09-03 22:09:46', '0000-00-00 00:00:00', '2015-09-04 14:50:34', '2015-09-04 14:50:38', '0', '', '', '1', '1', '0', '0', '0', '0', '0', '3', '3', 'online', '', '0', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Member 302');
INSERT INTO `Profiles` VALUES ('8', 'Member303', 'ekfwrf@aol.com', 'af96672611632899c7631315af417014c0bf4e13', 'fX953UgF', 'Active', '1', '0', 'male', '', '', '', '', '1993-09-08', '0', '2015-09-03 22:11:17', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0', '', '', '1', '1', '0', '0', '0', '0', '0', '3', '3', 'online', '', '0', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Member 303');
INSERT INTO `Profiles` VALUES ('9', 'rfg32', 'rfg3travel@yahoo.com', '22d43a5333f0703ffad6cbb7e3bd6492101f0bfb', 'BgegYXt/', 'Active', '1', '0', 'male', '', 'CJP MA Graduate working on projects', 'US', 'Sedona', '1963-10-27', '0', '2015-09-03 23:27:35', '2015-09-04 18:38:20', '2015-10-30 11:15:53', '2015-11-02 23:22:07', '0', '', '86336', '1', '1', '1', '5', '0', '0', '0', '3', '4', 'online', '', '0', '0', '', '', '', '', '', '', 'Single', '', '', '', '', '', '', '', 'Ray Garman');
INSERT INTO `Profiles` VALUES ('10', 'rfg33', 'rfg3@rfg3travel.com', 'a8919b690b39f8a14e9dec9e2f92170391badb50', 'WFdq89n9', 'Active', '1', '0', 'male', '', 'C5P MA Graduate Looking For Projects', 'US', 'Lexington', '1963-10-27', '0', '2015-09-03 23:33:34', '2015-09-04 18:41:39', '2015-11-02 15:38:46', '2015-11-03 21:27:24', '0', '', '40502', '1', '1', '1', '5', '0', '0', '0', '3', '4', 'online', '', '0', '0', '', '', '', '', '', '', 'Single', '', '', '', '', '', '', '', 'Ray Garman');
INSERT INTO `Profiles` VALUES ('11', 'Jayne', 'ray@thetastybrains.com', 'e6abeb5f48b905844bf196af0d0fc3c52bc8aac4', 'jgg/v4zF', 'Active', '1', '0', 'female', '', 'CJP Faculty & Program Director of CJP', 'MM', 'Rangoon', '1993-09-02', '0', '2015-09-04 00:41:10', '2015-09-04 14:22:09', '2015-10-30 03:41:53', '2015-10-30 13:33:09', '0', '', '00000', '1', '1', '1', '10', '0', '0', '0', '3', '4', 'online', '', '0', '0', '', '', '', '', '', '', 'Single', '', '', '', '', '', '', '', 'Jayne Docherty');
INSERT INTO `Profiles` VALUES ('12', 'nc02', 'cjvadivjfjv@aol.com', '99984b3149e9643219034b76db37e4f0e24d3dbd', '6GJfr/wc', 'Active', '1', '0', 'male', '', '', '', '', '1993-09-03', '0', '2015-09-04 14:51:45', '0000-00-00 00:00:00', '2015-09-04 14:52:42', '2015-09-07 13:39:35', '0', '', '', '1', '1', '0', '0', '0', '0', '0', '3', '4', 'online', '', '0', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'nick c');
INSERT INTO `Profiles` VALUES ('13', 'testuser', 'testuser@email.com', '9f5d56078a5beecef9380334fbb3c2ecf82ff647', 'khad+bdh', 'Active', '1', '0', 'male', '', '', '', '', '1993-10-04', '0', '2015-10-04 20:00:42', '0000-00-00 00:00:00', '2015-10-06 18:33:20', '2015-10-06 18:33:45', '0', '', '', '1', '1', '0', '0', '0', '0', '0', '3', '3', 'online', '', '0', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'test user');
INSERT INTO `Profiles` VALUES ('14', 'HelpDesk', 'helpdesk@cxipgroup.com', '5839ba36f3cd9e39a2cd661400fc3c098449147d', 'x2pi!!d+', 'Active', '1', '0', 'intersex', '', '', '', '', '1992-12-29', '0', '2015-11-02 14:32:06', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0', '', '', '1', '1', '0', '0', '0', '0', '0', '3', '3', 'online', '', '0', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Help Desk');

-- --------------------------------------------------------
