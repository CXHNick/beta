-- tables
DROP TABLE IF EXISTS `[db_prefix]main`;

-- system objects
DELETE FROM `sys_permalinks` WHERE `standard` = 'modules/?r=gprivate/';
 
-- admin menu
DELETE FROM `sys_menu_admin` WHERE `name` = 'modzzz_gprivate';
  
-- settings
SET @iCategId = (SELECT `ID` FROM `sys_options_cats` WHERE `name` = 'GPrivate' LIMIT 1);
DELETE FROM `sys_options` WHERE `kateg` = @iCategId;
DELETE FROM `sys_options_cats` WHERE `ID` = @iCategId;
DELETE FROM `sys_options` WHERE `Name` IN ('modzzz_gprivate_permalinks');
 
SET @iHandler := (SELECT `id` FROM `sys_alerts_handlers` WHERE `name` = 'modzzz_gprivate' LIMIT 1);
DELETE FROM `sys_alerts` WHERE `handler_id` = @iHandler;
DELETE FROM `sys_alerts_handlers` WHERE `id` = @iHandler;
 

