<?php
set_time_limit(9999999);
/***************************************************************************

*                            Dolphin Smart Community Builder

*                              -----------------

*     begin                : Mon Mar 23 2006

*     copyright            : (C) 2006 BoonEx Group

*     website              : http://www.boonex.com/

* This file is part of Dolphin - Smart Community Builder

*

* Dolphin is free software. This work is licensed under a Creative Commons Attribution 3.0 License. 

* http://creativecommons.org/licenses/by/3.0/

*

* Dolphin is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;

* without even the implied warranty of  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

* See the Creative Commons Attribution 3.0 License for more details. 

* You should have received a copy of the Creative Commons Attribution 3.0 License along with Dolphin, 

* see license.txt file; if not, write to marketing@boonex.com

***************************************************************************/
 
require_once( 'inc/header.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'design.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'admin_design.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'languages.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'utils.inc.php' );
 


echo "START .....<br /><br />";

echo "UPDATING DATABASE .....<br /><br />";


db_res("DELETE FROM `sys_options` WHERE `Name` IN ('bx_groups_boonex_events','bx_groups_events_hide')");
 
$iCategId = (int)db_value("SELECT `id` FROM  `sys_options_cats` WHERE `name`='Groups'");
db_res("  
INSERT INTO `sys_options` (`Name`, `VALUE`, `kateg`, `desc`, `Type`, `check`, `err_text`, `order_in_kateg`, `AvailableValues`) VALUES  
 ('bx_groups_boonex_events', 'on', $iCategId, 'Integrate with default events module', 'checkbox', '', '', '0', '') 
");
 
 
$fields = mysql_list_fields($db['db'], "bx_events_main"); 
$columns = mysql_num_fields($fields);
       
for ($i = 0; $i < $columns; $i++) {
    $field_array[] = mysql_field_name($fields, $i);
}
       
if (!in_array('group_id', $field_array)) {
	db_res("ALTER TABLE `bx_events_main` ADD `group_id` int(10) NOT NULL default '0'");
}


echo "SUCCESSFULLY UPDATED DATABASE ..... <br /><br />";