-- create tables
CREATE TABLE IF NOT EXISTS `[db_prefix]main` (
  `ID` int(10) unsigned NOT NULL auto_increment,
  `Type` varchar(100) NOT NULL default '',
  `EntryUri` varchar(255) NOT NULL,
  `OwnerID` int(10) unsigned NOT NULL,
  `Views` int(11) NOT NULL,
  `allow_view_to` int(11) NOT NULL,
  `allow_edit_to` varchar(16) NOT NULL,
  `allow_upload_photos_to` varchar(16) NOT NULL,
  `allow_upload_videos_to` varchar(16) NOT NULL,
  `allow_upload_sounds_to` varchar(16) NOT NULL,
  `allow_upload_files_to` varchar(16) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `EntryUri` (`EntryUri`),
  KEY `OwnerID` (`OwnerID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `[db_prefix]views_track` (
  `id` int(10) unsigned NOT NULL,
  `viewer` int(10) unsigned NOT NULL,
  `ip` int(10) unsigned NOT NULL,
  `ts` int(10) unsigned NOT NULL,
  KEY `id` (`id`,`viewer`,`ip`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

