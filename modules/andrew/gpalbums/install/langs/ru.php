<?php
/***************************************************************************
*
* IMPORTANT: This is a commercial product made by AndrewP. It cannot be modified for other than personal usage.
* The "personal usage" means the product can be installed and set up for ONE domain name ONLY.
* To be able to use this product for another domain names you have to order another copy of this product (license).
* This product cannot be redistributed for free or a fee without written permission from AndrewP.
* This notice may not be removed from the source code.
*
***************************************************************************/
$sLangCategory = 'Photo Albums for Groups';

$aLangContent = array(
    '_gpa_main' => 'Фотоальбомы',
    '_gpa_main_e' => 'Фотоальбомы',
    '_gpa_privacy_album_view' => 'Разрешить просматривать для',
    '_gpa_new_album' => 'Новый альбом',
    '_gpa_back' => 'Назад',
    '_gpa_share' => 'Поделиться',
    '_gpa_rss' => 'RSS',
    '_gpa_upload_photos' => 'Добавить фото',
    '_gpa_manage_photos' => 'Управление',
    '_gpa_edit_album' => 'Редактировать',
    '_gpa_prefix' => 'Префикс',
    '_gpa_prefix_expl' => 'Префикс для загружаемых фотографий',
    '_gpa_empty_need_upload' => 'Вы еще не загружали фотографии, выберите добавить фото в действиях',
    '_gpa_photos_of_album' => 'Фотографии альбома',
    '_gpa_images' => 'Фотографии',
    '_gpa_upload' => 'Добавить фото',
    '_gpa_fupload' => 'Оптом',
    '_gpa_uploadu' => 'Пин',
    '_gpa_photo_added_success' => 'Фотографии загружены успешно',
    '_gpa_photo_added_failed' => 'Не удалось загрузить фотографии',
    '_gpa_image_files' => 'Фотографии',
    '_gpa_privacy_restr' => 'У вас нет прав просматривать этот альбом',
    '_gpa_groups_privacy_fans' => 'Участники',
    '_gpa_groups_privacy_admins_only' => 'Только администраторы',
    '_gpa_select_images' => 'Кликните тут для выбора фотографий',
    '_gpa_add_description' => 'Добавить / Редактировать название и описание фотографии',
    '_gpa_public_mode' => 'Дать другим участникам группы возможность добафлять фотографии в этот альбом',
    '_gpa_download' => 'Скачать оригинал',
    '_gpa_albums' => 'Фотоальбомы',
    '_mma_agpalbums_feature' => 'Помечать избранные фото (Групповые Фотоальбомы)',
    '_gpa_featured_list' => 'Избранные фотографии групп',
    '_gpa_feature_action' => 'В избранное / Из избранного',
    '_gpa_featured' => 'Добавлено в избранное',
    '_gpa_defeatured' => 'Удалено из избранного',
    '_gpa_spy_upload' => '<a href="{profile_link}">{profile_nick}</a> загрузил новое фото <a href="{obj_url}">{obj_caption}</a> в групповой альбом',
    '_gpa_wall_post' => 'добавил фотоальбом',
    '_gpa_wall_object' => 'фотоальбом',
    '_gpa_photo_by' => 'Это фото было добавлено',
);
