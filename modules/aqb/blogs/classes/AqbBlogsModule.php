<?php
/***************************************************************************
* 
*     copyright            : (C) 2009 AQB Soft
*     website              : http://www.aqbsoft.com
*      
* IMPORTANT: This is a commercial product made by AQB Soft. It cannot be modified for other than personal usage.
* The "personal usage" means the product can be installed and set up for ONE domain name ONLY. 
* To be able to use this product for another domain names you have to order another copy of this product (license).
* 
* This product cannot be redistributed for free or a fee without written permission from AQB Soft.
* 
* This notice may not be removed from the source code.
* 
***************************************************************************/

function aqb_blogs_import($sClassPostfix, $aModuleOverwright = array())
{
    global $aModule;
    $a = $aModuleOverwright ? $aModuleOverwright : $aModule;
    if (!$a || $a['uri'] != 'aqb_blogs') {
        $oMain = BxDolModule::getInstance('AqbBlogsModule');
        $a = $oMain->_aModule;
    }
    bx_import ($sClassPostfix, $a) ;
}

bx_import('BxDolPaginate');
bx_import('BxDolAlerts');
bx_import('BxDolTwigModule');

define ('AQB_BLOGS_PHOTOS_CAT', 'Blog');
define ('AQB_BLOGS_PHOTOS_TAG', 'blog');

define ('AQB_BLOGS_VIDEOS_CAT', 'Blog');
define ('AQB_BLOGS_VIDEOS_TAG', 'blog');

define ('AQB_BLOGS_SOUNDS_CAT', 'Blog');
define ('AQB_BLOGS_SOUNDS_TAG', 'blog');

define ('AQB_BLOGS_FILES_CAT', 'Blog');
define ('AQB_BLOGS_FILES_TAG', 'blog');

define ('AQB_BLOGS_MAX_FANS', 100000);

class AqbBlogsModule extends BxDolTwigModule
{
    var $_oPrivacy;
    var $_aQuickCache = array ();
    var $_sPrefix;
    var $_sPrefixLangs;
    var $_sPrefixOptions;

    function AqbBlogsModule(&$aModule)
    {
        parent::BxDolTwigModule($aModule);
        $this->_sPrefix = $this->_oConfig->getUri();
        $this->_sPrefixLangs = $this->_oConfig->getSystemName('langs');
        $this->_sPrefixOptions = $this->_oConfig->getSystemName('options');
        $this->_sFilterName = $this->_oConfig->getFilterName();

        bx_import ('Privacy', $aModule);
        $this->_oPrivacy = new AqbBlogsPrivacy($this);

        $GLOBALS['oAqbBlogsModule'] = &$this;
    }

    function actionHome ()
    {
        parent::_actionHome(_t($this->_sPrefixLangs . '_page_title_home'));
    }

    function actionFiles ($sUri)
    {
        parent::_actionFiles ($sUri, _t($this->_sPrefixLangs . '_page_title_files'));
    }

    function actionSounds ($sUri)
    {
        parent::_actionSounds ($sUri, _t($this->_sPrefixLangs . '_page_title_sounds'));
    }

    function actionVideos ($sUri)
    {
        parent::_actionVideos ($sUri, _t($this->_sPrefixLangs . '_page_title_videos'));
    }

    function actionPhotos ($sUri)
    {
        parent::_actionPhotos ($sUri, _t($this->_sPrefixLangs . '_page_title_photos'));
    }

    function actionComments ($sUri)
    {
        parent::_actionComments ($sUri, _t($this->_sPrefixLangs . '_page_title_comments'));
    }

    function actionBrowseFans ($sUri)
    {
        parent::_actionBrowseFans ($sUri, 'isAllowedViewFans', 'getFansBrowse', $this->_oDb->getParam($this->_sPrefixOptions . '_perpage_browse_fans'), 'browse_fans/', _t($this->_sPrefixLangs . '_page_title_fans'));
    }

    function actionView ($sUri)
    {
        parent::_actionView ($sUri, _t($this->_sPrefixLangs . '_msg_pending_approval'));
    }

    function actionUploadPhotos ($sUri)
    {
        parent::_actionUploadMedia ($sUri, 'isAllowedUploadPhotos', 'images', array ('images_choice', 'images_upload'), _t($this->_sPrefixLangs . '_page_title_upload_photos'));
    }

    function actionUploadVideos ($sUri)
    {
        parent::_actionUploadMedia ($sUri, 'isAllowedUploadVideos', 'videos', array ('videos_choice', 'videos_upload'), _t($this->_sPrefixLangs . '_page_title_upload_videos'));
    }

    function actionUploadSounds ($sUri)
    {
        parent::_actionUploadMedia ($sUri, 'isAllowedUploadSounds', 'sounds', array ('sounds_choice', 'sounds_upload'), _t($this->_sPrefixLangs . '_page_title_upload_sounds'));
    }

    function actionUploadFiles ($sUri)
    {
        parent::_actionUploadMedia ($sUri, 'isAllowedUploadFiles', 'files', array ('files_choice', 'files_upload'), _t($this->_sPrefixLangs . '_page_title_upload_files'));
    }

    function actionBroadcast ($iEntryId)
    {
        parent::_actionBroadcast ($iEntryId, _t($this->_sPrefixLangs . '_page_title_broadcast'), _t($this->_sPrefixLangs . '_msg_broadcast_no_recipients'), _t($this->_sPrefixLangs . '_msg_broadcast_message_sent'));
    }

    function actionInvite ($iEntryId)
    {
        parent::_actionInvite ($iEntryId, $this->_sPrefix . '_invitation', $this->_oDb->getParam($this->_sPrefixOptions . '_max_email_invitations'), _t($this->_sPrefixLangs . '_msg_invitation_sent'), _t($this->_sPrefixLangs . '_msg_no_users_to_invite'), _t($this->_sPrefixLangs . '_page_title_invite'));
    }

    function  inviteToInbox($aProfile, $aTemplate, $aPlusOriginal){

        global $tmpl;
        require_once( BX_DIRECTORY_PATH_ROOT . 'templates/tmpl_' . $tmpl . '/scripts/BxTemplMailBox.php');

        $aMailBoxSettings = array
        (
            'member_id'  =>  $this->_iProfileId,
            'recipient_id'   => $aProfile['ID'],
            'messages_types'     =>  'mail',
        );

        $aComposeSettings = array
        (
            'send_copy' => false ,
            'send_copy_to_me' => false ,
            'notification' => false ,
        );
        $oMailBox = new BxTemplMailBox('mail_page', $aMailBoxSettings);

        $sMessageBody = $aTemplate['Body'];
        $sMessageBody = str_replace("<GroupName>", $aPlusOriginal['GroupName'] , $sMessageBody);
        $sMessageBody = str_replace("<GroupLocation>", $aPlusOriginal['GroupLocation'] , $sMessageBody);
        $sMessageBody = str_replace("<GroupUrl>", $aPlusOriginal['GroupUrl'] , $sMessageBody);
        $sMessageBody = str_replace("<AcceptUrl>", $aPlusOriginal['AcceptUrl'] .'/'. $aProfile['ID'] , $sMessageBody);
        $sMessageBody = str_replace("<InviterUrl>", $aPlusOriginal['InviterUrl'] , $sMessageBody);
        $sMessageBody = str_replace("<InviterNickName>", $aPlusOriginal['InviterNickName'] , $sMessageBody);
        $sMessageBody = str_replace("<EntryUrl>", $aPlusOriginal['EntryUrl'] , $sMessageBody);
        $sMessageBody = str_replace("<EntryName>", $aPlusOriginal['EntryName'] , $sMessageBody);
        $sMessageBody = str_replace("<InvitationText>", $aPlusOriginal['InvitationText'] , $sMessageBody);

        $oMailBox -> iWaitMinutes = 0;
        $oMailBox -> sendMessage($aTemplate['Subject'], $sMessageBody, $aProfile['ID'], $aComposeSettings);

   }

    function _getInviteParams ($aDataEntry, $aInviter)
    {
        return array (
                'EntryName' => $aDataEntry['title'],
                'EntryLocation' => _t($GLOBALS['aPreValues']['Country'][$aDataEntry['country']]['LKey']) . (trim($aDataEntry['city']) ? ', '.$aDataEntry['city'] : '') . ', ' . $aDataEntry['zip'],
                'EntryUrl' => BX_DOL_URL_ROOT . $this->_oConfig->getBaseUri() . 'view/' . $aDataEntry['uri'],
                'InviterUrl' => $aInviter ? getProfileLink($aInviter['ID']) : 'javascript:void(0);',
                'InviterNickName' => $aInviter ? getNickName($aInviter['ID']) : _t($this->_sPrefixLangs . '_user_unknown'),
                'InvitationText' => nl2br(process_pass_data(strip_tags($_POST['inviter_text']))),
            );
    }

    function actionCalendar ($iYear = '', $iMonth = '')
    {
        parent::_actionCalendar ($iYear, $iMonth, _t($this->_sPrefixLangs . '_page_title_calendar'));
    }

    function actionSearch ($sKeyword = '', $sCategory = '')
    {
        parent::_actionSearch ($sKeyword, $sCategory, _t($this->_sPrefixLangs . '_page_title_search'));
    }

    function actionAdd ()
    {
        parent::_actionAdd (_t($this->_sPrefixLangs . '_page_title_add'));
    }

    function actionEdit ($iEntryId)
    {
        parent::_actionEdit ($iEntryId, _t($this->_sPrefixLangs . '_page_title_edit'));
    }

    function actionDelete ($iEntryId)
    {
        parent::_actionDelete ($iEntryId, _t($this->_sPrefixLangs . '_msg_group_was_deleted'));
    }

    function actionMarkFeatured ($iEntryId)
    {
        parent::_actionMarkFeatured ($iEntryId, _t($this->_sPrefixLangs . '_msg_added_to_featured'), _t($this->_sPrefixLangs . '_msg_removed_from_featured'));
    }

    function actionJoin ($iEntryId, $iProfileId)
    {
        parent::_actionJoin ($iEntryId, $iProfileId, _t($this->_sPrefixLangs . '_msg_joined_already'), _t($this->_sPrefixLangs . '_msg_joined_request_pending'), _t($this->_sPrefixLangs . '_msg_join_success'), _t($this->_sPrefixLangs . '_msg_join_success_pending'), _t($this->_sPrefixLangs . '_msg_leave_success'));
    }

    function actionSharePopup ($iEntryId)
    {
        parent::_actionSharePopup ($iEntryId, _t($this->_sPrefixLangs . '_caption_share_group'));
    }

    function actionManageFansPopup ($iEntryId)
    {
        parent::_actionManageFansPopup ($iEntryId, _t($this->_sPrefixLangs . '_caption_manage_fans'), 'getFans', 'isAllowedManageFans', 'isAllowedManageAdmins', AQB_BLOGS_MAX_FANS);
    }

    function actionTags()
    {
        parent::_actionTags (_t($this->_sPrefixLangs . '_page_title_tags'));
    }

    function actionCategories()
    {
        parent::_actionCategories (_t($this->_sPrefixLangs . '_page_title_categories'));
    }

    function actionDownload ($iEntryId, $iMediaId)
    {
        $aFileInfo = $this->_oDb->getMedia ((int)$iEntryId, (int)$iMediaId, 'files');

        if (!$aFileInfo || !($aDataEntry = $this->_oDb->getEntryByIdAndOwner((int)$iEntryId, 0, true))) {
            $this->_oTemplate->displayPageNotFound ();
            exit;
        }

        if (!$this->isAllowedView ($aDataEntry)) {
            $this->_oTemplate->displayAccessDenied ();
            exit;
        }

        parent::_actionDownload($aFileInfo, 'media_id');
    }

    // ================================== external actions
    function serviceHomepageBlock ()
    {
        if (!$this->_oDb->isAnyPublicContent())
            return '';

        bx_import ('PageMain', $this->_aModule);
        $o = new AqbBlogsPageMain ($this);
        $o->sUrlStart = BX_DOL_URL_ROOT . '?';

        $sDefaultHomepageTab = $this->_oDb->getParam($this->_sPrefixOptions . '_homepage_default_tab');
        $sBrowseMode = $sDefaultHomepageTab;
        switch (bx_get($this->_sFilterName)) {
            case 'featured':
            case 'recent':
            case 'top':
            case 'popular':
            case $sDefaultHomepageTab:
                $sBrowseMode = bx_get($this->_sFilterName);
                break;
        }

        return $o->ajaxBrowse(
            $sBrowseMode,
            $this->_oDb->getParam($this->_sPrefixOptions . '_perpage_homepage'),
            array(
                _t($this->_sPrefixLangs . '_tab_featured') => array('href' => BX_DOL_URL_ROOT . '?' . $this->_sFilterName . '=featured', 'active' => 'featured' == $sBrowseMode, 'dynamic' => true),
                _t($this->_sPrefixLangs . '_tab_recent') => array('href' => BX_DOL_URL_ROOT . '?' . $this->_sFilterName . '=recent', 'active' => 'recent' == $sBrowseMode, 'dynamic' => true),
                _t($this->_sPrefixLangs . '_tab_top') => array('href' => BX_DOL_URL_ROOT . '?' . $this->_sFilterName . '=top', 'active' => 'top' == $sBrowseMode, 'dynamic' => true),
                _t($this->_sPrefixLangs . '_tab_popular') => array('href' => BX_DOL_URL_ROOT . '?' . $this->_sFilterName . '=popular', 'active' => 'popular' == $sBrowseMode, 'dynamic' => true),
            )
        );
    }

    function serviceProfileBlock ($iProfileId)
    {
        $iProfileId = (int)$iProfileId;
        $aProfile = getProfileInfo($iProfileId);
        bx_import ('PageMain', $this->_aModule);
        $o = new AqbBlogsPageMain ($this);
        $o->sUrlStart = getProfileLink($aProfile['ID']) . '?';

        return $o->ajaxBrowse(
            'user',
            $this->_oDb->getParam($this->_sPrefixOptions . '_perpage_profile'),
            array(),
            process_db_input ($aProfile['NickName'], BX_TAGS_NO_ACTION, BX_SLASHES_NO_ACTION),
            true,
            false
        );
    }

    function serviceProfileBlockJoined ($iProfileId)
    {
        $iProfileId = (int)$iProfileId;
        $aProfile = getProfileInfo($iProfileId);
        bx_import ('PageMain', $this->_aModule);
        $o = new AqbBlogsPageMain ($this);
        $o->sUrlStart = getProfileLink($aProfile['ID']) . '?';

        return $o->ajaxBrowse(
            'joined',
            $this->_oDb->getParam($this->_sPrefixOptions . '_perpage_profile'),
            array(),
            process_db_input ($aProfile['NickName'], BX_TAGS_NO_ACTION, BX_SLASHES_NO_ACTION),
            true,
            false
        );
    }

    function serviceGetMemberMenuItem ()
    {
        return parent::_serviceGetMemberMenuItem (_t($this->_sPrefixLangs), _t($this->_sPrefixLangs), $this->_oConfig->getIcon());
    }

    function serviceGetMemberMenuItemAddContent ()
    {
        if (!$this->isAllowedAdd())
            return '';

        return parent::_serviceGetMemberMenuItem (_t($this->_sPrefixLangs . '_group_single'), _t($this->_sPrefixLangs . '_group_single'), $this->_oConfig->getIcon(), false, '&' . $this->_sFilterName . '=add_group');
    }

    function serviceGetWallPost ($aEvent)
    {
        $aParams = array(
            'txt_object' => $this->_sPrefixLangs . '_wall_object',
            'txt_added_new_single' => $this->_sPrefixLangs . '_wall_added_new',
            'txt_added_new_plural' => $this->_sPrefixLangs . '_wall_added_new_items',
            'txt_privacy_view_event' => 'view',
            'obj_privacy' => $this->_oPrivacy
        );
        return parent::_serviceGetWallPost ($aEvent, $aParams);
    }

    function serviceGetWallPostComment($aEvent)
    {
        $aParams = array(
            'txt_privacy_view_event' => 'view',
            'obj_privacy' => $this->_oPrivacy
        );
        return parent::_serviceGetWallPostComment($aEvent, $aParams);
    }

    function serviceGetWallPostOutline($aEvent)
    {
        $aParams = array(
            'txt_privacy_view_event' => 'view',
            'obj_privacy' => $this->_oPrivacy,
            'templates' => array(
                'grouped' => 'wall_outline_grouped'
            )
        );
        return parent::_serviceGetWallPostOutline($aEvent, $this->_oConfig->getIcon(), $aParams);
    }

    function serviceGetSpyPost($sAction, $iObjectId = 0, $iSenderId = 0, $aExtraParams = array())
    {
        return parent::_serviceGetSpyPost($sAction, $iObjectId, $iSenderId, $aExtraParams, array(
            'add' => $this->_sPrefixLangs . '_spy_post',
            'change' => $this->_sPrefixLangs . '_spy_post_change',
            'join' => $this->_sPrefixLangs . '_spy_join',
            'rate' => $this->_sPrefixLangs . '_spy_rate',
            'commentPost' => $this->_sPrefixLangs . '_spy_comment',
        ));
    }

    function serviceGetSubscriptionParams ($sAction, $iEntryId)
    {
        $a = array (
            'change' => _t($this->_sPrefixLangs . '_sbs_change'),
            'commentPost' => _t($this->_sPrefixLangs . '_sbs_comment'),
            'rate' => _t($this->_sPrefixLangs . '_sbs_rate'),
            'join' => _t($this->_sPrefixLangs . '_sbs_join'),
        );

        return parent::_serviceGetSubscriptionParams ($sAction, $iEntryId, $a);
    }

    // ================================== admin actions
    function actionAdministration ($sUrl = '')
    {
        if (!$this->isAdmin()) {
            $this->_oTemplate->displayAccessDenied ();
            return;
        }

        $this->_oTemplate->pageStart();

        $aMenu = array(
            'pending_approval' => array(
                'title' => _t($this->_sPrefixLangs . '_menu_admin_pending_approval'),
                'href' => BX_DOL_URL_ROOT . $this->_oConfig->getBaseUri() . 'administration/pending_approval',
                '_func' => array ('name' => 'actionAdministrationManage', 'params' => array(false, 'administration/pending_approval')),
            ),
            'admin_entries' => array(
                'title' => _t($this->_sPrefixLangs . '_menu_admin_entries'),
                'href' => BX_DOL_URL_ROOT . $this->_oConfig->getBaseUri() . 'administration/admin_entries',
                '_func' => array ('name' => 'actionAdministrationManage', 'params' => array(true, 'administration/admin_entries')),
            ),
            'create' => array(
                'title' => _t($this->_sPrefixLangs . '_menu_admin_add_entry'),
                'href' => BX_DOL_URL_ROOT . $this->_oConfig->getBaseUri() . 'administration/create',
                '_func' => array ('name' => 'actionAdministrationCreateEntry', 'params' => array()),
            ),
            'settings' => array(
                'title' => _t($this->_sPrefixLangs . '_menu_admin_settings'),
                'href' => BX_DOL_URL_ROOT . $this->_oConfig->getBaseUri() . 'administration/settings',
                '_func' => array ('name' => 'actionAdministrationSettings', 'params' => array()),
            ),
        );

        if (empty($aMenu[$sUrl]))
            $sUrl = 'pending_approval';

        $aMenu[$sUrl]['active'] = 1;
        $sContent = call_user_func_array (array($this, $aMenu[$sUrl]['_func']['name']), $aMenu[$sUrl]['_func']['params']);

        echo $this->_oTemplate->adminBlock ($sContent, _t($this->_sPrefixLangs . '_page_title_administration'), $aMenu);
        $this->_oTemplate->addCssAdmin (array('admin.css', 'unit.css', 'twig.css', 'main.css', 'forms_extra.css', 'forms_adv.css'));
        $this->_oTemplate->pageCodeAdmin (_t($this->_sPrefixLangs . '_page_title_administration'));
    }

    function actionAdministrationSettings ()
    {
        return parent::_actionAdministrationSettings($this->_sPrefixOptions);
    }

    function actionAdministrationManage ($isAdminEntries = false, $sUrl = '')
    {
        return parent::_actionAdministrationManage ($isAdminEntries, $this->_sPrefixLangs . '_admin_delete', $this->_sPrefixLangs . '_admin_activate', $sUrl);
    }

    // ================================== events
    function onEventJoinRequest ($iEntryId, $iProfileId, $aDataEntry) {}

    function onEventJoinReject ($iEntryId, $iProfileId, $aDataEntry) {}
    
	function onEventJoinConfirm ($iEntryId, $iProfileId, $aDataEntry) {}

    function onEventFanRemove ($iEntryId, $iProfileId, $aDataEntry) {}

    function onEventFanBecomeAdmin ($iEntryId, $iProfileId, $aDataEntry)
    {
        parent::_onEventFanBecomeAdmin ($iEntryId, $iProfileId, $aDataEntry, $this->_sPrefix . '_fan_become_admin');
    }

    function onEventAdminBecomeFan ($iEntryId, $iProfileId, $aDataEntry)
    {
        parent::_onEventAdminBecomeFan ($iEntryId, $iProfileId, $aDataEntry, $this->_sPrefix . '_admin_become_fan');
    }

    // ================================== permissions
    function isAllowedView ($aDataEntry, $isPerformAction = false)
    {
        // admin and owner always have access
        if ($this->isAdmin() || $aDataEntry['author_id'] == $this->_iProfileId)
            return true;

        // check admin acl
        $this->_defineActions();
        $aCheck = checkAction($this->_iProfileId, ACTION_ID_BLOGS_PREMIUM_VIEW_BLOGS, $isPerformAction);
        if ($aCheck[CHECK_ACTION_RESULT] != CHECK_ACTION_RESULT_ALLOWED)
            return false;

        // check user group
        return $this->_oPrivacy->check('view', $aDataEntry['id'], $this->_iProfileId);
    }

    function isAllowedBrowse ($isPerformAction = false)
    {
        if ($this->isAdmin())
            return true;
        $this->_defineActions();
        $aCheck = checkAction($this->_iProfileId, ACTION_ID_BLOGS_PREMIUM_BROWSE, $isPerformAction);
        return $aCheck[CHECK_ACTION_RESULT] == CHECK_ACTION_RESULT_ALLOWED;
    }

    function isAllowedSearch ($isPerformAction = false)
    {
        if ($this->isAdmin())
            return true;
        $this->_defineActions();
        $aCheck = checkAction($this->_iProfileId, ACTION_ID_BLOGS_PREMIUM_SEARCH, $isPerformAction);
        return $aCheck[CHECK_ACTION_RESULT] == CHECK_ACTION_RESULT_ALLOWED;
    }

    function isAllowedAdd ($isPerformAction = false)
    {
        if ($this->isAdmin())
            return true;
        if (!$GLOBALS['logged']['member'])
            return false;
        $this->_defineActions();
        $aCheck = checkAction($this->_iProfileId, ACTION_ID_BLOGS_PREMIUM_ADD_BLOGS, $isPerformAction);
        return $aCheck[CHECK_ACTION_RESULT] == CHECK_ACTION_RESULT_ALLOWED;
    }

    function isAllowedEdit ($aDataEntry, $isPerformAction = false)
    {
        if ($this->isAdmin() || ($GLOBALS['logged']['member'] && $aDataEntry['author_id'] == $this->_iProfileId && isProfileActive($this->_iProfileId)))
            return true;

        // check acl
        $this->_defineActions();
        $aCheck = checkAction($this->_iProfileId, ACTION_ID_BLOGS_PREMIUM_EDIT_ANY_BLOGS, $isPerformAction);
        return $aCheck[CHECK_ACTION_RESULT] == CHECK_ACTION_RESULT_ALLOWED;
    }

    function isAllowedMarkAsFeatured ($aDataEntry, $isPerformAction = false)
    {
        if ($this->isAdmin())
            return true;
        $this->_defineActions();
        $aCheck = checkAction($this->_iProfileId, ACTION_ID_BLOGS_PREMIUM_MARK_AS_FEATURED, $isPerformAction);
        return $aCheck[CHECK_ACTION_RESULT] == CHECK_ACTION_RESULT_ALLOWED;
    }

    function isAllowedBroadcast ($aDataEntry, $isPerformAction = false)
    {
        return false;
    }

    function isAllowedDelete (&$aDataEntry, $isPerformAction = false)
    {
        if ($this->isAdmin() || ($GLOBALS['logged']['member'] && $aDataEntry['author_id'] == $this->_iProfileId && isProfileActive($this->_iProfileId)))
            return true;
        $this->_defineActions();
        $aCheck = checkAction($this->_iProfileId, ACTION_ID_BLOGS_PREMIUM_DELETE_ANY_BLOGS, $isPerformAction);
        return $aCheck[CHECK_ACTION_RESULT] == CHECK_ACTION_RESULT_ALLOWED;
    }

    function isAllowedJoin (&$aDataEntry)
    {
        if (!$this->_iProfileId)
            return false;
        return $this->_oPrivacy->check('join', $aDataEntry['id'], $this->_iProfileId);
    }

    function isAllowedSendInvitation (&$aDataEntry)
    {
        return $this->isAdmin() || $this->isEntryAdmin($aDataEntry) ? true : false;
    }

    function isAllowedShare (&$aDataEntry)
    {
        return true;
    }

    function isAllowedPostInForum(&$aDataEntry, $iProfileId = -1)
    {
        if (-1 == $iProfileId)
            $iProfileId = $this->_iProfileId;
        return $this->isAdmin() || $this->isEntryAdmin($aDataEntry) || $this->_oPrivacy->check('post_in_forum', $aDataEntry['id'], $iProfileId);
    }

    function isAllowedRate(&$aDataEntry)
    {
        if ($this->isAdmin())
            return true;
        return $this->_oPrivacy->check('rate', $aDataEntry['id'], $this->_iProfileId);
    }

    function isAllowedComments(&$aDataEntry)
    {
        if ($this->isAdmin())
            return true;
        return $this->_oPrivacy->check('comment', $aDataEntry['id'], $this->_iProfileId);
    }

    function isAllowedViewFans(&$aDataEntry)
    {
        if ($this->isAdmin())
            return true;
        return $this->_oPrivacy->check('view_fans', $aDataEntry['id'], $this->_iProfileId);
    }

    function isAllowedUploadPhotos(&$aDataEntry)
    {
        if (!BxDolRequest::serviceExists('photos', 'perform_photo_upload', 'Uploader'))
            return false;
        if (!$this->_iProfileId)
            return false;
        if ($this->isAdmin())
            return true;
        if (!$this->isMembershipEnabledForImages())
            return false;
        return $this->_oPrivacy->check('upload_photos', $aDataEntry['id'], $this->_iProfileId);
    }

    function isAllowedUploadVideos(&$aDataEntry)
    {
        if (!BxDolRequest::serviceExists('videos', 'perform_video_upload', 'Uploader'))
            return false;
        if (!$this->_iProfileId)
            return false;
        if ($this->isAdmin())
            return true;
        if (!$this->isMembershipEnabledForVideos())
            return false;
        return $this->_oPrivacy->check('upload_videos', $aDataEntry['id'], $this->_iProfileId);
    }

    function isAllowedUploadSounds(&$aDataEntry)
    {
        if (!BxDolRequest::serviceExists('sounds', 'perform_music_upload', 'Uploader'))
            return false;
        if (!$this->_iProfileId)
            return false;
        if ($this->isAdmin())
            return true;
        if (!$this->isMembershipEnabledForSounds())
            return false;
        return $this->_oPrivacy->check('upload_sounds', $aDataEntry['id'], $this->_iProfileId);
    }

    function isAllowedUploadFiles(&$aDataEntry)
    {
        if (!BxDolRequest::serviceExists('files', 'perform_file_upload', 'Uploader'))
            return false;
        if (!$this->_iProfileId)
            return false;
        if ($this->isAdmin())
            return true;
        if (!$this->isMembershipEnabledForFiles())
            return false;
        return $this->_oPrivacy->check('upload_files', $aDataEntry['id'], $this->_iProfileId);
    }

    function isAllowedCreatorCommentsDeleteAndEdit (&$aDataEntry, $isPerformAction = false)
    {
        if ($this->isAdmin())
            return true;
        if (getParam($this->_sPrefixOptions . '_author_comments_admin') && $this->isEntryAdmin($aDataEntry))
            return true;
        $this->_defineActions();
        $aCheck = checkAction($this->_iProfileId, ACTION_ID_BLOGS_PREMIUM_COMMENTS_DELETE_AND_EDIT, $isPerformAction);
        return $aCheck[CHECK_ACTION_RESULT] == CHECK_ACTION_RESULT_ALLOWED;
    }

    function isAllowedManageAdmins($aDataEntry)
    {
        if (($GLOBALS['logged']['member'] || $GLOBALS['logged']['admin']) && $aDataEntry['author_id'] == $this->_iProfileId && isProfileActive($this->_iProfileId))
            return true;
        return false;
    }

    function isAllowedManageFans($aDataEntry)
    {
        return $this->isEntryAdmin($aDataEntry);
    }

    function isFan($aDataEntry, $iProfileId = 0, $isConfirmed = true)
    {
        if (!$iProfileId)
            $iProfileId = $this->_iProfileId;
        return $this->_oDb->isFan ($aDataEntry['id'], $iProfileId, $isConfirmed) ? true : false;
    }

    function isEntryAdmin($aDataEntry, $iProfileId = 0)
    {
        if (!$iProfileId)
            $iProfileId = $this->_iProfileId;
        if (($GLOBALS['logged']['member'] || $GLOBALS['logged']['admin']) && $aDataEntry['author_id'] == $iProfileId && isProfileActive($iProfileId))
            return true;
        return $this->_oDb->isGroupAdmin ($aDataEntry['id'], $iProfileId) && isProfileActive($iProfileId);
    }

    function _defineActions ()
    {
        defineMembershipActions(
        	array(
        		'blogs premium view blogs', 
        		'blogs premium browse', 
        		'blogs premium search', 
        		'blogs premium add blogs', 
        		'blogs premium comments delete and edit', 
        		'blogs premium edit any blogs', 
        		'blogs premium delete any blogs', 
        		'blogs premium mark as featured', 
        		'blogs premium approve blogs'
        	),
        	'ACTION_ID_'
        );
    }

    function _browseMy (&$aProfile)
    {
        parent::_browseMy ($aProfile, _t($this->_sPrefixLangs . '_page_title_my_groups'));
    }

    function _formatLocation (&$aDataEntry, $isCountryLink = false, $isFlag = false)
    {
        $sFlag = $isFlag ? ' ' . genFlag($aDataEntry['country']) : '';
        $sCountry = _t($GLOBALS['aPreValues']['Country'][$aDataEntry['country']]['LKey']);
        if ($isCountryLink)
            $sCountry = '<a href="' . $this->_oConfig->getBaseUri() . 'browse/country/' . strtolower($aDataEntry['country']) . '">' . $sCountry . '</a>';
        return (trim($aDataEntry['address']) ? $aDataEntry['address'] . ', ' : '') . (trim($aDataEntry['city']) ? $aDataEntry['city'] . ', ' : '') . $sCountry . $sFlag;
    }

    function _formatSnippetTextForOutline($aEntryData)
    {
        return $this->_oTemplate->parseHtmlByName('wall_outline_extra_info', array(
            'desc' => $this->_formatSnippetText($aEntryData, 200),
            'location' => $this->_formatLocation($aEntryData, false, false),
            'likes_count' => $aEntryData['likes_count'],
        ));
    }
}
