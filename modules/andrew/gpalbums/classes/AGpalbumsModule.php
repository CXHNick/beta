<?php
/***************************************************************************
*
* IMPORTANT: This is a commercial product made by AndrewP. It cannot be modified for other than personal usage.
* The "personal usage" means the product can be installed and set up for ONE domain name ONLY.
* To be able to use this product for another domain names you have to order another copy of this product (license).
* This product cannot be redistributed for free or a fee without written permission from AndrewP.
* This notice may not be removed from the source code.
*
***************************************************************************/
bx_import('BxDolModule');
bx_import('BxDolPageView');
require_once('AGpalbumsPrivacy.php');
//require_once('AGpalbumsPageView.php');

/*
images rules:
xxxxxx.xx  - original
i_xxxxxx.xx - image 650*650
t_xxxxxx.xx - thumb 128*128
*/

class AGpalbumsModule extends BxDolModule {

    // inner variables
    var $oPrivacy;
    // var $oGPrivacy;
    var $aPageTmpl;
    var $sModuleUrlHalf;
    var $sModuleUrl;
    var $_iVisitorID;
    var $_iGrpID;
    var $_aGrpInfo;
    var $sUploadUrl;
    var $sUploadDir;

    // settings
    var $iThumbSize;
    var $iImgSize;

    /**
    * Constructor
    */
    function AGpalbumsModule($aModule) {
        parent::BxDolModule($aModule);

        $this->aPageTmpl = array(
            'name_index' => 7, 
            'header' => $GLOBALS['site']['title'],
            'header_text' => '',
        );

        $this->_iVisitorID = $this->getUserId();

        $this->iThumbSize = 128;
        $this->iImgSize = 650;
        $this->sUploadUrl = $this->_oConfig->getHomeUrl() . 'data/files/';
        $this->sUploadDir = $this->_oConfig->getHomePath() . 'data/files/';

        $this->oPrivacy = new AGpalbumsPrivacy($this);
        // $this->oGPrivacy = new AGpalbumsGrpPrivacy($this);
    }

    function actionHome() {
        $sCode = $this->_oTemplate->displayAccessDenied();
        //$sCode = '';

        $this->aPageTmpl['header'] = _t('_gpa_main');
        $this->aPageTmpl['header_text'] = _t('_gpa_main');
        $this->_oTemplate->pageCode($this->aPageTmpl, array('page_main_code' => $sCode));
    }

        //Nick
    /*function serviceViewFileBlock() {

        bx_import ('PageView', $this->_aModule);
        $oPage = new AGpalbumsPageView($this, $this->_oDb->getImageInfoByUri($sUri));
        
        return $oPage->getBlockCode_ViewFile();
    }*/

    function actionView($sUri) {

        //$this->_oTemplate->pageStart();

        //echo "<script type='text/javascript'>alert('$sUri');</script>";

        bx_import ('PageView', $this->_aModule);
        $oPage = new AGpalbumsPageView($this, $this->_oDb->getImageInfoByUri($sUri));
        //echo $oPage->getCode();

        //$sCode = $oPage->getCode();

        //$this->_oTemplate->pageStart();

        //$this->_oTemplate->pageCode($this->aPageTmpl, array('page_main_code' => $sCode));

        //$this->_oTemplate->pageCode(_t('_gpa_file_view'), true, true);

        //$doop = "doop";
        //echo "<script type='text/javascript'>alert('$doop');</script>";

    }



    function serviceGetGroupBlock($sGroupUri) {
        $sGroupUriSafe = process_db_input($sGroupUri, BX_TAGS_STRIP);

        $this->sModuleUrl = BX_DOL_URL_ROOT . $this->_oConfig->getBaseUri() . 'group/' . $sGroupUriSafe . '/';
        $this->_aGrpInfo = $this->_oDb->getGroupInfoByUri($sGroupUriSafe);

        $this->sModuleUrlHalf = $this->_oConfig->getBaseUri() . 'group/'.$sGroupUriSafe.'/';
        $this->_iGrpID = (int)$this->_aGrpInfo['id'];

        $sCSS = $this->_oTemplate->addCss('main.css', true);
        return $sCSS . $this->getWrap($this->getAlbumsBlock('last', false));
    }
    function serviceGetIndexBlock($sType = '') {
        $sCSS = $this->_oTemplate->addCss('main.css', true);
        return $sCSS . $this->getWrap($this->getAlbumsBlock('last', false, $sType));
    }

    function serviceIndexPageFeature() {
        require_once( $this->_oConfig->getClassPath() . 'AGpalbumsImgSearch.php');
        $oReSearch = new AGpalbumsImgSearch();
        $oReSearch->bGlobMode = true;
        $oReSearch->aCurrent['paginate']['perPage'] = 10;
        $oReSearch->aCurrent['sorting'] ='last'; // 'rand'
        $oReSearch->aCurrent['restriction']['feature_status']['value'] = 1;

        $sResult = $oReSearch->displayResultBlock();
        if ($oReSearch->aCurrent['paginate']['totalNum'] > 0 && $sResult != '<div class="result_block"><div class="clear_both"></div></div>') {
            $sResult = $GLOBALS['oFunctions']->centerContent($sResult, '.gpa_su');

            $sPagination = $oReSearch->showPagination2(false, BX_DOL_URL_ROOT . 'm/groups/home/');
            $sCss = $this->_oTemplate->addCss(array('main.css'), true);
            return array($sCss . $sResult, array(), $sPagination);
        }
    }

    function actionGroup($sGroupUri, $sSubAct = '', $sSubParam = '', $sPPhotoUri = '') {
        $sGroupUriSafe = process_db_input($sGroupUri, BX_TAGS_STRIP);
        $aGroupInfo = $this->_oDb->getGroupInfoByUri($sGroupUriSafe);

        $this->_aGrpInfo = $aGroupInfo;
        $this->_iGrpID = (int)$aGroupInfo['id'];
        $this->sModuleUrl = BX_DOL_URL_ROOT . $this->_oConfig->getBaseUri() . 'group/'.$sGroupUriSafe.'/';
        $this->sModuleUrlHalf = $this->_oConfig->getBaseUri() . 'group/'.$sGroupUriSafe.'/';

        $GLOBALS['oTopMenu']->setCustomVar('bx_groups_view_uri', $sGroupUriSafe);

        bx_import('BxDolPrivacy');
        $oPrivacy = new BxDolPrivacy('bx_groups_main', 'id', 'author_id');

        if ($_POST['action'] == 'accept_multi_files' && (int)$_POST['oid'] && $this->_iVisitorID == 0) {
            $this->_iVisitorID = (int)$_POST['oid'];
        }

        //$bPossibleToView = $oPrivacy->check('view_group', $this->_iGrpID, $this->_iVisitorID);
        $oGroups = BxDolModule::getInstance('BxGroupsModule');

        $bPossibleToView = $oGroups->_oDb->isFan($this->_iGrpID, getLoggedId(), 1);
        //$bPossibleToView = true;
        if (! $bPossibleToView) {
            $sCode = $this->_oTemplate->displayAccessDenied();
            $sCode = '';
            $aPageVars = array('page_main_code' => $sCode);
            $this->aPageTmpl['header'] = _t('_gpa_main');
            $this->_oTemplate->pageCode($this->aPageTmpl, $aPageVars);
            exit;
        }

        $aGrpVars = array ('BaseUri' => BX_DOL_URL_ROOT . 'm/groups/');
        $GLOBALS['oTopMenu']->setCustomSubActions($aGrpVars, 'bx_groups_title', false);

        $GLOBALS['oTopMenu']->setCustomSubHeader($aGroupInfo['title']);
        $GLOBALS['oTopMenu']->setCustomVar('bx_groups_view_uri', $sGroupUriSafe);

        $sAlbumUriSafe = ($sSubParam != '') ? process_db_input($sSubParam, BX_TAGS_STRIP) : '';
        if ($sAlbumUriSafe != '') {
            $iAlbumID = $this->_oDb->getAlbumIDByUri($sAlbumUriSafe);
            $aAlbumInfo = $this->_oDb->getAlbumInfo($iAlbumID);
            $sAlbumName = $aAlbumInfo['Title'];
        }

        $BreadcrumbElements = array(
            _t('_bx_groups') => BX_DOL_URL_ROOT . 'm/groups/home/',
            $aGroupInfo['title'] => BX_DOL_URL_ROOT . 'm/groups/view/' . $sGroupUriSafe,
            _t('_gpa_main') => $this->sModuleUrl
        );
        if ($sSubAct != '' && $sSubParam != '') {
            switch($sSubAct) {
                case 'upload':
                case 'fupload':
                case 'uploadu':
                    $BreadcrumbElements[$sAlbumName] = $this->sModuleUrl . 'album/' . $sAlbumUriSafe . '/';
                    $BreadcrumbElements[_t('_gpa_upload_photos')] = '';
                    break;
                case 'manage':
                    $BreadcrumbElements[$sAlbumName] = $this->sModuleUrl . 'album/' . $sAlbumUriSafe . '/';
                    $BreadcrumbElements[_t('_gpa_manage_photos')] = '';
                    break;
                case 'album':
                    $BreadcrumbElements[$sAlbumName] = $this->sModuleUrl . 'album/' . $sAlbumUriSafe . '/';
                    break;
            }
        }
        $GLOBALS['oTopMenu']->setCustomBreadcrumbs($BreadcrumbElements);

        switch ($sSubAct) {
            case 'new':
                $sCode = $this->getAlbumEditForm(0, $sSubAct);
                break;
            case 'edit':
                $sCode = $this->getAlbumEditForm($iAlbumID, $sSubAct);
                break;
            case 'delete':
                $sCode = $this->performAlbumDelete($iAlbumID);
                break;
            case 'album':
                if ($sPPhotoUri == '') {
                    $sCode = $this->getActionAlbum($sSubParam);
                } else {
                    $sPhotoUriSafe = process_db_input($sPPhotoUri, BX_TAGS_STRIP);
                    $sCode = $this->getActionAlbum($sSubParam, 'photo', $sPhotoUriSafe);
                }
                break;
            case 'upload':
            case 'fupload':
            case 'uploadu':
                $sCode = $this->getActionAlbum($sSubParam, $sSubAct);
                break;
            case 'flash':
            case 'xml_flash':
            case 'manage':
                $sCode = $this->getActionAlbum($sSubParam, $sSubAct);
                break;
            case 'share':
                $sCode = $this->getActionShare($sSubParam);
                break;
            default:
                $sCode = $this->getAlbumsBlock($sSubParam);
                break;
        }

        $this->aPageTmpl['css_name'] = array('main.css');
        $this->aPageTmpl['header'] = _t('_gpa_main');
        $this->aPageTmpl['header_text'] = _t('_gpa_main');
        $this->_oTemplate->pageCode($this->aPageTmpl, array('page_main_code' => $sCode));
    }
    function actionFeature($sUnitID = 0) {
        $iUnitID = (int)$sUnitID;
        $aPostInfo = $this->_oDb->getImageInfo($iUnitID);
        if ($this->isAllowedFeature()) {
            $iNewStatus = ($aPostInfo['featured'] == 1) ? 2 : 1;
            $this->_oDb->updateFeature($iUnitID, $iNewStatus);

            $sMsg = ($iNewStatus == 1) ? _t('_gpa_featured') : _t('_gpa_defeatured');
            header('Content-Type: text/html; charset=utf-8');
            echo MsgBox($sMsg, 2);
            exit;
        }
    }

    // ================================== permissions
    function isAdmin() {
        return isAdmin($this->_iVisitorID) || isModerator($this->_iVisitorID);
    }
    function isAllowedFeature($isPerformAction = false) {
        if ($this->isAdmin()) return true;
        if (isMember() == false) return false;
        $this->_defineActions();
        $aCheck = checkAction($this->_iVisitorID, A_AGPALBUMS_FEATURE, $isPerformAction);
        return $aCheck[CHECK_ACTION_RESULT] == CHECK_ACTION_RESULT_ALLOWED;
    }
    function _defineActions () {
        defineMembershipActions(array('agpalbums feature'), 'A_');
    }

    function getWrap($sCode) {
        return '<div class="bx-def-bc-padding">' . $sCode . '<div class="clear_both"></div></div>';
    }

    //sType added by Nick - get albums of different types
    function getAlbumsBlock($sSortParam = 'last', $bDesignBox = true, $sType = '') {
        require_once( $this->_oConfig->getClassPath() . 'AGpalbumsAlbumsSearch.php');
        $oAlbumsSearch = new AGpalbumsAlbumsSearch();

        $oAlbumsSearch->aCurrent['restriction']['group_id']['value'] = $this->_aGrpInfo['id'];
        //Nick
        if ($sType) {
                $oAlbumsSearch->aCurrent['restriction']['type']['value'] = $sType;
                $oAlbumsSearch->aCurrent['isLibrary'] = true;
        }

        // privacy
        if ($this->_iVisitorID) {
            // $oAlbumsSearch->aCurrent['restriction']['allow_view']['value'] = array(BX_DOL_PG_ALL, BX_DOL_PG_MEMBERS);
        } else {
            $oAlbumsSearch->aCurrent['restriction']['allow_view']['value'] = array(BX_DOL_PG_ALL);
        }

        if ($bDesignBox) {
            $oAlbumsSearch->aCurrent['paginate']['perPage'] = 12;
            $sRequest = $this->sModuleUrl . '&page={page}&per_page={per_page}';
            $oAlbumsSearch->aCurrent['paginate']['page_url'] = $sRequest;
        } else {
            // $oAlbumsSearch->aCurrent['paginate']['perPage'] = 2;
        }

        $sCode = $oAlbumsSearch->displayResultBlock();

        $sPagination = '';
        if ($bDesignBox) {
            $sPagination = $oAlbumsSearch->showSimplePagination();
        }

        $sCode = ($oAlbumsSearch->aCurrent['paginate']['totalNum'] == 0 || $sCode == '<div class="result_block"><div class="clear_both"></div></div>') ? MsgBox(_t('_Empty')) : $GLOBALS['oFunctions']->centerContent($sCode, '.gpa_su');

        $aExtraButtons = array(
            'back' => array('href' => $this->sModuleUrl, 'title' => _t('_gpa_main'), 'onclick' => '', 'active' => ($sSortParam == ''), 'icon' => 'chevron-left')
        );
        if ($this->canOperateInGrp()) {
            $aExtraButtons['add_album'] = array('href' => $this->sModuleUrl . 'new/', 'title' => _t('_gpa_new_album'), 'onclick' => '', 'active' => ($sSortParam == 'new'), 'icon' => 'plus-sign');
        }
        $sRSSActions = BxDolPageView::getBlockCaptionMenu(mktime(), $aExtraButtons);
        return ($bDesignBox) ? DesignBoxContent(_t('_gpa_main'), $this->getWrap($sCode), 1, $sRSSActions, $sPagination) : $sCode;
    }

    function getAlbumEditForm($iAlbumID = 0, $sSubParam = '') {
        if (! $this->canOperateInGrp()) return $this->_oTemplate->displayAccessDenied();
        if ($iAlbumID > 0 && $this->canOperateInAlb($iAlbumID) == false) return $this->_oTemplate->displayAccessDenied();

        $sSubmitC = _t('_Submit');
        $sAction = 'add';

        $sTitle = $sDesc = $sAlbumUri = '';
        $bPublicMode = false;
        $sExtraActionUrl = 'new/';

        // privacy
        $aInputPrivacyCustom = array (
            array('key' => 'f', 'value' => _t('_gpa_groups_privacy_fans')),
            array('key' => 'a', 'value' => _t('_gpa_groups_privacy_admins_only'))
        );
        $aInputPrivacyCustomPass = array (
            'pass' => 'Preg', 
            'params' => array('/^([0-9af]+)$/'),
        );
        $aAllowView = $this->oPrivacy->getGroupChooser($this->_iVisitorID,
            'gpalbums', 'album_view', $aInputPrivacyCustom, _t('_gpa_privacy_album_view'));

        unset($aAllowView['values'][0]); // me only
        // unset($aAllowView['values'][1]); // public
        // unset($aAllowView['values'][4]); // fans
        //$aAllowView['values'] = array_merge($aAllowView['values'], $aInputPrivacyCustom);
        //echoDbg($aAllowView['values']);
        $aAllowView['db'] = $aInputPrivacyCustomPass;

        $sTabTitle = _t('_gpa_new_album');
        if ($iAlbumID) {
            $sSubmitC = _t('_Save');
            $sAction = 'edit';

            $aAlbumInfo = $this->_oDb->getAlbumInfo($iAlbumID);
            $sTitle = $aAlbumInfo['Title'];
            $sDesc = $aAlbumInfo['Desc'];
            $sAlbumUri = $aAlbumInfo['Uri'];

            $bPublicMode = ($aAlbumInfo['public_mode'] == 1);

            $sExtraActionUrl = $sAction . '/' . $sAlbumUri . '/';
            $sTabTitle = _t('_gpa_edit_album');
        }

        //adding / editing form
        $aForm = array(
            'form_attrs' => array(
                'name' => 'create_album_form',
                'action' => $this->sModuleUrl . $sExtraActionUrl ,
                'method' => 'post',
            ),
            'params' => array (
                'db' => array(
                    'table' => 'agrp_palbums_albums',
                    'key' => 'ID',
                    'submit_name' => 'add_button',
                ),
            ),
            'inputs' => array(
                'action' => array(
                    'type' => 'hidden',
                    'name' => 'action',
                    'value' => $sAction,
                ),
                'AlbumType' => array(
                    'type' => 'select',
                    'name' => 'AlbumType',
                    'caption' => 'Album Type',
                    'values' => array('Photo', 'File', 'Video', 'Sound'),
                    //'value' => 'Photo',
                    'required' => true, 
                    ),
                'Title' => array(
                    'type' => 'text',
                    'name' => 'Title',
                    'caption' => _t('_Title'),
                    'required' => true,
                    'value' => $sTitle,
                    'checker' => array (
                        'func' => 'length',
                        'params' => array(3,255),
                        'error' => _t('_title_min_lenght', 3),
                    ),
                    'db' => array (
                        'pass' => 'Xss', 
                    ),
                ),
                'Desc' => array(
                    'type' => 'text',
                    'name' => 'Desc',
                    'caption' => _t('_Description'),
                    'required' => true,
                    'value' => $sDesc,
                    'checker' => array (
                        'func' => 'length',
                        'params' => array(3,255),
                        'error' => _t('_title_min_lenght', 3),
                    ),
                    'db' => array (
                        'pass' => 'Xss', 
                    ),
                ),
                'AllowAlbumView' => $aAllowView,
                'public_mode' => array(
                    'type' => 'checkbox',
                    'name' => 'public_mode',
                    'caption' => _t('_gpa_public_mode'),
                    'value' => 'on',
                    'checked' => $bPublicMode,
                    'db' => array (
                        'pass' => 'Boolean',
                    ),
                ),
                'add_button' => array(
                    'type' => 'input_set',
                    'colspan' => 'true',
                    0 => array (
                        'type' => 'submit',
                        'name' => 'add_button',
                        'value' => $sSubmitC,
                    ),
                    1 => array (
                        'type' => 'button',
                        'value' => _t('_Cancel'),
                        'attrs' => array('onclick' => "window.location.href = '{$this->sModuleUrl}'"),
                    ),
                )
            ),
        );

        if ($iAlbumID) {
            $aForm['inputs']['AllowAlbumView']['value'] = $aAlbumInfo['AllowAlbumView'];
            unset($aForm['inputs']['AlbumType']);
        }

        if (empty($aForm['inputs']['AllowAlbumView']['value']) || !$aForm['inputs']['AllowAlbumView']['value'])
            $aForm['inputs']['AllowAlbumView']['value'] = BX_DOL_PG_ALL;

        $sCode = '';
        $oForm = new BxTemplFormView($aForm);
        $oForm->initChecker();
        if ($oForm->isSubmittedAndValid()) {
            $aValsAdd = array();

            $iLastId = -1;
            if ($iAlbumID>0) {
                $oForm->update($iAlbumID, $aValsAdd);
                $iLastId = $iAlbumID;
            } else {
                $sWhen = time();
                $sAlbumUri = uriGenerate($_POST['Title'], 'agrp_palbums_albums', 'Uri');

                $aValsAdd = array (
                    'Owner' => $this->_iVisitorID,
                    'When' => $sWhen,
                    'Uri' => $sAlbumUri,
                    'GroupID' => $this->_aGrpInfo['id'],
                    'Type' => $aForm['inputs']['AlbumType']['values'][$_POST['AlbumType']],
                ); 

                $iLastId = $oForm->insert($aValsAdd);

                bx_import('BxDolAlerts');
                $oAlert = new BxDolAlerts('agrp_palbums', 'post', $this->_iGrpID, $this->_iVisitorID, array('obj_id' => $iLastId));
                $oAlert->alert();
            }

            header('Location: ' . $this->sModuleUrl . 'album/' . $sAlbumUri . '/');
        }

        $sRSSActions = BxDolPageView::getBlockCaptionMenu(mktime(), array(
            'back' => array('href' => $this->sModuleUrl, 'title' => _t('_gpa_main'), 'onclick' => '', 'active' => ($sSubParam == ''), 'icon' => 'chevron-left'),
            'add_album' => array('href' => $this->sModuleUrl . 'new/', 'title' => $sTabTitle, 'onclick' => '', 'active' => ($sSubParam == 'new' || $sSubParam == 'edit'), 'icon' => 'plus-sign'),
        ));
        return DesignBoxContent($sTitle, $this->getWrap($sCode . $oForm->getCode()), 1, $sRSSActions);
    }

    function getActionAlbum($sAlbumUriParam = 0, $sSubAction = '', $sPhotoUri = '') {
        $sAlbumUriSafe = process_db_input($sAlbumUriParam, BX_TAGS_STRIP);
        $iAlbumID = $this->_oDb->getAlbumIDByUri($sAlbumUriSafe);
        $aAlbumInfo = $this->_oDb->getAlbumInfo($iAlbumID);
        $iAlbumOwnerID = $aAlbumInfo['Owner']; //$this->_oDb->getAlbumOwnerByID($iAlbumID);
        $sCode = $sRateBlock = $sCommentsBlock = '';
        $iPhotoOwnId = 0;

        if ($iAlbumID) {
            $aUploadModes = array ('upload' => 1, 'fupload' => 1, 'uploadu' => 1);

            if ($sSubAction == '') {
                $bPossibleToView = $this->oPrivacy->check('album_view', $iAlbumID, $this->_iVisitorID);
                if (! $bPossibleToView) {
                    $sCode = MsgBox(_t('_gpa_privacy_restr'));
                    $aPageVars = array('page_main_code' => $sCode);
                    $this->aPageTmpl['header'] = _t('_gpa_photos_of_album');
                    $this->_oTemplate->pageCode($this->aPageTmpl, $aPageVars);
                    exit;
                }

                require_once( $this->_oConfig->getClassPath() . 'AGpalbumsExtraViews.php');
                $oExtraViews = new AGpalbumsExtraViews($this);
                $aViewButtons = $oExtraViews->getViewModesActions($this->_oConfig->getBaseUri() . 'group/' . $this->_aGrpInfo['uri'] . '/flash/' . $sAlbumUriSafe . '/');

                $aViewButtons['share'] = array('href' => '', 'title' => _t('_gpa_share'), 'onclick' => 'showPopupAnyHtml(\''.$this->sModuleUrlHalf.'share/\'); return false;', 'active' => false, 'icon' => 'share');
                $aViewButtons['rss'] = array('href' => $this->_oConfig->getBaseUri() . 'RSS/album/' . $iAlbumID, 'title' => _t('_gpa_rss'), 'onclick' => '', 'active' => false, 'icon' => 'rss');
                $sDBActions = BxDolPageView::getBlockCaptionMenu(mktime(), $aViewButtons);

                $sCode = $this->getPhotosBlock($iAlbumID, $sAlbumUriSafe, $sDBActions);

                $this->_oTemplate->addJs(array('jquery.fancybox-1.3.4.pack.js', 'jquery.mousewheel-3.0.4.pack.js'));
                $this->_oTemplate->addCss(array('jquery.fancybox-1.3.4.css'));
                $sCode .= $this->_oTemplate->addJs(array('main.js'), true);

                // comments
                bx_import('BxTemplCmtsView');
                $oCmtsView = new BxTemplCmtsView ('agrp_palbums', $iAlbumID);
                $sAlbumComments = $oCmtsView->getExtraCss();
                $sAlbumComments .= $oCmtsView->getExtraJs();
                $sAlbumComments .= (!$oCmtsView->isEnabled()) ? '' : $oCmtsView->getCommentsFirst();
                $sCode .= DesignBoxContent ( _t('_Comments'), $sAlbumComments, 1);
            } elseif ($sSubAction == 'flash'|| $sSubAction == 'xml_flash') {
                require_once( $this->_oConfig->getClassPath() . 'AGpalbumsExtraViews.php');
                $oExtraViews = new AGpalbumsExtraViews($this);
                $sXmlUrl = BX_DOL_URL_ROOT . $this->_oConfig->getBaseUri() . 'group/' . $this->_aGrpInfo['uri'] . '/xml_flash/' . $sAlbumUriSafe . '/';
                $sXml2Url = BX_DOL_URL_ROOT . $this->_oConfig->getBaseUri() . 'group/' . $this->_aGrpInfo['uri'] . '/flash/' . $sAlbumUriSafe . '/';
                $sAlbumUrl = BX_DOL_URL_ROOT . $this->_oConfig->getBaseUri() . 'group/' . $this->_aGrpInfo['uri'] . '/album/' . $sAlbumUriSafe . '/';
                $oExtraViews->getViewableResults($aImgInfo['Title'], $iAlbumID, $sSubAction, $sXmlUrl, $sXml2Url, $sAlbumUrl);
            } elseif ($sSubAction == 'photo' && $sPhotoUri != '') {
                $bPossibleToView = $this->oPrivacy->check('album_view', $iAlbumID, $this->_iVisitorID);
                if (! $bPossibleToView) {
                    $sCode = MsgBox(_t('_gpa_privacy_restr'));
                    $aPageVars = array('page_main_code' => $sCode);
                    $this->aPageTmpl['header'] = _t('_gpa_photos_of_album');
                    $this->_oTemplate->pageCode($this->aPageTmpl, $aPageVars);
                    exit;
                }

                if ($_POST['action'] == 'edit_desc' && ($_POST['desc'] != '' || $_POST['title'] != '') && (int)$_POST['id'] > 0 && $this->canOperateInAlb($iAlbumID)) {
                    $iPicID = (int)$_POST['id'];
                    $sNewTitle = process_db_input($_POST['title'], BX_TAGS_STRIP);
                    $sNewDesc = process_db_input($_POST['desc'], BX_TAGS_STRIP);
                    echo $this->_oDb->updateImageDescription($iPicID, $sNewTitle, $sNewDesc); exit;
                }

                $aImgInfo = $this->_oDb->getImageInfoByUri($sPhotoUri);
                $iPhotoOwnId = (int)$aImgInfo['owner'];
                list($iImgWidth, $iImgHeight) = explode("x", $aData['Size']);

                list($aImagesInAlbum, $iMaxHeight) = $this->_oDb->getImagesOfAlbum($iAlbumID, true);
                reset($aImagesInAlbum);
                $iCurKey = array_search((int)$aImgInfo['ID'], $aImagesInAlbum);

                $aPrevInfo = $aNextInfo = array();
                $sPrevUri = $sNextUri = '';
                $bPrev = $bNext = false;
                if ($aImagesInAlbum[$iCurKey+1]) {
                    $bNext = true;
                    $aNextInfo = $this->_oDb->getImageInfo($aImagesInAlbum[$iCurKey+1]);
                }
                if ($aImagesInAlbum[$iCurKey-1]) {
                    $bPrev = true;
                    $aPrevInfo = $this->_oDb->getImageInfo($aImagesInAlbum[$iCurKey-1]);
                }

                $sFilename = $this->sUploadUrl . 'i_' . $aImgInfo['Filename'];

                $aVariables = array (
                    'image_title' => process_text_output($aImgInfo['Title']),
                    'bx_if:prev' => array(
                        'condition' => $bPrev,
                        'content' => array(
                            'linkPrev'  => $this->sModuleUrl . 'album/' . $sAlbumUriSafe.'/'.$aPrevInfo['Uri'].'/',
                            'titlePrev' => process_text_output($aPrevInfo['Title']),
                        )
                    ),
                    'bx_if:next' => array(
                        'condition' => $bNext,
                        'content' => array(
                            'linkNext'  => $this->sModuleUrl . 'album/' . $sAlbumUriSafe.'/'.$aNextInfo['Uri'].'/',
                            'titleNext' => process_text_output($aNextInfo['Title']),
                        )
                    ),
                    'bx_if:owner' => array(
                        'condition' => $this->canOperateInAlb($iAlbumID),
                        'content' => array(
                            'currUrl' => $this->sModuleUrl . 'album/' . $sAlbumUriSafe.'/'.$sPhotoUri.'/',
                            'photo_id' => $aImgInfo['ID'],
                            'title' => $aImgInfo['Title'],
                            'description' => $aImgInfo['Description'],
                        )
                    ),
                    'description' => $aImgInfo['Description'],
                    'image_url' => $this->sUploadUrl . 'i_' . $aImgInfo['Filename'],
                    'photo_orig_url' => $this->sUploadUrl . $aImgInfo['Filename'],
                    'image_width' => $iImgWidth,
                    'image_height_ex' => $iMaxHeight+41,
                    'width_ext' => $this->iImgSize + 2,
                    'bx_if:b_can_feature' => array(
                        'condition' => ($this->isAllowedFeature()),
                        'content' => array(
                            'feature_url' => BX_DOL_URL_ROOT . $this->_oConfig->getBaseUri() . 'feature/' . $aImgInfo['ID'] . '/'
                        )
                    ),
                );
                $sCode = $this->_oTemplate->parseHtmlByName('photo_unit_full.html', $aVariables);
                if ($_GET['mode'] == 'ajax') {
                    header('Content-Type: text/html; charset=utf-8');
                    echo $sCode; exit;
                }
                $sCode = DesignBoxContent (_t('_gpa_photos_of_album') . ' ' . $aAlbumInfo['Title'], '<div class="group_imga_full_unit" id="group_imga_full_unit">'.$sCode.'</div>', 1); // , $sDBActions

                // rate block
                bx_import('BxTemplVotingView');
                $oVotingView = new BxTemplVotingView ('agrp_palbums_img', $aImgInfo['ID']);
                if ($oVotingView && $oVotingView->isEnabled()) {
                    $sVotePostRating = $oVotingView->getBigVoting(1);
                } else {
                    $sVotePostRating = $oVotingView->getBigVoting(0);
                }
                $sRateBlock = DesignBoxContent (_t('_Rate'), $this->getWrap($sVotePostRating), 1);

                // comments block
                bx_import('BxTemplCmtsView');
                $sComments = '';
                $oCmtsView = new BxTemplCmtsView('agrp_palbums_img', $aImgInfo['ID']);
                if ($oCmtsView->isEnabled()) {
                    $sComments .= $oCmtsView->getCommentsFirst();
                }
                $sCommentsBlock = DesignBoxContent(_t('_Comments'), $sComments, 1);

            } elseif ($sSubAction == 'manage') {
                if ($this->_iVisitorID) {
                    $sAlbumUriSafe = process_db_input($sAlbumUriParam, BX_TAGS_STRIP);
                    $iAlbumID = $this->_oDb->getAlbumIDByUri($sAlbumUriSafe);

                    if (! $this->canOperateInAlb($iAlbumID)) return $this->_oTemplate->displayAccessDenied();

                    //actions
                    if ($_REQUEST['action_delete'] && is_array($_REQUEST['gphotos'])) {
                        foreach ($_REQUEST['gphotos'] as $iDelImgID) {
                            $this->performImageDelete((int)$iDelImgID, true);
                        }
                    }

                    require_once( $this->_oConfig->getClassPath() . 'AGpalbumsImgSearch.php');
                    $oAlbumsSearch = new AGpalbumsImgSearch();
                    $oAlbumsSearch->aCurrent['paginate']['perPage'] = 12;
                    $oAlbumsSearch->aCurrent['restriction']['album_id']['value'] = (int)$iAlbumID;
                    $oAlbumsSearch->bShowCheckboxes = true;
                    //$oAlbumsSearch->aCurrent['sorting'] = 'name';
                    $oAlbumsSearch->sAlbumUri = $sAlbumUriSafe;

                    $sRequest = $this->_oConfig->getBaseUri() . 'group/' . $this->_aGrpInfo['uri'] . '/' . 'manage/' . $sAlbumUriSafe . '/&page={page}&per_page={per_page}';
                    $oAlbumsSearch->aCurrent['paginate']['page_url'] = $sRequest;

                    $sAlbumImages = $oAlbumsSearch->displayResultBlock();
                    $sAlbumImages .= $oAlbumsSearch->showSimplePagination();

                    $sAlbumImages = ($oAlbumsSearch->aCurrent['paginate']['totalNum'] == 0) ? MsgBox(_t('_Empty')) : $sAlbumImages;
                    $sAdmPanel = $oAlbumsSearch->showAdminActionsPanel('gphotos_box', array('action_delete' => '_Delete'), 'gphotos');

                    $sAdmContent = <<<EOF
<form method="post">
    <div id="gphotos_box" class="blog_posts_wrapper">
        {$sAlbumImages}
        <div class="clear_both"></div>
    </div>
    {$sAdmPanel}
</form>
EOF;
                    $this->_oTemplate->addJs(array('jquery.fancybox-1.3.4.pack.js', 'jquery.mousewheel-3.0.4.pack.js'));
                    $this->_oTemplate->addCss(array('jquery.fancybox-1.3.4.css'));
                    $sAdmContent .= $this->_oTemplate->addJs(array('main.js'), true);

                } else {
                    $sAdmContent = $this->_oTemplate->displayAccessDenied();
                }

                $sCode = DesignBoxContent(_t('_gpa_manage_photos'), $sAdmContent, 1, $sDBActions);
            } elseif (isset($aUploadModes[$sSubAction])) {
                if ($sSubAction == 'fupload' && (int)$_POST['oid']) $this->_iVisitorID = (int)$_POST['oid'];

                $bCanUploadInAlb = false;
                $bCanOperateInAlb = $this->canOperateInAlb($iAlbumID);
                if (! $bCanOperateInAlb) {
                    $bCanUploadInAlb = $this->canUploadInAlb($iAlbumID);
                }

                if ($bCanOperateInAlb || $bCanUploadInAlb) {
                    require_once($this->_oConfig->getClassPath() . 'AGpalbumsUploaders.php');
                    $oGpaUploaders = new AGpalbumsUploaders($this);
                    $sCode = $oGpaUploaders->serviceGetUploaderBlock($sAlbumUriParam, $iAlbumID, $sSubAction);
                } else {
                    return $this->_oTemplate->displayAccessDenied();
                }
            }

            $sInfoBlock = $this->getAlbumInfoBlock($iAlbumID, $iPhotoOwnId);
            $sActionsBlock = $this->getAlbumActionsBlock($iAlbumID, $sAlbumUriSafe);

            $aPageVars = array('page_main_code1' => $sInfoBlock . $sActionsBlock . $sRateBlock, 'page_main_code2' => $sCode . $sCommentsBlock);

            $GLOBALS['oTopMenu']->setCustomSubIconUrl('camera');
            $GLOBALS['oTopMenu']->setCustomSubHeader(htmlspecialchars($aAlbumInfo['Title']));
            // $GLOBALS['oTopMenu']->setCustomVar('album_uri', $sAlbumUriSafe . '/');

            $this->_oTemplate->setPageDescription($aAlbumInfo['Desc']);
            $this->_oTemplate->addPageKeywords($aAlbumInfo['Title']);

            $this->aPageTmpl['name_index'] = 526;
            $this->aPageTmpl['css_name'] = array('main.css');
            $this->aPageTmpl['header'] = $aAlbumInfo['Title'] . ' - ' . $this->_aGrpInfo['title'];
            $this->aPageTmpl['header_text'] = $aAlbumInfo['Title'] . ' - ' . $this->_aGrpInfo['title'];
            $this->_oTemplate->pageCode($this->aPageTmpl, $aPageVars);
            exit;
        } else {
            return $this->_oTemplate->displayAccessDenied();
        }
    }

    function getPhotosBlock($iAlbumID = 0, $sAlbumUriSafe = '', $sActions = '') {
        if ($iAlbumID) {
            require_once( $this->_oConfig->getClassPath() . 'AGpalbumsImgSearch.php');
            $oAlbumsSearch = new AGpalbumsImgSearch();
            $oAlbumsSearch->sAlbumUri = $sAlbumUriSafe;
            $oAlbumsSearch->aCurrent['paginate']['perPage'] = 12;
            //$oAlbumsSearch->aCurrent['sorting'] = 'name';

            $sRequest = $this->sModuleUrl . 'album/' . $sAlbumUriSafe. '/&page={page}&per_page={per_page}';
            $oAlbumsSearch->aCurrent['paginate']['page_url'] = $sRequest;

            $aVars = array ();
            $sSortTabs = $this->_oTemplate->parseHtmlByName('photos_sort_tabs.html', $aVars);

            $oAlbumsSearch->aCurrent['restriction']['album_id']['value'] = (int)$iAlbumID;
            $sCode = $sSortTabs . '<div class="gpa_alb_photos">' . $oAlbumsSearch->displayResultBlock() . '</div>';
            $sPagination = $oAlbumsSearch->showSimplePagination();

            $iAlbumOwnerID = $this->_oDb->getAlbumOwnerByID($iAlbumID);
            $sEmptyMsg = ($this->_iVisitorID == $iAlbumOwnerID) ? MsgBox(_t('_gpa_empty_need_upload')) : MsgBox(_t('_Empty'));
            $sCode = ($oAlbumsSearch->aCurrent['paginate']['totalNum'] == 0) ? $sEmptyMsg : $GLOBALS['oFunctions']->centerContent($sCode, '.gpa_su');
            return DesignBoxContent(_t('_gpa_photos_of_album'), $this->getWrap($sCode), 1, $sActions, $sPagination);
        }
    }

    function getAlbumInfoBlock($iAlbumID, $iPhotoOwnId) {
        $aAlbumInfo = $this->_oDb->getAlbumInfo($iAlbumID);

        $sOwnerThumb = get_member_thumbnail($aAlbumInfo['Owner'], 'none', true);
        $sDataTimeFormatted = getLocaleDate($aAlbumInfo['When']);

        $aVariables = array (
            'owner_thumb' => $sOwnerThumb,
            'when' => $sDataTimeFormatted,
            'description' => htmlspecialchars($aAlbumInfo['Desc']),
            'bx_if:fans_photo' => array(
                'condition' => ($iPhotoOwnId && $aAlbumInfo['Owner'] != $iPhotoOwnId),
                'content' => array(
                    'fan_thumb'  => get_member_thumbnail($iPhotoOwnId, 'none', true),
                )
            ),
        );
        $sSubjectSectContent = $this->_oTemplate->parseHtmlByName('info_block.html', $aVariables);
        return DesignBoxContent(_t('_Info'), /*$this->getWrap*/ ($sSubjectSectContent), 1);
    }

    function getAlbumActionsBlock($iAlbumID, $sAlbumUriSafe) {
        //Nick
        $gId = $this->_oDb->getGroupIdByAlbumId($iAlbumID);
        $aType = $this->_oDb->getAlbumTypeById($iAlbumID);
        $uploadLink = '';

        if ($aType == 'Photo' || $aType == 'File') {
            $uploadLink = $this->sModuleUrl . 'fupload/' . $sAlbumUriSafe . '/';
        } else {
            $uploadLink = $this->_oDb->getUploadLink($gId, $aType);
        }

        if ($aType == 'Video') {
            $embedLink = $this->_oDb->getEmbedLink($gId);
        }

        $bCanUploadInAlb = false;
        $bCanOperateInAlb = $this->canOperateInAlb($iAlbumID);
        if (! $bCanOperateInAlb) {
            $bCanUploadInAlb = $this->canUploadInAlb($iAlbumID);
            if ($bCanUploadInAlb) {
                $aActionKeys = array(
                    'create_album_url' => ($this->canOperateInGrp()) ? $this->sModuleUrl . 'new/' : null,
                    'edit_album_url' => null,
                    'manage_photos_url' => null,
                    //'upload_photos_url' => $this->sModuleUrl . 'fupload/' . $sAlbumUriSafe . '/',
                    'upload_photos_url' => $uploadLink,
                    'embed_videos_url' => ($aType == 'Video') ? $embedLink : null,
                    'delete_possibility_caption' => null,
                );
                $sActions = $GLOBALS['oFunctions']->genObjectsActions($aActionKeys, 'agrp_palbums', false);
            }
        } else {
            $aActionKeys = array(
                'create_album_url' => ($this->canOperateInGrp()) ? $this->sModuleUrl . 'new/' : null,
                'back2_albums_url' => $this->sModuleUrl,
                'edit_album_url' => ($bCanOperateInAlb) ? $this->sModuleUrl . 'edit/' . $sAlbumUriSafe . '/' : null,
                'album_url' => $this->sModuleUrlHalf . 'share/' . $sAlbumUriSafe . '/',
                'manage_photos_url' => ($bCanOperateInAlb) ? $this->sModuleUrl . 'manage/' . $sAlbumUriSafe . '/' : null,
                'sure_label' => _t('_Are you sure?'),
                'upload_photos_url' => ($bCanOperateInAlb) ? $uploadLink : null,
                'embed_videos_url' => ($aType == 'Video' && $bCanOperateInAlb) ? $embedLink : null,
                'album_delete_url' => $this->sModuleUrl . 'delete/' . $sAlbumUriSafe . '/',
                'delete_possibility_caption' => ($bCanOperateInAlb) ? _t('Delete') : null,
            );
            $sActions = $GLOBALS['oFunctions']->genObjectsActions($aActionKeys, 'agrp_palbums', false);
        }
        return ($sActions != '') ? DesignBoxContent(_t('_Actions'), $sActions, 1) : '';
    }

    function getActionShare($sAlbumUriParam = '') {
        $sAlbumUriSafe = process_db_input($sAlbumUriParam, BX_TAGS_STRIP);

        header('Content-type:text/html;charset=utf-8');
        $bAddTempleateExt = true;
        require_once (BX_DIRECTORY_PATH_INC . "shared_sites.inc.php");
        $aSitesPrepare = getSitesArray ($this->sModuleUrl . 'album/' . $sAlbumUriSafe . '/');
        $sIconsUrl = getTemplateIcon('digg.png');
        $sIconsUrl = str_replace('digg.png', '', $sIconsUrl);
        $aSites = array ();
        foreach ($aSitesPrepare as $k => $r) {
            $aSites[] = array (
                'icon' => $sIconsUrl . $r['icon'],
                'name' => $k,
                'url' => $r['url'],
            );
        }

        $aVarsContent = array (
            'bx_repeat:sites' => $aSites,
        );
        $aVarsPopup = array (
            'title' => _t('_gpa_share'),
            'content' => $this->_oTemplate->parseHtmlByName('popup_share' . ($bAddTempleateExt ? '.html' : ''), $aVarsContent),
        );
        echo $GLOBALS['oFunctions']->transBox($this->_oTemplate->parseHtmlByName('popup' . ($bAddTempleateExt ? '.html' : ''), $aVarsPopup), true);
        exit;
    }

    function actionRSS($sParamType = '', $sParamName = '') {
        $sUrlType = 'album/';
        switch ($sParamType) {
            case 'album':
                $iAlbumID = (int)$sParamName;
                $aAlbums = $this->_oDb->getLastImagesOfAlbum($iAlbumID);
                // $sUrlType = 'image/';
                $aAlbInfo = $this->_oDb->getAlbumInfo($iAlbumID);
                $iGrpId = $aAlbInfo['GroupID'];
                $aGrpInfo = $this->_oDb->getGroupInfo($iGrpId);
                $sUrlType = 'group/'.$aGrpInfo['uri'].'/album/'.$aAlbInfo['Uri'].'/';
                break;
            default:
                //$aAlbums = $this->_oDb->getLastAlbums();
                break;
        }

        foreach ($aAlbums as $iID => $aInfo) {
            $aAlbums[$iID]['UnitID'] = (int)$aInfo['ID'];
            $aAlbums[$iID]['OwnerID'] = (int)$aInfo['Owner'];
            $aAlbums[$iID]['UnitTitle'] = $aInfo['Title'];
            $aAlbums[$iID]['UnitLink'] = BX_DOL_URL_ROOT . $this->_oConfig->getBaseUri() . $sUrlType . $aInfo['Uri'] . '/';
            $aAlbums[$iID]['UnitDesc'] = $aInfo['Desc'];
            $aAlbums[$iID]['UnitDateTimeUTS'] = $aInfo['When'];
            $aAlbums[$iID]['UnitIcon'] = '';
        }

        $aAlbumInfo = $this->_oDb->getAlbumInfo($iAlbumID);
        $sUnitTitleC = $aAlbumInfo['Title'] . ' | ' . _t('_gpa_photos_of_album');
        $aThumbInfo = $this->_oDb->getImageInfo($aAlbumInfo['Thumb']);
        $sAlbumThumb = $this->sUploadUrl . 't_' . $aThumbInfo['Filename'];

        bx_import('BxDolRssFactory');
        $oRssFactory = new BxDolRssFactory();

        header('Content-Type: text/xml; charset=utf-8');
        echo $oRssFactory->GenRssByCustomData($aAlbums, $sUnitTitleC, BX_DOL_URL_ROOT . $this->_oConfig->getBaseUri() . 'home/', array(
            'Link' => 'UnitLink',
            'Title' => 'UnitTitle',
            'DateTimeUTS' => 'UnitDateTimeUTS',
            'Desc' => 'UnitDesc',
        ), $sAlbumThumb);
        exit;
    }

    function performAlbumDelete($iAlbumID) {
        if ($this->canOperateInAlb($iAlbumID)) {
            $aImagesInAlbum = $this->_oDb->getImagesOfAlbum($iAlbumID);
            foreach ($aImagesInAlbum as $iID => $sImgID) {
                $iImgID = (int)$sImgID;
                $this->performImageDelete($iImgID);
            }
            $this->_oDb->deleteAlbumImages($iAlbumID);
            $this->_oDb->deleteAlbum($iAlbumID);

            bx_import('BxDolCmts');
            $oCmts = new BxDolCmts('agrp_palbums', $iAlbumID);
            $oCmts->onObjectDelete();
        }
        header('Location: ' . $this->sModuleUrl);
    }

    function performImageDelete($iImgID, $bUseSqlDel = false) {
        if ($iImgID) {
            $aDelImgInfo = $this->_oDb->getImageInfo($iImgID);
            //$sDelFilename = $this->sUploadDir . $aDelImgInfo['Filename'];
            $sDelTFilename = $this->sUploadDir . 't_' . $aDelImgInfo['Filename'];
            if (file_exists($sDelTFilename)) {
                //@unlink($sDelFilename);
                $sDelIFilename = $this->sUploadDir . 'i_' . $aDelImgInfo['Filename'];
                @unlink($sDelTFilename);
                @unlink($sDelIFilename);

                if ($bUseSqlDel) {
                    $this->_oDb->deleteImage($iImgID);
                }
                return true;
            }
            if ($bUseSqlDel) {
                $this->_oDb->deleteImage($iImgID);
                return true;
            }
        }
        return false;
    }

    function canOperateInGrp() {
        if ($GLOBALS['logged']['admin']) { return true; }

        if ($this->_aGrpInfo['allow_upload_photos_to'] == 'a' && $this->isEntryAdmin($this->_aGrpInfo)) { return true; }
        if ($this->_aGrpInfo['allow_upload_photos_to'] == 'f' && $this->isFan($this->_aGrpInfo)) { return true; }
        return false;
    }

    function canOperateInAlb($iAlbumID) { // owners of albums can work in own albums
        if ($GLOBALS['logged']['admin']) { return true; }

        if ($this->isEntryAdmin($this->_aGrpInfo)) { return true; }
        $aAlbumInfo = $this->_oDb->getAlbumInfo($iAlbumID);
        if (is_array($aAlbumInfo) && count($aAlbumInfo)) {
            $iAlbOwner = (int)$aAlbumInfo['Owner'];
            return ($iAlbOwner == $this->_iVisitorID);
        }
        return false;
    }
    function canUploadInAlb($iAlbumID) { // in case of public album
        $aAlbumInfo = $this->_oDb->getAlbumInfo($iAlbumID);
        if (is_array($aAlbumInfo) && count($aAlbumInfo)) {
            if ($aAlbumInfo['public_mode'] == 1 && $this->isFan($this->_aGrpInfo)) return true;
        }
        return false;
    }

    function isFan($aDataEntry, $iProfileId = 0, $isConfirmed = true) {
        if (! $iProfileId)
            $iProfileId = $this->_iVisitorID;
        return $this->_oDb->isFan($aDataEntry['id'], $iProfileId, $isConfirmed) ? true : false;
    }
    function isEntryAdmin($aDataEntry, $iProfileId = 0) {
        if (! $iProfileId)
            $iProfileId = $this->_iVisitorID;
        if (($GLOBALS['logged']['member'] || $GLOBALS['logged']['admin']) && $aDataEntry['author_id'] == $iProfileId && isProfileActive($iProfileId))
            return true;
        return $this->_oDb->isGroupAdmin($aDataEntry['id'], $iProfileId) && isProfileActive($iProfileId);
    }

    function serviceGetSpyData () {
        return array(
            'handlers' => array(
                array('alert_unit' => 'gphotos', 'alert_action' => 'upload', 'module_uri' => 'gphotos', 'module_class' => 'Module', 'module_method' => 'get_spy_post'),
            ),
            'alerts' => array(
                array('unit' => 'gphotos', 'action' => 'upload'),
            )
        );
    }
    
    function serviceGetSpyPost($sAction, $iObjectId = 0, $iSenderId = 0, $aExtraParams = array()) {
        $aRet = array();

        switch($sAction) {
            case 'upload' :
                $aPostInfo = $this->_oDb->getImageInfo((int)$iObjectId);
                $aAlbumInfo = $this->_oDb->getAlbumInfo($aPostInfo['AlbumID']);
                $aGrpInfo = $this->_oDb->getGroupInfo($aAlbumInfo['GroupID']);

                $iRecipientId = 0;
                if (! empty($aPostInfo) && $aAlbumInfo && $aGrpInfo && $aGrpInfo['allow_view_group_to'] == '3' && $aAlbumInfo['AllowAlbumView'] == '3') {
                    $sSenderNickName        = $iSenderId ? getNickName($iSenderId) : _t('_Guest');
                    $sSenderProfileLink     = $iSenderId ? getProfileLink($iSenderId) : 'javascript:void(0)';
                    $sCaption = process_text_output($aPostInfo['Title']);
                    $sEntryUrl = BX_DOL_URL_ROOT . $this->_oConfig->getBaseUri() . 'group/' . $aGrpInfo['uri'] . '/album/' . $aAlbumInfo['Uri'] . '/' . $aPostInfo['Uri'] . '/';

                    $aRet = array(
                        'lang_key'  => '_gpa_spy_upload',
                        'params' => array(
                            'profile_nick'     => $sSenderNickName,
                            'profile_link'     => $sSenderProfileLink,
                            'obj_url'          => $sEntryUrl,
                            'obj_caption'      => $sCaption,
                        ),
                        'recipient_id' => $iRecipientId,
                        'spy_type' => 'content_activity',
                    );
                }
                break;
        }
        return $aRet;
    }
    function serviceGetWallPost($aEvent) {
        $aContent = unserialize($aEvent['content']);
        $iObjId = (int)$aContent['obj_id'];

        $aProfile = getProfileInfo($aEvent['owner_id']);
        if(empty($aProfile))
            return array('perform_delete' => true);
        if($aProfile['Status'] != 'Active')
            return "";

        $sOwner = getNickName((int)$aEvent['owner_id']);
        $sText = _t('_gpa_wall_' . $aEvent['action']);

        // privacy
        $bPossibleToView = $this->oPrivacy->check('album_view', $iObjId, $this->_iVisitorID);
        if (! $bPossibleToView) return;

        // $aPostInfo = $this->_oDb->getPostInfo($iObjId);
        $aPostInfo = $this->_oDb->getAlbumInfo($iObjId);
        $aGrpInfo = $this->_oDb->getGroupInfo($aPostInfo['GroupID']);

        // privacy 2
        // $bPossibleToView2 = $this->oGPrivacy->check('view_group', $aPostInfo['GroupID'], $this->_iVisitorID);
        // if (! $bPossibleToView2) return;

        $aVars = array(
            'cpt_user_name' => $sOwner,
            'cpt_added_new' => $sText,
            // 'cpt_object' => _t('_gpa_wall_object'),
            'post_id' => $aEvent['ID'],
            'cpt_item_url' => BX_DOL_URL_ROOT . $this->_oConfig->getBaseUri() . 'group/'.$aGrpInfo['uri'].'/album/'.$aPostInfo['Uri'].'/', //'news/' . $aPostInfo['Uri'] . '/',
            'cpt_item' => process_text_output($aPostInfo['Title']),
            'cnt_item_title' => process_text_output($aPostInfo['Title']),
            'cpt_item_description' => mb_substr(strip_tags($aPostInfo['Desc']), 0, 255),
        );
        return array(
            'title' => $sOwner . ' ' . $sText . ' ' . process_text_output($aPostInfo['Title']),
            'description' => '',
            'content' => $this->_oTemplate->parseHtmlByName('wall_post.html', $aVars)
        );
    }
}
