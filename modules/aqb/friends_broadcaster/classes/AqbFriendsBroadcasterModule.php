<?php
/***************************************************************************
*
*     copyright            : (C) 2014 AQB Soft
*     website              : http://www.aqbsoft.com
*
* IMPORTANT: This is a commercial product made by AQB Soft. It cannot be modified for other than personal usage.
* The "personal usage" means the product can be installed and set up for ONE domain name ONLY.
* To be able to use this product for another domain names you have to order another copy of this product (license).
*
* This product cannot be redistributed for free or a fee without written permission from AQB Soft.
*
* This notice may not be removed from the source code.
*
***************************************************************************/

bx_import('BxDolModule');
bx_import('BxDolAdminSettings');
bx_import('BxDolPrivacy');
require_once(BX_DIRECTORY_PATH_PLUGINS.'Services_JSON.php');


class AqbFriendsBroadcasterModule extends BxDolModule {
	/**
	 * Constructor
	 */
	function __construct($aModule) {
	    parent::__construct($aModule);
	}

	function serviceGetComposeForm() {
		return $this->getComposeForm();
	}

	function getComposeForm() {
		//Nick
		$recipId = bx_get('RecID');

		$replyMode = bx_get('mode');
		
		if ($msgId = bx_get('msgID')) {
		} else {
			$msgId = 0;
		}

		//echo "<script type='text/javascript'>alert('$recipId');</script>";

		$aMyFriends = $this->_oDb->getProfileFriends(getLoggedId());
		if (!$aMyFriends) return DesignBoxContent(_t('_aqb_friends_broadcaster_no_friends'), MsgBox(_t('_aqb_friends_broadcaster_no_friends')), 1);

		$oForm = $this->_oTemplate->getComposeForm((int)$recipId, (int)$msgId, (int)$replyMode);

		$sMessage = '';
		if ($oForm->isSubmittedAndValid() && $_POST['recipients']) {
			$sMessage = $this->sendMessage();

			unset($_POST['recipients']);
			unset($_POST['send']);
			unset($_POST['notify_by_email']);
			unset($_POST['send_copy_to_email']);
			unset($_REQUEST['send']);
			unset($_GET['send']);

			$oForm = $this->_oTemplate->getComposeForm(true);
		}

		return $this->_oTemplate->getFormWithFriendList($aMyFriends, $oForm, $sMessage);
	}

	function sendMessage() {
		$iSender = getLoggedId();

		//$recipList = substr($_POST['recipients'], 0, -1);
		//$recipientString = "To: " . $recipList . "<br><br>";
		$recipientString = "To: ";

		$aRecipients = explode(',', $_POST['recipients']);
		if (!$aRecipients) return MsgBox(_t('_aqb_friends_broadcaster_recipients_required'), 3);;

		$aMyFriends = $this->_oDb->getProfileFriends(getLoggedId());
		foreach ($aRecipients as $iKey => $iRecipient) {
			if (!in_array($iRecipient, $aMyFriends)) { 
				unset($aRecipients[$iKey]);
			} else {
				$recipientString .= '<a href=\'' . getProfileLink($iRecipient) . '\'>' . getNickName($iRecipient) . '</a>, ';
				//$recipientString .= getNickName($iRecipient) . ", ";
			}
		}

		$recipStr = substr($recipientString, 0, -2) . "<br><br>";	

		if (!$aRecipients) return MsgBox(_t('_aqb_friends_broadcaster_recipients_required'), 3);

		$sMessage = trim($_REQUEST['message']);
		if (!$sMessage) return MsgBox(_t('_aqb_friends_broadcaster_message_required'), 3);

		$sSubject = trim($_REQUEST['subject']);
		if (!$sSubject) return MsgBox(_t('_aqb_friends_broadcaster_subject_required'), 3);


    	defineMembershipActions(array('send messages'));
		$aCheck = checkAction($iSender, BX_SEND_MESSAGES, true);
        if ($aCheck[CHECK_ACTION_RESULT] != CHECK_ACTION_RESULT_ALLOWED) {
        	return MsgBox(_t('_FAILED_TO_SEND_MESSAGE_MEMBERSHIP_DISALLOW'), 3);
        }

        $sMessage2 = $recipStr . $sMessage;
        //Nick - auto sends copy to email (true)
        foreach ($aRecipients as $iRecipient) {
        	$this->_oDb->sendMessage($iSender, $iRecipient, $sSubject, $sMessage2, true, false);
        }

        header('Location: ' . BX_DOL_URL_ROOT . 'mail.php?mode=outbox');
        return true;
		//return MsgBox(_t('_aqb_friends_broadcaster_sent'), 3);
	}

	function serviceGetProfileActionButton($iProfile, $iViewer) {
		if ($iProfile == $iViewer || $this->_oDb->areFriends($iProfile, $iViewer)) return _t('_aqb_friends_broadcaster_action_button');
		else return false;
	}

}