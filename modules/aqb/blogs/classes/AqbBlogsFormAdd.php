<?php
/***************************************************************************
* 
*     copyright            : (C) 2009 AQB Soft
*     website              : http://www.aqbsoft.com
*      
* IMPORTANT: This is a commercial product made by AQB Soft. It cannot be modified for other than personal usage.
* The "personal usage" means the product can be installed and set up for ONE domain name ONLY. 
* To be able to use this product for another domain names you have to order another copy of this product (license).
* 
* This product cannot be redistributed for free or a fee without written permission from AQB Soft.
* 
* This notice may not be removed from the source code.
* 
***************************************************************************/

bx_import ('BxDolProfileFields');
bx_import ('BxDolFormMedia');

class AqbBlogsFormAdd extends BxDolFormMedia
{
    var $_oMain, $_oDb;

    function AqbBlogsFormAdd ($oMain, $iProfileId, $iEntryId = 0, $iThumb = 0, $isEdit = false)
    {
        $this->_oMain = $oMain;
        $this->_oDb = $oMain->_oDb;

        $sUri = $this->_oMain->_oConfig->getUri();
        $sPrefixLang = $this->_oMain->_oConfig->getSystemName('langs');

        $this->_aMedia = array ();
        if (BxDolRequest::serviceExists('photos', 'perform_photo_upload', 'Uploader'))
            $this->_aMedia['images'] = array (
                'post' => 'ready_images',
                'upload_func' => 'uploadPhotos',
                'tag' => AQB_BLOGS_PHOTOS_TAG,
                'cat' => AQB_BLOGS_PHOTOS_CAT,
                'thumb' => 'thumb',
                'module' => 'photos',
                'title_upload_post' => 'images_titles',
                'title_upload' => _t($sPrefixLang . '_form_caption_file_title'),
                'service_method' => 'get_photo_array',
            );

        if (BxDolRequest::serviceExists('videos', 'perform_video_upload', 'Uploader'))
            $this->_aMedia['videos'] = array (
                'post' => 'ready_videos',
                'upload_func' => 'uploadVideos',
                'tag' => AQB_BLOGS_VIDEOS_TAG,
                'cat' => AQB_BLOGS_VIDEOS_CAT,
                'thumb' => false,
                'module' => 'videos',
                'title_upload_post' => 'videos_titles',
                'title_upload' => _t($sPrefixLang . '_form_caption_file_title'),
                'service_method' => 'get_video_array',
            );

        if (BxDolRequest::serviceExists('sounds', 'perform_music_upload', 'Uploader'))
            $this->_aMedia['sounds'] = array (
                'post' => 'ready_sounds',
                'upload_func' => 'uploadSounds',
                'tag' => AQB_BLOGS_SOUNDS_TAG,
                'cat' => AQB_BLOGS_SOUNDS_CAT,
                'thumb' => false,
                'module' => 'sounds',
                'title_upload_post' => 'sounds_titles',
                'title_upload' => _t($sPrefixLang . '_form_caption_file_title'),
                'service_method' => 'get_music_array',
            );

        if (BxDolRequest::serviceExists('files', 'perform_file_upload', 'Uploader'))
            $this->_aMedia['files'] = array (
                'post' => 'ready_files',
                'upload_func' => 'uploadFiles',
                'tag' => AQB_BLOGS_FILES_TAG,
                'cat' => AQB_BLOGS_FILES_CAT,
                'thumb' => false,
                'module' => 'files',
                'title_upload_post' => 'files_titles',
                'title_upload' => _t($sPrefixLang . '_form_caption_file_title'),
                'service_method' => 'get_file_array',
            );

        bx_import('BxDolCategories');
        $oCategories = new BxDolCategories();

        // generate templates for custom form's elements
        $aCustomMediaTemplates = $this->generateCustomMediaTemplates ($oMain->_iProfileId, $iEntryId, $iThumb);

        // privacy
        $aInputPrivacyCustom = array ();
        $aInputPrivacyCustom[] = array ('key' => '', 'value' => '----');
        $aInputPrivacyCustom[] = array ('key' => 'f', 'value' => _t($sPrefixLang . '_privacy_fans_only'));
        $aInputPrivacyCustomPass = array (
            'pass' => 'Preg',
            'params' => array('/^([0-9f]+)$/'),
        );

        $aInputPrivacyCustom2 = array (
            array('key' => 'f', 'value' => _t($sPrefixLang . '_privacy_fans')),
            array('key' => 'a', 'value' => _t($sPrefixLang . '_privacy_admins_only'))
        );
        $aInputPrivacyCustom2Pass = array (
            'pass' => 'Preg',
            'params' => array('/^([fa]+)$/'),
        );

        $sPrivacySystemName = $this->_oMain->_oConfig->getSystemName('privacy');
        $aInputPrivacyView = $this->_oMain->_oPrivacy->getGroupChooser($iProfileId, $sPrivacySystemName, 'view', array(), _t($sPrefixLang . '_privacy_allow_view_to'));

        $aInputPrivacyViewFans = $this->_oMain->_oPrivacy->getGroupChooser($iProfileId, $sPrivacySystemName, 'view_fans', array(), _t($sPrefixLang . '_privacy_allow_view_fans_to'));
        $aInputPrivacyViewFans['values'] = array_merge($aInputPrivacyViewFans['values'], $aInputPrivacyCustom);

        $aInputPrivacyComment = $this->_oMain->_oPrivacy->getGroupChooser($iProfileId, $sPrivacySystemName, 'comment');
        $aInputPrivacyComment['values'] = array_merge($aInputPrivacyComment['values'], $aInputPrivacyCustom);
        $aInputPrivacyComment['db'] = $aInputPrivacyCustomPass;

        $aInputPrivacyRate = $this->_oMain->_oPrivacy->getGroupChooser($iProfileId, $sPrivacySystemName, 'rate');
        //$aInputPrivacyRate['value'] = 0;
        $aInputPrivacyRate['values'] = array_merge($aInputPrivacyRate['values'], $aInputPrivacyCustom);
        $aInputPrivacyRate['db'] = $aInputPrivacyCustomPass;

        $aInputPrivacyJoin = $this->_oMain->_oPrivacy->getGroupChooser($iProfileId, $sPrivacySystemName, 'join', array(), _t($sPrefixLang . '_privacy_allow_join_to'));

        $aInputPrivacyUploadPhotos = $this->_oMain->_oPrivacy->getGroupChooser($iProfileId, $sPrivacySystemName, 'upload_photos');
        $aInputPrivacyUploadPhotos['values'] = $aInputPrivacyRate['values'];
        //$aInputPrivacyUploadPhotos['db'] = $aInputPrivacyCustom2Pass;
        $aInputPrivacyUploadPhotos['db'] = $aInputPrivacyCustomPass;

        $aInputPrivacyUploadVideos = $this->_oMain->_oPrivacy->getGroupChooser($iProfileId, $sPrivacySystemName, 'upload_videos');
        $aInputPrivacyUploadVideos['values'] = $aInputPrivacyRate['values'];
        //$aInputPrivacyUploadPhotos['db'] = $aInputPrivacyCustom2Pass;
        $aInputPrivacyUploadPhotos['db'] = $aInputPrivacyCustomPass;

        $aInputPrivacyUploadSounds = $this->_oMain->_oPrivacy->getGroupChooser($iProfileId, $sPrivacySystemName, 'upload_sounds');
        $aInputPrivacyUploadSounds['values'] = $aInputPrivacyRate['values'];
        //$aInputPrivacyUploadSounds['db'] = $aInputPrivacyCustom2Pass;
        $aInputPrivacyUploadSounds['db'] = $aInputPrivacyCustomPass;

        $aInputPrivacyUploadFiles = $this->_oMain->_oPrivacy->getGroupChooser($iProfileId, $sPrivacySystemName, 'upload_files');
        $aInputPrivacyUploadFiles['values'] = $aInputPrivacyRate['values'];
        //$aInputPrivacyUploadFiles['db'] = $aInputPrivacyCustom2Pass;
        $aInputPrivacyUploadFiles['db'] = $aInputPrivacyCustomPass;

        //Nick - Enable user to post to a group report
        $oGroups = BxDolModule::getInstance('BxGroupsModule');
        $groupsJoinedArray = $oGroups->_oDb->getUserGroupsById($iOwnerID);
        $groupsInfo = $oGroups->_oDb->getUserGroupsInfoById($iOwnerID);

        $aCustomForm = array(

            'form_attrs' => array(
                'name'     => 'form_groups',
                'action'   => '',
                'method'   => 'post',
                'enctype' => 'multipart/form-data',
            ),

            'params' => array (
                'db' => array(
                    'table' => $this->_oDb->getFullName('table_main'),
                    'key' => 'id',
                    'uri' => 'uri',
                    'uri_title' => 'title',
                    'submit_name' => 'submit_form',
                ),
            ),

            'inputs' => array(

                'header_info' => array(
                    'type' => 'block_header',
                    'caption' => _t($sPrefixLang . '_form_header_info')
                ),

                'title' => array(
                    'type' => 'text',
                    'name' => 'title',
                    'caption' => _t($sPrefixLang . '_form_caption_title'),
                    'required' => true,
                    'checker' => array (
                        'func' => 'length',
                        'params' => array(3,100),
                        'error' => _t ($sPrefixLang . '_form_err_title'),
                    ),
                    'db' => array (
                        'pass' => 'Xss',
                    ),
                ),
                'desc' => array(
                    'type' => 'textarea',
                    'name' => 'desc',
                    'caption' => _t($sPrefixLang . '_form_caption_desc'),
                    'required' => true,
                    'html' => 2,
                    'checker' => array (
                        'func' => 'length',
                        'params' => array(20,64000),
                        'error' => _t ($sPrefixLang . '_form_err_desc'),
                    ),
                    'db' => array (
                        'pass' => 'XssHtml',
                    ),
                ),
                'tags' => array(
                    'type' => 'text',
                    'name' => 'tags',
                    'caption' => _t('_Tags'),
                    'info' => _t('_sys_tags_note'),
                    'required' => true,
                    'checker' => array (
                        'func' => 'avail',
                        'error' => _t ($sPrefixLang . '_form_err_tags'),
                    ),
                    'db' => array (
                        'pass' => 'Tags',
                    ),
                ),

                'categories' => $oCategories->getGroupChooser ($this->_oMain->_oConfig->getSystemName('categories'), (int)$iProfileId, true),

                // images

                'header_images' => array(
                    'type' => 'block_header',
                    'caption' => _t($sPrefixLang . '_form_header_images'),
                    'collapsable' => true,
                    'collapsed' => true,
                ),
                'thumb' => array(
                    'type' => 'custom',
                    'content' => $aCustomMediaTemplates['images']['thumb_choice'],
                    'name' => 'thumb',
                    'caption' => _t($sPrefixLang . '_form_caption_thumb_choice'),
                    'info' => _t($sPrefixLang . '_form_info_thumb_choice'),
                    'required' => false,
                    'db' => array (
                        'pass' => 'Int',
                    ),
                ),
                'images_choice' => array(
                    'type' => 'custom',
                    'content' => $aCustomMediaTemplates['images']['choice'],
                    'name' => 'images_choice[]',
                    'caption' => _t($sPrefixLang . '_form_caption_images_choice'),
                    'info' => _t($sPrefixLang . '_form_info_images_choice'),
                    'required' => false,
                ),
                'images_upload' => array(
                    'type' => 'custom',
                    'content' => $aCustomMediaTemplates['images']['upload'],
                    'name' => 'images_upload[]',
                    'caption' => _t($sPrefixLang . '_form_caption_images_upload'),
                    'info' => _t($sPrefixLang . '_form_info_images_upload'),
                    'required' => false,
                ),

                // videos

                'header_videos' => array(
                    'type' => 'block_header',
                    'caption' => _t($sPrefixLang . '_form_header_videos'),
                    'collapsable' => true,
                    'collapsed' => true,
                ),
                'videos_choice' => array(
                    'type' => 'custom',
                    'content' => $aCustomMediaTemplates['videos']['choice'],
                    'name' => 'videos_choice[]',
                    'caption' => _t($sPrefixLang . '_form_caption_videos_choice'),
                    'info' => _t($sPrefixLang . '_form_info_videos_choice'),
                    'required' => false,
                ),
                'videos_upload' => array(
                    'type' => 'custom',
                    'content' => $aCustomMediaTemplates['videos']['upload'],
                    'name' => 'videos_upload[]',
                    'caption' => _t($sPrefixLang . '_form_caption_videos_upload'),
                    'info' => _t($sPrefixLang . '_form_info_videos_upload'),
                    'required' => false,
                ),

                // sounds

                'header_sounds' => array(
                    'type' => 'block_header',
                    'caption' => _t($sPrefixLang . '_form_header_sounds'),
                    'collapsable' => true,
                    'collapsed' => true,
                ),
                'sounds_choice' => array(
                    'type' => 'custom',
                    'content' => $aCustomMediaTemplates['sounds']['choice'],
                    'name' => 'sounds_choice[]',
                    'caption' => _t($sPrefixLang . '_form_caption_sounds_choice'),
                    'info' => _t($sPrefixLang . '_form_info_sounds_choice'),
                    'required' => false,
                ),
                'sounds_upload' => array(
                    'type' => 'custom',
                    'content' => $aCustomMediaTemplates['sounds']['upload'],
                    'name' => 'sounds_upload[]',
                    'caption' => _t($sPrefixLang . '_form_caption_sounds_upload'),
                    'info' => _t($sPrefixLang . '_form_info_sounds_upload'),
                    'required' => false,
                ),

                // files

                'header_files' => array(
                    'type' => 'block_header',
                    'caption' => _t($sPrefixLang . '_form_header_files'),
                    'collapsable' => true,
                    'collapsed' => true,
                ),
                'files_choice' => array(
                    'type' => 'custom',
                    'content' => $aCustomMediaTemplates['files']['choice'],
                    'name' => 'files_choice[]',
                    'caption' => _t($sPrefixLang . '_form_caption_files_choice'),
                    'info' => _t($sPrefixLang . '_form_info_files_choice'),
                    'required' => false,
                ),
                'files_upload' => array(
                    'type' => 'custom',
                    'content' => $aCustomMediaTemplates['files']['upload'],
                    'name' => 'files_upload[]',
                    'caption' => _t($sPrefixLang . '_form_caption_files_upload'),
                    'info' => _t($sPrefixLang . '_form_info_files_upload'),
                    'required' => false,
                ),

                // privacy

                'header_privacy' => array(
                    'type' => 'block_header',
                    'caption' => _t($sPrefixLang . '_form_header_privacy'),
                ),

                $this->_oDb->getFullName('field_allow_view_to', false) => $aInputPrivacyView,

                'allow_view_fans_to' => $aInputPrivacyViewFans,

                'allow_comment_to' => $aInputPrivacyComment,

                'allow_rate_to' => $aInputPrivacyRate,

                'allow_join_to' => $aInputPrivacyJoin,

                'join_confirmation' => array (
                    'type' => 'hidden',
                    'name' => 'join_confirmation',
                	'value' => '0',
                    'checker' => array (
                        'func' => 'int',
                        'error' => _t ($sPrefixLang . '_form_err_join_confirmation'),
                    ),
                    'db' => array (
                        'pass' => 'Int',
                    ),
                ),

                'allow_upload_photos_to' => $aInputPrivacyUploadPhotos,

                'allow_upload_videos_to' => $aInputPrivacyUploadVideos,

                'allow_upload_sounds_to' => $aInputPrivacyUploadSounds,

                'allow_upload_files_to' => $aInputPrivacyUploadFiles,

                'group_reports' => array(
                    'type' => 'select',
                    'name' => 'group_reports', 
                    'values'=> $groupsJoinedArray,
                    'caption' => 'Move To Group? (Optional)',
                    'required' => false,
                ),

                'copy_reports' => array(
                    'type' => 'select',
                    'name' => 'copy_reports', 
                    'values'=> $groupsJoinedArray,
                    'caption' => 'Copy To Group? (Optional)',
                    'required' => false,
                ),

                'Submit' => array (
                    'type' => 'submit',
                    'name' => 'submit_form',
                    'value' => _t('_Submit'),
                    'colspan' => false,
                ),
            ),
        );

        if (!$isEdit) {
            unset($aCustomForm['inputs']['group_reports']);
            unset($aCustomForm['inputs']['copy_reports']);
        }

        if (!$aCustomForm['inputs']['images_choice']['content']) {
            unset ($aCustomForm['inputs']['thumb']);
            unset ($aCustomForm['inputs']['images_choice']);
        }

        if (!$aCustomForm['inputs']['videos_choice']['content'])
            unset ($aCustomForm['inputs']['videos_choice']);

        if (!$aCustomForm['inputs']['sounds_choice']['content'])
            unset ($aCustomForm['inputs']['sounds_choice']);

        if (!$aCustomForm['inputs']['files_choice']['content'])
            unset ($aCustomForm['inputs']['files_choice']);

        if (!isset($this->_aMedia['images'])) {
            unset ($aCustomForm['inputs']['header_images']);
            unset ($aCustomForm['inputs']['thumb']);
            unset ($aCustomForm['inputs']['images_choice']);
            unset ($aCustomForm['inputs']['images_upload']);
            //unset ($aCustomForm['inputs']['allow_upload_photos_to']);
        }

        if (!isset($this->_aMedia['videos'])) {
            unset ($aCustomForm['inputs']['header_videos']);
            unset ($aCustomForm['inputs']['videos_choice']);
            unset ($aCustomForm['inputs']['videos_upload']);
            //unset ($aCustomForm['inputs']['allow_upload_videos_to']);
        }

        if (!isset($this->_aMedia['sounds'])) {
            unset ($aCustomForm['inputs']['header_sounds']);
            unset ($aCustomForm['inputs']['sounds_choice']);
            unset ($aCustomForm['inputs']['sounds_upload']);
            //unset ($aCustomForm['inputs']['allow_upload_sounds_to']);
        }

        if (!isset($this->_aMedia['files'])) {
            unset ($aCustomForm['inputs']['header_files']);
            unset ($aCustomForm['inputs']['files_choice']);
            unset ($aCustomForm['inputs']['files_upload']);
            //unset ($aCustomForm['inputs']['allow_upload_files_to']);
        }

        $this->processMembershipChecksForMediaUploads ($aCustomForm['inputs']);

        parent::BxDolFormMedia($aCustomForm, 0, $iEntryId);
    }

}
