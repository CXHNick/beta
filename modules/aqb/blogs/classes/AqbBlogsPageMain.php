<?php
/***************************************************************************
* 
*     copyright            : (C) 2009 AQB Soft
*     website              : http://www.aqbsoft.com
*      
* IMPORTANT: This is a commercial product made by AQB Soft. It cannot be modified for other than personal usage.
* The "personal usage" means the product can be installed and set up for ONE domain name ONLY. 
* To be able to use this product for another domain names you have to order another copy of this product (license).
* 
* This product cannot be redistributed for free or a fee without written permission from AQB Soft.
* 
* This notice may not be removed from the source code.
* 
***************************************************************************/

bx_import('BxDolTwigPageMain');

class AqbBlogsPageMain extends BxDolTwigPageMain
{
	var $_sPrefix;
	var $_sPrefixLangs;
	var $_sPrefixOptions;

    function AqbBlogsPageMain(&$oMain)
    {
        $this->sSearchResultClassName = 'AqbBlogsSearchResult';
        $this->sFilterName = $oMain->_oConfig->getFilterName();

        $this->_sPrefix = $oMain->_oConfig->getUri();
        $this->_sPrefixLangs = $oMain->_oConfig->getSystemName('langs');
        $this->_sPrefixOptions = $oMain->_oConfig->getSystemName('options');

        parent::BxDolTwigPageMain($this->_sPrefix . '_main', $oMain);
    }

    function getBlockCode_LatestFeaturedGroup()
    {
        $aDataEntry = $this->oDb->getLatestFeaturedItem ();
        if (!$aDataEntry)
            return false;

        $aAuthor = getProfileInfo($aDataEntry['author_id']);

        $sImageUrl = '';
        $sImageTitle = '';
        $a = array ('ID' => $aDataEntry['author_id'], 'Avatar' => $aDataEntry['thumb']);
        $aImage = BxDolService::call('photos', 'get_image', array($a, 'file'), 'Search');

        aqb_blogs_import('Voting');
        $oRating = new AqbBlogsVoting ($this->oConfig->getSystemName('votes'), $aDataEntry['id']);

        $aVars = array (
            'bx_if:image' => array (
                'condition' => !$aImage['no_image'] && $aImage['file'],
                'content' => array (
                    'image_url' => !$aImage['no_image'] && $aImage['file'] ? $aImage['file'] : '',
                    'image_title' => !$aImage['no_image'] && $aImage['title'] ? $aImage['title'] : '',
                    'group_url' => BX_DOL_URL_ROOT . $this->oConfig->getBaseUri() . 'view/' . $aDataEntry['uri'],
                ),
            ),
            'group_url' => BX_DOL_URL_ROOT . $this->oConfig->getBaseUri() . 'view/' . $aDataEntry['uri'],
            'group_title' => $aDataEntry['title'],
            'author_title' => _t('_From'),
            'author_username' => getNickName($aAuthor['ID']),
            'author_url' => getProfileLink($aAuthor['ID']),
            'rating' => $oRating->isEnabled() ? $oRating->getJustVotingElement (true, $aDataEntry['id']) : '',
            'likes_count' => $aDataEntry['likes_count'],
            'country_city' => $this->oMain->_formatLocation($aDataEntry, false, true),
        );
        return $this->oTemplate->parseHtmlByName('latest_featured_group', $aVars);
    }

    function getBlockCode_Recent()
    {
        return $this->ajaxBrowse('recent', $this->oDb->getParam($this->_sPrefixOptions . '_perpage_main_recent'));
    }

    function getBlockCode_Mine() {
        aqb_blogs_import('SearchResult');
        $search = new AqbBlogsSearchResult();
        $search->aCurrent['restriction']['owner']['value'] = getLoggedId();
        return $search->displayResultBlock();
    }
}
