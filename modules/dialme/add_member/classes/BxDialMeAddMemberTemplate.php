<?php
/***************************************************************************
* Date				: Sun January 26, 2014
* Website			: http://www.dialme.com
*
* Product Name			: Add A New Member Module
* Product Version		: 1.0.0
* Product Use			: For Boonex Dolphin 7.0.x & 7.1.x
*
***************************************************************************/

bx_import ('BxDolTwigTemplate');

class BxDialMeAddMemberTemplate extends BxDolTwigTemplate {

	public function __construct(&$oConfig, &$oDb) {
	    parent::__construct($oConfig, $oDb);
    }
}

?>