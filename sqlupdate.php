<?php
set_time_limit(9999999);
 
 
require_once( 'inc/header.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'design.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'admin_design.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'languages.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'utils.inc.php' );


$oMain = BxDolModule::getInstance('BxEventsModule');
$iTime = time();
 
echo "START .....<br /><br />";

echo "UPDATING DATABASE .....<br /><br />";
 
db_res("UPDATE `sys_modules` SET `title`='<font color=\"blue\">Premium Events</font>', `version`='2.1.6', `vendor`='modzzz.com' WHERE `uri` = 'events' AND `db_prefix` = 'bx_events_'");
 
db_res("
CREATE TABLE IF NOT EXISTS `bx_events_youtube` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `id_entry` int(10) unsigned NOT NULL,
  `title` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
");
 
//add new map 
//BxDolService::call('events', 'sponsor_map_install');
 
db_res("INSERT INTO `sys_alerts_handlers` VALUES (NULL, 'bx_events_map_sponsor_install', '', '', 'if (''wmap'' == \$this->aExtras[''uri''] && \$this->aExtras[''res''][''result'']) BxDolService::call(''events'', ''sponsor_map_install'');')");
$iHandler = mysql_insert_id();
db_res("INSERT INTO `sys_alerts` VALUES (NULL , 'module', 'install', $iHandler);");



db_res("ALTER TABLE `sys_menu_top` CHANGE `Link` `Link` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT ''");
  
db_res("UPDATE `sys_email_templates` SET `Body` = REPLACE(`Body`, '<pre><BroadcastMessage></pre>', '<p><BroadcastMessage></p>') WHERE `Name` = 'bx_events_broadcast'");  


db_res("
CREATE TABLE IF NOT EXISTS `bx_events_activity` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `event_id` int(11) NOT NULL,
  `lang_key` varchar(100) collate utf8_general_ci NOT NULL,
  `params` text collate utf8_general_ci NOT NULL,
  `date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `type` enum('add','delete','change','commentPost','rate','join','unjoin','featured','unfeatured','makeAdmin','removeAdmin') collate utf8_general_ci NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `event_id` (`event_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci ;
"); 
 
db_res("
CREATE TABLE IF NOT EXISTS `bx_events_invite` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `id_entry` int(11) NOT NULL,
  `id_profile` int(11) NOT NULL,
  `code` varchar(100) collate utf8_general_ci NOT NULL, 
  `date` timestamp NOT NULL default CURRENT_TIMESTAMP,
   PRIMARY KEY  (`id`) 
 ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci ;
"); 

db_res("
	ALTER TABLE `bx_events_main`  
	ADD  `OrganizerName` varchar(255) NOT NULL default '',
	ADD  `OrganizerPhone` varchar(255) NOT NULL default '',
	ADD  `OrganizerEmail` varchar(255) NOT NULL default '',
	ADD  `OrganizerWebsite` varchar(255) NOT NULL default '',
	ADD  `Recurring`  ENUM( 'no', 'yes' ) NOT NULL DEFAULT 'no',
	ADD  `RecurringNum` int(11) unsigned NOT NULL default '0',
	ADD  `RecurringPeriod` varchar(255) NOT NULL default '',
	ADD  `Recurrence` INT( 11 ) NOT NULL, 
	ADD  `State` varchar(10) NOT NULL default '',
	ADD  `Street` varchar(200) NOT NULL default '',
	ADD  `Reminder` INT( 11 ) NOT NULL ,
	ADD  `ReminderDays` INT( 11 ) NOT NULL,
	ADD  `ReminderSent` INT( 11 ) NOT NULL,
	ADD  `EventMembershipViewFilter` varchar(100) NOT NULL default '',
	ADD  `VideoEmbed` TEXT NOT NULL, 
	ADD  `ParticipantsInfo`  TEXT NOT NULL,
	ADD  `allow_join_after_start` varchar(10) NOT NULL default '' 
"); 
  
db_res("ALTER TABLE `bx_events_main` CHANGE `allow_view_event_to` `allow_view_event_to`  VARCHAR( 16 ) NOT NULL"); 
 
db_res("DELETE FROM `sys_email_templates` WHERE `Name`='bx_events_invitation'"); 

db_res("  
INSERT INTO `sys_email_templates` (`Name`, `Subject`, `Body`, `Desc`, `LangID`) VALUES 
('bx_events_invitation', 'Invitation to event: <EventName>', '<bx_include_auto:_email_header.html />\r\n\r\n <p>Hello <NickName>,</p> <p><a href=\"<InviterUrl>\"><InviterNickName></a> has invited you to his event:</p> <pre><InvitationText></pre> <p> <b>Event Information:</b><br /> Name: <EventName><br /> Location: <EventLocation><br /> Date of beginning: <EventStart><br /> <a href=\"<EventUrl>\">More details</a><br /><br /> <a href=\"<AcceptUrl>\">Accept Invitation</a> </p><bx_include_auto:_email_footer.html />', 'Events invitation template', '0') 
");

  
$iMaxOrder = db_value("SELECT `Order` FROM `sys_page_compose_pages` ORDER BY `Order` DESC LIMIT 1");
$iMaxOrder++;
db_res("
INSERT INTO `sys_page_compose_pages` (`Name`, `Title`, `Order`) VALUES ('bx_events_local', 'Local Events Page', $iMaxOrder) 
");

$iMaxOrder++;
db_res("
INSERT INTO `sys_page_compose_pages` (`Name`, `Title`, `Order`) VALUES ('bx_events_local_state', 'Local Events State Page', $iMaxOrder) 
");
    
 
db_res("UPDATE `sys_page_compose` SET `Order`=`Order`+2 WHERE `Page`='bx_events_main' AND `Column`=3");

//db_res("UPDATE `sys_page_compose` SET `Order`=0, `Column`=0  WHERE `Page`='bx_events_main' AND `Caption`='_bx_events_block_upcoming_photo'");


db_res("
INSERT INTO `sys_page_compose` (`Page`, `PageWidth`, `Desc`, `Caption`, `Column`, `Order`, `Func`, `Content`, `DesignBox`, `ColWidth`, `Visible`, `MinWidth`) VALUES 
     
	('bx_events_view', '1140px', 'Event''s organizer block', '_bx_events_block_organizer', '3', '5', 'Organizer', '', '1', '28.1', 'non,memb', '0'),
 	('bx_events_view', '1140px', 'Event''s Participants Info block', '_bx_events_block_participants_info', '3', '7', 'ParticipantsInfo', '', '1', '28.1', 'non,memb', '0'),

    ('bx_events_view', '1140px', 'Event''s recurring block', '_bx_events_block_recurring', '2', '5', 'Recurring', '', '1', '71.9', 'non,memb', '0'),  
	('bx_events_view', '1140px', 'Event''s Video Embed block', '_bx_events_block_video_embed', '2', '7', 'VideoEmbed', '', '1', '71.9', 'non,memb', '0'), 
    ('bx_events_view', '1140px', 'Event''s local block', '_bx_events_block_local', '2', '8', 'Local', '', '1', '71.9', 'non,memb', '0'),
    ('bx_events_view', '1140px', 'Event''s other block', '_bx_events_block_other', '2', '9', 'Other', '', '1', '71.9', 'non,memb', '0'),	
    ('bx_events_view', '1140px', 'Event''s forum block', '_bx_events_block_forum_feed', '2', '10', 'Forum', '', '1', '71.9', 'non,memb', '0'), 
	('bx_events_view', '1140px', 'Event''s Sponsors block', '_bx_events_block_sponsors', '0', '0', 'Sponsors', '', '1', '71.9', 'non,memb', '0'), 
    ('bx_events_view', '1140px', 'Event''s News block', '_bx_events_block_news', '0', '0', 'News', '', '1', '71.9', 'non,memb', '0'), 
    ('bx_events_view', '1140px', 'Event''s Venue block', '_bx_events_block_venues', '0', '0', 'Venues', '', '1', '71.9', 'non,memb', '0'), 

	('bx_events_main', '1140px', 'Create Event', '_bx_events_block_create', '3', '0', 'Create', '', '1', '28.1', 'non,memb', '0'),
	('bx_events_main', '1140px', 'Search Event', '_bx_events_block_search', '3', '1', 'Search', '', '1', '28.1', 'non,memb', '0'),
	('bx_events_main', '1140px', 'Event Categories', '_bx_events_block_common_categories', '3', '2', 'Categories', '', '1', '28.1', 'non,memb', '0'),	 
	('bx_events_main', '1140px', 'Top Events', '_bx_events_block_top_list', '0', '0', 'TopList', '', '1', '28.1', 'non,memb', '0'),
	('bx_events_main', '1140px', 'Popular Events', '_bx_events_block_popular_list', '0', '0', 'PopularList', '', '1', '28.1', 'non,memb', '0'), 
    ('bx_events_main', '1140px', 'Past Events', '_bx_events_block_past_list', '0', '0', 'PastList', '', '1', '71.9', 'non,memb', '0'),
	('bx_events_main', '1140px', 'Events I Created', '_bx_events_block_created', '0', '0', 'Created', '', '1', '28.1', 'non,memb', '0'), 
 	('bx_events_main', '1140px', 'Events I Joined', '_bx_events_block_joined', '0', '0', 'Joined', '', '1', '28.1', 'non,memb', '0'),  
		
	('bx_events_main', '1140px', 'Featured Events', '_bx_events_block_featured', '2', '0', 'Featured', '', '1', '71.9', 'non,memb', '0'),
    ('bx_events_main', '1140px', 'Events Forum Posts', '_bx_events_block_forum', '2', '4', 'Forum', '', '1', '71.9', 'non,memb', '0'),
    ('bx_events_main', '1140px', 'Event Comments', '_bx_events_block_latest_comments', '2', '6', 'Comments', '', '1', '71.9', 'non,memb', '0'),
    ('bx_events_main', '1140px', 'Event Tags', '_tags_plural', '2', '7', 'Tags', '', '1', '71.9', 'non,memb', '0'),
	
	('bx_events_local_state', '1140px', 'Local State Events', '_bx_events_block_browse_state_events', '2', '0', 'StateEvents', '', '1', '71.9', 'non,memb', '0'),
	('bx_events_local_state', '1140px', 'Local States', '_bx_events_block_browse_state', '3', '0', 'States', '', '1', '28.1', 'non,memb', '0'),
		
	('bx_events_my', '1140px', 'User''s joined events', '_bx_events_block_joined_events', '2', '2', 'Joined', '', '0', '100', 'non,memb', '0'), 
 	
	('index', '1140px', 'Events Calendar', '_bx_events_block_calendar', 0, 0, 'PHP', 'bx_import(''BxDolService''); return BxDolService::call(''events'', ''calendar'');', 1, 71.9, 'non,memb', 0),   
	('member', '1140px', 'Member Events', '_bx_events_block_account', 0, 0, 'PHP', 'bx_import(''BxDolService''); return BxDolService::call(''events'', ''accountpage_block'');', 1, 71.9, 'non,memb', 0),  
	('bx_events_local', '1140px', 'Local Events', '_bx_events_block_browse_country', '2', '0', 'Region', '', '1', '100', 'non,memb', '0')  
 
"); 

/*
$aFirstCol = array('0' => 'Featured',
					'1' => 'RecentlyAddedList', 
					'3' => 'UpcomingList',
					'4' => 'Forum',
					'5' => 'Activities',
					'6' => 'Comments',
					'7' => 'Tags' 
			 );

$aSecondCol = array('0' => 'Create',
					'1' => 'Search',
					'2' => 'Categories',
					'3' => 'TopList',
					'4' => 'PopularList',
					'5' => 'PastList',
					'6' => 'Created',
					'7' => 'Joined',
					 '8' => 'Calendar' 
			 );
 
db_res("UPDATE `sys_page_compose` SET `Column`=0, `Order`=0 WHERE `Page` = 'bx_events_main' and `Func`='UpcomingPhoto'");


foreach($aFirstCol as $iOrder=>$sFunc)
	db_res("UPDATE `sys_page_compose` SET `Column`=1, `Order`={$iOrder} WHERE `Page` = 'bx_events_main' and `Func`='{$sFunc}'");

foreach($aSecondCol as $iOrder=>$sFunc)
	db_res("UPDATE `sys_page_compose` SET `Column`=2, `Order`={$iOrder} WHERE `Page` = 'bx_events_main' and `Func`='{$sFunc}'");
  
*/

$iCatRoot = (int)db_value("SELECT `ID` FROM `sys_menu_top` WHERE `Caption` = '_bx_events_menu_root' AND `Active`=1 AND `Type`='top' LIMIT 1");

db_res("   
INSERT INTO `sys_menu_top` (`ID`, `Parent`, `Name`, `Caption`, `Link`, `Order`, `Visible`, `Target`, `Onclick`, `Check`, `Editable`, `Deletable`, `Active`, `Type`, `Picture`, `Icon`, `BQuickLink`, `Statistics`) VALUES  
 (NULL, $iCatRoot, 'Events in my Area', '_bx_events_menu_local', 'modules/?r=events/browse/ilocal', 11, 'non,memb', '', '', '', 1, 1, 1, 'custom', '', '', 0, ''),
 (NULL, $iCatRoot, 'Drilldown Events', '_bx_events_menu_drilldown', 'modules/?r=events/local', 12, 'non,memb', '', '', '', 1, 1, 1, 'custom', '', '', 0, '') 
");

$iCategId = db_value("SELECT `id` FROM `sys_options_cats` WHERE `name`='Events'");
 
db_res("INSERT INTO `sys_options` (`Name`, `VALUE`, `kateg`, `desc`, `Type`, `check`, `err_text`, `order_in_kateg`, `AvailableValues`) VALUES
 ('bx_events_max_preview', '200', $iCategId, 'Length of event description snippet to show in blocks', 'digit', '', '', '0', ''),
 ('bx_events_forum_max_preview', '200', $iCategId, 'length of forum post snippet to show on main page', 'digit', '', '', '0', ''),
 ('bx_events_comments_max_preview', '200', $iCategId, 'length of comments snippet to show on main page', 'digit', '', '', '0', ''), 
 ('bx_events_perpage_main_popular', '4', $iCategId, 'Number of popular events to show on main page', 'digit', '', '', '0', ''),
 ('bx_events_perpage_main_featured', '4', $iCategId, 'Number of featured events to show on main page', 'digit', '', '', '0', ''), 
('bx_events_perpage_main_top', '4', $iCategId, 'Number of top rated events to show on main page', 'digit', '', '', '0', ''),
('bx_events_perpage_main_forum', '5', $iCategId, 'Number of forum posts to show on main page', 'digit', '', '', '0', ''),
('bx_events_perpage_main_comment', '5', $iCategId, 'Number of comments to show on main page', 'digit', '', '', '0', ''),
('bx_events_perpage_accountpage', '5', $iCategId, 'Number of events to show on account page', 'digit', '', '', '0', ''), 
('bx_events_state_field', '', $iCategId, 'name of profile state field (if you added one)', 'digit', '', '', '0', ''), 
('bx_events_perpage_view_subitems', '6', $iCategId, 'Number of items (Sponsors,Venues etc) to show on event view page', 'digit', '', '', '0', ''),
('bx_events_perpage_browse_subitems', '30', $iCategId, 'Number of items (Sponsors,Venues etc) to show on the sub section browse page', 'digit', '', '', '0', '')  
 ");

db_res(" 
INSERT INTO `sys_email_templates` (`Name`, `Subject`, `Body`, `Desc`, `LangID`) VALUES
('bx_events_recurr_notify', 'Event Notification at <SiteName>', '<bx_include_auto:_email_header.html />\r\n\r\n\r\n<p><b>Dear <RecipientName></b>,</p>\r\n\r\n<p>An event which you attended, <a href=\"<EventUrl>\"><b><EventTitle></b></a>, is scheduled to be held again on <EventStart>.</p>\r\n\r\n<p>You can view more details <a href=\"<EventUrl>\">here</a>:<br></p>\r\n\r\n\r\n\r\n<bx_include_auto:_email_footer.html />', 'participant notification of recurring event', '0');
"); 
 

db_res(" 
INSERT INTO `sys_email_templates` (`Name`, `Subject`, `Body`, `Desc`, `LangID`) VALUES
('bx_events_remind_notify', 'Event Reminder at <SiteName>', '<bx_include_auto:_email_header.html />\r\n\r\n\r\n<p><b>Dear <RecipientName></b>,</p>\r\n\r\n<p>This is a Reminder that an Event in which you are a participant, <a href=\"<EventUrl>\"><b><EventTitle></b></a>, is scheduled to be held on <EventStart>.</p>\r\n\r\n<p>You can view more details <a href=\"<EventUrl>\">here</a>:<br></p>\r\n\r\n<bx_include_auto:_email_footer.html />', 'participant reminder of event start', '0');
"); 
  
db_res("
INSERT INTO `sys_cron_jobs` ( `name`, `time`, `class`, `file`, `eval`) VALUES
 ( 'BxEvents', '*/5 * * * *', 'BxEventsCron', 'modules/boonex/events/classes/BxEventsCron.php', '')
");
 

db_res("INSERT INTO `sys_acl_actions` VALUES (NULL, 'events photos add', NULL);");
$iAction = mysql_insert_id( );
db_res("INSERT INTO `sys_acl_matrix` (`IDLevel`, `IDAction`) VALUES 
    (1, $iAction), (2, $iAction), (3, $iAction);");


db_res("INSERT INTO `sys_acl_actions` VALUES (NULL, 'events sounds add', NULL);");
$iAction = mysql_insert_id( );
db_res("INSERT INTO `sys_acl_matrix` (`IDLevel`, `IDAction`) VALUES 
    (1, $iAction), (2, $iAction), (3, $iAction);");


db_res("INSERT INTO `sys_acl_actions` VALUES (NULL, 'events videos add', NULL);");
$iAction = mysql_insert_id( );
db_res("INSERT INTO `sys_acl_matrix` (`IDLevel`, `IDAction`) VALUES 
    (1, $iAction), (2, $iAction), (3, $iAction);");


db_res("INSERT INTO `sys_acl_actions` VALUES (NULL, 'events files add', NULL);");
$iAction = mysql_insert_id( );
db_res("INSERT INTO `sys_acl_matrix` (`IDLevel`, `IDAction`) VALUES 
    (1, $iAction), (2, $iAction), (3, $iAction);");

db_res("INSERT INTO `sys_acl_actions` VALUES (NULL, 'events rss add', NULL);");
$iAction = mysql_insert_id( );
db_res("INSERT INTO `sys_acl_matrix` (`IDLevel`, `IDAction`) VALUES 
    (1, $iAction), (2, $iAction), (3, $iAction);");

 
///PAID LISTINGS 
 
db_res("
ALTER TABLE `bx_events_main`  
  ADD  `currency` varchar(255) NOT NULL default '', 
  ADD  `featured_expiry_date`  INT NOT NULL,
  ADD  `featured_date` INT NOT NULL, 
  ADD  `pre_expire_notify` int(11) NOT NULL, 
  ADD  `post_expire_notify` int(11) NOT NULL,   
  ADD  `expiry_date` int(11) NOT NULL default '0',
  ADD  `invoice_no` varchar(100) COLLATE utf8_general_ci NOT NULL 
");

db_res("ALTER TABLE `bx_events_main` CHANGE `Status` `Status` ENUM( 'approved', 'pending', 'expired' ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'approved'");
 
db_res("
CREATE TABLE IF NOT EXISTS `bx_events_packages` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) collate utf8_general_ci NOT NULL,
  `price` float NOT NULL,
  `days` int(11) NOT NULL,
  `description` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `videos` int(11) NOT NULL default '0', 
  `photos` int(11) NOT NULL default '0', 
  `sounds` int(11) NOT NULL default '0', 
  `files` int(11) NOT NULL default '0', 
  `featured` int(11) NOT NULL default '0', 
  `status` enum('active','pending') collate utf8_general_ci NOT NULL default 'active',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci ;
");

db_res("
CREATE TABLE IF NOT EXISTS `bx_events_invoices` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `invoice_no` varchar(100) NOT NULL,
  `price` float NOT NULL,
  `days` int(11) unsigned NOT NULL,
  `event_id` int(11) unsigned NOT NULL,
  `package_id` int(11) unsigned NOT NULL,
  `invoice_status` enum('pending','paid') NOT NULL default 'pending',
  `invoice_due_date` int(11) NOT NULL,
  `invoice_expiry_date` int(11) NOT NULL,
  `invoice_date` int(11) unsigned NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ;
");

db_res("
CREATE TABLE IF NOT EXISTS `bx_events_orders` (
  `id` int(11) unsigned NOT NULL auto_increment, 
  `invoice_no` varchar(100) COLLATE utf8_general_ci NOT NULL,
  `order_no` varchar(100) COLLATE utf8_general_ci NOT NULL, 
  `buyer_id` int(11) unsigned NOT NULL,
  `payment_method` varchar(100) COLLATE utf8_general_ci NOT NULL, 
  `order_status` ENUM( 'approved', 'pending' ) NOT NULL DEFAULT 'approved',
  `order_date` int(11) unsigned NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `profile_id` (`buyer_id`,`order_no`) 
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
");

db_res("
CREATE TABLE IF NOT EXISTS `bx_events_featured_orders` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `price` FLOAT UNSIGNED NOT NULL,
  `days` int(11) unsigned NOT NULL, 
  `item_id` int(11) unsigned NOT NULL,
  `buyer_id` int(11) unsigned NOT NULL,
  `trans_id` varchar(100) COLLATE utf8_general_ci NOT NULL,
  `trans_type` varchar(100) COLLATE utf8_general_ci NOT NULL, 
  `created` int(11) unsigned NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `featured_order_id` (`buyer_id`,`trans_id`) 
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
");

$iMaxOrder = db_value("SELECT `Order` FROM `sys_page_compose_pages` ORDER BY `Order` DESC LIMIT 1");
 
$iMaxOrder++;  
db_res("INSERT INTO `sys_page_compose_pages` (`Name`, `Title`, `Order`) VALUES ('bx_events_packages', 'Listing Packages', $iMaxOrder)"); 

db_res("
INSERT INTO `sys_page_compose` (`Page`, `PageWidth`, `Desc`, `Caption`, `Column`, `Order`, `Func`, `Content`, `DesignBox`, `ColWidth`, `Visible`, `MinWidth`) VALUES 
    ('bx_events_packages', '1140px', 'Events Packages', '_bx_events_block_packages', '2', '0', 'Packages', '', '1', '100', 'non,memb', '0')  
");
 
 

$iCategId = (int)db_value("SELECT `id` FROM  `sys_options_cats` WHERE `name`='Events'");
db_res("  
INSERT INTO `sys_options` (`Name`, `VALUE`, `kateg`, `desc`, `Type`, `check`, `err_text`, `order_in_kateg`, `AvailableValues`) VALUES

('bx_events_join_after_start', '', $iCategId, 'allow members to join events after they start', 'checkbox', '', '', '0', ''), 
('bx_events_free_expired', '0', $iCategId, 'number of days before free events listings expires (0-never expires)', 'digit', '', '', '0', ''), 
('bx_events_paypal_email', '', $iCategId, 'Paypal Email', 'digit', '', '', 0, ''),
('bx_events_paid_active', '', $iCategId, 'Activate Paid Events listings',  'checkbox', '', '', 0, ''), 
('bx_events_currency_code', 'USD', $iCategId, 'Currency code for checkout system (eg. USD,EURO,GBP)', 'digit', 'return strlen(\$arg0) > 0;', 'cannot be empty.', '0', ''),
('bx_events_paypal_item_desc', 'Events Package purchase', $iCategId, 'Item decription displayed on PayPal Package Purchase', 'digit', '', '', 0, ''), 
('bx_events_invoice_valid_days', '100', $iCategId, 'Number of Days before pending Invoices expire<br>blank or zero means no expiration', 'digit', '', '', 0, ''), 
('bx_events_default_country', 'US', $iCategId, 'default country for location', 'digit', '', '', 0, '') 
");
  
db_res("
INSERT INTO `sys_objects_actions` (`Caption`, `Icon`, `Url`, `Script`, `Eval`, `Order`, `Type`) VALUES 
    ('{TitlePurchaseFeatured}', 'shopping-cart', '{evalResult}', '', '\$oConfig = \$GLOBALS[''oBxEventsModule'']->_oConfig; return BX_DOL_URL_ROOT . \$oConfig->getBaseUri() . ''purchase_featured/{ID}'';', '16', 'bx_events'),
    ('{TitleExcel}', 'table', '{BaseUri}excel/{ID}', '', '', 10, 'bx_events'),     
    ('{TitlePrint}', 'print', '{BaseUri}print/{ID}', '', '', 11, 'bx_events')  
");

db_res("
INSERT INTO `sys_email_templates`(`Name`, `Subject`, `Body`, `Desc`, `LangID`) VALUES
('bx_events_expired', 'Your Event listing at <SiteName> has expired', '<bx_include_auto:_email_header.html />\r\n\r\n<p><b>Hello <NickName></b>,</p><p>Your Event listing, <a href=\"<ListLink>\"><ListTitle></a> at <a href=\"<SiteLink>\"><SiteName></a> has expired</p>\r\n\r\n<bx_include_auto:_email_footer.html />', 'Expired Event listing notification', '0');
");
 
db_res(" 
INSERT INTO `sys_email_templates`(`Name`, `Subject`, `Body`, `Desc`, `LangID`) VALUES
('bx_events_post_expired', 'Message about your expired Event listing at <SiteName>', '<bx_include_auto:_email_header.html />\r\n\r\n<p><b>Hello <NickName></b>,</p><p>Your Event listing, <a href=\"<ListLink>\"><ListTitle></a> at <a href=\"<SiteLink>\"><SiteName></a> has expired <b><Days></b> days ago</p>\r\n\r\n<bx_include_auto:_email_footer.html />', 'Post-Expired Event listing notification', '0');
");
 
db_res("  
INSERT INTO `sys_email_templates`(`Name`, `Subject`, `Body`, `Desc`, `LangID`) VALUES
('bx_events_expiring', 'Message about your expiring Event listing at <SiteName>', '<bx_include_auto:_email_header.html />\r\n\r\n<p><b>Hello <NickName></b>,</p><p>Your Event listing, <a href=\"<ListLink>\"><ListTitle></a> at <a href=\"<SiteLink>\"><SiteName></a> will expire in <b><Days></b> days<br></p>\r\n\r\n<bx_include_auto:_email_footer.html />', 'Expiring Event listing notification', '0');
");
 
db_res("   
INSERT INTO `sys_email_templates` (`Name`, `Subject`, `Body`, `Desc`, `LangID`) VALUES
('bx_events_featured_expire_notify', 'Your Featured Event Status at <SiteName> has expired', '<bx_include_auto:_email_header.html />\r\n\r\n\r\n<p><b>Dear <NickName></b>,</p>\r\n\r\n<p>This is inform you that your Featured Status for the Event listing, <a href=\"<ListLink>\"><ListTitle></a> at <SiteName> has expired. You may purchase Featured Status again at any time you desire <br></p>\r\n\r\n\r\n\r\n<bx_include_auto:_email_footer.html />', 'Featured Event Status Expire Notification', '0');
");
 
db_res("  
INSERT INTO `sys_email_templates` (`Name`, `Subject`, `Body`, `Desc`, `LangID`) VALUES
('bx_events_featured_admin_notify', 'A member purchased Featured Event Status at <SiteName>', '<bx_include_auto:_email_header.html />\r\n\r\n\r\n<p><b>Dear Administrator</b>,</p>\r\n\r\n<p><a href=\"<NickLink>\"><NickName></a> has just purchased Featured Status for the Event listing, <a href=\"<ListLink>\"><ListTitle></a>, for <Days> days at <SiteName><br></p>\r\n\r\n\r\n\r\n<bx_include_auto:_email_footer.html />', 'Featured Event Purchase Admin Notification', '0');
");
 
db_res("   
INSERT INTO `sys_email_templates` (`Name`, `Subject`, `Body`, `Desc`, `LangID`) VALUES
('bx_events_featured_buyer_notify', 'Your Featured Event Status purchase at <SiteName>', '<bx_include_auto:_email_header.html />\r\n\r\n\r\n<p><b>Dear <NickName></b>,</p>\r\n\r\n<p>This is confirmation of your Featured Status purchase at <SiteName> for Event listing, <a href=\"<ListLink>\"><ListTitle></a>. It will be Featured for <Days> days<br></p>\r\n\r\n\r\n\r\n<bx_include_auto:_email_footer.html />', 'Featured Event Purchase Buyer Notification', '0');
");
 
$iCatRoot = (int)db_value("SELECT `ID` FROM `sys_menu_top` WHERE `Parent`=0 AND `Name`='Events' AND `Type`='top' AND `Caption`='_bx_events_menu_root'"); 
 
db_res(" 
INSERT INTO `sys_menu_top` (`ID`, `Parent`, `Name`, `Caption`, `Link`, `Order`, `Visible`, `Target`, `Onclick`, `Check`, `Editable`, `Deletable`, `Active`, `Type`, `Picture`, `Icon`, `BQuickLink`, `Statistics`) VALUES(NULL, $iCatRoot, 'Events Packages', '_bx_events_menu_packages', 'modules/?r=events/packages', 13, 'non,memb', '', '', '', 1, 1, 1, 'custom', '', '', 0, '');
");
 
  
db_res("INSERT INTO `sys_acl_actions` VALUES (NULL, 'events purchase featured', NULL)");
$iAction = mysql_insert_id( );
db_res("INSERT INTO `sys_acl_matrix` (`IDLevel`, `IDAction`) VALUES 
    (2, $iAction), (3, $iAction)");
 

 

/**********[BEGIN] SPONSORS**************/

db_res(" 
CREATE TABLE IF NOT EXISTS `bx_events_sponsor_main` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `event_id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL default '',
  `uri` varchar(255) NOT NULL,
  `desc` text NOT NULL,
  `status` enum('approved','pending') NOT NULL default 'approved', 
  `thumb` int(11) NOT NULL,
  `created` int(11) NOT NULL,  
  `views` int(11) NOT NULL,
  `rate` float NOT NULL,
  `rate_count` int(11) NOT NULL, 
  `comments_count` int(11) NOT NULL,
  `featured` tinyint(4) NOT NULL,
  `allow_view_to` VARCHAR( 16 ) NOT NULL, 
  `allow_comment_to` varchar(16) NOT NULL,
  `allow_rate_to` varchar(16) NOT NULL, 
  `allow_upload_photos_to` varchar(16) NOT NULL, 

  `website` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL default '',
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL default '',
  `telephone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL default '',
  `fax` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL default '',  

  `country` varchar(2) NOT NULL default 'US',
  `state` varchar(50) NOT NULL default '',
  `city` varchar(50) NOT NULL default '',
  `zip` varchar(16) NOT NULL default '',
  `address1` varchar(255) NOT NULL default '',
 
  PRIMARY KEY (`id`),
  UNIQUE KEY `loc_sponsor_uri` (`uri`) 
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
");  

db_res(" 
CREATE TABLE IF NOT EXISTS `bx_events_sponsor_images` (
  `entry_id` int(10) unsigned NOT NULL,
  `media_id` int(10) unsigned NOT NULL,
  UNIQUE KEY `entry_id` (`entry_id`,`media_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
 ");  

db_res(" 
CREATE TABLE IF NOT EXISTS `bx_events_sponsor_rating` (
  `gal_id` smallint( 6 ) NOT NULL default '0',
  `gal_rating_count` int( 11 ) NOT NULL default '0',
  `gal_rating_sum` int( 11 ) NOT NULL default '0',
  UNIQUE KEY `gal_id` (`gal_id`)
) ENGINE=MYISAM DEFAULT CHARSET=utf8;
");  

db_res("  
CREATE TABLE IF NOT EXISTS `bx_events_sponsor_rating_track` (
  `gal_id` smallint( 6 ) NOT NULL default '0',
  `gal_ip` varchar( 20 ) default NULL,
  `gal_date` datetime default NULL,
  KEY `gal_ip` (`gal_ip`, `gal_id`)
) ENGINE=MYISAM DEFAULT CHARSET=utf8;
");  

db_res("  
CREATE TABLE IF NOT EXISTS `bx_events_sponsor_cmts` (
  `cmt_id` int( 11 ) NOT NULL AUTO_INCREMENT ,
  `cmt_parent_id` int( 11 ) NOT NULL default '0',
  `cmt_object_id` int( 12 ) NOT NULL default '0',
  `cmt_author_id` int( 10 ) unsigned NOT NULL default '0',
  `cmt_text` text NOT NULL ,
  `cmt_mood` tinyint( 4 ) NOT NULL default '0',
  `cmt_rate` int( 11 ) NOT NULL default '0',
  `cmt_rate_count` int( 11 ) NOT NULL default '0',
  `cmt_time` datetime NOT NULL default '0000-00-00 00:00:00',
  `cmt_replies` int( 11 ) NOT NULL default '0',
  PRIMARY KEY ( `cmt_id` ),
  KEY `cmt_object_id` (`cmt_object_id` , `cmt_parent_id`)
) ENGINE=MYISAM DEFAULT CHARSET=utf8;
");  

db_res("  
CREATE TABLE IF NOT EXISTS `bx_events_sponsor_cmts_track` (
  `cmt_system_id` int( 11 ) NOT NULL default '0',
  `cmt_id` int( 11 ) NOT NULL default '0',
  `cmt_rate` tinyint( 4 ) NOT NULL default '0',
  `cmt_rate_author_id` int( 10 ) unsigned NOT NULL default '0',
  `cmt_rate_author_nip` int( 11 ) unsigned NOT NULL default '0',
  `cmt_rate_ts` int( 11 ) NOT NULL default '0',
  PRIMARY KEY (`cmt_system_id` , `cmt_id` , `cmt_rate_author_nip`)
) ENGINE=MYISAM DEFAULT CHARSET=utf8;
");  

  
 
db_res("INSERT INTO `sys_objects_vote` VALUES (NULL, 'bx_events_sponsor', 'bx_events_sponsor_rating', 'bx_events_sponsor_rating_track', 'gal_', '5', 'vote_send_result', 'BX_PERIOD_PER_VOTE', '1', '', '', 'bx_events_sponsor_main', 'rate', 'rate_count', 'id', 'BxEventsSponsorVoting', 'modules/boonex/events/classes/BxEventsSponsorVoting.php');
");  

db_res("INSERT INTO `sys_objects_cmts` VALUES (NULL, 'bx_events_sponsor', 'bx_events_sponsor_cmts', 'bx_events_sponsor_cmts_track', '0', '1', '90', '5', '1', '-3', 'none', '0', '1', '0', 'cmt', 'bx_events_sponsor_main', 'id', 'comments_count', 'BxEventsSponsorCmts', 'modules/boonex/events/classes/BxEventsSponsorCmts.php');
");   
 
db_res("INSERT INTO `sys_objects_actions` (`Caption`, `Icon`, `Url`, `Script`, `Eval`, `Order`, `Type`) VALUES 
    ('{TitleEdit}', 'edit', '{evalResult}', '', '\$oConfig = \$GLOBALS[''oBxEventsModule'']->_oConfig; return  BX_DOL_URL_ROOT . \$oConfig->getBaseUri() . ''sponsor/edit/{ID}'';', '0', 'bx_events_sponsor'),
    ('{TitleDelete}', 'remove', '', 'getHtmlData( ''ajaxy_popup_result_div_{ID}'', ''{evalResult}'', false, ''post'');return false;', '\$oConfig = \$GLOBALS[''oBxEventsModule'']->_oConfig; return  BX_DOL_URL_ROOT . \$oConfig->getBaseUri() . ''sponsor/delete/{ID}'';', '1', 'bx_events_sponsor');
");  
 

$iMaxOrder = (int)db_value("SELECT `Order` FROM `sys_page_compose_pages` ORDER BY `Order` DESC LIMIT 1");
   
db_res("INSERT INTO `sys_page_compose_pages` (`Name`, `Title`, `Order`) VALUES ('bx_events_sponsors_view', 'Event Sponsor View', $iMaxOrder)");

$iMaxOrder++;
db_res("INSERT INTO `sys_page_compose_pages` (`Name`, `Title`, `Order`) VALUES ('bx_events_sponsors_browse', 'Event Sponsor Browse', $iMaxOrder)");

db_res(" 
INSERT INTO `sys_page_compose` (`Page`, `PageWidth`, `Desc`, `Caption`, `Column`, `Order`, `Func`, `Content`, `DesignBox`, `ColWidth`, `Visible`, `MinWidth`) VALUES 
    
    ('bx_events_sponsors_browse', '1140px', 'Event Sponsor''s browse block', '_bx_events_block_browse_sponsors', '2', '0', 'Browse', '', '1', '100', 'non,memb', '0'),

    ('bx_events_sponsors_view', '1140px', 'Event Sponsor''s actions block', '_bx_events_block_actions', '3', '0', 'Actions', '', '1', '28.1', 'non,memb', '0'),
	('bx_events_sponsors_view', '1140px', 'Event Sponsor''s info block', '_bx_events_block_info', '3', '1', 'Info', '', '1', '28.1', 'non,memb', '0'),
    ('bx_events_sponsors_view', '1140px', 'Event Sponsor''s rate block', '_bx_events_block_rate', '3', '2', 'Rate', '', '1', '28.1', 'non,memb', '0'),    
	('bx_events_sponsors_view', '1140px', 'Event Sponsor''s contact block', '_bx_events_block_contact', '3', '3', 'Contact', '', '1', '28.1', 'non,memb', '0'),    
    ('bx_events_sponsors_view', '1140px', 'Event Sponsor''s location block', '_bx_events_block_location', '3', '4', 'Location', '', '1', '28.1', 'non,memb', '0'),     
	('bx_events_sponsors_view', '1140px', 'Event Sponsor''s map view', '_bx_events_block_map', 3, 5, 'PHP', 'return BxDolService::call(''wmap'', ''location_block'', array(''events_sponsor'', \$this->aDataEntry[\$this->_oDb->_sFieldId]));', 1, 28.1, 'non,memb', 0),
	
	('bx_events_sponsors_view', '1140px', 'Event Sponsor''s description block', '_bx_events_block_desc', '2', '0', 'Desc', '', '1', '71.9', 'non,memb', '0'),
    ('bx_events_sponsors_view', '1140px', 'Event Sponsor''s photos block', '_bx_events_block_photos', '2', '1', 'Photos', '', '1', '71.9', 'non,memb', '0'),
    ('bx_events_sponsors_view', '1140px', 'Event Sponsor''s comments block', '_bx_events_block_comments', '2', '2', 'Comments', '', '1', '71.9', 'non,memb', '0');    
"); 
 
db_res("
INSERT INTO `sys_objects_actions` (`Caption`, `Icon`, `Url`, `Script`, `Eval`, `Order`, `Type`) VALUES  
    ('{TitleSponsorAdd}', 'plus-circle', '{evalResult}', '', '\$oConfig = \$GLOBALS[''oBxEventsModule'']->_oConfig; return BX_DOL_URL_ROOT . \$oConfig->getBaseUri() . ''sponsor/add/{ID}'';', '15', 'bx_events'); 
");

db_res("UPDATE `sys_menu_top` SET `Link` = CONCAT(`Link`, '|modules/?r=events/sponsor/add/|modules/?r=events/sponsor/edit/|modules/?r=events/sponsor/view/|modules/?r=events/sponsor/browse/') WHERE `Parent`=0 AND `Name`='Events' AND `Type`='system' AND `Caption`='_bx_events_menu_root'"); 
 


$iCatRoot =(int)db_value("SELECT ID FROM `sys_menu_top` WHERE `Parent`=0 AND `Name`='Events' AND `Type`='system' AND `Caption`='_bx_events_menu_root'"); 

db_res("
 INSERT INTO `sys_menu_top` (`ID`, `Parent`, `Name`, `Caption`, `Link`, `Order`, `Visible`, `Target`, `Onclick`, `Check`, `Editable`, `Deletable`, `Active`, `Type`, `Picture`, `Icon`, `BQuickLink`, `Statistics`) VALUES (NULL, $iCatRoot, 'Event View Sponsors', '_bx_events_menu_view_sponsors', 'modules/?r=events/sponsor/browse/{bx_events_view_uri}', 5, 'non,memb', '', '', '', 1, 1, 1, 'custom', '', '', 0, '') 
"); 


/**********[END] SPONSORS**************/
 


/**********[BEGIN] NEWS **************/

db_res(" 
CREATE TABLE IF NOT EXISTS `bx_events_news_main` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `event_id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL default '',
  `uri` varchar(255) NOT NULL,
  `desc` text NOT NULL,
  `status` enum('approved','pending') NOT NULL default 'approved', 
  `thumb` int(11) NOT NULL,
  `created` int(11) NOT NULL,  
  `views` int(11) NOT NULL,
  `rate` float NOT NULL,
  `rate_count` int(11) NOT NULL, 
  `comments_count` int(11) NOT NULL,
  `featured` tinyint(4) NOT NULL,
  `allow_view_to` VARCHAR( 16 ) NOT NULL, 
  `allow_comment_to` varchar(16) NOT NULL,
  `allow_rate_to` varchar(16) NOT NULL, 
  `allow_upload_photos_to` varchar(16) NOT NULL, 
  PRIMARY KEY (`id`),
  UNIQUE KEY `loc_news_uri` (`uri`) 
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
");  

db_res(" 
CREATE TABLE IF NOT EXISTS `bx_events_news_images` (
  `entry_id` int(10) unsigned NOT NULL,
  `media_id` int(10) unsigned NOT NULL,
  UNIQUE KEY `entry_id` (`entry_id`,`media_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
 ");  

db_res(" 
CREATE TABLE IF NOT EXISTS `bx_events_news_rating` (
  `gal_id` smallint( 6 ) NOT NULL default '0',
  `gal_rating_count` int( 11 ) NOT NULL default '0',
  `gal_rating_sum` int( 11 ) NOT NULL default '0',
  UNIQUE KEY `gal_id` (`gal_id`)
) ENGINE=MYISAM DEFAULT CHARSET=utf8;
");  

db_res("  
CREATE TABLE IF NOT EXISTS `bx_events_news_rating_track` (
  `gal_id` smallint( 6 ) NOT NULL default '0',
  `gal_ip` varchar( 20 ) default NULL,
  `gal_date` datetime default NULL,
  KEY `gal_ip` (`gal_ip`, `gal_id`)
) ENGINE=MYISAM DEFAULT CHARSET=utf8;
");  

db_res("  
CREATE TABLE IF NOT EXISTS `bx_events_news_cmts` (
  `cmt_id` int( 11 ) NOT NULL AUTO_INCREMENT ,
  `cmt_parent_id` int( 11 ) NOT NULL default '0',
  `cmt_object_id` int( 12 ) NOT NULL default '0',
  `cmt_author_id` int( 10 ) unsigned NOT NULL default '0',
  `cmt_text` text NOT NULL ,
  `cmt_mood` tinyint( 4 ) NOT NULL default '0',
  `cmt_rate` int( 11 ) NOT NULL default '0',
  `cmt_rate_count` int( 11 ) NOT NULL default '0',
  `cmt_time` datetime NOT NULL default '0000-00-00 00:00:00',
  `cmt_replies` int( 11 ) NOT NULL default '0',
  PRIMARY KEY ( `cmt_id` ),
  KEY `cmt_object_id` (`cmt_object_id` , `cmt_parent_id`)
) ENGINE=MYISAM DEFAULT CHARSET=utf8;
");  

db_res("  
CREATE TABLE IF NOT EXISTS `bx_events_news_cmts_track` (
  `cmt_system_id` int( 11 ) NOT NULL default '0',
  `cmt_id` int( 11 ) NOT NULL default '0',
  `cmt_rate` tinyint( 4 ) NOT NULL default '0',
  `cmt_rate_author_id` int( 10 ) unsigned NOT NULL default '0',
  `cmt_rate_author_nip` int( 11 ) unsigned NOT NULL default '0',
  `cmt_rate_ts` int( 11 ) NOT NULL default '0',
  PRIMARY KEY (`cmt_system_id` , `cmt_id` , `cmt_rate_author_nip`)
) ENGINE=MYISAM DEFAULT CHARSET=utf8;
");  

  
 
db_res("INSERT INTO `sys_objects_vote` VALUES (NULL, 'bx_events_news', 'bx_events_news_rating', 'bx_events_news_rating_track', 'gal_', '5', 'vote_send_result', 'BX_PERIOD_PER_VOTE', '1', '', '', 'bx_events_news_main', 'rate', 'rate_count', 'id', 'BxEventsNewsVoting', 'modules/boonex/events/classes/BxEventsNewsVoting.php');
");  

db_res("INSERT INTO `sys_objects_cmts` VALUES (NULL, 'bx_events_news', 'bx_events_news_cmts', 'bx_events_news_cmts_track', '0', '1', '90', '5', '1', '-3', 'none', '0', '1', '0', 'cmt', 'bx_events_news_main', 'id', 'comments_count', 'BxEventsNewsCmts', 'modules/boonex/events/classes/BxEventsNewsCmts.php');
");   
 
db_res("INSERT INTO `sys_objects_actions` (`Caption`, `Icon`, `Url`, `Script`, `Eval`, `Order`, `Type`) VALUES 
    ('{TitleEdit}', 'edit', '{evalResult}', '', '\$oConfig = \$GLOBALS[''oBxEventsModule'']->_oConfig; return  BX_DOL_URL_ROOT . \$oConfig->getBaseUri() . ''news/edit/{ID}'';', '0', 'bx_events_news'),
    ('{TitleDelete}', 'remove', '', 'getHtmlData( ''ajaxy_popup_result_div_{ID}'', ''{evalResult}'', false, ''post'');return false;', '\$oConfig = \$GLOBALS[''oBxEventsModule'']->_oConfig; return  BX_DOL_URL_ROOT . \$oConfig->getBaseUri() . ''news/delete/{ID}'';', '1', 'bx_events_news');
");  
 

$iMaxOrder = (int)db_value("SELECT `Order` FROM `sys_page_compose_pages` ORDER BY `Order` DESC LIMIT 1");
   
db_res("INSERT INTO `sys_page_compose_pages` (`Name`, `Title`, `Order`) VALUES ('bx_events_news_view', 'Event News View', $iMaxOrder)");

$iMaxOrder++;
db_res("INSERT INTO `sys_page_compose_pages` (`Name`, `Title`, `Order`) VALUES ('bx_events_news_browse', 'Event News Browse', $iMaxOrder)");

db_res(" 
INSERT INTO `sys_page_compose` (`Page`, `PageWidth`, `Desc`, `Caption`, `Column`, `Order`, `Func`, `Content`, `DesignBox`, `ColWidth`, `Visible`, `MinWidth`) VALUES 
    
    ('bx_events_news_browse', '1140px', 'Event News''s browse block', '_bx_events_block_browse_news', '2', '0', 'Browse', '', '1', '100', 'non,memb', '0'),

    ('bx_events_news_view', '1140px', 'Event News''s actions block', '_bx_events_block_actions', '3', '0', 'Actions', '', '1', '28.1', 'non,memb', '0'),
	('bx_events_news_view', '1140px', 'Event News''s info block', '_bx_events_block_info', '3', '1', 'Info', '', '1', '28.1', 'non,memb', '0'),
    ('bx_events_news_view', '1140px', 'Event News''s rate block', '_bx_events_block_rate', '3', '2', 'Rate', '', '1', '28.1', 'non,memb', '0'),    
    ('bx_events_news_view', '1140px', 'Event News''s description block', '_bx_events_block_desc', '2', '0', 'Desc', '', '1', '71.9', 'non,memb', '0'),
    ('bx_events_news_view', '1140px', 'Event News''s photos block', '_bx_events_block_photos', '2', '1', 'Photos', '', '1', '71.9', 'non,memb', '0'),
    ('bx_events_news_view', '1140px', 'Event News''s comments block', '_bx_events_block_comments', '2', '2', 'Comments', '', '1', '71.9', 'non,memb', '0');    
"); 
 
db_res("
INSERT INTO `sys_objects_actions` (`Caption`, `Icon`, `Url`, `Script`, `Eval`, `Order`, `Type`) VALUES  
    ('{TitleNewsAdd}', 'plus-circle', '{evalResult}', '', '\$oConfig = \$GLOBALS[''oBxEventsModule'']->_oConfig; return BX_DOL_URL_ROOT . \$oConfig->getBaseUri() . ''news/add/{ID}'';', '15', 'bx_events'); 
");

db_res("UPDATE `sys_menu_top` SET `Link` = CONCAT(`Link`, '|modules/?r=events/news/add/|modules/?r=events/news/edit/|modules/?r=events/news/view/|modules/?r=events/news/browse/') WHERE `Parent`=0 AND `Name`='Events' AND `Type`='system' AND `Caption`='_bx_events_menu_root'"); 
 


$iCatRoot =(int)db_value("SELECT ID FROM `sys_menu_top` WHERE `Parent`=0 AND `Name`='Events' AND `Type`='system' AND `Caption`='_bx_events_menu_root'"); 

db_res("
 INSERT INTO `sys_menu_top` (`ID`, `Parent`, `Name`, `Caption`, `Link`, `Order`, `Visible`, `Target`, `Onclick`, `Check`, `Editable`, `Deletable`, `Active`, `Type`, `Picture`, `Icon`, `BQuickLink`, `Statistics`) VALUES (NULL, $iCatRoot, 'Event View News', '_bx_events_menu_view_news', 'modules/?r=events/news/browse/{bx_events_view_uri}', 5, 'non,memb', '', '', '', 1, 1, 1, 'custom', '', '', 0, '') 
"); 


/**********[END] NEWS **************/


/**********[BEGIN] VENUE **************/

db_res(" 
CREATE TABLE IF NOT EXISTS `bx_events_venue_main` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `event_id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL default '',
  `uri` varchar(255) NOT NULL,
  `desc` text NOT NULL,
  `status` enum('approved','pending') NOT NULL default 'approved', 
  `thumb` int(11) NOT NULL,
  `created` int(11) NOT NULL,  
  `views` int(11) NOT NULL,
  `rate` float NOT NULL,
  `rate_count` int(11) NOT NULL, 
  `comments_count` int(11) NOT NULL,
  `featured` tinyint(4) NOT NULL,
  `allow_view_to` VARCHAR( 16 ) NOT NULL, 
  `allow_comment_to` varchar(16) NOT NULL,
  `allow_rate_to` varchar(16) NOT NULL, 
  `allow_upload_photos_to` varchar(16) NOT NULL, 
  PRIMARY KEY (`id`),
  UNIQUE KEY `loc_venue_uri` (`uri`) 
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
");  

db_res(" 
CREATE TABLE IF NOT EXISTS `bx_events_venue_images` (
  `entry_id` int(10) unsigned NOT NULL,
  `media_id` int(10) unsigned NOT NULL,
  UNIQUE KEY `entry_id` (`entry_id`,`media_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
 ");  

db_res(" 
CREATE TABLE IF NOT EXISTS `bx_events_venue_rating` (
  `gal_id` smallint( 6 ) NOT NULL default '0',
  `gal_rating_count` int( 11 ) NOT NULL default '0',
  `gal_rating_sum` int( 11 ) NOT NULL default '0',
  UNIQUE KEY `gal_id` (`gal_id`)
) ENGINE=MYISAM DEFAULT CHARSET=utf8;
");  

db_res("  
CREATE TABLE IF NOT EXISTS `bx_events_venue_rating_track` (
  `gal_id` smallint( 6 ) NOT NULL default '0',
  `gal_ip` varchar( 20 ) default NULL,
  `gal_date` datetime default NULL,
  KEY `gal_ip` (`gal_ip`, `gal_id`)
) ENGINE=MYISAM DEFAULT CHARSET=utf8;
");  

db_res("  
CREATE TABLE IF NOT EXISTS `bx_events_venue_cmts` (
  `cmt_id` int( 11 ) NOT NULL AUTO_INCREMENT ,
  `cmt_parent_id` int( 11 ) NOT NULL default '0',
  `cmt_object_id` int( 12 ) NOT NULL default '0',
  `cmt_author_id` int( 10 ) unsigned NOT NULL default '0',
  `cmt_text` text NOT NULL ,
  `cmt_mood` tinyint( 4 ) NOT NULL default '0',
  `cmt_rate` int( 11 ) NOT NULL default '0',
  `cmt_rate_count` int( 11 ) NOT NULL default '0',
  `cmt_time` datetime NOT NULL default '0000-00-00 00:00:00',
  `cmt_replies` int( 11 ) NOT NULL default '0',
  PRIMARY KEY ( `cmt_id` ),
  KEY `cmt_object_id` (`cmt_object_id` , `cmt_parent_id`)
) ENGINE=MYISAM DEFAULT CHARSET=utf8;
");  

db_res("  
CREATE TABLE IF NOT EXISTS `bx_events_venue_cmts_track` (
  `cmt_system_id` int( 11 ) NOT NULL default '0',
  `cmt_id` int( 11 ) NOT NULL default '0',
  `cmt_rate` tinyint( 4 ) NOT NULL default '0',
  `cmt_rate_author_id` int( 10 ) unsigned NOT NULL default '0',
  `cmt_rate_author_nip` int( 11 ) unsigned NOT NULL default '0',
  `cmt_rate_ts` int( 11 ) NOT NULL default '0',
  PRIMARY KEY (`cmt_system_id` , `cmt_id` , `cmt_rate_author_nip`)
) ENGINE=MYISAM DEFAULT CHARSET=utf8;
");  

  
 
db_res("INSERT INTO `sys_objects_vote` VALUES (NULL, 'bx_events_venue', 'bx_events_venue_rating', 'bx_events_venue_rating_track', 'gal_', '5', 'vote_send_result', 'BX_PERIOD_PER_VOTE', '1', '', '', 'bx_events_venue_main', 'rate', 'rate_count', 'id', 'BxEventsVenueVoting', 'modules/boonex/events/classes/BxEventsVenueVoting.php');
");  

db_res("INSERT INTO `sys_objects_cmts` VALUES (NULL, 'bx_events_venue', 'bx_events_venue_cmts', 'bx_events_venue_cmts_track', '0', '1', '90', '5', '1', '-3', 'none', '0', '1', '0', 'cmt', 'bx_events_venue_main', 'id', 'comments_count', 'BxEventsVenueCmts', 'modules/boonex/events/classes/BxEventsVenueCmts.php');
");   
 
db_res("INSERT INTO `sys_objects_actions` (`Caption`, `Icon`, `Url`, `Script`, `Eval`, `Order`, `Type`) VALUES 
    ('{TitleEdit}', 'edit', '{evalResult}', '', '\$oConfig = \$GLOBALS[''oBxEventsModule'']->_oConfig; return  BX_DOL_URL_ROOT . \$oConfig->getBaseUri() . ''venue/edit/{ID}'';', '0', 'bx_events_venue'),
    ('{TitleDelete}', 'remove', '', 'getHtmlData( ''ajaxy_popup_result_div_{ID}'', ''{evalResult}'', false, ''post'');return false;', '\$oConfig = \$GLOBALS[''oBxEventsModule'']->_oConfig; return  BX_DOL_URL_ROOT . \$oConfig->getBaseUri() . ''venue/delete/{ID}'';', '1', 'bx_events_venue');
");  
 

$iMaxOrder = (int)db_value("SELECT `Order` FROM `sys_page_compose_pages` ORDER BY `Order` DESC LIMIT 1");
   
db_res("INSERT INTO `sys_page_compose_pages` (`Name`, `Title`, `Order`) VALUES ('bx_events_venues_view', 'Event Venue View', $iMaxOrder)");

$iMaxOrder++;
db_res("INSERT INTO `sys_page_compose_pages` (`Name`, `Title`, `Order`) VALUES ('bx_events_venues_browse', 'Event Venue Browse', $iMaxOrder)");

db_res(" 
INSERT INTO `sys_page_compose` (`Page`, `PageWidth`, `Desc`, `Caption`, `Column`, `Order`, `Func`, `Content`, `DesignBox`, `ColWidth`, `Visible`, `MinWidth`) VALUES 
    
    ('bx_events_venues_browse', '1140px', 'Event Venue''s browse block', '_bx_events_block_browse_venues', '2', '0', 'Browse', '', '1', '100', 'non,memb', '0'),

    ('bx_events_venues_view', '1140px', 'Event Venue''s actions block', '_bx_events_block_actions', '3', '0', 'Actions', '', '1', '28.1', 'non,memb', '0'),
	('bx_events_venues_view', '1140px', 'Event Venue''s info block', '_bx_events_block_info', '3', '1', 'Info', '', '1', '28.1', 'non,memb', '0'),
	('bx_events_venues_view', '1140px', 'Event Venue''s rate block', '_bx_events_block_rate', '3', '2', 'Rate', '', '1', '28.1', 'non,memb', '0'),    
    ('bx_events_venues_view', '1140px', 'Event Venue''s description block', '_bx_events_block_desc', '2', '0', 'Desc', '', '1', '71.9', 'non,memb', '0'),
    ('bx_events_venues_view', '1140px', 'Event Venue''s photos block', '_bx_events_block_photos', '2', '1', 'Photos', '', '1', '71.9', 'non,memb', '0'),
    ('bx_events_venues_view', '1140px', 'Event Venue''s comments block', '_bx_events_block_comments', '2', '2', 'Comments', '', '1', '71.9', 'non,memb', '0');    
"); 
 
db_res("
INSERT INTO `sys_objects_actions` (`Caption`, `Icon`, `Url`, `Script`, `Eval`, `Order`, `Type`) VALUES  
    ('{TitleVenueAdd}', 'plus-circle', '{evalResult}', '', '\$oConfig = \$GLOBALS[''oBxEventsModule'']->_oConfig; return BX_DOL_URL_ROOT . \$oConfig->getBaseUri() . ''venue/add/{ID}'';', '15', 'bx_events'); 
");

db_res("UPDATE `sys_menu_top` SET `Link` = CONCAT(`Link`, '|modules/?r=events/venue/add/|modules/?r=events/venue/edit/|modules/?r=events/venue/view/|modules/?r=events/venue/browse/') WHERE `Parent`=0 AND `Name`='Events' AND `Type`='system' AND `Caption`='_bx_events_menu_root'"); 
 


$iCatRoot =(int)db_value("SELECT ID FROM `sys_menu_top` WHERE `Parent`=0 AND `Name`='Events' AND `Type`='system' AND `Caption`='_bx_events_menu_root'"); 

db_res("
 INSERT INTO `sys_menu_top` (`ID`, `Parent`, `Name`, `Caption`, `Link`, `Order`, `Visible`, `Target`, `Onclick`, `Check`, `Editable`, `Deletable`, `Active`, `Type`, `Picture`, `Icon`, `BQuickLink`, `Statistics`) VALUES (NULL, $iCatRoot, 'Event View Venues', '_bx_events_menu_view_venues', 'modules/?r=events/venue/browse/{bx_events_view_uri}', 5, 'non,memb', '', '', '', 1, 1, 1, 'custom', '', '', 0, '') 
"); 


/**********[END] VENUE **************/



/*** BEGIN Relist and Extend **/

db_res("INSERT INTO `sys_acl_actions` VALUES (NULL, 'events extend', NULL)");
$iAction = mysql_insert_id( );
db_res("INSERT INTO `sys_acl_matrix` (`IDLevel`, `IDAction`) VALUES 
    (2, $iAction), (3, $iAction)"); 

db_res("INSERT INTO `sys_acl_actions` VALUES (NULL, 'events relist', NULL)");
$iAction = mysql_insert_id( );
db_res("INSERT INTO `sys_acl_matrix` (`IDLevel`, `IDAction`) VALUES 
    (2, $iAction), (3, $iAction)");


db_res("INSERT INTO `sys_acl_actions` VALUES (NULL, 'events purchase', NULL)");
$iAction = mysql_insert_id( );
db_res("INSERT INTO `sys_acl_matrix` (`IDLevel`, `IDAction`) VALUES 
    (2, $iAction), (3, $iAction)");

 
db_res("
INSERT INTO `sys_objects_actions` (`Caption`, `Icon`, `Url`, `Script`, `Eval`, `Order`, `Type`) VALUES 
    ('{TitleRelist}', 'refresh', '{evalResult}', '', '\$oConfig = \$GLOBALS[''oBxEventsModule'']->_oConfig; return BX_DOL_URL_ROOT . \$oConfig->getBaseUri() . ''relist/{ID}'';', '16', 'bx_events'), 
    ('{TitleExtend}', 'wrench', '{evalResult}', '', '\$oConfig = \$GLOBALS[''oBxEventsModule'']->_oConfig; return BX_DOL_URL_ROOT . \$oConfig->getBaseUri() . ''extend/{ID}'';', '17', 'bx_events'),
	('{TitlePremium}', 'shopping-cart', '{evalResult}', '', '\$oConfig = \$GLOBALS[''oBxEventsModule'']->_oConfig; return BX_DOL_URL_ROOT . \$oConfig->getBaseUri() . ''premium/{ID}'';', '18', 'bx_events')  
"); 


db_res("UPDATE `sys_menu_top` SET `Link` = CONCAT(`Link`, '|modules/?r=events/relist/|modules/?r=events/extend/|modules/?r=events/premium/|modules/?r=events/purchase_featured/') WHERE `Parent`=0 AND `Name`='Events' AND `Type`='system' AND `Caption`='_bx_events_menu_root'"); 
 
/*** END Relist and Extend **/

/*** BEGIN Rss **/ 
db_res("DELETE FROM `sys_page_compose` WHERE `Page`='bx_events_view' AND `Func`='CustomRSS'");

 
db_res("
CREATE TABLE IF NOT EXISTS `bx_events_rss` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `id_entry` int(10) unsigned NOT NULL,
  `url` varchar(255) NOT NULL,
  `name` varchar(100) NOT NULL,  
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
");  
/*** END Rss **/
 
 
$iLevelStandard = 2;
$iLevelPromotion = 3;

$iAction = (int)db_value("SELECT `id` FROM `sys_acl_actions` WHERE name= 'events broadcast message'");
db_res("
INSERT INTO `sys_acl_matrix` (`IDLevel`, `IDAction`) VALUES 
    ($iLevelStandard, $iAction), ($iLevelPromotion, $iAction)
");

/*  
db_res("
INSERT INTO `sys_objects_actions` (`Caption`, `Icon`, `Url`, `Script`, `Eval`, `Order`, `Type`) VALUES 
     ('{evalResult}', 'modules/boonex/events/|events.png', '{BaseUri}browse/joined', '', 'return \$GLOBALS[''logged''][''member''] || \$GLOBALS[''logged''][''admin''] ? _t(''_bx_events_action_events_joined'') : '''';', '4', 'bx_events_title');
    
 ");
*/


db_res("UPDATE `sys_email_templates` SET `Body` = REPLACE(`Body`, '<bx_include_auto:_email_header.html />\r\n\r\n', '<bx_include_auto:_email_header.html />\r\n\r\n') WHERE `Name` LIKE 'bx_events%'");

db_res("UPDATE `sys_email_templates` SET `Body` = REPLACE(`Body`, '\r\n\r\n<bx_include_auto:_email_footer.html />', '<bx_include_auto:_email_footer.html />') WHERE `Name` LIKE 'bx_events%'");
 
db_res("UPDATE `sys_email_templates` SET `Body` = REPLACE(`Body`, '<p style=\"font: bold 10px Verdana; color:red\"><SiteName> mail delivery system!!! <br />Auto-generated e-mail, please, do not reply!!!</p></body></html>', '<bx_include_auto:_email_footer.html />') WHERE `Name` LIKE 'bx_events%'");
 
db_res("UPDATE `sys_email_templates` SET `Body` = REPLACE(`Body`, '\r\n\r\n<bx_include_auto:_email_footer.html />', '<bx_include_auto:_email_footer.html />') WHERE `Name` LIKE 'bx_events%'");

//db_res("DELETE FROM `sys_options` WHERE `Name` = 'bx_events_main_upcoming_event_from_featured_only'");
 

//[BEGIN] 2.1.4

db_res("UPDATE `sys_modules` SET `version`='2.1.4' WHERE `uri` = 'events' AND `db_prefix` = 'bx_events_'");

db_res("
 ALTER TABLE `bx_events_main`
 ADD `Zip` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL default '' 
 ");
 
 
db_res("INSERT INTO `sys_objects_actions` ( `Caption`, `Icon`, `Url`, `Script`, `Eval`, `Order`, `Type`, `bDisplayInSubMenuHeader`) VALUES
( '{evalResult}', 'plus', '', 'showPopupAnyHtml (site_url+''m/events/add_participant/{ID}'')', '\$oEvent = BxDolModule::getInstance(''BxEventsModule''); return (\$oEvent->isAllowedAddParticipant({ID})) ? _t(''_bx_events_action_title_add_participant'') : '''';', 20, 'bx_events', 0)  
");

db_res("
CREATE TABLE IF NOT EXISTS `bx_events_monitored_groups` (
  `event_id` int(10) unsigned NOT NULL,
  `group_id` int(10) unsigned NOT NULL,
  UNIQUE KEY `entry_id` (`event_id`,`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
");

 
db_res(" 
INSERT INTO `sys_alerts_handlers` VALUES (NULL, 'bx_events', 'BxEventsResponse', 'modules/boonex/events/classes/BxEventsResponse.php', '') 
");
$iHandler = mysql_insert_id();

db_res(" 
INSERT INTO `sys_alerts` VALUES  
(NULL, 'bx_groups', 'join', $iHandler),
(NULL, 'bx_groups', 'join_confirm', $iHandler) 
");
 

db_res(" 
INSERT INTO `sys_objects_actions` (`Caption`, `Icon`, `Url`, `Script`, `Eval`, `Order`, `Type`) VALUES 
    ('{TitleManageAdmins}', 'users', '', 'showPopupAnyHtml (''{BaseUri}manage_admins_popup/{ID}'');', '', '24', 'bx_events'),
    ('{TitleAdminAdd}', 'plus-circle', '{evalResult}', '', '\$oConfig = \$GLOBALS[''oBxEventsModule'']->_oConfig; return BX_DOL_URL_ROOT . \$oConfig->getBaseUri() . ''add_admin/{ID}'';', '25', 'bx_events') 
"); 
 

db_res("  
UPDATE `sys_menu_top` SET `Link` = CONCAT(`Link`,'|modules/?r=events/add_admin/') WHERE `Parent`=0 AND `Caption`='_bx_events_menu_root' AND `Type`='system' AND `Active`=1 
");

db_res(" 
INSERT INTO `sys_page_compose` (`Page`, `PageWidth`, `Desc`, `Caption`, `Column`, `Order`, `Func`, `Content`, `DesignBox`, `ColWidth`, `Visible`, `MinWidth`) VALUES 
    ('bx_events_view', '1140px', 'Event''s admins block', '_bx_events_block_admins', '3', '2', 'Admins', '', '1', '28.1', 'non,memb', '0')  
");

//[END] 2.1.4
 

//[BEGIN] 2.1.5 
db_res("UPDATE `sys_modules` SET `version`='2.1.5' WHERE `uri` = 'events' AND `db_prefix` = 'bx_events_'");
 
 
db_res(" 
INSERT INTO `sys_objects_actions` (`Caption`, `Icon`, `Url`, `Script`, `Eval`, `Order`, `Type`) VALUES 
    ('{TitleEmbed}', 'film', '{BaseUri}embed/{URI}', '', '', '9', 'bx_events') 
");
 
db_res("
UPDATE `sys_menu_top` SET `Link` = CONCAT(`Link`, '|modules/?r=events/embed/') WHERE `Parent`=0 AND `Name`='Events' AND `Caption`='_bx_events_menu_root' AND `Type`='system'
"); 
//[END] 2.1.5



//[BEGIN] 2.1.6
db_res("UPDATE `sys_modules` SET `version`='2.1.6' WHERE `uri` = 'events' AND `db_prefix` = 'bx_events_'");
   
$fields = mysql_list_fields($db['db'], "bx_events_main"); 
$columns = mysql_num_fields($fields);
       
for ($i = 0; $i < $columns; $i++) {
    $field_array[] = mysql_field_name($fields, $i);
}
       
if (!in_array('Parent', $field_array)) {
	db_res("ALTER TABLE `bx_events_main` ADD `Parent` int(11) NOT NULL");
}
  
  
db_res("ALTER TABLE `bx_events_main` CHANGE `Title` `Title` VARCHAR( 255 ) NOT NULL DEFAULT ''");
 

$aEvents = $oMain->_oDb->getAll("SELECT MAX(`ID`) as `ID`, `Parent` FROM `bx_events_main` WHERE  `Parent`!=0 GROUP BY `Parent`");
 
foreach($aEvents as $aEachEvent){
	$aLatestEvent = $oMain->_oDb->getRow("SELECT `Title`, `EntryUri` FROM `bx_events_main` WHERE `ID`=".$aEachEvent['ID']); 
	$sLatestTitle = $aLatestEvent['Title'];
	$sLatestUri = $aLatestEvent['EntryUri']; 

	$oMain->_oDb->query("DELETE FROM `bx_events_main` WHERE `Parent`=".$aEachEvent['Parent']);

	$oMain->_oDb->query("UPDATE `bx_events_main` SET `Title`='$sLatestTitle', `EntryUri`='$sLatestUri', `Status`='approved' WHERE `ID`=".$aEachEvent['Parent']); 
}

$oMain->_oDb->query("DELETE FROM `bx_events_main` WHERE `Parent`!=0"); 
//[END] 2.1.6

//[BEGIN] 2.1.7
db_res("UPDATE `sys_modules` SET `version`='2.1.7' WHERE `uri` = 'events' AND `db_prefix` = 'bx_events_'");
 
  
db_res("
UPDATE `sys_menu_top` SET `Check` = '\$oMain = BxDolModule::getInstance(''BxEventsModule''); return (\$oMain!=null) ? \$oMain->isActiveMenuLink(''forum'', ''{bx_events_view_uri}'') : false;'
WHERE `Caption`='_bx_events_menu_view_forum' AND `Type`='custom'
");


if (BxDolModule::getInstance('BxWmapModule')) {
    
	db_res("UPDATE `bx_wmap_parts` SET `join_field_state` = 'State', `join_field_zip` = 'Zip', `join_field_address` = 'Street' WHERE `part` ='events'"); 
 	  
	$oMain = BxDolModule::getInstance('BxEventsModule');
	 
	$aEvents = $oMain->_oDb->getAll("SELECT `ID` FROM `bx_events_main`");
	 
	foreach($aEvents as $aEachEvent){ 
		$iEntryId = $aEachEvent['ID'];
        BxDolService::call('wmap', 'response_entry_add', array('events', $iEntryId)); 
	} 
}  
//[END] 2.1.7
 

//[BEGIN] 2.1.8  
$oMain->_oDb->query("ALTER TABLE `bx_events_main` CHANGE `Status` `Status` ENUM( 'approved', 'pending', 'expired', 'past' ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'pending'");

$oMain->_oDb->query("UPDATE `bx_events_main` SET `Status`='past' WHERE `EventEnd` <= $iTime"); 
//[END] 2.1.8


//[BEGIN] 2.2.0  
  
db_res("INSERT INTO `sys_acl_actions` VALUES (NULL, 'events allow embed', NULL);");
$iAction = mysql_insert_id( );
db_res("INSERT INTO `sys_acl_matrix` (`IDLevel`, `IDAction`) VALUES 
    (2, $iAction), (3, $iAction)");

//[END] 2.2.0


echo "SUCCESSFULLY UPDATED DATABASE ..... <br /><br />";
 

echo "FINISHED <br />";