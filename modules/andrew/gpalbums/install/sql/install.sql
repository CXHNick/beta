-- create tables
CREATE TABLE IF NOT EXISTS `[db_prefix]_albums` (
  `ID` int(10) unsigned NOT NULL auto_increment,
  `GroupID` int(11) NOT NULL,
  `Owner` int(10) unsigned default NULL,
  `Title` varchar(255) default '',
  `Uri` varchar(255) NOT NULL default '',
  `Desc` text NOT NULL,
  `When` int(11) NOT NULL default '0',
  `Thumb` int(11) NOT NULL default '0',
  `CommentsCount` int(11) default '0',
  `AllowAlbumView` varchar(16) NOT NULL,
  `public_mode` int(1) default '0',
  PRIMARY KEY  (`ID`),
  KEY `Owner` (`Owner`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `[db_prefix]_units` (
  `ID` int(10) unsigned NOT NULL auto_increment,
  `AlbumID` int(11) NOT NULL default '0',
  `Title` varchar(255) default '',
  `Description` varchar(255) default '',
  `Uri` varchar(255) NOT NULL default '',
  `When` int(11) NOT NULL default '0',
  `Size` varchar(10) default '',
  `Filename` varchar(255) default '',
  `rate` float NOT NULL,
  `rateCount` int(11) unsigned NOT NULL,
  `commentsCount` int(11) NOT NULL,
  `featured` int(1) default '2',
  `owner` int(10) unsigned default NULL,
  PRIMARY KEY  (`ID`),
  KEY `AlbumID` (`AlbumID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `[db_prefix]_cmts` (
  `cmt_id` int(11) NOT NULL auto_increment,
  `cmt_parent_id` int(11) NOT NULL default '0',
  `cmt_object_id` int(11) NOT NULL default '0',
  `cmt_author_id` int(10) unsigned NOT NULL default '0',
  `cmt_text` text NOT NULL,
  `cmt_mood` tinyint NOT NULL default '0',
  `cmt_rate` int(11) NOT NULL default '0',
  `cmt_rate_count` int(11) NOT NULL default '0',
  `cmt_time` datetime NOT NULL default '0000-00-00 00:00:00',
  `cmt_replies` int(11) NOT NULL default '0',
  PRIMARY KEY  (`cmt_id`),
  KEY `cmt_object_id` (`cmt_object_id`,`cmt_parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `[db_prefix]_img_cmts` (
  `cmt_id` int(11) NOT NULL auto_increment,
  `cmt_parent_id` int(11) NOT NULL default '0',
  `cmt_object_id` int(11) NOT NULL default '0',
  `cmt_author_id` int(10) unsigned NOT NULL default '0',
  `cmt_text` text NOT NULL,
  `cmt_mood` tinyint NOT NULL default '0',
  `cmt_rate` int(11) NOT NULL default '0',
  `cmt_rate_count` int(11) NOT NULL default '0',
  `cmt_time` datetime NOT NULL default '0000-00-00 00:00:00',
  `cmt_replies` int(11) NOT NULL default '0',
  PRIMARY KEY  (`cmt_id`),
  KEY `cmt_object_id` (`cmt_object_id`,`cmt_parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `[db_prefix]_img_rating` (
  `gpap_id` int(11) unsigned NOT NULL default '0',
  `gpap_rating_count` int(11) unsigned NOT NULL default '0',
  `gpap_rating_sum` int(11) unsigned NOT NULL default '0',
  UNIQUE KEY `med_id` (`gpap_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `[db_prefix]_img_voting_track` (
  `gpap_id` int(11) unsigned NOT NULL default '0',
  `gpap_ip` varchar(20) default NULL,
  `gpap_date` datetime default NULL,
  KEY `med_ip` (`gpap_ip`,`gpap_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


-- page compose pages
INSERT INTO `sys_page_compose` (`ID`, `Page`, `PageWidth`, `Desc`, `Caption`, `Column`, `Order`, `Func`, `Content`, `DesignBox`, `ColWidth`, `Visible`, `MinWidth`) VALUES
(NULL, 'bx_groups_view', '1140px', 'Group''s photo albums', '_gpa_main', 2, 10, 'PHP', 'return BxDolService::call(''gphotos'', ''get_group_block'', array($this->aDataEntry[''uri'']));', 1, 71.9, 'non,memb', 0),
(NULL, 'bx_groups_main', '1140px', 'Feature Group Photos', '_gpa_featured_list', 2, 1, 'PHP', 'return BxDolService::call(''gphotos'', ''index_page_feature'', array());', 1, 71.9, 'non,memb', 0);

INSERT INTO `sys_page_compose` (`ID`, `Page`, `PageWidth`, `Desc`, `Caption`, `Column`, `Order`, `Func`, `Content`, `DesignBox`, `ColWidth`, `Visible`, `MinWidth`) VALUES
(NULL, 'index', '1140px', 'Recent Group Albums', '_gpa_main', 2, 0, 'PHP', 'return BxDolService::call(''gphotos'', ''get_index_block'', array());', 1, 71.9, 'non,memb', 0),
(NULL, 'index', '1140px', 'Feature Group Photos', '_gpa_featured_list', 2, 1, 'PHP', 'return BxDolService::call(''gphotos'', ''index_page_feature'', array());', 1, 71.9, 'non,memb', 0);

-- actions
INSERT INTO `sys_objects_actions` (`ID`, `Caption`, `Icon`, `Url`, `Script`, `Eval`, `Order`, `Type`, `bDisplayInSubMenuHeader`) VALUES
(NULL, '_gpa_new_album', 'plus-sign', '{evalResult}', '', 'return ''{create_album_url}'';', 1, '[db_prefix]', 0),
(NULL, '_gpa_upload_photos', 'upload', '{evalResult}', '', 'return ''{upload_photos_url}'';', 2, '[db_prefix]', 0),
(NULL, '_gpa_edit_album', 'edit', '{evalResult}', '', 'return ''{edit_album_url}'';', 3, '[db_prefix]', 0),
(NULL, '{evalResult}', 'remove', '', 'sAlbDelUrl = ''{album_delete_url}''; if (confirm(''{sure_label}'')) { window.open (sAlbDelUrl, ''_self''); }', 'return ''{delete_possibility_caption}'';', 4, '[db_prefix]', 0),
(NULL, '_gpa_manage_photos', 'random', '{evalResult}', '', 'return ''{manage_photos_url}'';', 5, '[db_prefix]', 0);

INSERT INTO `sys_objects_actions` (`ID`, `Caption`, `Icon`, `Url`, `Script`, `Eval`, `Order`, `Type`, `bDisplayInSubMenuHeader`) VALUES
(NULL, '_gpa_albums', 'folder-open', 'm/gphotos/group/{URI}', '', '', 20, 'bx_groups', 0);

-- settings
INSERT INTO `sys_options` (`Name`, `VALUE`, `kateg`, `desc`, `Type`, `check`, `err_text`, `order_in_kateg`, `AvailableValues`) VALUES
('permalinks_agpalbums', 'on', 26, 'Enable friendly permalinks for Photo Albums for Groups module', 'checkbox', '', '', NULL, '');

-- permalinks
INSERT INTO `sys_permalinks`(`standard`, `permalink`, `check`) VALUES
('modules/?r=gphotos/', 'm/gphotos/', 'permalinks_agpalbums');

-- top menu
SET @iCatRoot := (SELECT `Parent` FROM `sys_menu_top` WHERE `Caption` = '_bx_groups_menu_view_comments' LIMIT 1);
INSERT INTO `sys_menu_top`(`ID`, `Parent`, `Name`, `Caption`, `Link`, `Order`, `Visible`, `Target`, `Onclick`, `Check`, `Editable`, `Deletable`, `Active`, `Type`, `Picture`, `Icon`, `BQuickLink`, `Statistics`) VALUES
(NULL, @iCatRoot, 'Group Photo Album View', '_gpa_main', 'modules/?r=gphotos/group/{bx_groups_view_uri}', 5, 'non,memb', '', '', '', 1, 1, 1, 'custom', 'group', '', 0, '');

SET @sCatRootLinks := (SELECT `Link` FROM `sys_menu_top` WHERE `ID` = @iCatRoot LIMIT 1);
UPDATE `sys_menu_top` SET `Link` = CONCAT(@sCatRootLinks, '|modules/?r=gphotos/group/') WHERE `sys_menu_top`.`ID` = @iCatRoot LIMIT 1 ;

-- comments objects
INSERT INTO `sys_objects_cmts` (`ID`, `ObjectName`, `TableCmts`, `TableTrack`, `AllowTags`, `Nl2br`, `SecToEdit`, `PerView`, `IsRatable`, `ViewingThreshold`, `AnimationEffect`, `AnimationSpeed`, `IsOn`, `IsMood`, `RootStylePrefix`, `TriggerTable`, `TriggerFieldId`, `TriggerFieldComments`, `ClassName`, `ClassFile`) VALUES
(NULL, '[db_prefix]', '[db_prefix]_cmts', 'sys_cmts_track', 0, 1, 90, 5, 1, -3, 'none', 0, 1, 0, 'cmt', '[db_prefix]_albums', 'ID', 'CommentsCount', '', ''),
(NULL, '[db_prefix]_img', '[db_prefix]_img_cmts', 'sys_cmts_track', 0, 1, 90, 5, 1, -3, 'none', 0, 1, 0, 'cmt', '[db_prefix]_units', 'ID', 'commentsCount', '', '');

-- vote objects
INSERT INTO `sys_objects_vote` (`ID`, `ObjectName`, `TableRating`, `TableTrack`, `RowPrefix`, `MaxVotes`, `PostName`, `IsDuplicate`, `IsOn`, `className`, `classFile`, `TriggerTable`, `TriggerFieldRate`, `TriggerFieldRateCount`, `TriggerFieldId`, `OverrideClassName`, `OverrideClassFile`) VALUES
(NULL, '[db_prefix]_img', '[db_prefix]_img_rating', '[db_prefix]_img_voting_track', 'gpap_', 5, 'vote_send_result', 'BX_PERIOD_PER_VOTE', 1, '', '', '[db_prefix]_units', 'rate', 'rateCount', 'ID', '', '');

-- privacy
INSERT INTO `sys_privacy_actions` (`module_uri`, `name`, `title`, `default_group`) VALUES
('gphotos', 'album_view', '_gpa_privacy_album_view', '3');

-- chart
SET @iMaxOrderCharts = (SELECT MAX(`order`)+1 FROM `sys_objects_charts`);
INSERT INTO `sys_objects_charts` (`object`, `title`, `table`, `field_date_ts`, `field_date_dt`, `query`, `active`, `order`) VALUES
('[db_prefix]', '_gpa_main', '[db_prefix]_albums', 'When', '', '', 1, @iMaxOrderCharts);

-- Membership
SET @iLevelNonMember := 1;
SET @iLevelStandard := 2;
SET @iLevelPromotion := 3;

INSERT INTO `sys_acl_actions` VALUES (NULL, 'agpalbums feature', NULL);
SET @iAction = (SELECT LAST_INSERT_ID());
INSERT INTO `sys_acl_matrix` (`IDLevel`, `IDAction`) VALUES 
    (@iLevelPromotion, @iAction);

-- Groups timeline:

-- group wall handler
-- INSERT INTO `a_gwall_handlers` (`alert_unit`, `alert_action`, `module_uri`, `module_class`, `module_method`, `groupable`, `group_by`, `timeline`, `outline`) VALUES ('agrp_palbums', 'post', 'gphotos', 'Module', 'get_wall_post', 0, '', 1, 0);
-- SET @iGWid = (SELECT `id` FROM `sys_alerts_handlers` WHERE `name`='agtimeline');
-- INSERT INTO `sys_alerts` (`unit`, `action`, `handler_id`) VALUES ('agrp_palbums', 'post', @iGWid);