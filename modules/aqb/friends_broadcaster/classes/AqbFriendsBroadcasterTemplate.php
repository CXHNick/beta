<?php
/***************************************************************************
*
*     copyright            : (C) 2014 AQB Soft
*     website              : http://www.aqbsoft.com
*
* IMPORTANT: This is a commercial product made by AQB Soft. It cannot be modified for other than personal usage.
* The "personal usage" means the product can be installed and set up for ONE domain name ONLY.
* To be able to use this product for another domain names you have to order another copy of this product (license).
*
* This product cannot be redistributed for free or a fee without written permission from AQB Soft.
*
* This notice may not be removed from the source code.
*
***************************************************************************/

bx_import('BxDolModuleTemplate');
bx_import('BxTemplFormView');
bx_import('BxDolPaginate');


class AqbFriendsBroadcasterTemplate extends BxDolModuleTemplate {
	/**
	 * Constructor
	 */
    var $recipId = array();

	function __construct(&$oConfig, &$oDb) {
	    parent::__construct($oConfig, $oDb);
	}

	function getComposeForm($recID = 0, $msgId = 0, $replyMode = 0, $bReset = false) {
		
        if (isset($recID)) {
            $this->recipId = array($recID);
        }

        if ($msgId != 0) {
            $subject = $this->_oDb->getSubject($msgId);
            $body = $this->_oDb->getBody($msgId);
            if ($replyMode == 1) {
                $this->recipId = $this->_oDb->getRecip($msgId);
            } else if ($replyMode == 2) {
                $this->recipId = $this->_oDb->getRecipients($msgId);
            }
        } else {
            $subject = '';
            $body = '';
        }

        $aForm = array(
            'form_attrs' => array(
                'action' => BX_DOL_URL_ROOT.$this->_oConfig->getBaseUri(),
                'method' => 'post',
                'onsubmit' => 'aqb_friends_broadcaster_get_recipients_list(); return true;'
            ),
            'params' => array(
                'db' => array(
                    'submit_name' => 'send',
                ),
            ),
            'inputs' => array(
            	'recipients' => array(
                    'type' => 'hidden',
                    'name' => 'recipients',
                ),
            	'subject' => array(
                    'type' => 'text',
                    'name' => 'subject',
                    'caption' => _t('_aqb_friends_broadcaster_subject'),
                    'value' => $subject,
                    'checker' => array(
                    	'func' => 'avail',
                    	'error' => _t('_aqb_friends_broadcaster_subject_required'),
                    ),
                ),
                'message' => array(
                    'type' => 'textarea',
                    'html' => 2,
                    'colspan' => true,
                    'name' => 'message',
                    'value' => $body,
                    'checker' => array(
                    	'func' => 'avail',
                    	'error' => _t('_aqb_friends_broadcaster_message_required'),
                    ),
                ),
                /*'send_copy_to_email' => array(
                    'type' => 'checkbox',
                    'caption' => _t('_Send copy to personal email'),
                    'name' => 'send_copy_to_email',
                    'checked' => $_POST['send_copy_to_email'],
                ),
                'notify_by_email' => array(
                    'type' => 'checkbox',
                    'caption' => _t('_Notify by e-mail'),
                    'name' => 'notify_by_email',
                    'checked' => $_POST['notify_by_email'],
                ),*/
                'submit' => array(
                    'type' => 'submit',
                    'name' => 'send',
                    'value' => _t('_aqb_friends_broadcaster_send'),
                ),
            ),
        );

        $oForm = new BxTemplFormView($aForm);
        if (!$bReset) $oForm->initChecker($_REQUEST);
        return $oForm;
	}

	function getFormWithFriendList($aMyFriends, $oForm, $sMessage) {
		$aFriendsTmpl = array();
        

        $aChecked = $oForm->isSubmitted() && $_POST['recipients'] ? /*explode(',', $_POST['recipients'])*/array() : $this->recipId;
		foreach ($aMyFriends as $iFriendID) {
			$aFriendsTmpl[] = array(
				'thumb' => get_member_thumbnail($iFriendID, 'left', true),
				'friend_id' => $iFriendID,
                'checked' => in_array($iFriendID, $aChecked) ? 'checked="checked"' : '',
			);
		}

        $sFirendSelector = $this->parseHtmlByName('friends_selector.html', array(
            'bx_if:recipients_required' => array(
                'condition' => $oForm->isSubmitted() && !$_POST['recipients'],
                'content' => array(),
            ),
            'bx_repeat:recipients' => $aFriendsTmpl,
        ));

        $iPageWidth = intval(substr($this->getPageWidth(), 0, -2));

        return $this->parseHtmlByName('friends_broadcaster.html', array(
            'recipients' => DesignBoxContent(_t('_aqb_friends_broadcaster_recipients'), $sFirendSelector, 1),
            'form' => DesignBoxContent(_t('_aqb_friends_broadcaster_top_menu_item'), $sMessage.$oForm->getCode(), 11),
            'form_block_width' => '71.9%',
            'recipients_block_width' => $iPageWidth - 685 - 80,
        ));
	}
}