<?php
/**
 * Copyright (c) BoonEx Pty Limited - http://www.boonex.com/
 * CC-BY License - http://creativecommons.org/licenses/by/3.0/
 */

//ini_set('display_errors', 'On');
//error_reporting(E_ALL);

function cxipmods_library_import ($sClassPostfix, $aModuleOverwright = array()) {
    global $aModule;
    $a = $aModuleOverwright ? $aModuleOverwright : $aModule;
    if (!$a || $a['uri'] != 'tblock') {
        $oMain = BxDolModule::getInstance('BxLibraryModule');
        $a = $oMain->_aModule;
    }
    bx_import ($sClassPostfix, $a) ;
}

require_once( BX_DIRECTORY_PATH_INC . 'admin_design.inc.php' ); 
require_once( BX_DIRECTORY_PATH_INC . 'utils.inc.php' ); 
require_once( BX_DIRECTORY_PATH_PLUGINS . 'Services_JSON.php' );

bx_import('BxDolPaginate');
bx_import('BxDolAlerts');
bx_import('BxDolTwigModule');
bx_import('BxDolAdminSettings'); 
bx_import('BxTemplSearchResult');

class BxLibraryModule extends BxDolTwigModule
{
    var $_oPrivacy;
    var $_aQuickCache = array ();

    function BxLibraryModule (&$aModule)
    {

        parent::BxDolTwigModule($aModule);

        $this->_sFilterName = 'cxipmods_library_filter';
        $this->_sPrefix = 'cxipmods_library';

        $GLOBALS['oBxLibraryModule'] = &$this; 

        //$message3 = "hiii";
        //echo "<script type='text/javascript'>alert('$message3');</script>";

    }

    function actionHome () {

        $this->_oTemplate->pageStart();

        bx_import ('PageHome', $this->_aModule);
        $oPage = new BxLibraryPageHome($this);
        echo $oPage->getCode();


        //$this->_oTemplate->pageStart();

        $this->_oTemplate->pageCode(_t('_cxipmods_library_home'), true, true);
    }


    function serviceProfileAlbums($iProfileId=0) {
        

        $iProfileId = $this->_iProfileId;

        $aBlock = BxDolService::call('photos', 'get_profile_album_block', array($iProfileId), 'Search');

        //$aBlock = BxDolService::call('photos', 'get_browse_block', BxDolService::call('photos', 'get_photo_array', array($iProfileId), 'Search'), 'Search');

        return $aBlock;  
    }
    

}

?>