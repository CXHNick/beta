<?php

bx_import('BxDolZoomApi');
bx_import('BxDolSubscription');

class BxBaseZoomTestView extends BxDolPageView {
	
	var $zoom;
	//var $guestPage = "zoomguest.html";
	//var $isThanks;

	function BxBaseZoomTestView() {

		$this->zoom = new BxDolZoomApi();
		parent::BxDolPageView('zoomtest');
	}

	function getBlockCode_Zoom() {

		global $oSysTemplate;

		$startMeeting = '';
		$startLink = '';
		$output = '';

		if (!($userId = $this->getCurrentUser())) {
			$startMeeting = "Sign up for Zoom";
			$startLink = "https://zoom.us/signup";
			$meetings = '';

			$aVars = array(
			'start_link' => $startLink,
			'start_meeting' => $startMeeting,
			);

			$output = $oSysTemplate->parseHtmlByName("zoomguest.html", $aVars);

		} else {
			//$iProfileID = getLoggedId();
			//$output = $this->createRoomForm($iProfileID);

			$oGroups = BxDolModule::getInstance('BxGroupsModule');
	        $groupsJoinedArray = $oGroups->_oDb->getUserGroupsById($iProfileID);
	        $groupsInfo = $oGroups->_oDb->getUserGroupsInfoById($iProfileID);
	        array_push($groupsJoinedArray, "Associates");
	        array_push($groupsJoinedArray, "Members");

			$selectCode = "<br>Who can see it? <select name=\"group_restrict\">";
                        foreach($groupsJoinedArray as $group) {
                            $group = htmlspecialchars($group);
                            $selectCode .= "<option value=\"" . $group . "\">" . $group . "</option>";
                        } 
                        $selectCode .= "</select>";

			$startMeeting = "Start a meeting";
			$aVars = array(
			//'start_link' => $startLink,
			'start_meeting' => $startMeeting,
			'select_group_code' => $selectCode,
			);

			$output = $oSysTemplate->parseHtmlByName('zoom.html', $aVars);
		}

		//return MsgBox(_t('_Empty'));
		return $output;
	}

	/*function createRoomForm($iProfileID) {

		$oGroups = BxDolModule::getInstance('BxGroupsModule');
        $groupsJoinedArray = $oGroups->_oDb->getUserGroupsById($iProfileID);
        $groupsInfo = $oGroups->_oDb->getUserGroupsInfoById($iProfileID);
        array_push($groupsJoinedArray, "Associates");
        array_push($groupsJoinedArray, "Members");

        $sLink = 'zoomtest.php';
        $roomTopic = "Topic: ";
        $sCaptionErrorC = "Topic cannot be empty";


        $aForm = array(
            'form_attrs' => array(
                'name' => 'CreateZoomRoomForm',
                'action' => $sLink,
                'method' => 'post',
                'enctype' => 'multipart/form-data',
            ),
            'params' => array (
                'db' => array(
                    'table' => `bx_blogs_main`,
                    'key' => 'PostID',
                    'submit_name' => 'add_button',
                ),
            ),
            'inputs' => array(
                'RoomTopic' => array(
                    'type' => 'text',
                    'name' => 'RoomTopic',
                    'caption' => $roomTopic,
                    'required' => true,
                    'checker' => array (
                        'func' => 'length',
                        'params' => array(1,255),
                        'error' => $sCaptionErrorC,
                    ),
                    'db' => array (
                        'pass' => 'Xss',
                    ),
                ),
                'group_restrict' => array(
                    'type' => 'select',
                    'name' => 'group_restrict', 
                    'values'=> $groupsJoinedArray,
                    'caption' => 'Who can join it?',
                    'required' => true,
                ),
                'add_button' => array(
                    'type' => 'submit',
                    'name' => 'add_button',
                    'value' => "Go!",
                ),
                ),
            );

		$oForm = new BxBaseFormView($aForm);
        //$oForm->initChecker();
        if ($oForm->isSubmittedAndValid()) {
            $this->CheckLogged();

            //$restrict = $aForm['inputs']['group_restrict']['values'][$_POST['group_restrict']];
            //$topic = $aForm['inputs']['RoomTopic']['values'][$_POST['RoomTopic']];

        }

        $addingForm = $oForm->getCode();
        $sAddingFormVal = '<div class="blogs-view bx-def-bc-padding">' . $sAddingForm . '</div>';

        return DesignBoxContent ("yes", '<div class="blogs-view bx-def-bc-padding">' . $sAddingForm . '</div>');
        //return $addingFormVal;
	}*/

	function getBlockCode_Rooms() {

		global $oSysTemplate;
		
		$userId = $this->getCurrentUser();
		$sCode = '';

		if ($userId) {
			
			//$meetings = json_decode($this->zoom->listMeetings($userId), true);

			$meetings = array();
			$users = json_decode($this->zoom->listUsers(), true);

			for ($i = 0; $i < (int)$users[total_records]; $i++) {
					
					$otherUserId = $users[users][$i]['id'];

					$meetings = json_decode($this->zoom->listMeetings($otherUserId), true);
					
					if ($meetings[total_records] > 0) {
						for($x = 0; $x < $meetings[total_records]; $x++) {
							$meeting = $meetings[meetings][$x];
							if ($this->isAllowedViewMeeting(getLoggedId(), $meeting[id])) {
								$sCode .= $this->zoom_unit($meeting);
							}
						}
					}

					$meetings = null;
			}

			if (!$sCode) {
				return MsgBox(_t('_Empty'));
			}
			//echo $meetings['meetings'][0][join_url];
//https://zoom.us/j/513263411hiherroherroherroherroherro
			//if ($meetings['total_records'] > 0) {
			/*if (sizeof($meetings['meetings']) > 0) {
				//echo "hi";
				for($i = 0; $i < $meetings['total_records']; $i++) {
					$meeting = $meetings['meetings'][$i];
					$sCode .= $this->zoom_unit($meeting);
					//echo "herro";
					//echo $meetings[0][join_url];
					//$sCode .= $this->zoom_unit($meetings['meetings'][$i][join_url], $meetings['meetings'][$i][topic], $meetings['meetings'][$i][created_at], $meetings['meetings'][$i][host_id]);	
				}
			} else {
				return MsgBox(_t('_Empty'));
			}*/


		} else {
			return MsgBox(_t('_Empty'));
		}

		return $oSysTemplate->parseHtmlByName('default_padding.html', array('content' => $sCode));
	}

	function createMeeting($topic, $restrict) {
		
		$userId = $this->getCurrentUser();

		$meeting = json_decode($this->zoom->createAMeeting($userId, $topic), true);

		$startLink = $meeting[start_url];
				$id = $meeting[id];
		$iProfileID = getLoggedId();

		db_res("INSERT INTO `zoom_rooms` SET `id` = '$id', `creator` = '$iProfileID', `viewable` = '$restrict' ");


		//echo $restrict;

		//$link = '<script>window.open(' . $startLink . ')</script>';
		//return $startLink;
		//echo $link;
		//return 'window.location.href=<q>' .  $startLink . '</q>';
		header('Location: ' . $startLink);
	}

	function getCurrentUser() {

		$iProfileId = getLoggedId();
		$profileInfo = getProfileInfo($iProfileId);
		$email = $profileInfo['Email'];
		$userId = '';

		$users = json_decode($this->zoom->listUsers(), true);


		for ($i = 0; $i < sizeof($users['users']); $i++) {
			if (strcasecmp($users['users'][$i]['email'], $email) == 0) {
				$userId = $users['users'][$i]['id'];
				break;
			}
		}

		if ($userId == '') {
			return false;
		}

		//echo $userId;
		//KSditxNfTcyMoQkg4K1CRQKSditxNfTcyMoQkg4K1CRQ

		return $userId;
	}

	function createUser($email) {
		$this->zoom->createAUser($email);

		$this->isThanks = true;
		//$mesage = "Thanks for joining! Please activate your account via email to begin using Zoom.";
		//echo "<script type='text/javascript'>alert('$message');</script>";
		//$this->guestPage = "zoomthanks.html";
	}

	function getUserByHostId($hostId) {

		$users = json_decode($this->zoom->listUsers(), true);
		$email = '';

		for ($i = 0; $i < sizeof($users['users']); $i++) {
			if (strcasecmp($users['users'][$i]['id'], $hostId) == 0) {
				//echo $users['users'][$i]['id'];
				$email = $users['users'][$i]['email'];
				break;
			}
		}

		$query = "SELECT `ID` FROM `Profiles` WHERE `Email` = '$email' LIMIT 1";

		return (int)db_value($query);
		//return 1;
	}

	function zoom_unit($roomInfo) {

		global $oSysTemplate;

		//echo $joinUrl;
    	
		$roomUrl = $roomInfo[join_url];
		$roomTopic = $roomInfo[topic];
		$created = $roomInfo[duration];
		$roomId = $roomInfo[id];

		$hostId = $roomInfo['host_id'];
		$userId = $this->getUserByHostId($hostId);
		$userInfo = getProfileInfo($userId);
		$author = $userInfo['NickName'];
		$authorUrl = getProfileLink($userId);

		$permiss = db_value("SELECT `viewable` FROM `zoom_rooms` WHERE `id` = '$roomId'");

		$currId = $this->getCurrentUser();
		
		if ($hostId == $currId) {
			$deleteMeeting = "<p style='display:inline'>| </p><a href='zoomtest.php?delId=" . $roomId . "'>Delete Room</a>";  
		} else{
			$deleteMeeting = '';
			$endMeeting = '';
		}

		if ($permiss !== "None" && $permiss !== "Associates" && $permiss !== "Members") {
			$oGroups = BxDolModule::getInstance('BxGroupsModule');
			$groupId = $oGroups->_oDb->getGroupIdByTitle($permiss);
			$permiss = $oGroups->getGroupLink($groupId);
		}

    	$aVars = array(
    		'url' => $roomUrl,
    		'title' => $roomTopic,
    		'started' => 'Created by: ',
    		'author_url' => $authorUrl,
    		'author' => $author,
    		'permission' => ' Open to ' . $permiss,
    		'meeting_id' => $roomId,
    		'host_id' => $hostId,
    		'delete' => $deleteMeeting,
    		);


    	return $oSysTemplate->parseHtmlByName("zoom_unit.html", $aVars);
	}

	function isAllowedViewMeeting($iProfileID, $meetingID) {

		$permission = db_value("SELECT `viewable` FROM `zoom_rooms` WHERE `id` = '$meetingID'");
		$creator = db_value("SELECT `creator` FROM `zoom_rooms` WHERE `id` = '$meetingID'");

		if ($permission == '') {
			return true;
		}

		if ($permission == "None") {
			return true;
		} else if ($permission == "Members") {
			return true;
		} else if ($permission == "Associates") {
			if ($this->isFriends($creator, $iProfileID)) {
				return true;
			} else {
				return false;
			}
		} else {
			if ($this->isInGroup($iProfileID, $permission)) {
				return true;
			} else {
				return false;
			}
		}
	}

	function isFriends($personOne, $personTwo) {

		return db_res("SELECT * FROM `sys_friend_list` WHERE `Profile` = '$personOne' AND `ID` = '$personTwo'") || db_res("SELECT * FROM `sys_friend_list` WHERE `Profile` = '$personTwo' AND `ID` = '$personOne'");
	}

	function isInGroup($iProfileID, $permission) {
		
		$oGroups = BxDolModule::getInstance('BxGroupsModule');
		$groupId = $oGroups->_oDb->getGroupIdByTitle($permission);

		return $oGroups->_oDb->isFan($groupId, $iProfileID, true);
	}

	function getGroupRoomsBlock($GroupID) {

		global $oSysTemplate;

		$userId = $this->getCurrentUser();
		$oGroups = BxDolModule::getInstance('BxGroupsModule');
		$title = $oGroups->_oDb->getGroupTitleById($GroupID);

		if ($userId) {

			$meetings = array();
			$users = json_decode($this->zoom->listUsers(), true);

				for ($i = 0; $i < (int)$users[total_records]; $i++) {
					
					$otherUserId = $users[users][$i]['id'];

					$meetings = json_decode($this->zoom->listMeetings($otherUserId), true);
					
					if ($meetings[total_records] > 0) {
						for($x = 0; $x < $meetings[total_records]; $x++) {
							$meeting = $meetings[meetings][$x];
							//if ($this->isAllowedViewMeeting(getLoggedId(), $meeting[id])) {
							$mtgId = $meeting[id];
							$permiss = db_value("SELECT `viewable` FROM `zoom_rooms` WHERE `id` = '$mtgId'");
							if ($permiss == $title) {
								$sCode .= $this->zoom_unit($meeting);
							}
							//}
						}
					}

					$meetings = null;
			}

		} else {
			return MsgBox("Sign up for Zoom in the Com tab to view!");
		}

		return $oSysTemplate->parseHtmlByName('default_padding.html', array('content' => $sCode));
	}

	function deleteMeeting($id) {
		$userId = $this->getCurrentUser();
		$this->zoom->deleteAMeeting($id, $userId);
	}

	function endMeeting($id) {
		$userId = $this->getCurrentUser();
		$this->zoom->endAMeeting($id, $userId);
	}
}
