<?php
/***************************************************************************
*
* IMPORTANT: This is a commercial product made by AndrewP. It cannot be modified for other than personal usage.
* The "personal usage" means the product can be installed and set up for ONE domain name ONLY.
* To be able to use this product for another domain names you have to order another copy of this product (license).
* This product cannot be redistributed for free or a fee without written permission from AndrewP.
* This notice may not be removed from the source code.
*
***************************************************************************/
bx_import('BxDolConfig');
require_once(BX_DIRECTORY_PATH_ROOT . "flash/modules/video/inc/constants.inc.php");

class AGpalbumsConfig extends BxDolConfig {

	var $aFilesConfig = array();
	var $aFilePostfix = array();
    // array of shared file's memberships

    /**
     * Constructor
     */
    function AGpalbumsConfig($aModule) {
        parent::BxDolConfig($aModule);

         $this->aFilesConfig = array (
            'browse' => array('postfix' => THUMB_FILE_NAME . IMAGE_EXTENSION, 'image' => true, 'w' => 240, 'h' => 240, 'square' => true),
        	'browse2x' => array('postfix' => THUMB_FILE_NAME . '_2x' . IMAGE_EXTENSION, 'image' => true, 'w' => 480, 'h' => 480, 'square' => true),
            'poster' => array('postfix' => IMAGE_EXTENSION, 'image' => true),
            'main' => array('postfix' => FLV_EXTENSION),
            'mpg' => array('postfix' => '.mpg'),
            'file' => array('postfix' => MOBILE_EXTENSION),
            'm4v' => array('postfix' => M4V_EXTENSION),
        );

        /*$this->aGlParams = array(
            'mode_top_index' => 'bx_videos_mode_index',
            'category_auto_approve' => 'category_auto_activation_bx_videos',
            'number_all' => 'bx_videos_number_all',
            'number_index' => 'bx_videos_number_index',
            'number_user' => 'bx_videos_number_user',
            'number_related' => 'bx_videos_number_related',
            'number_top' => 'bx_videos_number_top',
            'number_browse' => 'bx_videos_number_browse',
            'number_previous_rated' => 'bx_videos_number_previous_rated',
            'number_albums_browse' => 'bx_videos_number_albums_browse',
            'number_albums_home' => 'bx_videos_number_albums_home',
            'file_width' => 'bx_videos_file_width',
            'file_height' => 'bx_videos_file_height',
            'allowed_exts' => 'bx_videos_allowed_exts',
            'profile_album_name' => 'bx_videos_profile_album_name',
        );*/

        $sProto = 'http://';
        if (0 == strncmp('https', BX_DOL_URL_ROOT, 5))
            $sProto = 'https://';
        
        if(!defined("YOUTUBE_VIDEO_PLAYER"))
            define("YOUTUBE_VIDEO_PLAYER", '<iframe width="100%" height="360" src="' . $sProto . 'www.youtube-nocookie.com/embed/#video#?rel=0&amp;showinfo=0#autoplay#" frameborder="0" allowfullscreen></iframe>');

        if(!defined("YOUTUBE_VIDEO_EMBED"))
            define("YOUTUBE_VIDEO_EMBED", '<iframe width="480" height="360" src="' . $sProto . 'www.youtube-nocookie.com/embed/#video#?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>');

        $this->initConfig();
    }

    function initConfig()
    {
        foreach ($this->aFilesConfig as $k => $a)
            $this->aFilePostfix[$k] = $a['postfix'];
    }
}
